from collections import deque
import csv
import matplotlib
import matplotlib.pyplot as plt


maxlen = 20
window = deque(maxlen=maxlen)

with open('data_extracted_6.csv') as f_input:
    csv_input = csv.reader(f_input, skipinitialspace=True)
    header = next(csv_input)

    freq = []
    x = []

    for v1, v2 in csv_input:
        v1 = float(v1)
        window.append(v1)

        if len(window) == maxlen:
            x.append(v1)
            freq.append(maxlen / ((window[-1] - window[0])))

    plt.plot(x, freq)
    plt.show()
