1. Setup your GitLab account with SSH authentification.
2. Clone this repository to a destination on your computer.
3. Modify or add files in that destination.
4. Commit **relevant** changes. You could use TortoiseGit or git-cola GUIs for this. Do not commit build folders, pdf or auxillary files.
5. If you see there is an unwanted file, add it to the list in .gitignore, so it does not happen again.
6. Push your commits when you have tested that it does not conflict with others code.
7. Pull, so that you can see our changes and stay up to date. You cannot push if you are not up to date.
8. Update the Scrum backlog(s). https://tree.taiga.io/project/madsherlock-rsd-2016/backlog
9. Write guides in the form of readable bash scripts. This allows people to run it or study what you have done to set something up.
10. Write commit messages so it is clear what you will add to the project.
