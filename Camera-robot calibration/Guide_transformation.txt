How to use the matlab file "transformation.m" in order to find the 4x4 tranformation matrix that contains Rotation and Translation

1. Find corresponding points in both Smart Camera and robot
2. In a excel file introduce by rows the Smart Camera points (x and y) in pixels and add a zero as the third coordinate, as the plane on the conveyor belt is consider to be the plane z=0. Name the file "camera_points.xlsx"
3. In another excel file introduce by rows the corresponding robot points with coordinates x, y and z of the tool frame. Name the file "robot_points.xlsx"
4. Make sure the two previous files and the matlab file "transformation.m" are in the same folder
5. Run the matlab file, and it will create a file named "Transformation.txt" which contains the elements of the 4x4 transformation matrix separated by commas. 