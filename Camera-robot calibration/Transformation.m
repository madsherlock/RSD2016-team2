camera_points= xlsread('camera_points')
robot_points=xlsread('robot_points')

%%camera_points = camera_points - repmat([375 240 0],5,1)

camera_points = camera_points * 100 / 465;

mean_camera_points = mean(camera_points);
mean_robot_points = mean(robot_points);

H = zeros(3);
for i = 1:size(camera_points)
    H = (camera_points(i,:) - mean_camera_points)' * (robot_points(i,:) - mean_robot_points) + H;
end

H;
[U,S,V] = svd(H);
Rotation = V * U';
Translation = -Rotation * mean_camera_points' + mean_robot_points';

Transform = [Rotation Translation ; 0 0 0 1]

hom_camera_points = [camera_points  ones(size(camera_points,1),1)];
hom_robot_points = [robot_points  ones(size(robot_points,1),1)];
point = Transform * hom_camera_points';
point'

dlmwrite('Transformation.txt', Transform)