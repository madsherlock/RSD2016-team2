#include "TCP.h"
#include <string.h>
#include <unistd.h>

namespace tcp_client_server
{

// ========================= CLIENT =========================

/** \brief Initialize a TCP client object.
 *
 * This function initializes the TCP client object using the address and the
 * port as specified.
 *
 * The port is expected to be a host side port number (i.e. 59200).
 *
 * The \p addr parameter is a textual address. It may be an IPv4 or IPv6
 * address and it can represent a host name or an address defined with
 * just numbers. If the address cannot be resolved then an error occurs
 * and constructor throws.
 *
 * \note
 * The socket is open in this process. If you fork() or exec() then the
 * socket will be closed by the operating system.
 *
 * \warning
 * We only make use of the first address found by getaddrinfo(). All
 * the other addresses are ignored.
 *
 * \exception tcp_client_server_runtime_error
 * The server could not be initialized properly. Either the address cannot be
 * resolved, the port is incompatible or not available, or the socket could
 * not be created.
 *
 * \param[in] addr  The address to convert to a numeric IP.
 * \param[in] port  The port number.
 */
tcp_client::tcp_client(const std::string& addr, int port) : f_port(port), f_addr(addr)
{
    char decimal_port[16];
    snprintf(decimal_port, sizeof(decimal_port), "%d", f_port);
    decimal_port[sizeof(decimal_port) / sizeof(decimal_port[0]) - 1] = '\0';
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_TCP;
    int r(getaddrinfo(addr.c_str(), decimal_port, &hints, &f_addrinfo));
    if(r != 0 || f_addrinfo == NULL)
    {
        //std::cerr << addr << std::endl;
        throw tcp_client_server_runtime_error(("invalid address or port: \"" + addr + ":" + decimal_port + "\"").c_str());
    }

    f_socket = socket(f_addrinfo->ai_family, SOCK_DGRAM | SOCK_CLOEXEC, IPPROTO_TCP);
    if(f_socket == -1)
    {
        freeaddrinfo(f_addrinfo);
        throw tcp_client_server_runtime_error(("could not create socket for: \"" + addr + ":" + decimal_port + "\"").c_str());
    }
}

/** \brief Clean up the TCP client object.
 *
 * This function frees the address information structure and close the socket
 * before returning.
 */
tcp_client::~tcp_client()
{
    freeaddrinfo(f_addrinfo);
    close(f_socket);
}

void tcp_client::closeSocket()
{
    freeaddrinfo(f_addrinfo);
    close(f_socket);
}

/** \brief Retrieve a copy of the socket identifier.
 *
 * This function return the socket identifier as returned by the socket()
 * function. This can be used to change some flags.
 *
 * \return The socket used by this TCP client.
 */
int tcp_client::get_socket() const
{
    return f_socket;
}

/** \brief Retrieve the port used by this TCP client.
 *
 * This function returns the port used by this TCP client. The port is
 * defined as an integer, host side.
 *
 * \return The port as expected in a host integer.
 */
int tcp_client::get_port() const
{
    return f_port;
}

/** \brief Retrieve a copy of the address.
 *
 * This function returns a copy of the address as it was specified in the
 * constructor. This does not return a canonalized version of the address.
 *
 * The address cannot be modified. If you need to send data on a different
 * address, create a new TCP client.
 *
 * \return A string with a copy of the constructor input address.
 */
std::string tcp_client::get_addr() const
{
    return f_addr;
}

/** \brief Send a message through this TCP client.
 *
 * This function sends \p msg through the TCP client socket. The function
 * cannot be used to change the destination as it was defined when creating
 * the tcp_client object.
 *
 * The size must be small enough for the message to fit. In most cases we
 * use these in Snap! to send very small signals (i.e. 4 bytes commands.)
 * Any data we would want to share remains in the Cassandra database so
 * that way we can avoid losing it because of a TCP message.
 *
 * \param[in] msg  The message to send.
 * \param[in] size  The number of bytes representing this message.
 *
 * \return -1 if an error occurs, otherwise the number of bytes sent. errno
 * is set accordingly on error.
 */
int tcp_client::send(std::string data)
{
    return sendto(f_socket, data.c_str(), data.size(), 0, f_addrinfo->ai_addr, f_addrinfo->ai_addrlen);
}

std::string tcp_client::receive(int length)
{
    char buffer[length];
    if(read(f_socket, buffer, length))
    {
        std::string str(buffer);
        return str;
    }
    else
        return "";
}



// ========================= SERVER =========================

/** \brief Initialize a TCP server object.
 *
 * This function initializes a TCP server object making it ready to
 * receive messages.
 *
 * The server address and port are specified in the constructor so
 * if you need to receive messages from several different addresses
 * and/or port, you'll have to create a server for each.
 *
 * The address is a string and it can represent an IPv4 or IPv6
 * address.
 *
 * Note that this function calls connect() to connect the socket
 * to the specified address. To accept data on different TCP addresses
 * and ports, multiple TCP servers must be created.
 *
 * \note
 * The socket is open in this process. If you fork() or exec() then the
 * socket will be closed by the operating system.
 *
 * \warning
 * We only make use of the first address found by getaddrinfo(). All
 * the other addresses are ignored.
 *
 * \exception tcp_client_server_runtime_error
 * The tcp_client_server_runtime_error exception is raised when the address
 * and port combinaison cannot be resolved or if the socket cannot be
 * opened.
 *
 * \param[in] addr  The address we receive on.
 * \param[in] port  The port we receive from.
 */
tcp_server::tcp_server(const std::string& addr, int port) : f_port(port), f_addr(addr)
{
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
    }
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
       error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
             sizeof(serv_addr)) < 0)
             error("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd,
                (struct sockaddr *) &cli_addr,
                &clilen);
    if (newsockfd < 0)
         error("ERROR on accept");
    bzero(buffer,256);
    n = read(newsockfd,buffer,255);
    if (n < 0) error("ERROR reading from socket");
    printf("Here is the message: %s\n",buffer);
    n = write(newsockfd,"I got your message",18);
    if (n < 0) error("ERROR writing to socket");
    close(newsockfd);
    close(sockfd);


    char decimal_port[16];
    snprintf(decimal_port, sizeof(decimal_port), "%d", f_port);
    decimal_port[sizeof(decimal_port) / sizeof(decimal_port[0]) - 1] = '\0';
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_
    hints.ai_protocol = IPPROTO_TCP;
    int r(getaddrinfo(addr.c_str(), decimal_port, &hints, &f_addrinfo));
    if(r != 0 || f_addrinfo == NULL)
    {
        throw tcp_client_server_runtime_error(("invalid address or port for TCP socket: \"" + addr + ":" + decimal_port + "\"").c_str());
    }
    f_socket = socket(f_addrinfo->ai_family, SOCK_DGRAM | SOCK_CLOEXEC, IPPROTO_TCP);
    if(f_socket == -1)
    {
        freeaddrinfo(f_addrinfo);
        throw tcp_client_server_runtime_error(("could not create TCP socket for: \"" + addr + ":" + decimal_port + "\"").c_str());
    }
    r = bind(f_socket, f_addrinfo->ai_addr, f_addrinfo->ai_addrlen);
    if(r != 0)
    {
        freeaddrinfo(f_addrinfo);
        close(f_socket);
        throw tcp_client_server_runtime_error(("could not bind TCP socket with: \"" + addr + ":" + decimal_port + "\"").c_str());
    }
}

/** \brief Clean up the TCP server.
 *
 * This function frees the address info structures and close the socket.
 */
tcp_server::~tcp_server()
{
    freeaddrinfo(f_addrinfo);
    close(f_socket);
}

void tcp_server::closeSocket()
{
    freeaddrinfo(f_addrinfo);
    close(f_socket);
}


/** \brief The socket used by this TCP server.
 *
 * This function returns the socket identifier. It can be useful if you are
 * doing a select() on many sockets.
 *
 * \return The socket of this TCP server.
 */
int tcp_server::get_socket() const
{
    return f_socket;
}

/** \brief The port used by this TCP server.
 *
 * This function returns the port attached to the TCP server. It is a copy
 * of the port specified in the constructor.
 *
 * \return The port of the TCP server.
 */
int tcp_server::get_port() const
{
    return f_port;
}

/** \brief Return the address of this TCP server.
 *
 * This function returns a verbatim copy of the address as passed to the
 * constructor of the TCP server (i.e. it does not return the canonalized
 * version of the address.)
 *
 * \return The address as passed to the constructor.
 */
std::string tcp_server::get_addr() const
{
    return f_addr;
}

/** \brief Wait on a message.
 *
 * This function waits until a message is received on this TCP server.
 * There are no means to return from this function except by receiving
 * a message. Remember that TCP does not have a connect state so whether
 * another process quits does not change the status of this TCP server
 * and thus it continues to wait forever.
 *
 * Note that you may change the type of socket by making it non-blocking
 * (use the get_socket() to retrieve the socket identifier) in which
 * case this function will not block if no message is available. Instead
 * it returns immediately.
 *
 * \param[in] msg  The buffer where the message is saved.
 * \param[in] max_size  The maximum size the message (i.e. size of the \p msg buffer.)
 *
 * \return The number of bytes read or -1 if an error occurs.
 */
std::string tcp_server::receive(int length)
{
    char buffer[length];
    if(read(f_socket, buffer, length))
    {
        std::string str(buffer);
        return str;
    }
    else
        return "";
}

int tcp_server::send(std::string data)
{
    return sendto(f_socket, data.c_str(), data.size(), 0, f_addrinfo->ai_addr, f_addrinfo->ai_addrlen);
}

} // namespace tcp_client_server

// vim: ts=4 sw=4 et
