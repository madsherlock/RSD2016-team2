// Includes
#include <iostream>
#include <queue>
#include <boost/thread.hpp>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "kukaRSIParser.h"
#include "UDP.h"
#include "PracticalSocket.h"
#include <rw/math.hpp>
#include <rw/math/Q.hpp>
#include <rw/trajectory/RampInterpolator.hpp>

#define DEBUG                   false
#define TCP_IP_SERVER           "192.168.100.50"
//#define TCP_IP_SERVER         "192.168.100.50"
#define UDP_IP_SERVER           "192.168.100.50"
#define UDP_IP_CLIENT           "192.168.100.100"
#define UDP_PORT_SERVER         49000
#define UDP_PORT_CLIENT         53453
#define TCP_PORT_SERVER         49002

#define RSI_TIME                0.004   // s, needs to be 0.004 because of changes in RSI_ON()
#define DEGREETORAD             (M_PI/180.0)
#define RADTODEGREE             (180.0/M_PI)
#define JOINT_SPEED_MAX         80
#define JOINT_SPEED_NORMAL      20
#define JOINT_SPEED_BRAKE       10
#define JOINT_ACC_MAX           50
#define JOINT_ACC_NORMAL        20
const rw::math::Q QMAX(6, 170, 135, 60, 185, 25, 350);
const rw::math::Q QMIN(6, -170, -100, -200, -185, -210, -350);
const rw::math::Q speedBrake(6, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE);
const rw::math::Q accNormal(6, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL);

// Max joint speed
//360 °/s
//300 °/s
//360 °/s
//381 °/s
//388 °/s
//615 °/s

template <typename T>
class SynchronisedQueue
{
private:
	std::queue<T> m_queue;              // Use STL queue to store data
	boost::mutex m_mutex;               // The mutex to synchronise on
	boost::condition_variable m_cond;   // The condition to wait for

public:
	// Add data to the queue and notify others
	void enqueue(const T& data)
	{
		// Acquire lock on the queue
		boost::unique_lock<boost::mutex> lock(m_mutex);

		// Add the data to the queue
		m_queue.push(data);

		// Notify others that data is ready
		m_cond.notify_one();
	}

	// Get data from the queue. Wait for data if not available
	T dequeue()
	{
		// Acquire lock on the queue
		boost::unique_lock<boost::mutex> lock(m_mutex);

		// When there is no data, wait till someone fills it.
		// Lock is automatically released in the wait and obtained
		// again after the wait
		while(m_queue.size()==0)
			m_cond.wait(lock);

		// Retrieve the data from the queue
		T result = m_queue.front();
		m_queue.pop();

		return result;
	} 

	int size()
	{
		// Acquire lock on the queue
		boost::unique_lock<boost::mutex> lock(m_mutex);
		return m_queue.size();
	}

	T front()
	{
		// Acquire lock on the queue
		boost::unique_lock<boost::mutex> lock(m_mutex);
		return m_queue.front();
	}

	T back()
	{
		// Acquire lock on the queue
		boost::unique_lock<boost::mutex> lock(m_mutex);
		return m_queue.back();
	}

	void clear(bool smoothBrake)
	{
		// Acquire lock on the queue
		boost::unique_lock<boost::mutex> lock(m_mutex);

		std::queue<rw::math::Q> empty;

		if(m_queue.empty() == false && smoothBrake)
		{
			rw::math::Q qStart = m_queue.front();
			m_queue.pop();

			if(m_queue.empty() == false)
			{
				rw::math::Q qNext = m_queue.front();
				m_queue.pop();
				rw::math::Q qDelta = qNext-qStart;

				for(unsigned i=0; i<qDelta.size(); i++)
				{
					if(qDelta[i] > 0.05)
						qNext[i] += 1.0;
					else if(qDelta[i] < -0.05)
						qNext[i] -= 1.0;
				}

				rw::trajectory::RampInterpolator<rw::math::Q> rampInterpolator(qStart, qNext, speedBrake, accNormal);
				for(double j = 0.0; j <= rampInterpolator.duration(); j += RSI_TIME)
					empty.push(rampInterpolator.x(j));
			}
		}

		std::swap(m_queue, empty);
	}
};

// Global variables
boost::thread *_serverThread, *_clientThread;
SynchronisedQueue<std::string> _sendQueue;
SynchronisedQueue<rw::math::Q> _qQueue;
rw::math::Q _qCurrent(6,0,0,0,0,0,0), _qOffset(6, 0, -90, 90, 0, 90, 0);
boost::mutex _qMutex;
bool _safety;
boost::mutex _safetyMutex;
bool _gripper = 0; // Global gripper value default 0(open)
bool _gripOut1;
bool _gripOut2;
boost::mutex _gripOutMutex;

// Prototypes
bool setConfigurationCallback(rw::math::Q);
bool setGripperCallback(double); // adding gripper prototype

// Functions
void client()
{
	// Variables
	udp_client_server::udp_client client(UDP_IP_CLIENT, UDP_PORT_CLIENT);
	std::string str;

	while(true)
	{
		try
		{
			// Get frame from queue
			str = _sendQueue.dequeue();

			// Inform
			if(DEBUG)
				std::cout << "Sent: \n" << str << std::endl << std::endl;

			// Send frame
			client.send(str.c_str(), str.size());

			// Signal interrupt point
			boost::this_thread::interruption_point();
		}
		catch(const boost::thread_interrupted&)
		{
			std::cout << "- Client thread interrupted. Exiting thread.-" << std::endl;
			break;
		}
	}
}

void server()
{
	// Variables
	udp_client_server::udp_server server(UDP_IP_SERVER, UDP_PORT_SERVER);
	KukaRSIParser rsiParser;
	char *msg = new char[1000];
	std::string str, oldStr;
	rw::math::Q qCurrentReal(6);
	int oldIPOC = 0;
	rw::math::Q qNew(6);
	rw::math::Q qCurrent(6);

	// Loop
	while(true)
	{
		try
		{
			// Receive packet from client
			int msgSize = server.recv(msg, 1000);

			if(msgSize != -1)
			{
				// Convert to string
				str = (std::string(msg)).substr(0,msgSize-1);

				// Parse packet
				rsiParser.parseXML(str);

				// Get IPOC
				int IPOC = rsiParser.getIPOC();

				// Get safety
				_safetyMutex.lock();
				_safety = rsiParser.getDigout();
				_safetyMutex.unlock();
				
				// Get grip output
				_gripOutMutex.lock();
				_gripOut1 = rsiParser.getGripout1();
				_gripOutMutex.unlock();
				
				_gripOutMutex.lock();
				_gripOut2 = rsiParser.getGripout2();
				_gripOutMutex.unlock();

				// Get current joint configuration
				KukaRSIParser::RSI_AIPos AIPos = rsiParser.getQ();
				for(int i=0; i<6; i++)
					qCurrentReal[i] = AIPos.q[i];

				// Get current in offset coordinates
				_qMutex.lock();
				_qCurrent = qCurrentReal - _qOffset;
				qCurrent = _qCurrent;
				_qMutex.unlock();

				// Check IPOC
				if((IPOC-oldIPOC) > (RSI_TIME*1000)*2)
				{
					std::cout << "Connection to robot (re)established!" << std::endl;
					_qQueue.clear(false);
					qNew = qCurrent;
				}
				oldIPOC = IPOC;

				// Inform
				if(DEBUG)
					std::cout << "Received: \n" << str << std::endl << std::endl;

				// Check delay
				int delay = rsiParser.getDelay();
				if(delay > 0)
				{
					std::cerr << "Delay: " << delay << std::endl;
				}
				/****************************************************************************/
				qNew = qCurrent;
				if(_qQueue.size())
					qNew = _qQueue.dequeue();

				// Update new pos
				for(int i=0; i<6; i++)
					AIPos.q[i] = qNew[i];
				/****************************************************************************/

				// Construct return frame
				str = rsiParser.constructFile(AIPos, _gripper); // include global gripper value

				// Send to send queue
				_sendQueue.enqueue(str);

				oldStr = str;
			}
			else
			{
				std::cerr << "Received empty frame!" << std::endl;
			}

			// Signal interrupt point
			boost::this_thread::interruption_point();
		}
		catch(const boost::thread_interrupted&)
		{
			std::cout << "- Server thread interrupted. Exiting thread." << std::endl;
			break;
		}
	}
}

void signalCallback(int signal)
{
	// Interrupt threads
	_serverThread->interrupt();
	_clientThread->interrupt();

	// Exit program
	exit(1);
}

bool setConfigurationCallback(rw::math::Q q, rw::math::Q speed)
{
	rw::math::Q qEnd(6);
	_qMutex.lock();
	rw::math::Q qStart = _qCurrent;
	_qMutex.unlock();

	if(_qQueue.size())
		qStart = _qQueue.back();

	// Get joint values
	for(unsigned int i=0; i<qEnd.size(); i++)
		qEnd(i) = q[i] * RADTODEGREE;

	// Check joint values
	for(unsigned int i=0; i<qEnd.size(); i++)
	{
		if(qEnd(i) > QMAX(i) || qEnd(i) < QMIN(i))
		{
			std::cerr << "Joint limit overrule!" << std::endl;
			return false;
		}
	}

	// Check speed values
	rw::math::Q qSpeed(6);
	for(unsigned int i=0; i<qSpeed.size(); i++)
	{
		if(speed[i] < 1.0 || speed[i] > JOINT_SPEED_MAX)
			qSpeed(i) = JOINT_SPEED_NORMAL;
		else
			qSpeed(i) = speed[i];
	}

	// Check acceleration values
	// TODO

	// Create interpolated trajectory
	rw::trajectory::RampInterpolator<rw::math::Q> rampInterpolator(qStart, qEnd, qSpeed, accNormal);
	for(double j = 0.0; j <= rampInterpolator.duration(); j += RSI_TIME)
		_qQueue.enqueue(rampInterpolator.x(j));

	return true;
}

/*
Modification for gripper control
Checking the value from msg if valid setting global varibel.
*/
bool setGripperCallback(double grip)
{
	double new_grip = grip;

	// Check grip values
	if(new_grip == 0)
	{
		_gripper = 0;
	}
	else if(new_grip == 1)
	{
		_gripper = 1;
	}
	else
	{
		std::cerr << "No valid griper value!" << std::endl;
		return false;
	}
	return true;
}
/*
End of modification
*/

// TCP client handling function
void HandleTCPClient(TCPSocket *sock)
{
	// Inform
	std::cout << "Got connection from: " << sock->getForeignAddress() << "," << sock->getForeignPort() << std::endl;

	while(true)
	{
		std::string msg = sock->recv(150);

		if(msg.empty())
		{
			break;
		}
		else
		{
			//if(DEBUG)
			//std::cout << msg << std::endl;

			if(msg.find("GetSafety") != std::string::npos)
			{
				boost::unique_lock<boost::mutex> lock(_safetyMutex);
				if(_safety)
					sock->send("true");
				else
					sock->send("false");
			}
			else if(msg.find("GetQueueSize") != std::string::npos)
			{
				std::string str = SSTR(_qQueue.size());
				sock->send(str);
			}
			else if(msg.find("StopRobot") != std::string::npos)
			{
				_qQueue.clear(true);
				//sock->send("Stopped");
			}
			else if(msg.find("GetConf") != std::string::npos)
			{
				_qMutex.lock();
				rw::math::Q qTemp = _qCurrent;
				_qMutex.unlock();

				std::ostringstream qStream;
				qStream << qTemp;
				std::string qStr = qStream.str();
				qStr = qStr.substr(0, qStr.size()); // 4-4
				sock->send(qStr);
			}
			else if(msg.find("IsMoving") != std::string::npos)
			{
				if(_qQueue.size())
					sock->send("true");
				else
					sock->send("false");
			}
			else if(msg.find("SetConf") != std::string::npos)
			{
				// Get Q
				std::string msg2 = msg;
				// Isolate data
				rw::math::Q q(6);
				int i = 0;
				while(msg2[i++] != '{');
				msg2 = msg2.substr(i, msg2.size()-i);
				i = 0;
				while(msg[i++] != '}');
				msg2 = msg2.substr(0, i-1);
				// Get data
				std::stringstream ss(msg2);
				double d = 0.0;
				i = 0;
				while(ss >> d)
				{
					q[i++] = d;
					if(ss.peek() == ',' || ss.peek() == ' ')
						ss.ignore();
				}

				// Get speed
				rw::math::Q speed(6);

				// Isolate data
				i = 0;
				while(msg[i++] != 'V');
				msg = msg.substr(i+1, msg.size()-i-1);
				i = 0;
				while(msg[i++] != '}');
				msg = msg.substr(0, i-1);

				// Get data
				std::stringstream ss2(msg);
				d = 0.0;
				i = 0;
				while(ss2 >> d)
				{
					speed[i++] = d;
					if(ss2.peek() == ',' || ss2.peek() == ' ')
						ss2.ignore();
				}

				setConfigurationCallback(q, speed);

				if(DEBUG)
					std::cout << q << "\n" << speed << std::endl;
			}
			/*
			Modification for gripper control
			*/
			else if(msg.find("SetGrip") != std::string::npos) // check for msg with "SetGrip"
			{
				// Get Grip
				std::string msg2 = msg;
				//std::cout << "msg: " << msg << std::endl;

				// Isolate data
				double grip;
				int i = 0;
				while(msg2[i++] != '{');
				msg2 = msg2[i];
				//std::cout << "msg2: " << msg2 << std::endl;

				// Get data
				std::stringstream ss(msg2);
				double d = 0.0;
				ss >> d;
				grip = d;

				setGripperCallback(grip);
				//std::cout << grip << std::endl;

				if(DEBUG)
					std::cout << "value og grip input: " << grip << std::endl;
			}
			else if(msg.find("IsGripOpen") != std::string::npos)
			{
				if(_gripOut1 == 1 && _gripOut2 == 0)
					sock->send("true");
				else
					sock->send("false");
			}
			/*
			End of modification
			*/
			else
				sock->send("error");
		}
	}

	delete sock;
}

int main()
{
	// Create and run server and client threads
	_serverThread = new boost::thread(server);
	_clientThread = new boost::thread(client);

	// Handle signals
	struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = signalCallback;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	sigaction(SIGINT, &sigIntHandler, NULL);

	// Inform
	std::cout << "UDP Server+Client is running.." << std::endl;

	// Create server and handle connections
	try
	{
		// Inform
		std::cout << "TCP Server is running.." << std::endl;
		TCPServerSocket server(TCP_PORT_SERVER);

		// Run forever
		while(true)
		{
			// Wait for a client to connect
			HandleTCPClient(server.accept());
		}

		server.closeConnection();
		std::cout << "Connection closed" << std::endl;
	}
	catch (SocketException &e)
	{
		std::cerr << e.what() << std::endl;
	}

	// Interrupt threads
	_serverThread->interrupt();
	_clientThread->interrupt();

	// Return
	return 0;
}
