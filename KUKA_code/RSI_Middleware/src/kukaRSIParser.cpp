#include "kukaRSIParser.h"

std::string _sendString = "<Sen Type=\"ROS\"><EStr></EStr></Sen>";

KukaRSIParser::KukaRSIParser()
{
	// Init variables
	_IPOC = -1;
}

/*KukaRSIParser::KukaRSIParser(std::string package)
{
// Init variables
_IPOC = -1;

// Load
loadFile(package);
}*/

KukaRSIParser::~KukaRSIParser()
{

}

bool KukaRSIParser::parseXML(std::string xmlString, bool loadFromFile)
{
	// Open document
	if(loadFromFile)
	{
		if(_doc.LoadFile(xmlString.c_str()) != 0)
		{
			std::cerr << "Error opening file '" << xmlString << "'!" << std::endl;
			return false;
		}
	}
	else
	{
		if(_doc.Parse(xmlString.c_str()) != 0)
		{
			std::cerr << "Error parsing string!" << std::endl;
			return false;
		}
	}

	// Check if "Rob" file and robot = KUKA
	if(std::string(_doc.RootElement()->Value()) == "Rob" && std::string(_doc.FirstChildElement("Rob")->FirstAttribute()->Value()) == "KUKA")
	{
		// Get RIst
		XMLElement *AIPosElement = _doc.RootElement()->FirstChildElement("AIPos");

		if(AIPosElement != 0)
		{
			_AIPos.q[0] = atof(AIPosElement->Attribute("A1"));
			_AIPos.q[1] = atof(AIPosElement->Attribute("A2"));
			_AIPos.q[2] = atof(AIPosElement->Attribute("A3"));
			_AIPos.q[3] = atof(AIPosElement->Attribute("A4"));
			_AIPos.q[4] = atof(AIPosElement->Attribute("A5"));
			_AIPos.q[5] = atof(AIPosElement->Attribute("A6"));
		}
		else
		{
			std::cerr << "Error finding AIPos element!" << std::endl;
			return false;
		}

		// Get Digout (External emergency)
		XMLElement *DigElement = _doc.RootElement()->FirstChildElement("Digout");

		if(DigElement != 0)
		{
			_digOut = atoi(DigElement->Attribute("o1"));
		}
		else
		{
			std::cerr << "Error finding Digout element!" << std::endl;
			return false;
		}

		// Get delay
		XMLElement *DelayElement = _doc.RootElement()->FirstChildElement("Delay");

		if(DelayElement != 0)
		{
			_delay = atoi(DelayElement->Attribute("D"));
		}
		else
		{
			std::cerr << "Error finding delay element!" << std::endl;
			return false;
		}

		// Get IPOC
		XMLElement *IPOCElement = _doc.RootElement()->FirstChildElement("IPOC");

		if(IPOCElement != 0)
			_IPOC = atoi(IPOCElement->FirstChild()->Value());
		else
		{
			std::cerr << "Error finding IPOC element!" << std::endl;
			return false;
		}
		
		// Get Grip open/close
		XMLElement *GripElement = _doc.RootElement()->FirstChildElement("GRIP");
		
		if(GripElement != 0)
		{
			_gripOut1 = atoi(GripElement->Attribute("o1"));
			_gripOut2 = atoi(GripElement->Attribute("o2"));
		}
		else
		{
			std::cerr << "Error finding GRIP element!" << std::endl;
			return false;
		}
	}
	else
	{
		std::cerr << "XML Root element != \"Rob\" or Robot type != \"KUKA\"" << std::endl;
		return false;
	}

	return true;
}

std::string KukaRSIParser::constructFile(RSI_AIPos AIPos, bool grip)
{
	XMLDocument newDoc;

	if(newDoc.Parse(_sendString.c_str()) != 0)
	{
		std::cerr << "Error parsing sendString!" << std::endl;
		return "";
	}

	// Add AIPos element
	newDoc.RootElement()->InsertEndChild(newDoc.NewElement("AKorr"));

	for(int i=0; i<6; i++)
	{
		std::string attribute = "A" + SSTR(i+1);
		std::stringstream str;
		str << std::fixed << std::setprecision(DEF_PRECISION) << AIPos.q[i];
		std::string value = str.str();
		newDoc.RootElement()->FirstChildElement("AKorr")->SetAttribute(attribute.c_str(), value.c_str());
	}
	/*
	Modification for gripper control
	*/
	// Add Grip
	// i1 = 1 and i2 = 0 open
	newDoc.RootElement()->InsertEndChild(newDoc.NewElement("GRIP"));
	std::string attribute1 = "i1";
	std::string attribute2 = "i2";
	if(grip == 1) //closed
	{
		newDoc.RootElement()->FirstChildElement("GRIP")->SetAttribute(attribute1.c_str(), 0);
		newDoc.RootElement()->FirstChildElement("GRIP")->SetAttribute(attribute2.c_str(), 1); 
	}
	else //open
	{
		newDoc.RootElement()->FirstChildElement("GRIP")->SetAttribute(attribute1.c_str(), 1);
		newDoc.RootElement()->FirstChildElement("GRIP")->SetAttribute(attribute2.c_str(), 0);
	}
	/*
	End of modification
	*/

	// Add IPOC
	if(_IPOC == -1)
	{
		std::cerr << "IPOC Number unknown!" << std::endl;
		return "";
	}

	newDoc.RootElement()->InsertEndChild(newDoc.NewElement("IPOC"));
	newDoc.RootElement()->FirstChildElement("IPOC")->InsertEndChild(newDoc.NewText(boost::lexical_cast<std::string>(_IPOC).c_str()));



	// Get string
	XMLPrinter printer;
	newDoc.Accept(&printer);

	// Return XML formatted string
	return std::string(printer.CStr());
}

