#ifndef KUKARSIPARSER_INCLUDED
#define KUKARSIPARSER_INCLUDED

// Includes
#include <iostream>
#include <boost/lexical_cast.hpp>
#include "tinyxml2.h"
#include <iomanip>

// Define
#define DEF_PRECISION           3
#define SSTR(x)                 dynamic_cast<std::ostringstream &>(( std::ostringstream() << std::dec << x )).str()
#define DTOSTR(x)               dynamic_cast<std::ostringstream &>(( std::ostringstream() << std::dec << x )).str()

// Namespaces
using namespace tinyxml2;

class KukaRSIParser
{

public:
	enum RSI_TECH_C
	{
		C11 = 0,
		C12 = 1,
		C13 = 2,
		C14 = 3,
		C15 = 4,
		C16 = 5,
		C17 = 6,
		C18 = 7,
		C19 = 8,
		C110 = 9
	};

	enum RSI_TECH_T
	{
		T21 = 0,
		T22 = 1,
		T23 = 2,
		T24 = 3,
		T25 = 4,
		T26 = 5,
		T27 = 6,
		T28 = 7,
		T29 = 8,
		T210 = 9
	};

	struct RSI_R
	{
		double X, Y, Z;
		double A, B, C;

		RSI_R()
		{
			X = Y = Z = A = B = C = 0.0;
		}

		RSI_R(double x, double y, double z, double a, double b, double c)
		{
			X = x;
			Y = y;
			Z = z;
			A = a;
			B = b;
			C = c;
		}

		friend std::ostream& operator << (std::ostream& stream, const RSI_R &p)
		{
			return stream << "P(" << p.X << "," << p.Y << "," << p.Z << "), RPY(" << p.A << "," << p.B << "," << p.C << ")";
		}
	};

	struct RSI_Tech
	{
		double C_T[10];

		RSI_Tech()
		{
			C_T[T21] = 1.09;
			C_T[T22] = 2.08;
			C_T[T23] = 3.07;
			C_T[T24] = 4.06;
			C_T[T25] = 5.05;
			C_T[T26] = 6.04;
			C_T[T27] = 7.03;
			C_T[T28] = 8.02;
			C_T[T29] = 9.01;
			C_T[T210] = 10.0;
		}

		/*RSI_Tech()
		{
		for(unsigned int i=0; i<10; i++)
		C_T[i] = 0.0;
		}*/

		RSI_Tech(double val)
		{
			for(unsigned int i=0; i<10; i++)
				C_T[i] = val;
		}

		friend std::ostream& operator << (std::ostream& stream, const RSI_Tech &p)
		{
			stream << "Tech(";

			for(unsigned int i=0; i<9; i++)
				stream << p.C_T[i] << ",";
			stream << p.C_T[9] << ")";

			return stream;
		}
	};

	struct RSI_AIPos
	{
		double q[6];

		RSI_AIPos()
		{
			for(unsigned int i=0; i<6; i++)
				q[i] = 0.0;
		}

		friend std::ostream& operator << (std::ostream& stream, const RSI_AIPos &p)
		{
			stream << "Q(";

			for(unsigned int i=0; i<5; i++)
				stream << p.q[i] << ",";
			stream << p.q[5] << ")";

			return stream;
		}
	};

	// Constructors and deconstructor
	KukaRSIParser();
	~KukaRSIParser();

	// Functions
	bool parseXML(std::string xmlString, bool loadFromFile = false);
	std::string constructFile(RSI_AIPos AIPos, bool grip); // changed to include gripper value

	RSI_AIPos getQ(void) { return _AIPos; }
	int getDigout(void) { return _digOut; }
	int getIPOC(void) { return _IPOC; }
	int getDelay(void) { return _delay; }
	int getGripout1(void) {return _gripOut1; }
	int getGripout2(void) {return _gripOut2; }

private:
	// Functions
	//

	// Variables
	XMLDocument _doc;
	RSI_AIPos _AIPos;
	int _digOut;
	int _IPOC;
	int _delay;
	int _gripOut1;
	int _gripOut2;
};

#endif

