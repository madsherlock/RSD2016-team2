Where to find information
=========================
* Below, in this readme.
* [project_structure.txt](https://gitlab.com/madsherlock/RSD2016-team2/raw/master/project_structure.txt) contains information about the file/folder structure of this project.
* The **guides** folder contains guides.
  * One important guide is **guides**/*ros_protocols.txt* containing detailed information about the protocols of our ROS packages. It is the go-to place for hurried documentation.
  * Another one is **guides**/*catkin_make test.txt*, which contains instructions on how to debug by building one ROS package at a time.

Important information
=====================
* Don't use frobomind_make. Don't! It's a tool made by someone who doesn't want to follow ROS standards.
*We should try to follow ROS standards as much as possible*.
* If you wish to build just *one* package, use:

```bash
catkin_make --only-pkg-with-deps package_name
```
* If you need a list of all packages in frobomind (and don't wish to specify anything in fmMake.txt),
look in **catkin_ws_rsd_team2/src/metapackage_frobomind_rsd/***package.xml*.
This contains all remotely relevant frobomind packages.
Now, if you want to add frobomind dependencies to a package, you can do so in your package.xml
by adding build_depend and run_depend entries based on **catkin_ws_rsd_team2/src/metapackage_frobomind_rsd/***package.xml*.
Remember to also add the same dependencies in CMakeLists.txt.
* Our project uses the following [Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules "Git - Submodules"):
  * [markerlocator](https://github.com/FroboLab/MarkerLocator "MarkerLocator")
  * [frobomind](https://github.com/FroboLab/frobomind "FroboMind")
  * [frobo_rsd2016_team2](https://gitlab.com/madsherlock/frobo-rsd2016-team2 "frobo_rsd2016_team2") (The package to be built on the Raspberry Pi)
  * [sick_tim](https://github.com/uos/sick_tim "ROS driver for LIDAR")
  * [rsd-pi-setup](https://gitlab.com/madsherlock/rsd-pi-setup "Raspberry Pi setup scripts")  
  
   Feel free to edit **frobo_rsd2016_team2**, located in our **catkin_ws_rsd_team2/src/** folder. 
   Just remember to commit and push changes from inside that folder.
   Same goes for **rsd-pi-setup**, which is located in the root of this repository.

Installation
============
Common first steps for all RSD platforms
----------------------------------------
* Install ROS Desktop full (Indigo or Kinetic).
* Clone RSD 2016 team 2 repository to anywhere. The --recursive option is important.

```bash
git clone --recursive git@gitlab.com:madsherlock/RSD2016-team2.git
cd RSD2016-team2
```

  * If you were a fool and didn't remember --recursive, try running:

```bash
git submodule init
git submodule update
```

* Install libwebsockets dependency of HMI.

```bash
bash guides/libwebsocket.sh
```
* Install Dependencies
  * Modbus module for Python - dependency of conveyor_belt package.
  * PugiXML - dependency of control_node_r

```bash
sudo apt install python-pymodbus libbluetooth-dev libpugixml-dev
```
* Initialize catkin workspace.

```bash
cd catkin_ws_rsd_team2/src
catkin_init_workspace
cd ..
```
* Add the workspace to your shell for future login.

```bash
mkdir -p devel
touch devel/setup.bash
echo "source $(readlink -f devel/setup.bash)" >> ~/.bashrc
```

Laptop install (all packages)
-----------------------------
* Build all packages.

```bash
catkin_make
```
* See what errors you get.
* Fix your errors (by installing dependencies)
* Report exactly what you did to Mikael, or update this readme.
* Load workspace.

```bash
source $HOME/.bashrc
```
* Install udev rules. Will prompt for user password.

```bash
cd src/frobo_rsd2016_team2/udev_rules
./install_udev_rules.sh
cd ../../../
```

Control PC install (HMI and robot cell packages)
------------------------------------------------
* Build Control PC metapackage.

```bash
catkin_make --only-pkg-with-deps metapackage_controlpc
```
* Load workspace.

```bash
source $HOME/.bashrc
```
* TODO: Install RSI stuff?

Raspberry Pi install (Mobile Robot relevant packages)
-----------------------------------------------------
* Build the package for the Mobile Robot. Only use one core for this (to keep RAM consumption low).

```bash
catkin_make -j1 --only-pkg-with-deps frobo_rsd2016_team2
```
* Load workspace.

```bash
source $HOME/.bashrc
```
* Install udev rules. Will prompt for Pi's password.

```bash
cd src/frobo_rsd2016_team2/udev_rules
./install_udev_rules.sh
cd ../../../
```

Just frobomind packages install
-------------------------------
* Build frobomind packages relevant for RSD.

```bash
catkin_make --only-pkg-with-deps metapackage_frobomind_rsd
```
* Load workspace.

```bash
source $HOME/.bashrc
```

Simulation packages install
---------------------------
* Build frobit simulation packages.

```bash
catkin_make --only-pkg-with-deps metapackage_frobitsim
```
* Load workspace.

```bash
source $HOME/.bashrc
```


Contribution
============
* If you have found a cool way to do/install something, write a guide for it in the **guides** folder.
* If you write code, wrap it in a ROS package or as a node in an existing ROS package. You should know how to do this by now.

Generally, for ROS packages
---------------------------
This is the file/folder structure that Mikael uses.
In *catkin_ws_rsd_team2/src/*:
```
package_name
├── include
│   └── package_name
│       └── header_to_be_used_by_other_packages.h
├── msg
│   └── message_file.msg
├── scripts
│   └── python_scripts.py
├── src
│   ├── headers_local_to_this_package.h
│   └── source_files.cpp
├── srv
│   └── service_file.srv
├── CMakeLists.txt
├── package.xml
└── setup.py
```

C++ based ROS Package
---------------------
This is a small guide on how to convert your shitty C++ code into a ROS Package and integrate it into the project.
Your shitty code consists of:
* *CMakeLists.txt*
* *main.cpp*
* *some_shitty_class.h*
* *some_shitty_class.cpp*

All in the same folder...

Your code depends on the markerlocator package, so that is reflected somewhat poorly in your *CMakeLists.txt*.

* Go to the **src** folder of our workspace:

```bash
cd catkin_ws_rsd_team2/src
```
* Create a new package named my_shitty_package

```bash
catkin_create_pkg my_shitty_package
```
* Open the two files in my_shitty_package and make sure you add the dependencies of your program,
including markerlocator, to both files. The *CMakeLists.txt* infrastructure is important,
don't change the order of things, and don't remove the comments.
* Add the contents of your *CMakeLists.txt* to the one created by catkin. Make sure there are no duplicate entries.
* Move *main.cpp* and *some_shitty_class.cpp* into **my_shitty_package/src/**.
* Decide if you want *some_shitty_class.h* to be available to other packages.
  * No? It's that shitty. Just put it in **my_shitty_package/src/**
  * Yes? Put it in **my_shitty_package/include/my_shitty_package/**.
* Go back and modify *CMakeLists.txt* to reflect new file locations.
If you decided to make some_shitty_class.h available to other packages,
make sure to uncomment this part:

```cmake
## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )
```
* See if it compiles now. You can do this in at least two ways.
  * Open *CMakeLists.txt* in your favorite IDE (Qt Creator), and build it.
  You will see relevant warnings.
  * Go to **catkin_ws_rsd_team2** and call:

```bash
catkin_make --only-pkg-with-deps my_shitty_package
```
* Google your problems.

Python based ROS Package
------------------------
This is a small guide on how to convert your crappy Python code into a ROS Package and integrate it into the project.
Your crappy code consists of one node:
* *my_crappy_node.py*

And it imports rospy and markerlocator.
* Go to the **src** folder of our workspace:

```bash
cd catkin_ws_rsd_team2/src
```
* Create a new package named my_crappy_package

```bash
catkin_create_pkg my_crappy_package
```
* Open the two files in my_crappy_package and make sure you add the dependencies of your program,
including rospy and markerlocator, to both files. The *CMakeLists.txt* infrastructure is important,
don't change the order of things, and don't remove the comments.
Do uncomment this part:

```cmake
## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()
```
* Create *setup.py* in **my_crappy_package** directory, containing:

```python
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup
d = generate_distutils_setup(
  scripts=['scripts/my_crappy_node.py'],)
setup(**d)
```
* Move *my_crappy_node.py* into **my_crappy_package/scripts/**.
* Make sure *my_crappy_node.py* begins with the following line (for rosrun to work):

```python
#!/usr/bin/env python
```
* Make sure *my_crappy_node.py* is executable (for rosrun to work):

```bash
chmod +x my_crappy_node.py
```
* Go to **catkin_ws_rsd_team2** and call:

```bash
catkin_make --only-pkg-with-deps my_crappy_package
rosrun my_crappy_package my_crappy_node.py
```
* Google your problems.
