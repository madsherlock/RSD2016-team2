#include "command_center.h"

command_center::command_center():
    current_state(StopConveor),
    order(),
    robot(),
    nodehandle("~"),
    getConveyor_configurations(nodehandle.serviceClient<conv_service::get>("/conveyor_belt_get_service")),
    setConveyor_configurations(nodehandle.serviceClient<conv_service::set>("/conveyor_belt_set_service")),
    loop_rate(100),
    robot_arm_client(nodehandle.serviceClient<kuka_ros::getConfiguration>("/KukaNode/GetConfiguration")),
    robot_arm_set_client(nodehandle.serviceClient<kuka_ros::setConfiguration>("/KukaNode/SetConfiguration")),
    gripper_set_client(nodehandle.serviceClient<kuka_ros::setGripper>("/KukaNode/SetGripper")),
    gripper_get_client(nodehandle.serviceClient<kuka_ros::getIsGripOpen>("/KukaNode/IsGripOpen")),
    stop_robot_arm_client(nodehandle.serviceClient<kuka_ros::stopRobot>("/KukaNode/StopRobot")),
    grasp_planning(nodehandle.serviceClient<vrep_common::simGetLinearJointsPathToPoint>("/vrep/SimGetLinearJointsPathToPoint")),
    getBrick(nodehandle.serviceClient<smart_camera::robot_coords_array>("/Robot_coords_server/get_bricks_positions")),
    start_cam(nodehandle.serviceClient<smart_camera::conveyor_stop>("/Camera_stop_conveyor_server/start_the_camera"))
{
    std::cout << "Command center - Constructed" << std::endl;
}

void command_center::decrement_order(int color)
{
    //Color RGB [Rød Gul Blå]
    order[color] -=1;
}

void command_center::sort_bricks()
{
    current_state = MoveConveyor_forward;
    srv_handler();
}

bool command_center::srv_handler()
{
    std::bitset<4> msg_bit;
    std_msgs::Int8 msg;
    conv_service::set srv;
    vrep_common::simGetLinearJointsPathToPoint grasp;
    smart_camera::robot_coords_array brick_information;
    smart_camera::conveyor_stop  reqorder;
    float A = 138.0*M_PI/180.0;
    float B = -13.3*M_PI/180.0;
    float C = -169.3*M_PI/180.0;
    switch(current_state)
    {

        case Start_Camera:
            reqorder.request.order.blue_order = 1;
            reqorder.request.order.red_order = 1;
            reqorder.request.order.yellow_order = 1;
            return start_cam.call(reqorder);
            break;
        case MoveConveyor_forward:

            std::cout << "MoveConveyor_forward -  start" << std::endl;
            msg_bit.set(3,1);
            msg_bit.set(2,1);
            msg_bit.set(1,0);
            msg_bit.set(0,1);
            msg.data = int(msg_bit.to_ulong());
            srv.request.request = int(msg_bit.to_ulong());//msg.data;
            return setConveyor_configurations.call(srv); // TODO: Test whether it terminates correctly
            break;

        case MoveConveyor_backward:

            std::cout << "MoveConveyor_backward -  start" << std::endl;
            msg_bit.set(3,1);
            msg_bit.set(2,0);
            msg_bit.set(1,0);
            msg_bit.set(0,1);
            msg.data = int(msg_bit.to_ulong());

            srv.request.request = int(msg_bit.to_ulong());//msg.data;

            return setConveyor_configurations.call(srv); // TODO: Test whether it terminates correctly
            break;

        case StopConveor:

            std::cout << "Stop conveyor" << std::endl;
            msg_bit.set(3,0);
            msg_bit.set(2,0);
            msg_bit.set(1,0);
            msg_bit.set(0,0);

            msg.data = int(msg_bit.to_ulong());

            srv.request.request = int(msg_bit.to_ulong());//msg.data;

            return setConveyor_configurations.call(srv); // TODO: Test whether it terminates correctly
            break;
        case Move_to_pick_brick_position:

            std::cout << "Move_to_pick_brick_position" << std::endl;
            robot.setgrip(1);
            robot.setarm(1.5411399602890015, 0.0, 0.0, 0.0, 0.0, 0.0);
            robot.waitformove();
            robot.setarm(1.5411399602890015, 0.9510650038719177, 0.1599940061569214, 0.02874559909105301, -1.1557199954986572, 0.0);

            return robot.waitformove(); // TODO: Test whether it terminates correctly.


            break;
        case Move_to_bin:

            std::cout << "Move_to_bin!" << std::endl;
            robot.setarm(1.5411399602890015, 0.9510650038719177, 0.1599940061569214, 0.02874559909105301, -1.1557199954986572, 0.0);
            robot.waitformove();
            robot.setarm(1.5411399602890015, 0.0, 0.0, 0.0, 0.0, 0.0);
            robot.waitformove();
            robot.setarm(0.0,0.0,0.0,0.0,0.0,0.0);
            robot.waitformove();
            robot.setarm(-0.19006599485874176, 0.6030809879302979, 0.4159640073776245, 0.0, -0.9978569746017456, -0.8207589983940125);
            robot.waitformove();
            std::cout << "Grip" << std::endl;
            robot.setgrip(1);
            robot.setarm(0.0,0.0,0.0,0.0,0.0,0.0);

            return robot.waitformove(); // TODO: Test whether it terminates correctly.

            break;
        case Deliver_bricks:

            std::cout << "Deliver_bricks" << std::endl;
            robot.setarm(-0.19006599485874176, 0.8900129795074463, 0.3457320034503937, 0.00010471999848959967, -1.2151700258255005, -0.8206189870834351);
            robot.waitformove();
            robot.setarm(0.26954901218414307, 0.9190899729728699, 0.26858898997306824, 0.02326519973576069, -1.1692299842834473, -0.3824020028114319);
            robot.waitformove();
            robot.setarm(0.21012000739574432, 0.7567750215530396, 0.3878819942474365, 0.018744800239801407, -1.1257699728012085, 1.0236899852752686);
            robot.waitformove();
            robot.setarm(0.24748800694942474, 0.670835018157959, 0.5734800100326538, 0.025918100029230118, -1.2257299423217773, 1.053529977798462);
            robot.waitformove();
            robot.setarm(0.24860499799251556, 0.8245459794998169, 0.5260069966316223, 0.03715810179710388, -1.3320200443267822, 1.0430099964141846);
            robot.waitformove();
            robot.setarm(0.17027400434017181, 1.0159599781036377, 0.05195850133895874, 0.014940000139176846, -1.0502300262451172, 0.9878739714622498);
            robot.waitformove();
            robot.setarm(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
            std::cout << "End" << std::endl;
            return robot.waitformove(); // Todo: Test whether it terminates correctly.

            break;
         case Pick_brick:
            std::cout << "Pick brick called" << std::endl;
            grasp.request.steps = 100;
            grasp.request.startJoints = {1.5411399602890015, 0.9510650038719177, 0.1599940061569214, 0.02874559909105301, -1.1557199954986572, 0.0};
            //smart_camera (x,y,z,r,p,y)
            getBrick.call(brick_information);
            std::cout << "Not stalling!" << std::endl;
            std::cout << brick_information.response.array.array.size() << std::endl;
            std::cout << "For loop" << std::endl;
            for(int i = 0 ; i < brick_information.response.array.array.size() ; i++)
            {
                float x = brick_information.response.array.array.at(i).X/1000;
                float y = brick_information.response.array.array.at(i).Y/1000;
                float z = brick_information.response.array.array.at(i).Z/1000;
                float angle = brick_information.response.array.array.at(i).angle;
                float color = brick_information.response.array.array.at(i).color;
                std::cout << "Grasp planning!" << std::endl;
                std::cout << "x: " << x << " y: " << y << " z: " << z << " angle: " << angle << " color: " << color << std::endl;
                grasp.request.goalPoint = {x, y, z, A,B,C};
                if(grasp_planning.call(grasp))
                {
                    std::cout << "grasp true" << std::endl;
                }
                else
                {
                    std::cout << "grasp false" << std::endl;
                }
                    ;
                std::cout << "path" << std::endl;
                for(int i = 0 ; i < grasp.response.steps*6 ; i+=6 )
                {
                    for(int j = 0; j < 6 ; j++)
                    {
                        std::cout << grasp.response.path[i+0] << ",";
                        std::cout << grasp.response.path[i+1] << ",";
                        std::cout << grasp.response.path[i+2] << ",";
                        std::cout << grasp.response.path[i+3] << ",";
                        std::cout << grasp.response.path[i+4] << ",";
                        std::cout << grasp.response.path[i+5] << std::endl;

                        robot.setarm(grasp.response.path[i+0],
                                     grasp.response.path[i+1],
                                     grasp.response.path[i+2],
                                     grasp.response.path[i+3],
                                     grasp.response.path[i+4],
                                     grasp.response.path[i+5]);
                    }
                    std::cout << std::endl;
                }

            }
            std::cout << "pick brick end" << std::endl;
            return true;
            break;

         default:
            std::cout << "HEYYY...!";
            return false;
            break;
    }
}

void command_center::deliver_bricks()
{

}


