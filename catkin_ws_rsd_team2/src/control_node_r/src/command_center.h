#ifndef COMMAND_CENTER_H
#define COMMAND_CENTER_H
#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <conv_service/get.h>
#include <conv_service/set.h>
#include <kuka_ros/getConfiguration.h>
#include <kuka_ros/setConfiguration.h>
#include <kuka_ros/getIsGripOpen.h>
#include <kuka_ros/setGripper.h>
#include <kuka_ros/stopRobot.h>
#include <vrep_common/simGetLinearJointsPathToPoint.h>
#include "robot_control.h"
#include <bitset>
#include <smart_camera/robot_coords_array.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>
#include <smart_camera/brick_order.h>
#include <smart_camera/conveyor_stop.h>

class command_center
{
public:
    enum commands{MoveConveyor_forward=0,
                MoveConveyor_backward,
                StopConveor,
                Move_to_pick_brick_position,
                Pick_brick,
                Move_to_bin,
                Deliver_bricks,
                Start_Camera,
                Stop_Camera

               };
    command_center();
    void sort_bricks();
    void deliver_bricks();
    commands current_state;
    bool srv_handler();
    int order[3];
    void decrement_order(int);
    robot_control robot;
private:
    ros::NodeHandle nodehandle;
    ros::ServiceClient getConveyor_configurations;
    ros::ServiceClient setConveyor_configurations;
    ros::Rate loop_rate;
    ros::ServiceClient robot_arm_client;
    ros::ServiceClient robot_arm_set_client;
    ros::ServiceClient gripper_set_client;
    ros::ServiceClient gripper_get_client;
    ros::ServiceClient stop_robot_arm_client;
    ros::ServiceClient grasp_planning;
    ros::ServiceClient getBrick;
    ros::ServiceClient start_cam;


};

#endif // COMMAND_CENTER_H
