#include <iostream>
#include <ros/ros.h>
#include "subscriberandpublisher.h"
#include "command_center.h"

int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "control_node_r");

  //Create an object of class SubscribeAndPublish that will take care of everything
  SubscriberAndPublisher subpub;

  ros::spin();

  return 0;
}
