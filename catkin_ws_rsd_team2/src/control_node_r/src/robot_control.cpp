#include "robot_control.h"

robot_control::robot_control() : robot_arm_set_client(n.serviceClient<kuka_ros::setConfiguration>("/KukaNode/SetConfiguration")),
gripper_set_client(n.serviceClient<kuka_ros::setGripper>("/KukaNode/SetGripper")),
ismoving_client(n.serviceClient<kuka_ros::getIsMoving>("/KukaNode/IsMoving"))
{
    std::cout << "Robot_control is constructed" << std::endl;
}


void robot_control::setarm(double q1, double q2, double q3, double q4, double q5, double q6)
{
     kuka_ros::setConfiguration srv_arm;

     for(int i = 0; i < 6; i++)
     {
         srv_arm.request.speed[i] = 15;
     }
     srv_arm.request.q[0] = q1;
     srv_arm.request.q[1] = q2;
     srv_arm.request.q[2] = q3;
     srv_arm.request.q[3] = q4;
     srv_arm.request.q[4] = q5;
     srv_arm.request.q[5] = q6;

     if (robot_arm_set_client.call(srv_arm))
     {
         std::cout << "Hey you got a Q and it is: " << srv_arm.response << std::endl;
     }
     else
     {
         ROS_ERROR("Failed to call service arm");
         //std::cout << "Failed to call service arm: " << srv_arm.response << std::endl;
     }
}


void robot_control::setgrip(int myint)
{
    kuka_ros::setGripper srv_grip;

    if(myint == 0)
    {
        srv_grip.request.grip[0] = 0;
    }
    else if (myint == 1)
    {
        srv_grip.request.grip[0] = 1;
    }
    if (gripper_set_client.call(srv_grip))
    {
        std::cout << "Gripper is now open: " << srv_grip.response << std::endl;
    }
    else
    {
        ROS_ERROR("Failed to call service grip");
        //std::cout << "Failed to call service grip: " << srv_grip.response << std::endl;
    }

}

bool robot_control::IsMoving()
{
    kuka_ros::getIsMoving srv_IsMoving;

    if (ismoving_client.call(srv_IsMoving))
    {
        return srv_IsMoving.response.isMoving;
    }
    else
    {
        std::cout << "Somthing went wrong" << std::endl;
        return true; // Was true before.. I mean false was true before..
    }
}

bool robot_control::waitformove()
{
    std::cout << "waitformove start" << std::endl;
    bool moving = true; // Was true before
    while(moving)
    {
        sleep(5);
        moving = IsMoving();
    }
    std::cout << "waitformove stop" << std::endl;

    return moving;
}

