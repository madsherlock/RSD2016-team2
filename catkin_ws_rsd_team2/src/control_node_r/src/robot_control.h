#ifndef ROBOT_CONTROL_H
#define ROBOT_CONTROL_H
#include <ros/ros.h>
#include <kuka_ros/getConfiguration.h>
#include <kuka_ros/setConfiguration.h>
#include <kuka_ros/getIsGripOpen.h>
#include <kuka_ros/setGripper.h>
#include <kuka_ros/stopRobot.h>
#include <kuka_ros/getIsMoving.h>
class robot_control
{
private:
    ros::NodeHandle n;
    ros::ServiceClient robot_arm_set_client;
    ros::ServiceClient gripper_set_client;
    ros::ServiceClient ismoving_client;

public:
    robot_control();
    void setarm(double q1, double q2, double q3, double q4, double q5, double q6);
    void setgrip(int myint);
    bool IsMoving();
    bool waitformove();
};



#endif // ROBOT_CONTROL_H
