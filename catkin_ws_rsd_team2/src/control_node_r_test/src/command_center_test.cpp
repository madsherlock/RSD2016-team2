#include "command_center_test.h"

command_center_test::command_center_test():
    current_state(StopConveor),
    order(),
    robot(),
    nodehandle("~"),
    getConveyor_configurations(nodehandle.serviceClient<conv_service::get>("/conveyor_belt_get_service")),
    setConveyor_configurations(nodehandle.serviceClient<conv_service::set>("/conveyor_belt_set_service")),
    loop_rate(100),
    robot_arm_client(nodehandle.serviceClient<kuka_ros::getConfiguration>("/KukaNode/GetConfiguration")),
    robot_arm_set_client(nodehandle.serviceClient<kuka_ros::setConfiguration>("/KukaNode/SetConfiguration")),
    gripper_set_client(nodehandle.serviceClient<kuka_ros::setGripper>("/KukaNode/SetGripper")),
    gripper_get_client(nodehandle.serviceClient<kuka_ros::getIsGripOpen>("/KukaNode/IsGripOpen")),
    stop_robot_arm_client(nodehandle.serviceClient<kuka_ros::stopRobot>("/KukaNode/StopRobot"))
{
    std::cout << "Command center - Constructed" << std::endl;
}

void command_center_test::decrement_order(int color)
{
    //Color RGB [Rød Gul Blå]
    order[color] -=1;
}

void command_center_test::sort_bricks()
{
    current_state = MoveConveyor_forward;
    srv_handler();
}

bool command_center_test::srv_handler()
{
    std::bitset<4> msg_bit;
    std_msgs::Int8 msg;
    conv_service::set srv;
    switch(current_state)
    {
        case MoveConveyor_forward:

            std::cout << "MoveConveyor_forward -  start" << std::endl;
            msg_bit.set(3,1);
            msg_bit.set(2,1);
            msg_bit.set(1,0);
            msg_bit.set(0,1);
            msg.data = int(msg_bit.to_ulong());
            srv.request.request = int(msg_bit.to_ulong());//msg.data;
            return setConveyor_configurations.call(srv); // TODO: Test whether it terminates correctly
            break;

        case MoveConveyor_backward:

            std::cout << "MoveConveyor_backward -  start" << std::endl;
            msg_bit.set(3,1);
            msg_bit.set(2,0);
            msg_bit.set(1,0);
            msg_bit.set(0,1);
            msg.data = int(msg_bit.to_ulong());

            srv.request.request = int(msg_bit.to_ulong());//msg.data;

            return setConveyor_configurations.call(srv); // TODO: Test whether it terminates correctly
            break;

        case StopConveor:

            std::cout << "Stop conveyor" << std::endl;
            msg_bit.set(3,0);
            msg_bit.set(2,0);
            msg_bit.set(1,0);
            msg_bit.set(0,0);

            msg.data = int(msg_bit.to_ulong());

            srv.request.request = int(msg_bit.to_ulong());//msg.data;

            return setConveyor_configurations.call(srv); // TODO: Test whether it terminates correctly
            break;
        case Move_to_pick_brick_position:

            std::cout << "Move_to_pick_brick_position" << std::endl;
            robot.setgrip(0);
            robot.setarm(1.5411399602890015, 0.0, 0.0, 0.0, 0.0, 0.0);
            //robot.waitformove();
            robot.setarm(1.5411399602890015, 0.9510650038719177, 0.1599940061569214, 0.02874559909105301, -1.1557199954986572, 0.0);
            //robot.waitformove();

            return robot.waitformove(); // TODO: Test whether it terminates correctly.


            break;
        case Move_to_bin:

            std::cout << "Move_to_bin!" << std::endl;
            robot.setgrip(1);
            robot.setarm(1.5411399602890015, 0.9510650038719177, 0.1599940061569214, 0.02874559909105301, -1.1557199954986572, 0.0);
            //robot.waitformove();
            robot.setarm(1.5411399602890015, 0.0, 0.0, 0.0, 0.0, 0.0);
            //robot.waitformove();
            robot.setarm(0.0,0.0,0.0,0.0,0.0,0.0);
            //robot.waitformove();
            robot.setarm(-0.19006599485874176, 0.6030809879302979, 0.4159640073776245, 0.0, -0.9978569746017456, -0.8207589983940125);
            robot.waitformove();
            std::cout << "Grip" << std::endl;
            robot.setgrip(0);
            robot.setarm(0.0,0.0,0.0,0.0,0.0,0.0);

            return robot.waitformove(); // TODO: Test whether it terminates correctly.

            break;
        case Deliver_bricks:

            std::cout << "Deliver_bricks" << std::endl;
            robot.setarm(-0.17285700142383575, 0.7395129799842834, 0.4003959894180298, 0.0008377580088563263, -1.1196600198745728, -0.8040210008621216);
            //robot.waitformove();
            robot.setarm(-0.37510600686073303, 0.7812089920043945, 0.3131299912929535, -0.0077667199075222015, -1.0744199752807617, -0.9986420273780823);
            //robot.waitformove();
            robot.setarm(-0.3756299912929535, 0.9533339738845825, 0.2544339895248413, -0.009930919855833054, -1.1881200075149536, -0.9967749714851379);
            //robot.waitformove();
            robot.setarm(0.007103490177541971, 0.9177460074424744, 0.34461501240730286, 0.010890900157392025, -1.2436699867248535, -0.6336240172386169);
            //robot.waitformove();
            robot.setarm(0.0065449802204966545, 0.7185350060462952, 0.408720999956131, 0.007836529985070229, -1.1085799932479858, -0.6308839917182922);
            //robot.waitformove();
            robot.setarm(0.21012000739574432, 0.7567750215530396, 0.3878819942474365, 0.018744800239801407, -1.1257699728012085, 1.0236899852752686);
            //robot.waitformove();
            robot.setarm(0.24748800694942474, 0.670835018157959, 0.5734800100326538, 0.025918100029230118, -1.2257299423217773, 1.053529977798462);
            //robot.waitformove();
            robot.setarm(0.24860499799251556, 0.8245459794998169, 0.5260069966316223, 0.03715810179710388, -1.3320200443267822, 1.0430099964141846);
            //robot.waitformove();
            robot.setarm(0.17027400434017181, 1.0159599781036377, 0.05195850133895874, 0.014940000139176846, -1.0502300262451172, 0.9878739714622498);
            //robot.waitformove();
            robot.setarm(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
            std::cout << "End" << std::endl;
            return robot.waitformove(); // Todo: Test whether it terminates correctly.

            break;

         default:
            std::cout << "HEYYY...!";
            return false;
            break;
    }
}

void command_center_test::deliver_bricks()
{

}


