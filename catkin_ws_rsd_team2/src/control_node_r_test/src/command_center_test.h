#ifndef COMMAND_CENTER_test_H
#define COMMAND_CENTER_test_H
#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <conv_service/get.h>
#include <conv_service/set.h>
#include <kuka_ros/getConfiguration.h>
#include <kuka_ros/setConfiguration.h>
#include <kuka_ros/getIsGripOpen.h>
#include <kuka_ros/setGripper.h>
#include <kuka_ros/stopRobot.h>
#include "robot_control_test.h"
#include <bitset>
class command_center_test
{
public:
    enum commands{MoveConveyor_forward=0,
                MoveConveyor_backward,
                StopConveor,
                Move_to_pick_brick_position,
                //Pick_brick
                Move_to_bin,
                Deliver_bricks
                //StartCamera
                //StopCamera

               };
    command_center_test();
    void sort_bricks();
    void deliver_bricks();
    commands current_state;
    bool srv_handler();
    int order[3];
    void decrement_order(int);
    robot_control_test robot;
private:
    ros::NodeHandle nodehandle;
    ros::ServiceClient getConveyor_configurations;
    ros::ServiceClient setConveyor_configurations;
    ros::Rate loop_rate;
    ros::ServiceClient robot_arm_client;
    ros::ServiceClient robot_arm_set_client;
    ros::ServiceClient gripper_set_client;
    ros::ServiceClient gripper_get_client;
    ros::ServiceClient stop_robot_arm_client;


};

#endif // COMMAND_CENTER_test_H
