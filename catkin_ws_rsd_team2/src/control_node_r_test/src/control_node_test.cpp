#include <iostream>
#include <ros/ros.h>
#include "subscriberandpublisher_test.h"
#include "command_center_test.h"

int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "control_node_r_test");

  //Create an object of class SubscribeAndPublish that will take care of everything
  SubscriberAndPublisher_test subpub;

  ros::spin();

  return 0;
}
