#include "subscriberandpublisher_test.h"

SubscriberAndPublisher_test::SubscriberAndPublisher_test() :
    command(),
   n_("~"),
   mes_client_pub_(n_.advertise<std_msgs::String>("/rc/processNode/getCurrentState", 1, true)),
   mes_client_sub_(n_.subscribe("/MESState", 1, &SubscriberAndPublisher_test::callback_mes, this)),
   camera_control_pub_(n_.advertise<std_msgs::String>("/published_camera_topic", 1, true)),
   camera_control_sub_(n_.subscribe("/subscribed_camera_topic", 1, &SubscriberAndPublisher_test::callback_camera, this)),
   mes_notification(true)
{
    std::cout << "SubPub - Constructed" << std::endl;
}

std::string SubscriberAndPublisher_test::check_xml(std::string source)
{
// tag::code[]
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load(source.c_str());

    if (result)
    {
        std::cout <<"Event: " << doc.child("log_entry").child("event").child_value() << std::endl;
        //std::cout <<"Time: " << doc.child("log_entry").child("time").child_value() << std::endl;

        pugi::xml_node nodes = doc.child("log_entry").child("order");
        int i = 0;
        for(pugi::xml_node tool = nodes.child("brick"); tool; tool = tool.next_sibling("brick"))
        {
            //std::cout << tool.attribute("type").value() << ": " << tool.child_value() << std::endl;
            command.order[i] = std::stoul(std::string(tool.child_value()));
            i++;
        }
        //std::cout <<"Comment: " << doc.child("log_entry").child("comment").child_value() << std::endl;
    }
    else
    {
        //std::cout << "XML [" << source << "] parsed with errors, attr value: [" << doc.child("node").attribute("attr").value() << "]\n";
        //std::cout << "Error description: " << result.description() << "\n";
        //std::cout << "Error offset: " << result.offset << " (error at [..." << (source.c_str() + result.offset) << "]\n\n";
    }

    std::string output = doc.child("log_entry").child("event").child_value();
    return output;
// end::code[]
}


void SubscriberAndPublisher_test::callback_mes(const std_msgs::String& input)
{
    std_msgs::String output;
    //std::cout << "Received message from mes: " << input.data << std::endl;
    //std::cout << std::endl;
    std::string input_event = check_xml(input.data);

    //std::cout << command.order[0] <<" "<< command.order[1] <<" "<< command.order[2] << std::endl;

    if(input_event == "Wait_for_new_task")
    {
        //
        //
        std::cout << "Wait for new task - received" << std::endl;
    }
    else if(input_event == "wait_for_task_accept")
    {
        // Take order _  store it to std_msgs::String current_order.
        // save in comand_center
        // Send order to camera
    }
    else if(input_event == "Wait_for_bricks")
    {
        std::cout << "Event occured!" << std::endl;
        //Start Camera
        //Servce call: Start Camera
        //set command.current_status = start_camera:
        //command.srv_handler();
        //Start Conveyor belt -- command.current_statatus, srv_handler;
        command.current_state = command.MoveConveyor_forward;
        command.srv_handler();
        command.current_state = command.Move_to_pick_brick_position;
        command.srv_handler();
        //std::cout << "Subscribe closed!" << std::endl;
        //mes_client_sub_.shutdown();
        mes_notification = false;

    }

    else if(input_event == "Sort_bricks")
    {
        command.current_state = command.StopConveor;
        command.srv_handler();
        std::cout << "Sort_bricks - received " << std::endl;
        command.current_state = command.Move_to_bin;
        command.srv_handler();
        output.data = "FINISHED";
        mes_client_pub_.publish(output);
    }

    else if(input_event == "Deliver_to_mobile")
    {
        //Do the delivery to the mobile robot!
        //Some command actions.
        std::cout << "Deliver to bricks - received " << std::endl;
        command.current_state = command.Deliver_bricks;
        command.srv_handler();

        output.data = "FINISHED_DELIVER";
        mes_client_pub_.publish(output);
    }


}

void SubscriberAndPublisher_test::callback_camera(const std_msgs::String& input)
{
    std_msgs::String output;

    //Receive messages from Camera:
    //-------------------------------------------------
    // Loop until order fulfilled
    //-------------------------------------------------
    if(input.data == "stop - conveyour")
    {
        //Stop conveyourbelt
        std::cout << "Camera detected a brick - Stop conveyor - received" << std::endl;
        command.current_state = command.StopConveor;
        command.srv_handler();
        //Srv call  while(getBrick.response != empty)
        //{Move robot arm to brick
        // Decrement the order
        // }
        //Restart the conveyor belt
    }
    //-------------------------------------------------
    //Restarts the topic subscribing to the MES Client
    else if(input.data == "Continue")
    {
            ros::Time time = ros::Time::now();
            std::cout << time << std::endl;
            // If value is the same for a long time
            // Order is fulfilled.
            mes_notification = true;
            // And restart the communication with the mes_servcer.
            // mes_notification=true;
    }
    if(mes_notification==true)
    {
        std::cout << "MES Subscribe channel opened" << std::endl;
        mes_client_sub_ = n_.subscribe("/MESState", 1, &SubscriberAndPublisher_test::callback_mes, this);
        //camera_client_sub_.shutdown();
    }
    camera_control_pub_.publish(output);

}


