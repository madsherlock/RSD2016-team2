#ifndef SUBSCRIBERANDPUBLISHER_test_H
#define SUBSCRIBERANDPUBLISHER_test_H

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <string>
#include "command_center_test.h"
#include <pugixml.hpp>
#include <pugiconfig.hpp>

class SubscriberAndPublisher_test
{
    public:
        SubscriberAndPublisher_test();
        command_center_test command;
        void callback_mes(const std_msgs::String& input);
        void callback_camera(const std_msgs::String& input);
    private:
        ros::NodeHandle n_;
        ros::Publisher mes_client_pub_;
        ros::Subscriber mes_client_sub_;
        ros::Publisher camera_control_pub_;
        ros::Subscriber camera_control_sub_;
        bool mes_notification;
        std::string check_xml(std::string);

};

#endif // SUBSCRIBERANDPUBLISHER_test_H
