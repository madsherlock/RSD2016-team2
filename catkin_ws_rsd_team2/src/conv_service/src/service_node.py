#!/usr/bin/env python
#-*- coding: utf-8 -*-

import rospy
import conv_service.srv as custom_type
from std_srvs.srv import SetBool
from std_msgs.msg import Bool
from pymodbus.client.sync import ModbusTcpClient
import numpy as np

server = ModbusTcpClient('192.168.100.80', port=502)

pub_safety = rospy.Publisher('/fmSafety/emergency_stop',Bool,queue_size=1)

connection = server.connect()

def getConfiguration(req):
    print "Get was called"

    result_0 = server.read_coils(0,1)
    result_1 = server.read_coils(1,1)
    result_2 = server.read_coils(2,1)
    result_3 = server.read_coils(3,1)

    readings = str(int(result_0.bits[0]))+str(int(result_1.bits[0]))+str(int(result_2.bits[0]))+str(int(result_3.bits[0]))

    return custom_type.getResponse(readings)

def setConfiguration(req):
    print "Set was called"

    received = '{0:08b}'.format(req.request)

    server.write_coil(0, int(received[4]))  #bit0
    server.write_coil(1, int(received[5]))  #bit1
    server.write_coil(2, int(received[6]))  #bit2
    server.write_coil(3, int(received[7]))  #bit3

    result_0 = server.read_coils(0,1)
    result_1 = server.read_coils(1,1)
    result_2 = server.read_coils(2,1)
    result_3 = server.read_coils(3,1)

    readings = str(int(result_0.bits[0]))+str(int(result_1.bits[0]))+str(int(result_2.bits[0]))+str(int(result_3.bits[0]))

    return custom_type.setResponse(readings)

def hmi_emergency(Empty):
    server.write_coil(5,False)
    print "Emergency stop called"
    result=server.read_coils(4,1)
    pub_safety.publish(result.bits[0])
    return [result.bits[0],'Empty']

def hmi_reset(Empty):
    server.write_coil(5,True)
    print "Emergency reset called"
    result=server.read_coils(4,1)
    pub_safety.publish(result.bits[0])
    return [result.bits[0],'Empty']

def main():
    rospy.init_node('conveyor_service')
    get_service = rospy.Service('conveyor_belt_get_service', custom_type.get, getConfiguration)
    set_service = rospy.Service('conveyor_belt_set_service', custom_type.set, setConfiguration)
    em_stop = rospy.Service('/fmSafety/press_emergency_stop',SetBool,hmi_emergency)
    em_reset = rospy.Service('/fmSafety/press_emergency_reset',SetBool,hmi_reset)

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        result = server.read_coils(4,1)
        pub_safety.publish(result.bits[0])
        rate.sleep()
   
    rospy.spin()

if __name__ == '__main__':
    print "runnning"
    main()
