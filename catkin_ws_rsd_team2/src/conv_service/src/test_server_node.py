#!/usr/bin/env python
#-*- coding: utf-8 -*-


import rospy
import conv_service.srv as custom_type
from pymodbus.client.sync import ModbusTcpClient
import numpy as np

bit_0 = False
bit_1 = False
bit_2 = False
bit_3 = False


def getConfiguration(req):
    print "Get was called"

    readings = str(int(bit_0)) + str(int(bit_1)) + str(int(bit_2)) + str(int(bit_3))

    return custom_type.getResponse(readings)

def setConfiguration(req):
    print "Set was called"

    received = '{0:08b}'.format(req.request)

    bit_0 = received[0]  #bit0
    bit_1 = received[1]  #bit1
    bit_2 = received[2]  #bit2
    bit_3 = received[3]  #bit3

    readings = str(int(bit_0)) + str(int(bit_1)) + str(int(bit_2)) + str(int(bit_3))

    custom_type.setResponse(readings)


def main():
    rospy.init_node('conveyor_service')
    get_service = rospy.Service('conveyor_belt_get_service', custom_type.get, getConfiguration)
    set_service = rospy.Service('conveyor_belt_set_service', custom_type.set, setConfiguration)
    rospy.spin()

if __name__ == '__main__':
    print "runnning"
    main()
