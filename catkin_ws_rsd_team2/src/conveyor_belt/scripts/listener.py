#!/usr/bin/env python

import rospy
#from std_msgs.msg import Empty
from std_msgs.msg import Int8
from std_msgs.msg import String
from std_msgs.msg import Bool
from std_srvs.srv import Empty
from std_srvs.srv import SetBool
from pymodbus.client.sync import ModbusTcpClient
import numpy as np
import timeit

server = ModbusTcpClient('192.168.100.80', port=502)
connection = server.connect()

rospy.init_node('conveyor_control', anonymous=True)
pub_manual = rospy.Publisher('/conveyor_status_manual', String, queue_size=1)
pub_auto = rospy.Publisher('/conveyor_status_auto', String, queue_size=1)
pub_safety = rospy.Publisher('/fmSafety/emergency_stop',Bool,queue_size=1)
rospy.sleep(2.0)

number = 0

def callback(data):
    start = timeit.default_timer()
    global number


    print "Callback start: " + str(data.data)   
    
    result_0 = server.read_coils(0,1)
    result_1 = server.read_coils(1,1)
    result_2 = server.read_coils(2,1)
    result_3 = server.read_coils(3,1)


    readings = str(int(result_0.bits[0]))+str(int(result_1.bits[0]))+str(int(result_2.bits[0]))+str(int(result_3.bits[0]))

    pub_auto.publish(String(readings))
    stop = timeit.default_timer()
    
    number=number+1
    print "Message received" + " " + str(number) + " "+ str(stop - start)
    


def callback_2(data):
    start = timeit.default_timer()
    #print "Callback start: " + str(data.data)
    
    received = '{0:08b}'.format(data.data)
    
    print "received data: "  + str(received)
    
    #print type(int(received[4]))
    #print type(int(received[5]))
    #print type(int(received[6]))
    #print type(int(received[7]))
    server.write_coil(0, int(received[4]))  #bit0
    server.write_coil(1, int(received[5]))  #bit1
    server.write_coil(2, int(received[6]))  #bit2
    server.write_coil(3, int(received[7]))  #bit3

    result_0 = server.read_coils(0,1)
    result_1 = server.read_coils(1,1)
    result_2 = server.read_coils(2,1)
    result_3 = server.read_coils(3,1)

    readings = str(int(result_0.bits[0]))+str(int(result_1.bits[0]))+str(int(result_2.bits[0]))+str(int(result_3.bits[0]))

    pub_manual.publish(String(readings))
    
    stop = timeit.default_timer()
    print "New values: " + readings +  " Time: " + str(stop - start)


def hmi_emergency(Empty):

    print "Emergency stop called"
    server.write_coil(5, False)
    result = server.read_coils(4,1)
    pub_safety.publish(result.bits[0])
    return [result.bits[0],'Empty']

	
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.Subscriber('conveyor_control', Int8, callback)
    rospy.Subscriber('conveyor_HMI_control',Int8,callback_2)
    server.write_coil(5, True)

    rospy.Service('/fmSafety/press_emergency_stop',SetBool,hmi_emergency)

    # spin() simply keeps python from exiting until this node is stopped
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        result = server.read_coils(4,1)
        pub_safety.publish(result.bits[0])
        rate.sleep()


    rospy.spin()

if __name__ == '__main__':
    print "Running"
    listener()
