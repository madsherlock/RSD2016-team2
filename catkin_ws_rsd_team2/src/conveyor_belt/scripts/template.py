#!/usr/bin/env python

import rospy
from std_msgs.msg import Empty
from std_msgs.msg import Int8
from std_msgs.msg import String
import numpy as np

bit_0 = False
bit_1 = False
bit_2 = False
bit_3 = False



rospy.init_node('conveyor_control', anonymous=True)
pub_manual = rospy.Publisher('/conveyor_status_manual', String, queue_size=1)
pub_auto = rospy.Publisher('/conveyor_status_auto', String, queue_size=1)
pub_safety = rosypy.Publisher('/safety_status',Bool, queue_size=1)
pub_stop = rospy.Publisher('/emergency_stop',Bool,queue_size=1)
rospy.sleep(2.0)


#readings = str(int(bit_0)) + str(int(bit_1)) + str(int(bit_2)) + str(int(bit_3))
#pub.publish(String(readings))


def callback(data):
    readings = str(int(bit_0)) + str(int(bit_1)) + str(int(bit_2)) + str(int(bit_3))
	
    pub_auto.publish(String(readings))
    
    print "Message received"


def callback_2(data):
    
    received = '{0:b}'.format(data.data)
    print data.data
    
    print received[0]
    print received[1]
    print received[2]
    print received[3]
	
    bit_0 = received[0]  #bit0
    bit_1 = received[1]  #bit1
    bit_2 = received[2]  #bit2
    bit_3 = received[3]  #bit3

    readings = str(int(bit_0)) + str(int(bit_1)) + str(int(bit_2)) + str(int(bit_3))
   
   
    print readings
    pub_manual.publish(String(readings))

def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.Subscriber('conveyor_control', Int8, callback)
    rospy.Subscriber('conveyor_HMI_control',Int8,callback_2)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    print "Running"
    listener()
