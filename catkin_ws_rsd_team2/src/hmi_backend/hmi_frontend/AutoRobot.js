var auto_transition_tolerance = false;
function disable_transition_tolerance()
{
	auto_transition_tolerance = true;
}
function initialize_AutoRobot(){
	if(not_connected){
		connect('ws://'+callback_IP+':'+callback_port);
	}
	set_active_prototol("AutoRobot");
	auto_transition_tolerance = false;
	setTimeout(disable_transition_tolerance, 3000);
}

function AutoRobot() {
	console.log("AutoRobot: " + active_protocol + " substate: " + substate);
	if( active_protocol == "AutoRobot" && not_connected == false ) {
		if( substate == 0 ) {
			console.log("sending the protocol msg");
			websocket.send("AutoRobot");
			substate++;
		} else if ( substate == 1 ) {
			if( received_msg == "Ack AutoRobot" ) {
				console.log("received the ack msg");
				protocol_string = "Enabled";
				substate++;
				update_mr_status_enable = true;
				update_mr_status();
				mobile_robot_busy("ready"); 
			} else if(received_msg == "Robot Disconnected"){
				active_protocol = "inactive";
				substate = 999;
			} else if(received_msg == "Robot Busy"){
				substate--;
				console.log("Robot is busy");
				setTimeout(AutoRobot, 10000);
			}
		} else if( substate == 3 ) {
			console.log("updating mr status values");
			var mobile_robot_status = received_msg.split("\n");
			var mr_pos = mobile_robot_status[5].split(":")[1].split(",");
			var vel_lin = mobile_robot_status[0].split(":")[1];
			var vel_ang = mobile_robot_status[1].split(":")[1];
			var robot_busy = mobile_robot_status[2].split(":")[1];
			var battery = mobile_robot_status[4].split(":")[1];
			var tipper_status = mobile_robot_status[6].split(":")[1];
			var mr_nav_mode = mobile_robot_status[7].split(":")[1];
			mobile_robot_busy(robot_busy);
			adjust_bar('a', (vel_lin / 0.7) * 100); //to percent
			adjust_bar('b', (vel_ang / 0.6) * 100);
			adjust_bar('c', tipper_status);
			adjust_bar('d', battery);
			if(auto_transition_tolerance){
				if(mr_nav_mode == "manual"){
					//stop_auto_robot();
				}
				update_nav_mode(mr_nav_mode);
			}
			move_robot_on_map_x   = mr_pos[0];
			move_robot_on_map_y   = mr_pos[1];
			move_robot_on_map_yaw = mr_pos[2];
			substate = 2;
		}
	} else if(substate < 999){
		console.log("stuck in " + active_protocol + "\n");
	} else {
		substate = 0;
	}
}

function stop_auto_robot(){
	if(active_protocol == "AutoRobot" && substate == 2)
	{
		console.log("sending: stop Auto robot");	
		websocket.send("stop auto protocol");
		update_mr_status_enable = false;
	}
}
