function initialize_MoveRobot(){
	if(not_connected){
		connect('ws://'+callback_IP+':'+callback_port);
	}
	set_active_prototol("MoveRobot");
}

function MoveRobot() {
	console.log("MoveRobot: " + active_protocol + " substate: " + substate);
	if( active_protocol == "MoveRobot" && not_connected == false ) {
		if( substate == 0 ) {
			console.log("sending the protocol msg");
			websocket.send("MoveRobot");
			substate++;
		} else if ( substate == 1 ) {
			if( received_msg == "Ack MoveRobot" ) {
				console.log("received the ack msg");
				protocol_string = "Enabled";
				substate++;
				mobile_robot_busy("ready"); 
			} else if(received_msg == "Robot Disconnected"){
				active_protocol = "inactive";
				substate = 999;
			} else if(received_msg == "Robot Busy"){
				substate--;
				console.log("Robot is busy");
				setTimeout(MoveRobot, 10000);
			}
		// substate 2 is separate functions as this is not called by the dispatcher
		} else if( substate == 3 ) {
			console.log("updating mr status values");
			var mobile_robot_status = received_msg.split("\n");
			var mr_pos = mobile_robot_status[5].split(":")[1].split(",");
			var vel_lin = mobile_robot_status[0].split(":")[1];
			var vel_ang = mobile_robot_status[1].split(":")[1];
			var robot_busy = mobile_robot_status[2].split(":")[1];
			var battery = mobile_robot_status[4].split(":")[1];
			var tipper_status = mobile_robot_status[6].split(":")[1];
			var mr_nav_mode = mobile_robot_status[7].split(":")[1];
			mobile_robot_busy(robot_busy);
			adjust_bar('a', (vel_lin / 0.7) * 100);
			adjust_bar('b', (vel_ang / 0.6) * 100);
			adjust_bar('c', tipper_status);
			adjust_bar('d', battery);
			update_nav_mode(mr_nav_mode);
			move_robot_on_map_x   = mr_pos[0];
			move_robot_on_map_y   = mr_pos[1];
			move_robot_on_map_yaw = mr_pos[2];
			substate = 2;
			if(update_mr_status_enable == false && brake_pressed == false) {
				update_mr_status_enable = true;
				update_mr_status();
			}
			if(brake_pressed){
				move_robot("brake");
			}
		}
	} else if(substate < 999){
		console.log("stuck in " + active_protocol + " substate: " + substate + "\n");
	} else {
		substate = 0;
	}
}

var desired_tipper_pos = 100;
function move_robot(direction){
	if(active_protocol == "MoveRobot" && substate == 2){
		if(brake_pressed){
			brake_pressed = false;
		}
		mobile_robot_busy("taken"); 
		if(direction == "tipper"){
			direction = "tipper: " + desired_tipper_pos;
			if(desired_tipper_pos == 100) {
				desired_tipper_pos = 25;
			} else {
				desired_tipper_pos = 100;
			}
		}
		console.log("sending: move robot " + direction);
		websocket.send(direction);
		substate++;
	} else {
		update_nav_mode("manual");
	}
}
function stop_move_robot() //stop_move_robot
{
	if(active_protocol == "MoveRobot" && substate == 2)
	{
		console.log("sending: stop Move robot");
		websocket.send("EndMoveRobot");
		update_mr_status_enable = false;
	}
}

function getChar(event) {
	if (event.which!=0 && event.charCode!=0) {
		return String.fromCharCode(event.which)   // the rest
	} else {
		return null // special key
	}
}

window.onkeydown = keyboard_handler;
var brake_pressed = false;
function keyboard_handler(e) {
	e = e || window.event;
	
	// See https://css-tricks.com/snippets/javascript/javascript-keycodes/ for an overview of keycodes
	if (e.keyCode == '38') {        //arrow up
		move_robot("forward");
	} else if (e.keyCode == '40') { //arrow down
		move_robot("reverse");
	} else if (e.keyCode == '37') { //arrow left
		move_robot("left");
	} else if (e.keyCode == '39') { //arrow right
		move_robot("right");
	} else if (e.keyCode == '32') { //space
		if(active_protocol =="MoveRobot"){
			move_robot("brake");
			brake_pressed = true;
		} else {
			press_stop(true);
		}
	} else if (e.keyCode == '27') { //escape
		press_stop(true);
	} else {
		return true;
	}
	return false;
}
