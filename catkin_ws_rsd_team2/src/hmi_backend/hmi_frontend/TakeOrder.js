function initialize_TakeOrder(){
	if(not_connected){
		connect('ws://'+callback_IP+':'+callback_port);
	}
	//set active protocol
	set_active_prototol("TakeOrder");
}

function TakeOrder() {
	if( active_protocol == "TakeOrder" && not_connected == false ) {
		if( substate == 0 ) {
			console.log("sending the protocol msg");
			websocket.send("TakeOrder");
			substate++;
		} else if ( substate == 1 ) {
			if( received_msg == "Ack TakeOrder" ) {
				console.log("received the ack msg");
				var name = prompt("Select filename", protocol_string);
				protocol_string = name;
				websocket.send(protocol_string);
				substate++;
				setTimeout(TakeOrder, 10);
			}
		} else if ( substate == 2 ) {
			if( received_msg == "It resolves" ) { 
				websocket.send(order_list.value || "empty order");
				console.log("write complete");
				active_protocol = "inactive";
				substate = 999;
			} else if( received_msg == "File exists" ) {
				var name = prompt("File already exist, write the name again to overwrite the file on server", protocol_string);
				protocol_string = name;
				received_msg = "";
				websocket.send(protocol_string);
			} else if( received_msg == "Illegal chars" ) { 
				var name = prompt("Filename contained illegal chars", protocol_string);
				protocol_string = name;
				websocket.send(protocol_string);
				received_msg = "";
			}
		} 
	} else if(substate < 999){
		console.log("Stuck in " + active_protocol + "\n");
		setTimeout(TakeOrder, 1000);
	} else {
		substate = 0;
	}
}
