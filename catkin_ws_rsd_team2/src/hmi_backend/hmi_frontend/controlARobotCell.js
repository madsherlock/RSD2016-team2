//var auto_transition_tolerance = false;
//function disable_transition_tolerance()
//{
//	auto_transition_tolerance = true;
//}

var transition_complete = true;

function initialize_control_Arobot_cell()
{
	console.log("EH");
	if(not_connected)
	{
		connect('ws://'+callback_IP+':'+callback_port);
	}
	set_active_prototol("ControlARobotCell");
	document.getElementById("cellstatus").style.backgroundColor = "Green";
	update_HMI = false;
    slider_toggle(true);
    auto_pushed = false;
    //update_nav_mode('auto')
//	auto_transition_tolerance = false;
//	setTimeout(disable_transition_tolerance, 3000);
}

var ra_pos = [0,0,0,0,0,0,0];
function control_Arobot_cell() 
{
	console.log("active protocol: " + active_protocol + " substate: " + substate);
	
	if( active_protocol == "ControlARobotCell" && not_connected == false ) {
		if( substate == 0 ) 
		{
			console.log("sending the protocol msg: ControlARobotCell");
			websocket.send("ControlARobotCell");
			substate++;
		} 
		else if ( substate == 1 ) 
		{
			if( received_msg == "Ack ControlARobotCell" || received_msg == "Ack Stopping ControlMRobotCell" ) 
			{
				console.log("received the ack msg");
				protocol_string = "Enabled";
				substate++;
				update_rc_status_enable = true;
				update_rc_status();
				//mobile_robot_busy("ready");
			} 
			else if(received_msg == "RobotCell Disconnected")
			{
				active_protocol = "inactive";
				substate = 999;
			} 
			else if(received_msg == "RobotCell Busy")
			{
				substate--;
				console.log("RobotCell is busy");
				setTimeout(control_Arobot_cell, 10000);
			}
       } 
       else if( substate == 3 ) 
       {
			console.log("updating rc status values");
			var robotcell_status = received_msg.split("\n");
			ra_pos[0] = (robotcell_status[4].split(":")[1].split(";")[0]);
			ra_pos[1] = (robotcell_status[4].split(":")[1].split(";")[1]);
			ra_pos[2] = (robotcell_status[4].split(":")[1].split(";")[2]);
			ra_pos[3] = (robotcell_status[4].split(":")[1].split(";")[3]);
			ra_pos[4] = (robotcell_status[4].split(":")[1].split(";")[4]);
			ra_pos[5] = (robotcell_status[4].split(":")[1].split(";")[5]);
			//var ra_vel = robotcell_status[].split(":")[1];
			//var ra_acc = robotcell_status[].split(":")[1];
			var ra_grip = robotcell_status[5].split(":")[1];
			console.log("gripper msg: ", ra_grip);
			var cellstatus = robotcell_status[6].split(":")[1];
			//var robotcell_busy = robotcell_status[].split(":")[1];
			var conveyor_speed = robotcell_status[1].split(":")[1];
			var conveyor_status = robotcell_status[2].split(":")[1];
			var conveyor_dir = robotcell_status[3].split(":")[1];
			var rc_nav_mode = robotcell_status[0].split(":")[1];
			var emergency_stop_pressed = (robotcell_status[7].split(":")[1] == "OFF") ? false : true;
			update_emergency_stop(emergency_stop_pressed);
			//robotcell_busy(robot_busy);
			update_robotcell( ra_pos, ra_grip, conveyor_speed, conveyor_status, conveyor_dir, rc_nav_mode, cellstatus);
			if(auto_transition_tolerance)
			{
				update_nav_mode(rc_nav_mode);
			}
			transition_complete = true;
			websocket.send("Received current status");
			if(manual_pushed == true)
			{
				update_nav_mode("manual");
			}
			substate = 2;			
		}
	} 
	else if(substate < 999)
	{
		console.log("stuck in " + active_protocol + "\n");
	} 
	else 
	{
		substate = 0;
	}		
}

function stop_auto_robotcell()
{	
	console.log(active_protocol," ",substate, "  call close! in auto")
	
	if(active_protocol == "ControlARobotCell" && substate == 2 )
	{
		console.log("Inside stopA-1");
		console.log("sending: Stop ControlARobotcell");
		websocket.send("Stop ControlARobotcell");
		update_rc_status_enable = false;
	}
		if(active_protocol == "ControlARobotCell" && substate == 3 )
	{
		console.log("Inside stopA-2");
		console.log("sending: Stop ControlARobotcell");
		websocket.send("Stop ControlARobotcell");
		update_rc_status_enable = false;
	}
}	

