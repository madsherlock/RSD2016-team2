function initialize_control_Mrobot_cell(){
	if(not_connected)
	{
		connect('ws://'+callback_IP+':'+callback_port);
	}
	set_active_prototol("ControlMRobotCell");
	document.getElementById("cellstatus").style.backgroundColor = "Red";
    document.getElementById("cellstatus").value = "Status: Manual";
	update_HMI = true;
	manual_pushed = false;
	slider_toggle(false);
}

function control_Mrobot_cell() {
	console.log("active protocol: " + active_protocol + " substate: " + substate +  " connected: " + not_connected);
	if( active_protocol == "ControlMRobotCell" && not_connected == false ) {
		if( substate == 0 ) 
		{
			console.log("sending the protocol msg: ControlMRobotCell ");
			websocket.send("ControlMRobotCell");
			substate++;
		} 
		else if ( substate == 1 ) 
		{
			if( received_msg == "Ack ControlMRobotcell" ) 
			{
				console.log("received the ack msg");
				protocol_string = "Enabled";
				substate++;
				//mobile_robot_busy("ready");
			}
			else if(received_msg == "RobotCell Disconnected")
			{
				active_protocol = "inactive";
				substate = 999;
			}
			else if(received_msg == "RobotCell Busy")
			{
				substate--;
				console.log("RobotCell is busy");
				setTimeout(MoveRobot, 10000);
			}
		}
		else if (substate == 3)
		{    
			console.log("Received current status from backend - sending ack to backend");
			websocket.send("Received current status - In Manual-mode");
			transition_complete = true;
			if(auto_pushed == true)
			{
				update_nav_mode("unknown");
			}
			substate = 2;
		}
	}
}

function move_conveyor(command)
{
	console.log("move_conveyor " + command);
	if(active_protocol == "ControlMRobotCell" && substate == 2)
	{
		transition_complete = false;
		websocket.send(command);
		substate++;
		console.log("sending: move conveyor belt " + command);
	}
	else 
	{	
		console.log("What Am i doing here?");
		update_nav_mode("manual");
    }
}

function set_gripper(command)
{
	if(active_protocol == "ControlMRobotCell" && substate == 2)
	{
		transition_complete = false;
		websocket.send(command);
		substate++;
		console.log("sending: setting gripper " + command);
    }
    else {
		update_nav_mode("manual");
    }
}

function set_robot_arm(command)
{
	if(active_protocol == "ControlMRobotCell" && substate == 2)
	{
		transition_complete = false;
		websocket.send("Set_Arm:" + command);
		substate++;
		console.log("sending: setting robot arm " + command);
    }
    else 
    {
		update_nav_mode("manual");
	}
}

function stop_manual_robotcell()
{
	console.log(active_protocol," ",substate, "  call close! in manual")
	if(active_protocol == "ControlMRobotCell" && substate == 2)
	{
		console.log("Inside the stupid function");
		console.log("sending: Stop ControlMRobotcell");
		websocket.send("Stop ControlMRobotcell");
		update_rc_status_enable = false;
	}
}	
