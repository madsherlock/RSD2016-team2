var robot_is_stopped = false;

function update_emergency_stop(enable){
	if(!robot_is_stopped && enable){
		robot_is_stopped = enable;
		toggle_emergency_state();
	} else {
		robot_is_stopped = enable;
	}
}
// function press_stop(enable){
// 	update_emergency_stop(enable); //TODO: remove when ros is ready
// }
function toggle_emergency_state(){
	if(robot_is_stopped){
		set_emergency_state("toggle");
		setTimeout(toggle_emergency_state, 1/1.4*500, robot_is_stopped); //1.4 flashing Hz
	} else {
		set_emergency_state("off");
	}
}

var emergency_status_toggle = false; //true means on, false means off
function set_emergency_state(state){
	if(state == "off"){
		emergency_status_toggle = false;
	} else {
		emergency_status_toggle = !emergency_status_toggle;
	}
	emergency_col_red = "#BC0003";
	emergency_col_normal = "#638EF1";
	if(emergency_status_toggle){
		document.getElementById("emergency_status").innerHTML    = "<h1>EMERGENCY STOP ENGAGED </h1>";
		document.getElementById("content").style.borderLeft      = "solid " + emergency_col_red + " 20px"; 
		document.getElementById("content").style.borderBottom    = "solid " + emergency_col_red + " 20px"; 
		document.getElementById("content").style.borderTop       = "solid " + emergency_col_red + " 20px";
		document.getElementById("categories").style.borderRight  = "solid " + emergency_col_red + " 10px";
		document.getElementById("categories").style.borderBottom = "solid " + emergency_col_red + "  5px";
		document.getElementById("categories").style.borderTop    = "solid " + emergency_col_red + " 10px";
		document.getElementById("nav_mode").style.borderRight  = "solid " + emergency_col_red + " 10px";
		document.getElementById("nav_mode").style.borderBottom = "solid " + emergency_col_red + "  5px";
		document.getElementById("nav_mode").style.borderTop    = "solid " + emergency_col_red + " 10px";
	} else {
		if(state == "off"){
			document.getElementById("emergency_status").innerHTML = "";
		} else {
			document.getElementById("emergency_status").innerHTML = "<h1>EMERGENCY STOP ENGAGED </h1>";
		}
		document.getElementById("content").style.borderLeft      = "solid " + emergency_col_normal + " 20px"; 
		document.getElementById("content").style.borderBottom    = "solid " + emergency_col_normal + " 20px"; 
		document.getElementById("content").style.borderTop       = "solid " + emergency_col_normal + " 20px"; 
		document.getElementById("categories").style.borderRight  = "solid " + emergency_col_normal + " 10px"; 
		document.getElementById("categories").style.borderBottom = "solid " + emergency_col_normal + "  5px"; 
		document.getElementById("categories").style.borderTop    = "solid " + emergency_col_normal + " 10px"; 
		document.getElementById("nav_mode").style.borderRight  = "solid " + emergency_col_normal + " 10px"; 
		document.getElementById("nav_mode").style.borderBottom = "solid " + emergency_col_normal + "  5px"; 
		document.getElementById("nav_mode").style.borderTop    = "solid " + emergency_col_normal + " 10px"; 
	}
}
