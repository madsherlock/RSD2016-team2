//WebSocket connection
function connect(websocketServerLocation) {
	if (typeof MozWebSocket != "undefined") {
		websocket = new MozWebSocket(websocketServerLocation,"simple-protocol");
	} else {
		websocket = new WebSocket(websocketServerLocation,"simple-protocol");
	}
	websocket.onopen = function () {
		not_connected = false;
	};

	websocket.onmessage = function (evt) {
		received_msg = evt.data;
		console.log("Message received = " + received_msg);
		dispatcher();
	};
	websocket.onclose = function () {
		not_connected = true;
		//try to reconnect in 10 seconds
		setTimeout(function(){
			connect(websocketServerLocation)
		}, 10000);
	};
};

function set_active_prototol(new_protocol) {
	if( active_protocol != new_protocol ) {
		setTimeout(function(){
			if( active_protocol == "inactive" ) {
				console.log("new protocol: " + new_protocol);
				active_protocol = new_protocol;
				substate = 0;
				dispatcher();
			} else {
				set_active_prototol(new_protocol);
			}
		}, 1000);
	}
}

function dispatcher(){ 
	if( active_protocol == "TakeOrder") 
	{
		TakeOrder();
	} 
	else if( active_protocol == "MoveRobot" ) 
	{
		MoveRobot();
	}
	else if( active_protocol == "AutoRobot" ) 
	{
		AutoRobot();
	} 
	else if( active_protocol == "ControlMRobotCell" ) 
	{
		control_Mrobot_cell();
	}
	else if( active_protocol == "ControlARobotCell" ) 
	{
		control_Arobot_cell();
	}
	else if (active_protocol == "oeeControl" )
	{
		oee_control();
	}
}

var update_mr_status_enable = false;
function update_mr_status()
{
	if( (active_protocol == "AutoRobot" || active_protocol == "MoveRobot") && substate == 2 && update_mr_status_enable) 
	{
		websocket.send("Nowhere"); //Tell the robot there is no change, so we get a new message
		move_robot_on_map();
		substate++;
	}
	if(update_mr_status_enable) 
	{
		setTimeout(update_mr_status, 250);
	}
}

var update_rc_status_enable = false;
function update_rc_status()
{
	if( (active_protocol == "ControlARobotCell" || active_protocol == "ControlMRobotCell") && substate == 2 && update_rc_status_enable) 
	{
		transition_complete = false;
		websocket.send("Requesting current status"); //Tell the robot there is no change, so we get a new message
		substate++;
	}
	if(update_rc_status_enable) 
	{
		setTimeout(update_rc_status, 250);
	}
}

var update_oee_status_enable = false;
function update_oee_status()
{
	if( (active_protocol == "oeeControl") && substate == 2 && update_oee_status_enable) 
	{
		transition_complete = false;
		websocket.send("Requesting current status"); //Tell the backend there is no change, so we get a new message
		substate++;
	}
	if(update_oee_status_enable) 
	{
		setTimeout(update_oee_status, 250);
	}
}

function stop_all_protocols()
{
	console.log("Stop protocol called");
	stop_move_robot();
	stop_auto_robot();
	stop_manual_robotcell();
	stop_auto_robotcell();
	stop_oee();
	active_protocol = "inactive";
	substate = 999;
}
