var ring_button;
var color_ring_btn_green = "#00E600";
var color_ring_btn_red   = "#E00000";

//These offsets are how far RIGHT and how far DOWN you have to go in the map image to find the origin.
//This is why they are NEGATIVE.
//var mr_offset_y = -140;
//var mr_offset_x = -300;
var original_map_width = 978;
var original_map_height = 888;
var desired_img_height  = 525;
var map_offset_x = 374;
var map_offset_y = 377;
var mr_offset_x = -(original_map_width-374); //978 is width of image. 374 is origin from LEFT.
var mr_offset_y = -(original_map_height-377); //888 is height of image. 377 is origin from TOP.
//scaled img is 525x477
var map_scale_factor = desired_img_height/original_map_height;
map_offset_x = 220;
map_offset_y = 223;
mr_offset_x = -(Math.round(original_map_height*map_scale_factor)-160);//new img pts(220 from left, find out why )
mr_offset_y = -(Math.round(original_map_height*map_scale_factor)-map_offset_y);//new img pts

var mr_img_orig_width = 341;
var mr_img_orig_height = 206;
var mr_img_height = 48*map_scale_factor;
var mr_img_width = Math.round((mr_img_height*mr_img_orig_width)/mr_img_orig_height);


var mr_meters_to_pixels = 100/map_scale_factor;
var mr_meters_to_pixels_x = mr_meters_to_pixels;
var mr_meters_to_pixels_y = mr_meters_to_pixels;

// read the image dimensions
// "bars" are the status indicators for speed, battery percentage, etc.
var bar_dim_x = 107;
var bar_dim_y = 136; //136 for 5, 261 for 10

function menu_robot(){
	stop_all_protocols();
	update_active_menu("menu_mobile_robot");
	var content = ' '+
	'     <div class="progress_bars_wrapper">                                                                                                                                                                '+
	'       <div class="progress_bar_wrapper" style="position: absolute;">                                                                                                                                                                '+
	'           <div class="progress_bar" id="bar_a_background" style="height: ' + bar_dim_y + 'px; width: ' + bar_dim_x + 'px; position: absolute; z-index=1; background: linear-gradient(blue, black); border-radius: 10px 10px 10px 10px;"> </div><!-- '+
	'        --><div class="progress_bar" id="bar_a_hidden" style="height: '+bar_dim_y+'px; width:' + bar_dim_x + 'px; position: absolute;z-index=2; bottom: '+(-bar_dim_y)+'px; background: black; border-radius: 11px 11px 11px 11px;"> </div><!-- '+
	'        --><img class="progress_bar" id="bar_a_frame" src="scale_5_slim.png" style="position:absolute; z-index=3; filter: hue-rotate(20deg); border-radius: 10px 10px 10px 10px;"/>                                                                                                                '+
	'       </div> '+
	'       <div class="progress_bar_wrapper" style="position: absolute; margin-left:'+(bar_dim_x+25)+'px;">                                                                                                                                                                '+
	'           <div class="progress_bar" id="bar_b_background" style="height: ' + bar_dim_y + 'px; width: ' + bar_dim_x + 'px; position: absolute; z-index=1; background: linear-gradient(blue, black); border-radius: 10px 10px 10px 10px;"> </div><!-- '+
	'        --><div class="progress_bar" id="bar_b_hidden" style="height: '+bar_dim_y+'px; width:' + bar_dim_x + 'px; position: absolute; z-index=2; bottom: '+(-bar_dim_y)+'px; background: black; border-radius: 11px 11px 11px 11px;"> </div><!-- '+
	'        --><img class="progress_bar" id="bar_b_frame" src="scale_5_slim.png" style="position:absolute; z-index=3; filter: hue-rotate(220deg); border-radius: 10px 10px 10px 10px;"/>                                                                                                                '+
	'       </div>                                                                                                                                                                                             '+
	'       <div class="progress_bar_wrapper" style="position: absolute; margin-left:'+2*(bar_dim_x+25)+'px;">                                                                                                                                                                '+
	'           <div class="progress_bar" id="bar_c_background" style="height: ' + bar_dim_y + 'px; width: ' + bar_dim_x + 'px; position: absolute; z-index=1; background: linear-gradient(blue, black); border-radius: 10px 10px 10px 10px;"> </div><!-- '+
	'        --><div class="progress_bar" id="bar_c_hidden" style="height: '+bar_dim_y+'px; width:' + bar_dim_x + 'px; position: absolute; z-index=2; bottom: '+(-bar_dim_y)+'px; background: black; border-radius: 11px 11px 11px 11px;"> </div><!-- '+
	'        --><img class="progress_bar" id="bar_c_frame" src="scale_5_slim.png" style="position:absolute; z-index=3; filter: hue-rotate(130deg); border-radius: 10px 10px 10px 10px;"/>                                                                                                                '+
	'       </div>                                                                                                                                                                                             '+
	'     </div>                                                                                                                                                                                             '+
	'       <div class="progress_bar_wrapper" style="position: absolute; margin-left:'+3*(bar_dim_x+25)+'px;">                                                                                                                                                                '+
	'           <div class="progress_bar" id="bar_d_background" style="height: ' + bar_dim_y + 'px; width: ' + bar_dim_x + 'px; position: absolute; z-index=1; background: linear-gradient(blue, black); border-radius: 10px 10px 10px 10px;"> </div><!-- '+
	'        --><div class="progress_bar" id="bar_d_hidden" style="height: '+bar_dim_y+'px; width:' + bar_dim_x + 'px; position: absolute; z-index=2; bottom: '+(-bar_dim_y)+'px; background: black; border-radius: 11px 11px 11px 11px;"> </div><!-- '+
	'        --><img class="progress_bar" id="bar_d_frame" src="scale_5_slim.png" style="position:absolute; z-index=3; filter: hue-rotate(70deg); border-radius: 10px 10px 10px 10px;"/>                                                                                                                '+
	'       </div>                                                                                                                                                                                             '+
	'     </div>                                                                                                                                                                                             '+
	'     <div id="labels" style="position:absolute; top: 200px; left: 275px;"> <p>Linear</p> </div>                                                                                                                                                                                             '+
	'     <div id="labels" style="position:absolute; top: 200px; left: 400px;"> <p>Angular</p> </div>                                                                                                                                                                                             '+
	'     <div id="labels" style="position:absolute; top: 200px; left: 540px;"> <p>Tipper</p> </div>                                                                                                                                                                                             '+
	'     <div id="labels" style="position:absolute; top: 200px; left: 670px;"> <p>Battery</p> </div>                                                                                                                                                                                             '+
	'     <div id="labels" style="position:absolute; top: 120px; left: 650px;"> <p id="battery-pct" style="width: 80px; text-align:right;">0%</p> </div>                                                                                                                                                                                             '+
	'     <div id="navigation" style="min-width: 521px; position: absolute; top: 300px; left: 305px;">                                                                                                                                                    '+
	'       <input class="circle circUL" type="button" onclick="move_robot(\'forward left\')">                                                                                                               '+
	'       <input class="nav up" type="button"  onclick="move_robot(\'forward\')">                                                                                                                          '+
	'       <input class="outercircle circUR ringBTN" style="position: relative; left:4px; bottom:-05px; border-radius: 0 130px 0 0; " type="button" onclick="stop_move_robot()" value="          S\n                 T\n                      O\n                            P">                              '+
	'       <input class="outercircle circUR" style="position: relative; right: 140px;z-index=2; bottom:7px; background-color: black; border-radius: 0 105px 0 0; width:105px; height:105px;" type="button"> '+ 
	'       <input class="circle circUR" style="position: relative; right: 260px; z-index=3;" type="button" onclick="move_robot(\'forward right\')">                                                         '+                                                        
	'       <br/>                                                                                                                                                                                            '+
	'       <input class="nav left" type="button" onclick="move_robot(\'left\')"><!-- This comment removes a gap                                                                                             '+
	'    --><input class="nav right" type="button" onclick="move_robot(\'right\')">                                                                                                                          '+
	'       <br/>                                                                                                                                                                                            '+
	'       <input class="circle circBL" type="button" style="position: relative; top:-19px;" onclick="move_robot(\'reverse left\')">                                                                        '+
	'       <input class="nav down" type="button" style="position: relative; top:-20px;" onclick="move_robot(\'reverse\')">                                                                                  '+
	'       <input class="outercircle circUR angular" style="position: relative; left:4px; top:-4px; border-radius: 0 0 130px 0;" type="button" onclick="move_robot(\'tipper\')" value="                     Tip">                                                             '+
	'       <input class="outercircle circUR"  style="position: relative; right: 140px;z-index=2; top:-17px; background-color: black; border-radius: 0 0 105px 0; width:105px; height:105px;" type="button"> '+ 
	'       <input class="circle circBR text-left" type="button"  style="position: relative; top:-11px; right: 260px; z-index=3;" onclick="move_robot(\'reset_marker_pose\')" value="Reset\nPose">                                             '+
	'     </div>                                                                                                                                                                                             '+
	'     <div id="robot_position_container" style="position:absolute; left: 775px;min-width:'+original_map_width+'px;">                                                                                                                                                                '+
// 	'         <img src="map.png"          style="position: relative; top: 0; left: 0;"/>                                                                                                                     '+
	'         <img src="trumap2.png"   id="map"     ondragover="allowDrop(event)" ondrop="drop_marker(event)" onclick="point_it(event)"  ondragstart="return false;" style="z-index=-1;position: relative; top: 0; left: 0; '+'height:'+desired_img_height+'px; width:auto;     border-top-left-radius:  30px !important; border-bottom-left-radius: 30px !important;"/> '+
	'         <img src="mobile_robot2.png" id="mobile_robot_position" style="z-index=2; width: ' + Math.round(mr_img_width) + 'px; height: ' + Math.round(mr_img_height) + 'px; position: relative; top: ' + Math.round(mr_offset_y+mr_img_height/2) + 'px; left: ' + Math.round(mr_offset_x-mr_img_width/2) + 'px;"/>                      '+
// 	'         <p>You pointed on x = <input type="text" id="form_x" size="4" /> y = <input type="text" id="form_y" size="4" /> <input type="button" value="go" onclick="debug_pos()"></p>                     '+
	'     </div>                                                                                                                                                                                             ';
	document.getElementById("content").innerHTML = content;
	document.getElementById("content").style.height = "550px";
	ring_button = document.getElementsByClassName('ringBTN')[0];
	
	var mobile_robot_position = document.getElementById("mobile_robot_position");
	var cs = getComputedStyle(mobile_robot_position);
	console.log("Mobile robot position left: "+mobile_robot_position.style.left+". Height: "+cs.height+".");
	//mobile_robot_position.style.left=String(parseInt(mobile_robot_position.style.left)-(parseInt(mobile_robot_position.style.width)/2))+"px";
	initialize_MoveRobot();
	update_nav_mode("manual");
}

function debug_pos(){
	move_robot_on_map_x = document.getElementById("form_x").value;
	move_robot_on_map_y = document.getElementById("form_y").value;
	move_robot_on_map();
}

//click on image
function point_it(event){
	//get the pos on page, subtract the size of the image
	var pos_x = event.offsetX?(event.offsetX):event.pageX-document.getElementById("map").offsetRight;
	var pos_y = event.offsetY?(event.offsetY):event.pageY-document.getElementById("map").offsetBottom;
// 	(Math.round(mr_offset_x + move_robot_on_map_x*mr_meters_to_pixels_x -mr_img_width/2)) + "px";
	move_robot_on_map_x = (pos_x+mr_offset_x+3*mr_img_width)/mr_meters_to_pixels_x;
	move_robot_on_map_y = (-pos_y-mr_offset_y-2.7*mr_img_height)/mr_meters_to_pixels_y;
	move_robot("wpt: " + move_robot_on_map_x + ", " + move_robot_on_map_y + ", " + move_robot_on_map_yaw);
	//DEBUG
// 	document.getElementById("form_x").value = move_robot_on_map_x;
// 	document.getElementById("form_y").value = move_robot_on_map_y;
// 	move_robot_on_map();
}
//drag mobile robot
function drop_marker(event){
	event.preventDefault();
	var rect = document.getElementById("map").getBoundingClientRect();
	pos_x = event.clientX-rect.left;
	pos_y = event.clientY-rect.top;
// 	move_robot_on_map_x = pos_x;
// 	move_robot_on_map_y = pos_y;
	move_robot_on_map_x = (pos_x+mr_offset_x+3*mr_img_width)/mr_meters_to_pixels_x;
	move_robot_on_map_y = (-pos_y-mr_offset_y-2.7*mr_img_height)/mr_meters_to_pixels_y;
	move_robot("wpt: " + move_robot_on_map_x + ", " + move_robot_on_map_y + ", " + move_robot_on_map_yaw);
	//DEBUG
// 	document.getElementById("form_x").value = move_robot_on_map_x;
// 	document.getElementById("form_y").value = move_robot_on_map_y;
// 	move_robot_on_map();
}
//allow MR to be dragged onto map
function allowDrop(event) {
	event.preventDefault();
}


function adjust_bar(number,pct) {
	var h = Math.round((1-(Math.abs(pct) / 100)) * bar_dim_y);
	if(pct < 0) {
		document.getElementById("bar_"+number+"_background").style.background = "linear-gradient(red, black)";
	} else {
		document.getElementById("bar_"+number+"_background").style.background = "linear-gradient(green, black)";
	}
	if(number == 'd') {
		document.getElementById("battery-pct").innerHTML = Math.round(pct)+"%";
	}
	if(h < 0) {
		document.getElementById("bar_"+number+"_hidden").style.height = h+"px";
		document.getElementById("bar_"+number+"_hidden").style.background = "black";
		document.getElementById("bar_"+number+"_hidden").style.bottom = (-h)+"px";
	} else if(h > 0) {
		document.getElementById("bar_"+number+"_hidden").style.height = h+"px";
		document.getElementById("bar_"+number+"_hidden").style.background = "black";
		document.getElementById("bar_"+number+"_hidden").style.bottom = (-h)+"px";
	} else {
		document.getElementById("bar_"+number+"_hidden").style.maxHeight = 1;
		document.getElementById("bar_"+number+"_hidden").style.background = "none";
	}
}

var number_red    = 0;
var number_blue   = 0;
var number_yellow = 0;

function menu_order(){
		update_active_menu("menu_order");
		stop_all_protocols();
		update_nav_mode("unknown");
		var content = '<div id="controls">                                                                                                                 '+
		              '   <input class="button" type="button" style="background-color: red;" value="red brick"    onclick="add_order(\'red\')"> <br/>             '+
		              '   <input class="button" type="button" style="background-color: blue;" value="blue brick"   onclick="add_order(\'blue\')"> <br/>           '+
		              '   <input class="button" type="button" style="background-color: yellow;" value="yellow brick" onclick="add_order(\'yellow\')"> <br/>       '+
		              '   <input class="button" type="button" style="background-color: #d16767;" value="send order" onclick="initialize_TakeOrder()"> <br/>       '+
		              ' </div>                                                                                                                                    '+
		              ' <textarea readonly id="order_list">                                                                                                       '+
		              ' </textarea><br/>                                                                                                                          ';
	document.getElementById("content").innerHTML = content;
		refresh_order();
}

function add_order(col){
	
	var str = "Enter the number of bricks";
	
	if(col == "red"){
		number_red = prompt(str, number_red);
	} else if(col == "blue"){
		number_blue = prompt(str,number_blue);
	} else if(col == "yellow"){
		number_yellow = prompt(str,number_yellow);
	}
	refresh_order();
}

var mobile_robot_taken_status = "taken";
function mobile_robot_busy(status){
	if(status == "ready"){
		ring_button.style.backgroundColor = color_ring_btn_green; 
	} else {
		ring_button.style.backgroundColor = color_ring_btn_red; 
		mobile_robot_taken_status = "taken";
	}
	mobile_robot_taken_status = status;
}

var move_robot_on_map_x = 0;
var move_robot_on_map_y = 0;
var move_robot_on_map_yaw = 0;

function move_robot_on_map(){
	console.log("updating robot map to: " + move_robot_on_map_x + " , " + move_robot_on_map_y + " , " + move_robot_on_map_yaw);
	document.getElementById("mobile_robot_position").style.left = (Math.round(mr_offset_x + move_robot_on_map_x*mr_meters_to_pixels_x -mr_img_width/2)) + "px";
	document.getElementById("mobile_robot_position").style.top  = (Math.round(mr_offset_y - move_robot_on_map_y*mr_meters_to_pixels_y +mr_img_height/2)) + "px";
	document.getElementById("mobile_robot_position").style.transform="rotate(" + String(360-parseInt(move_robot_on_map_yaw))    + "deg)"; 
	document.getElementById("mobile_robot_position").style.visibility = "visible";
}

var manual_pushed = false;
var auto_pushed = false;
function update_nav_mode(active)
{	
	console.log("Updating nav mode: " + active_protocol + " and active: " + active);
	
	if( active_protocol == "AutoRobot" && active == "manual")
	{
		stop_all_protocols();
		active_protocol = "inactive";
		substate = 999;
		initialize_MoveRobot();
	} 
	else if( active_protocol == "MoveRobot" && active != "manual")
	{
		stop_all_protocols();
		console.log("stopping robot and going to " + active);
		if(active != "unknown")
		{
			initialize_AutoRobot();
		}	 		
	}
	else if ( active_protocol == "ControlARobotCell" && active == "manual" && transition_complete == true)
	{
		//Stop_current_protocol
		stop_all_protocols();
		console.log("I called it!");
		//Change substate
		active_protocol = "inactive";
		substate = 999;
		initialize_control_Mrobot_cell();
	}
	else if( active_protocol == "ControlMRobotCell" && active != "manual" && transition_complete == true)
	{
		console.log("You want to go back to auto inside update_nav_mode");
		stop_all_protocols();
		console.log("stopping robot and going to " + active);
		if(active != "unknown")
		{
			initialize_control_Arobot_cell();
		}	 		
	}
	
	if(active_protocol == "ControlARobotCell" && active == "manual" && transition_complete == false)
	{
		manual_pushed = true;
	}

	if(active_protocol == "ControlMRobotCell" && active != "manual" && transition_complete == false)
	{
	    console.log("auto_pushed set!");
		auto_pushed = true;	
	}
	var visited = false;
	for(var i = 0; i < nav_modes.length; i++){
		if(active == nav_modes[i]) {
			visited = true;
			document.getElementById("nav_mode_" + nav_modes[i]).style.borderTopRightRadius = 30+"px";
			document.getElementById("nav_mode_" + nav_modes[i]).style.borderBottomRightRadius = 30+"px";
			document.getElementById("nav_mode_" + nav_modes[i]).style.minWidth = 130+"px";
		} else {
			document.getElementById("nav_mode_" + nav_modes[i]).style.borderTopRightRadius = "0px";
			document.getElementById("nav_mode_" + nav_modes[i]).style.borderBottomRightRadius = "0px";
			document.getElementById("nav_mode_" + nav_modes[i]).style.minWidth = 110+"px";
		}
	}
	if(!visited){
		for(var i = 0; i < hidden_nav_modes.length; i += 2){
			if(active == hidden_nav_modes[i]) {
				document.getElementById("nav_mode_" + hidden_nav_modes[i+1]).style.borderTopRightRadius = 30+"px";
				document.getElementById("nav_mode_" + hidden_nav_modes[i+1]).style.borderBottomRightRadius = 30+"px";
				document.getElementById("nav_mode_" + hidden_nav_modes[i+1]).style.minWidth = 130+"px";
			}
		}
	}
}
function update_active_menu(active){
	for(var i = 0; i < menu_options.length; i++){ 
		if(active == menu_options[i] ) {
			document.getElementById(menu_options[i]).style.borderTopRightRadius = 30+"px";
			document.getElementById(menu_options[i]).style.borderBottomRightRadius = 30+"px";
			document.getElementById(menu_options[i]).style.minWidth = 130+"px";
		} else {
			document.getElementById(menu_options[i]).style.borderTopRightRadius = "0px";
			document.getElementById(menu_options[i]).style.borderBottomRightRadius = "0px";
			document.getElementById(menu_options[i]).style.minWidth = 110+"px";
		}
	}
}

function refresh_order(){
	var date = new Date();
  var time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
	var this_order ='<?xml version="1.0" encoding="utf-8"?>\n' +
	'<order>\n' +
	'    <time>' + time + '</time>\n' + 
	'    <bricks>\n' +
	'        <red>'    + number_red    + '</red>\n'  +
	'        <blue>'   + number_blue   + '</blue>\n' +
	'        <yellow>' + number_yellow + '</yellow>\n' +
	'    </bricks>\n' +
	'    <status>' + mobile_robot_taken_status + '</status>\n'+
	'</order>';
	order_list.value = this_order;
}
