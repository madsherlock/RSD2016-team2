var update_values = false;
var brickpos = 150;
var q_robot_arm = [0,0,0,0,0,0];
var update_HMI = false;

function menu_robotcell()
{
	console.log("Call auto again");
	update_nav_mode("unknown");
	if(auto_pushed == false)
	{
	stop_all_protocols();
	update_active_menu("menu_robotcell");
	var content = '' +
		' '+
		'<div id="Q_sliders">                                                                                                                   '+
		'    <input class="button" type="button" style="background-color: red;"  value="Set Q"  onclick="set_pos()">  <br/>                 '+ 
		'    <br/><p></p>                                                                                        '+
		'    <div>                                                                                                                   '+
		'        <div id="q6_value" style="float:left; min-width:30px; text-align: right; color:white;">0.2</div>                             '+
		'        <div style="float:left"> <input type="range" id="q6_slider" min ="'+(-350*(Math.PI/180.0))+'" max="'+(350*(Math.PI/180.0))+'" step ="0.1" value="0.2" onchange="slider_onchange(\'q6\')" style="z-index: 3;"></div>'+
		'        <div id="title" style="float:left; color:white;">q6</div>                                                                     '+
		'    </div>'+
		'    <br/><p></p>                                                                                        '+
		'    <div>                                                                                                                   '+
		'        <div id="q5_value" style="float:left; min-width:30px; text-align: right; color:white;">0.2</div>                             '+
		'        <div style="float:left"> <input type="range" id="q5_slider" min ="'+(-210*(Math.PI/180.0))+'" max="'+(25*(Math.PI/180.0))+'" step ="0.1" value="0.2" onchange="slider_onchange(\'q5\')" style="z-index: 3;"></div>'+
		'        <div id="title" style="float:left; color:white;">q5</div>                                                                     '+
		'    </div>'+ 
		'    <br/><p></p>                                                                                        '+
		'    <div>                                                                                                                   '+
		'        <div id="q4_value" style="float:left; min-width:30px; text-align: right; color:white;">0.2</div>                             '+
		'        <div style="float:left"> <input type="range" id="q4_slider" min ="'+(-185*(Math.PI/180.0))+'" max="'+(185*(Math.PI/180.0))+'" step ="0.1" value="0.2" onchange="slider_onchange(\'q4\')" style="z-index: 3;"></div>'+
		'        <div id="title" style="float:left; color:white;">q4</div>                                                                     '+
		'    </div>'+ 
		'    <br/><p></p>                                                                                        '+
		'    <div>                                                                                                                   '+
		'        <div id="q3_value" style="float:left; min-width:30px; text-align: right; color:white;">0.2</div>                             '+
		'        <div style="float:left"> <input type="range" id="q3_slider" min ="'+(-200*(Math.PI/180.0))+'" max="'+(60*(Math.PI/180.0))+'" step ="0.1" value="0.2" onchange="slider_onchange(\'q3\')" style="z-index: 3;"></div>'+
		'        <div id="title" style="float:left; color:white;">q3</div>                                                                     '+
		'    </div>'+
		'    <br/><p></p>                                                                                        '+
		'    <div>                                                                                                                   '+
		'        <div id="q2_value" style="float:left; min-width:30px; text-align: right; color:white;">0.2</div>                             '+
		'        <div style="float:left"> <input type="range" id="q2_slider" min ="'+(-100*(Math.PI/180.0))+'" max="'+(135*(Math.PI/180.0))+'" step ="0.1" value="0.2" onchange="slider_onchange(\'q2\')" style="z-index: 3;"></div>'+
		'        <div id="title" style="float:left; color:white;">q2</div>                                                                     '+
		'    </div>'+ 
		'    <br/><p></p>                                                                                        '+
		'    <div>                                                                                                                   '+
		'        <div id="q1_value" style="float:left; min-width:30px; text-align: right; color:white;">0.2</div>                             '+
		'        <div style="float:left"> <input type="range" id="q1_slider" min ="'+(-170*(Math.PI/180.0))+'" max="'+(170*(Math.PI/180.0))+'" step ="0.1" value="0.2" onchange="slider_onchange(\'q1\')" style="z-index: 3;"></div>'+
		'        <div id="title" style="float:left; color:white;">q1</div>                                                                     '+
		'    </div>';
content = content +  
    '     <div id="robotcell_position_container" style="position:absolute; top: 60px; left: 450px;min-width:335px;">                                                                                                                                                                '+
    '         <img src="kuka_gripper_open.png" id="KUKAimg" style="position: absolute; z-index: -1;"/>                      '+
    '     </div>			'+
    '     <input class="button" type="button" id="gripperbutton" style="background-color:  yellow; position:absolute; top: 420px; left: 260px;"  value="Gripper open"  onclick="set_Gripper()"><br/>		'+
	'     <input class="cellstate" id="cellstatus" style="background-color:  red; position:absolute; top: 470px; left: 260px;"  value="Status: "  disabled><br/>		'+

    '	  <div id="conveyor_position_container" style="position:absolute; top: 120px; left: 850px;min-width:200px;">                                                                                                                                                                '+
    '         <img src="conveyor.png" style="position: absolute;"/>   '+
    ' 	  <div id ="conveyoranimate" style="position: relative; top: ' + brickpos + 'px; left: 38px;"></div>	'+


    '     <input class="nav up" type="button" style="position: relative; top:-10px; left: 100px;" onclick="ForwardConveyor()" >                                                                                                                          '+
    '     <input class="con" type="button" id="conveyorbutton" style="position:relative; top: 61px; left: 45px;"  value="Stop Belt"  onclick="set_conveyor()"><br/>		'+
    '     <input class="nav down" type="button" style="position: relative; top:35px; left: 100px;" onclick="BackwardConveyor()" >                                                                                  ';
	content = content + '</div>';
	console.log("q robot arm: ", q_robot_arm)
	document.getElementById("content").innerHTML = content;
	initialize_control_Arobot_cell();
	}
}


function set_pos(){
	if (update_HMI) {
		var str = "Enter Q, seperate values with ','";
		var Q_in = "";
		Q_in = prompt(str,Q_in);
		q_robot_arm = Q_in.split(",");
		for(var i = 1; i <= q_robot_arm.length; i++){
			set_slider("q"+i,q_robot_arm[i - 1]);
		}
		if (active_protocol == "ControlMRobotCell") {
			set_robot_arm(q_robot_arm);
		}
	}
}

function set_slider(id, val){
    var slider_id = id + "_slider";
    var value_id = id + "_value";
    document.getElementById(slider_id).value = val;
    document.getElementById(value_id).innerHTML = Number(val).toFixed(1);
}

function update_slider(id){
	if (active_protocol == "ControlMRobotCell") {
		var slider_id = id + "_slider";
		var value_id = id + "_value";
		var x = document.getElementById(slider_id).value;
		document.getElementById(value_id).innerHTML = Number(x).toFixed(1);
		console.log("update slider: ", id, " ", x, " ", id[1], " ", q_robot_arm); 
		q_robot_arm[id[1]-1] = x;
		set_robot_arm(q_robot_arm);
		if(update_values)
		{
			setTimeout(update_slider,100,id);
		}
	}
}

//function slider_onmousedown(q){
//	update_values = true;  
//	update_slider(q);
//}
function slider_onchange(q){
	update_values = false; 
	update_slider(q);
}

var gripper_open = true;
function set_Gripper(){
	if (update_HMI) {
		if (gripper_open) {
			if (active_protocol == "ControlMRobotCell") {
				set_gripper("closeGripper");
			}
			document.getElementById("KUKAimg").src = "kuka_gripper_closed.png";
			document.getElementById("gripperbutton").value = "Gripper close";
			gripper_open = false;
		} else {
			if (active_protocol == "ControlMRobotCell") {
				set_gripper("openGripper");
			}
			document.getElementById("KUKAimg").src = "kuka_gripper_open.png";
			document.getElementById("gripperbutton").value = "Gripper open";
			gripper_open = true;
		}
	}
}

var forwardclick = 0;
var backwardclick = 0;
var conveyorActive = false;
var conveyorinterval;

function set_conveyor() {
	if (update_HMI) {
		if (conveyorActive) {
			if (active_protocol == "ControlMRobotCell") {
				move_conveyor("Deactive");
			}
			conveyorActive = !conveyorActive;
			clearInterval(conveyorinterval);
			document.getElementById("conveyorbutton").value = "Deactive";
			document.getElementById("conveyorbutton").style.backgroundColor = "Red";
			forwardclick = 0;
			backwardclick = 0;
		} else {
			if (active_protocol == "ControlMRobotCell") {
				move_conveyor("speed0");
			}
			conveyorActive = !conveyorActive;
			document.getElementById("conveyorbutton").value = "Active";
			document.getElementById("conveyorbutton").style.backgroundColor = "Green";
		}
	}
}


function ForwardConveyor() {
	if (update_HMI) {
		var conelemF = document.getElementById("conveyoranimate");
		if (conveyorActive && backwardclick == 0) {
			if (forwardclick == 0) {
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speedF1");
				}
				forwardclick = 1;
				clearInterval(conveyorinterval);
				conveyorinterval = setInterval(frame, 35);
			} else if (forwardclick == 1){
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speedF2");
				}
				forwardclick = 2;
				clearInterval(conveyorinterval);
				conveyorinterval = setInterval(frame, 10);
			} else if (forwardclick == 2){
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speedF3");
				}
				forwardclick = 3;
				clearInterval(conveyorinterval);
				conveyorinterval = setInterval(frame, 1);
			}
		} else if (conveyorActive && backwardclick != 0) {
			if (backwardclick == 1) {
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speed0");
				}
				backwardclick = 0;
				clearInterval(conveyorinterval);
			} else {
				backwardclick = backwardclick - 2;
				BackwardConveyor();
			}
		}
		function frame() {
			if (forwardclick == 1 || forwardclick == 2 || forwardclick == 3 ) {
				if (brickpos <= 0) {
					brickpos = 255;
					conelemF.style.top = brickpos;
				} else {
					brickpos = brickpos - 1; 
					conelemF.style.top = brickpos + 'px';
				}
			}
		}
		console.log(forwardclick, backwardclick);
	}
}

function BackwardConveyor() {
	if (update_HMI) {
		var conelemB = document.getElementById("conveyoranimate");
		if (conveyorActive && forwardclick == 0) {
			if (backwardclick == 0) {
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speedB1");
				}
				backwardclick = 1;
				clearInterval(conveyorinterval);
				conveyorinterval = setInterval(frame, 35);
			} else if (backwardclick == 1){
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speedB2");
				}
				backwardclick = 2;
				clearInterval(conveyorinterval);
				conveyorinterval = setInterval(frame, 10);
			} else if (backwardclick == 2){
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speedB3");
				}
				backwardclick = 3;
				clearInterval(conveyorinterval);
				conveyorinterval = setInterval(frame, 1);
			}
		} else if (conveyorActive && forwardclick != 0) {
			if (forwardclick == 1) {
				if (active_protocol == "ControlMRobotCell") {
					move_conveyor("speed0");
				}
				forwardclick = 0;
				clearInterval(conveyorinterval);
			} else {
				forwardclick = forwardclick - 2;
				ForwardConveyor();
			}
		}
		function frame() {
			if (backwardclick == 1 || backwardclick == 2 || backwardclick == 3 ) {
				if (brickpos >= 255) {
					brickpos = 0;
					conelemB.style.top = brickpos;
				} else {
					brickpos = brickpos + 1; 
					conelemB.style.top = brickpos + 'px';
				}
			}
		}
		console.log(forwardclick, backwardclick);
	}
}
function update_robotcell(ra_pos, ra_grip, conveyor_speed, conveyor_status, conveyor_dir, rc_nav_mode, cellstatus ) {
	update_HMI = true;
	//cellstatus
	document.getElementById("cellstatus").value = cellstatus;
	
	//ra_pos
 	for(var i = 1; i <= 6; i++)
	{
	    console.log("q"+i,ra_pos[i-1]);
	    q_robot_arm[i-1] = ra_pos[i-1];
	    set_slider("q"+i,ra_pos[i-1]);
	}
    //ra_vel
    
    //ra_grip
	if (ra_grip == 1) {
	    document.getElementById("KUKAimg").src = "kuka_gripper_open.png";
	    document.getElementById("gripperbutton").value = "Gripper open";
	    gripper_open = true;
	} else if (ra_grip == 0) {	    
	    document.getElementById("KUKAimg").src = "kuka_gripper_closed.png";
	    document.getElementById("gripperbutton").value = "Gripper close";
	    gripper_open = false;
	}
	
	//robotcell_busy
	
	//conveyor_status
	//conveyor_dir
	//conveyor_speed
	
	console.log("Am I here?"," ", conveyor_status); 
 	if (conveyor_status == "ON") 
 	{
 		if (conveyor_speed != "NONE") 
 		{
 			if(conveyor_dir == "FORWARD") 
 			{
 				if (conveyor_speed == "SLOW") 
 				{
 					forwardclick = 0;
 					ForwardConveyor();
 				} 
 				else if (conveyor_speed == "MEDIUM") 
 				{
 					forwardclick = 1;
 					ForwardConveyor();
 				} 
 				else if (conveyor_speed == "FAST") 
 				{
 					forwardclick = 2;
 					ForwardConveyor();
 				}
 			} 
 			else if (conveyor_dir == "BACKWARDS") 
 			{
 				if (conveyor_speed == "SLOW") 
 				{
 					backwardclick = 0;
 					BackwardConveyor();
 				} 
				else if (conveyor_speed == "MEDIUM") 
				{
 					backwardclick = 1;
 					BackwardConveyor();
 				} 
 				else if (conveyor_speed == "FAST") 
 				{
 					backwardclick = 2;
 					BackwardConveyor();
 				}
 			}
 		}
 		conveyorActive = false;
 		set_conveyor();
 	} 
 	else if (conveyor_status == "OFF") 
 	{
 		conveyorActive = true;
 		set_conveyor();
 	}
	
	//rc_nav_mode Is component taken or not. 
	update_HMI = false;
}

function slider_toggle(ED_slider_input)
{
	if (ED_slider_input) {
		for(var i = 1; i <= 6; i++){
			document.getElementById("q"+i+"_slider").disabled = true;
		}
	} else if (!ED_slider_input) {
		for(var i = 1; i <= 6; i++){
			document.getElementById("q"+i+"_slider").disabled = false;
		}
	}
}
