var OEE = ["0%", "99%"];
var AVAILABILITY = ["00:00:00", "99:99:99"];//= Run Time / Total Time
var PERFORMANCE = [0,9];//= Total Count / Target Counter
var QUALITY = [0,9];//Good Count / Total Count

var TARGET_COUNTER  = [0,9];
var TOTAL_COUNT = [0,9];
var GOOD_COUNT = [0,9];

var RUN_TIME = ["00:00:00", "99:99:99"];
var SETUP_TIME = ["00:00:00", "99:99:99"];
var DOWN_TIME = ["00:00:00", "99:99:99"];
var TOTAL_TIME = ["00:00:00", "99:99:99"];

var RM = "RM";
var RC = "RC";

var button_type = "";
function menu_oee()
{
	console.log("OEE");
	update_nav_mode("unknown");
	stop_all_protocols();
	update_active_menu("menu_oee");
	var content = ' '+
	'<div style="width: 100%; overflow: hidden;"> '+
		'<div style="width: 500px; float: left; color: #ffffff;"> '+ 
			'<h2>Mobile</h2> '+
			'<div style="width: 100%; overflow: hidden;"> '+
				'<div style="width: 200px; float: left;"> '+
					'<p style="color:#009900;"> OEE 			</p> '+
					'<p style="color:#ff3300;"> AVAILABILITY 	</p> '+
					'<p style="color:#ff9e20;"> PERFORMANCE 	</p> '+
					'<p style="color:#009900;"> QUALITY 		</p> '+
					' <br> '+
					'<p style="color:#ff3300;"> Target Count 	</p> '+
					'<p style="color:#ff9e20;"> Total Count 	</p> '+
					'<p style="color:#009900;"> Good Count 		</p> '+
					' <br> '+
					'<p style="color:#ff3300;"> Run Time 		</p> '+
					'<p style="color:#ff9e20;"> Setup Time 		</p> '+
					'<p style="color:#009900;"> Down Time		</p> '+
					'<p style="color:#ff3300;"> Total Time 		</p> '+
				'</div> '+
				'<div style="width: 140px; float: left;"> '+
					'<p id="RM_OEE" style="color:#009900;"> '+ OEE[0] 				+' </p> '+
					'<p id="RM_Availability" style="color:#ff3300;"> '+ AVAILABILITY[0]  	+' </p> '+
					'<p id="RM_Performance" style="color:#ff9e20;"> '+ PERFORMANCE[0] 		+' </p> '+
					'<p id="RM_Quality" style="color:#009900;"> '+ QUALITY[0] 			+' </p> '+
					' <br> '+
					'<p id="RM_TargetCount" style="color:#ff3300;"> '+ TARGET_COUNTER[0] 	+' </p> '+
					'<p id="RM_TotalCount" style="color:#ff9e20;"> '+ TOTAL_COUNT[0] 		+' </p> '+
					'<p id="RM_GoodCount" style="color:#009900;"> '+ GOOD_COUNT[0] 		+' </p> '+
					' <br> '+
					'<p id="RM_RunTime" style="color:#ff3300;"> '+ RUN_TIME[0] 			+' </p> '+
					'<p id="RM_SetupTime" style="color:#ff9e20;"> '+ SETUP_TIME[0] 		+' </p> '+
					'<p id="RM_DownTime" style="color:#009900;"> '+ DOWN_TIME[0] 		+' </p> '+
					'<p id="RM_TotalTime" style="color:#ff3300;"> '+ TOTAL_TIME[0] 		+' </p> '+
				'</div> '+
				'<div style="margin-left: 200px; margin-top: 30px;"> '+
					'<input class="button" id="RM_Run_Time" type="button" style="background-color: green;" value="Run Time" onclick="Run_Time('+ RM +')"> <br/> '+
					' <br> '+
					'<input class="button" id="RM_Down_Time" type="button" style="background-color: red;" value="Down Time" onclick="Down_Time(' + RM + ')"> <br/> '+
					' <br> '+
					'<input class="button" id="RM_bad_1" type="button" style="background-color: #ff9e20;" value="Bad Robot" onclick="bad_robot('+ RM +')"> <br/> '+
				'</div> '+
			'</div> '+
		'</div> '+
		'<div style="margin-left: 520px; color: #ffffff;"> '+
			'<h2>Robotcell</h2> '+
			'<div style="width: 100%; overflow: hidden;"> '+
				'<div style="width: 200px; float: left;"> '+
					'<p style="color:#009900;"> OEE 			</p> '+
					'<p style="color:#ff3300;"> AVAILABILITY 	</p> '+
					'<p style="color:#ff9e20;"> PERFORMANCE 	</p> '+
					'<p style="color:#009900;"> QUALITY 		</p> '+
					' <br> '+
					'<p style="color:#ff3300;"> Target Count 	</p> '+
					'<p style="color:#ff9e20;"> Total Count 	</p> '+
					'<p style="color:#009900;"> Good Count 		</p> '+
					' <br> '+
					'<p style="color:#ff3300;"> Run Time 		</p> '+
					'<p style="color:#ff9e20;"> Setup Time 		</p> '+
					'<p style="color:#009900;"> Down Time		</p> '+
					'<p style="color:#ff3300;"> Total Time 		</p> '+
				'</div> '+
				'<div style="width: 140px; float: left;"> '+
					'<p id="RC_OEE" style="color:#009900;"> '+ OEE[1] 				+' </p> '+
					'<p id="RC_Availability" style="color:#ff3300;"> '+ AVAILABILITY[1]  	+' </p> '+
					'<p id="RC_Performance" style="color:#ff9e20;"> '+ PERFORMANCE[1] 		+' </p> '+
					'<p id="RC_Quality" style="color:#009900;"> '+ QUALITY[1] 			+' </p> '+
					' <br> '+
					'<p id="RC_TargetCount" style="color:#ff3300;"> '+ TARGET_COUNTER[1] 	+' </p> '+
					'<p id="RC_TotalCount" style="color:#ff9e20;"> '+ TOTAL_COUNT[1] 		+' </p> '+
					'<p id="RC_GoodCount" style="color:#009900;"> '+ GOOD_COUNT[1] 		+' </p> '+
					' <br> '+
					'<p id="RC_RunTime" style="color:#ff3300;"> '+ RUN_TIME[1] 			+' </p> '+
					'<p id="RC_SetupTime" style="color:#ff9e20;"> '+ SETUP_TIME[1] 		+' </p> '+
					'<p id="RC_DownTime" style="color:#009900;"> '+ DOWN_TIME[1] 		+' </p> '+
					'<p id="RC_TotalTime" style="color:#ff3300;"> '+ TOTAL_TIME[1] 		+' </p> '+
				'</div> '+
				'<div style="margin-left: 200px; margin-top: 30px;"> '+
					'<input class="button" id="RC_Run_Time" type="button" style="background-color: green;" value="Run Time" onclick="Run_Time('+ RC +')"> <br/> '+
					' <br> '+
					'<input class="button" id="RC_Down_Time" type="button" style="background-color: red;" value="Down Time" onclick="Down_Time('+ RC +')"> <br/> '+
					' <br> '+
					'<input class="button" id="RC_bad_1" type="button" style="background-color: #ff9e20;" value="Bad Robot" onclick="bad_robot('+ RC +')"> <br/> '+
				'</div> '+
			'</div> '+
		'</div> '+
	'</div> ';
	document.getElementById("content").innerHTML = content;
	initialize_oee_control();
}

function Run_Time(run_time_string) {
	button_pushed = true;
	if(run_time_string == "RM")
	{
		button_type = "button_type:RM_Run_Time_button";
		
		var RM_elm = document.getElementById("RM_Run_Time");
		if(RM_elm.style.backgroundColor == "red")
		{
			RM_elm.style.backgroundColor = "green";
		}
		else if (RM_elm.style.backgroundColor == "green")
		{
			RM_elm.style.backgroundColor = "red";
		}
	} else if (run_time_string == "RC") {
		button_type = "button_type:RC_Run_Time_button";
		var RC_elm = document.getElementById("RC_Run_Time");
		if(RC_elm.style.backgroundColor == "red")
		{
			RC_elm.style.backgroundColor = "green";
		}
		else if (RC_elm.style.backgroundColor == "green")
		{
			RC_elm.style.backgroundColor = "red";
		}		
	}
}

function Down_Time(down_time_string) {
	button_pushed = true;
	if(down_time_string == "RM")
	{
		button_type = "button_type:RM_Down_Time_button";
		var RM_elm = document.getElementById("RM_Down_Time");
		if(RM_elm.style.backgroundColor == "red")
		{
			RM_elm.style.backgroundColor = "green";
		}
		else if (RM_elm.style.backgroundColor == "green")
		{
			RM_elm.style.backgroundColor = "red";
		}
	} else if (down_time_string == "RC") {
		button_type = "button_type:RC_Down_Time_button";
		var RC_elm = document.getElementById("RC_Down_Time");
		if(RC_elm.style.backgroundColor == "red")
		{
			RC_elm.style.backgroundColor = "green";
		}
		else if (RC_elm.style.backgroundColor == "green")
		{
			RC_elm.style.backgroundColor = "red";
		}		
	}
}

function bad_robot(bad_robot_string) {
	button_pushed = true;
	if(bad_robot_string == "RM")
	{
			button_type = "button_type:RM_BAD_1";
			console.log(bad_robot_string + " bad_robot_lvl: " + bad_robot_lvl);
	}
	else if (bad_robot_string == "RC")
	{
			button_type = "button_type:RC_BAD_1";
			console.log(bad_robot_string + " bad_robot_lvl: " + bad_robot_lvl);
	}
}

function update_oee() {
	
	var RM_elm = document.getElementById("RM_OEE");
	var RC_elm = document.getElementById("RC_OEE");
	RM_elm.innerHTML = OEE[0];
	RC_elm.innerHTML = OEE[1];

	RM_elm = document.getElementById("RM_Availability");
	RC_elm = document.getElementById("RC_Availability");
	RM_elm.innerHTML = (RUN_TIME[0]/TOTAL_TIME[0]); //availability
	RC_elm.innerHTML = (RUN_TIME[1]/TOTAL_TIME[1]); //availability
	
	RM_elm = document.getElementById("RM_Performance");
	RC_elm = document.getElementById("RC_Performance");
	RM_elm.innerHTML = (TOTAL_COUNT[0]/TARGET_COUNTER[0]); //performance
	RC_elm.innerHTML = (TOTAL_COUNT[1]/TARGET_COUNTER[1]); //performance
	
	RM_elm = document.getElementById("RM_Quality");
	RC_elm = document.getElementById("RC_Quality");
	RM_elm.innerHTML = (GOOD_COUNT[0]/TOTAL_COUNT[0]); //quality
	RC_elm.innerHTML = (GOOD_COUNT[1]/TOTAL_COUNT[1]); //quality

	RM_elm = document.getElementById("RM_TargetCount");
	RC_elm = document.getElementById("RC_TargetCount");
	RM_elm.innerHTML = TARGET_COUNTER[0];
	RC_elm.innerHTML = TARGET_COUNTER[1];
	
	RM_elm = document.getElementById("RM_TotalCount");
	RC_elm = document.getElementById("RC_TotalCount");
	RM_elm.innerHTML = TOTAL_COUNT[0];
	RC_elm.innerHTML = TOTAL_COUNT[1];
	
	RM_elm = document.getElementById("RM_GoodCount");
	RC_elm = document.getElementById("RC_GoodCount");
	RM_elm.innerHTML = GOOD_COUNT[0];
	RC_elm.innerHTML = GOOD_COUNT[1];
	
	RM_elm = document.getElementById("RM_RunTime");
	RC_elm = document.getElementById("RC_RunTime");
	RM_elm.innerHTML = RUN_TIME[0];
	RC_elm.innerHTML = RUN_TIME[1];
	
	RM_elm = document.getElementById("RM_SetupTime");
	RC_elm = document.getElementById("RC_SetupTime");
	RM_elm.innerHTML = SETUP_TIME[0];
	RC_elm.innerHTML = SETUP_TIME[1];
	
	RM_elm = document.getElementById("RM_DownTime");
	RC_elm = document.getElementById("RC_DownTime");
	RM_elm.innerHTML = DOWN_TIME[0];
	RC_elm.innerHTML = DOWN_TIME[1];
	
	RM_elm = document.getElementById("RM_TotalTime");
	RC_elm = document.getElementById("RC_TotalTime");
	RM_elm.innerHTML = TOTAL_TIME[0];
	RC_elm.innerHTML = TOTAL_TIME[1];

}
