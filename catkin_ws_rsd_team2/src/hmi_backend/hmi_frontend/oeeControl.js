
var transition_complete = true;
var button_pushed = false;

function initialize_oee_control()
{
	console.log("EH");
	if(not_connected)
	{
		connect('ws://'+callback_IP+':'+callback_port);
	}
	set_active_prototol("oeeControl");
}

function oee_control() 
{
	console.log("active protocol: " + active_protocol + " substate: " + substate);
	
	if( active_protocol == "oeeControl" && not_connected == false ) {
		if( substate == 0 ) 
		{
			console.log("sending the protocol msg: oeeControl");
			websocket.send("oeeControl");
			substate++;
		} 
		else if ( substate == 1 ) 
		{
			if( received_msg == "Ack oeeControl" ) 
			{
				console.log("received the ack msg");
				protocol_string = "Enabled";
				substate++;
				update_oee_status_enable = true;
				update_oee_status();
			} 
			else if(received_msg == "Robot Disconnected")
			{
				active_protocol = "inactive";
				substate = 999;
			} 
			else if(received_msg == "Robot Busy")
			{
				substate--;
				console.log("Robot is busy");
				setTimeout(oee_Control, 10000);
			}
       } 
       else if( substate == 3 ) 
       {
			console.log("updating oee status values");
			var oee_status = received_msg.split("\n");
			TARGET_COUNTER[0] 	= (oee_status[0].split(":")[1].split(";")[0]);
			TARGET_COUNTER[1] 	= (oee_status[0].split(":")[1].split(";")[1]);
			TOTAL_COUNT[0] 		= (oee_status[1].split(":")[1].split(";")[0]);
			TOTAL_COUNT[1] 		= (oee_status[1].split(":")[1].split(";")[1]);
			GOOD_COUNT[0] 		= (oee_status[2].split(":")[1].split(";")[0]);
			GOOD_COUNT[1] 		= (oee_status[2].split(":")[1].split(";")[1]);
			RUN_TIME[0] 		= (oee_status[3].split(":")[1].split(";")[0]);
			RUN_TIME[1] 		= (oee_status[3].split(":")[1].split(";")[1]);
			SETUP_TIME[0] 		= (oee_status[4].split(":")[1].split(";")[0]);
			SETUP_TIME[1] 		= (oee_status[4].split(":")[1].split(";")[1]);
			DOWN_TIME[0] 		= (oee_status[5].split(":")[1].split(";")[0]);
			DOWN_TIME[1] 		= (oee_status[5].split(":")[1].split(";")[1]);
			TOTAL_TIME[0] 		= (oee_status[6].split(":")[1].split(";")[0]);
			TOTAL_TIME[1] 		= (oee_status[6].split(":")[1].split(";")[1]);
			
			update_oee()
			transition_complete = true;
			if(button_pushed == true)
			{
				button_pushed = false;
				websocket.send(button_type);
				substate = 3;
			} else {
				websocket.send("Received current status");
			}
			substate = 2;			
		}
	} 
	else if(substate < 999)
	{
		console.log("stuck in " + active_protocol + "\n");
	} 
	else 
	{
		substate = 0;
	}		
}

function stop_oee()
{	
	console.log(active_protocol," ",substate, "  call close! in oee")
	
	if(active_protocol == "oeeControl" && substate == 2 )
	{
		console.log("Inside stopA-1");
		console.log("sending: Stop oeeControl");
		websocket.send("Stop oeeControl");
		update_oee_status_enable = false;
	}
}	
