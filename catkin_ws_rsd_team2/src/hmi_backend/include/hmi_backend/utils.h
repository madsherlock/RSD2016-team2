#pragma once
#include <msgs/nmea.h>
#include <array>
#include <msgs/RoutePt.h>
#include <fstream>

double battery_percent(const msgs::nmeaPtr& battery, double supply_voltage_scale_factor=0.03747);

struct pose_t {
    double x;
    double y;
    double theta;
    int task;
    pose_t() : x(0), y(0), theta(0), task(-1000000) {}
    pose_t(std::string str) : x(0), y(0), theta(0), task(-1000000){
        std::istringstream split(str);
        std::string part;
        try{
            std::getline(split, part, ',');
            x = std::stod(part);
            std::getline(split, part, ',');
            y = std::stod(part);
            std::getline(split, part);
            theta = std::stod(part);
        } catch(std::runtime_error e){
            std::cerr << "pose_t constructor: bad constructor, keeping defaults" << e.what() << std::endl;
            x = 0;
            y = 0;
            theta = 0;
        }
    }
    pose_t(double xx, double yy, double tt = 0, int def_task = -1000000) : x(xx), y(yy), theta(tt) , task(def_task){}
    friend std::ostream& operator<<(std::ostream& os , const pose_t& pose) {
        os << pose.x << ',' << pose.y << ',' << pose.theta << '\n';
        return os;
    }
    void convert(msgs::RoutePt &pt){
        pt.easting     = x;
        pt.northing    = y;
        pt.heading     = theta;
        pt.pause       = NAN;
        pt.linear_vel  = NAN;
        pt.angular_vel = NAN;
        pt.id          = std::string();
        pt.nav_mode    = 0; //pure pursuit
        pt.cmd         = -1000000;
        pt.task        = task;
    }
};

static std::vector<pose_t> gate_to_road;
static std::vector<pose_t> to_drone_gate;
static std::vector<pose_t> to_line = {{
                                                    pose_t(0.00,0.00,1.0),
                                                    pose_t(1.18,0.00,1.0),
                                                    pose_t(1.18,0.50,1.0),
                                                    pose_t(0.00,0.50,1.0),
                                                    pose_t(1.00,0.25,1.0)
                                                }};
static std::vector<pose_t> line_to_charger = {{
                                                    pose_t(0.00,0.00,3.0),
                                                    pose_t(0.18,0.00,3.0),
                                                    pose_t(0.18,0.16,3.0),
                                                    pose_t(0.00,0.16,3.0),
                                                    pose_t(0.18,0.00,3.0)
                                                }};
/**
 * @brief The FreespaceAnalyzer class is a pixel-checker to see if a pixel is a valid marker position.
 * If a ray moves in a singular direction towards a point, it will cross
 * a number of edges of a polygon.
 * If the number of crossings is odd, the point is inside,
 * if the number of crossings is even, the point is outside
 * http://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
 */
class FreespaceAnalyzer {

    /**
     * @brief freespace_polygon Polygon defined by pixels in the MarkerLocator camera image.
     * Inside this polygon are valid pixel positions for the marker mounted on the FrobitPro.
     * This includes the visible part of the dispenser area in freespace.
     */
    const std::array<std::array<double,2>,22> freespace_polygon =
    {{
         {{1028,800}},	//edge of dispenser entrance, nearest to robot cell door
         {{1233,802}},	//closer to robot cell door
         {{1536,744}},	//near pillar, closest to dispenser
         {{1729,774}},	//near pillar, closest to robot cell door
         {{1752,820}},	//near robot cell door, close to pillar
         {{1880,818}},	//near robot cell door, close to sink
         {{1854,723}},	//near orange shelves, close to robot cell door and sink
         {{1832,706}},	//near orange shelves, a bit further from robot cell door and sink
         {{1794,513}},	//near orange shelves
         {{1632,22}},	//top right part of image: near orange shelves, furthest away from robot cell door
         {{142,25}},	//top left part of image
         {{71,578}},	//bottom left part of image, near farming robots
         {{663,623}},	//near dispenser area, close to farming robots
         {{780,791}},	//edge of dispenser area, same side as farming robots
         {{895,795}},	//edge of dispenser entrance, nearest to farming robots
         // dispenser area
         {{891,897}},	//inside dispenser area, near entrance, nearest to farming robots
         {{834,896}},	//inside dispenser area, in corner, nearest to farming robots and entrance
         {{824,985}},	//inside dispenser area, near left edge, almost under stairs
         {{1152,960}},	//inside dispenser area, near right edge, almost under stairs
         {{1152,901}},	//inside dispenser area, in corner, nearest to pillar by robot cell door
         {{1025,899}},	//inside dispenser area, near entrance, nearest to robot cell
         //close
         {{1028,800}}	//edge of dispenser entrance, nearest to robot cell door
     }};

    /**
     * @brief freespace_dispenser_area Polygon defined by pixels in the MarkerLocator camera image.
     * @see freespace_polygon
     */
    const std::array<std::array<double,2>,8> freespace_dispenser_area =
    {{
         {{895,795}},	//edge of dispenser entrance, nearest to farming robots
         {{891,897}},	//inside dispenser area, near entrance, nearest to farming robots
         {{834,896}},	//inside dispenser area, in corner, nearest to farming robots and entrance
         {{824,985}},	//inside dispenser area, near left edge, almost under stairs
         {{1152,960}},	//inside dispenser area, near right edge, almost under stairs
         {{1152,901}},	//inside dispenser area, in corner, nearest to pillar by robot cell door
         {{1025,899}},	//inside dispenser area, near entrance, nearest to robot cell
         {{1028,800}}	//edge of dispenser entrance, nearest to robot cell door
     }};

    /**
     * @brief rayCast Ray-casting algorithm
     * Tests if the point is between the two points in the y direction
     * and if the point is to the left of the line.
     * @param verti Vertex with low index
     * @param vertj Vertex with high index
     * @param point Test point
     * @return to the left of line
     */
    virtual bool rayCast(const std::array<double,2> &verti, const std::array<double,2> &vertj, const std::array<double,2> &point) const {
        const double & verti_x = verti[0];
        const double & verti_y = verti[1];
        const double & vertj_x = vertj[0];
        const double & vertj_y = vertj[1];
        if ( ((verti_y>point[1]) != (vertj_y>point[1])) &&
             (point[0] < (vertj_x-verti_x) * (point[1]-verti_y) / (vertj_y-verti_y) + verti_x) )
            return true;
        else
            return false;
    }

public:

    virtual ~FreespaceAnalyzer() { }

    /**
     * @brief pointInFreespace Checks whether a point is in freespace
     * @param point First element is x, second element is y.
     * @return true if point is in freespace
     */
    virtual bool pointInFreespace(const std::array<double,2> &point) const {
        bool point_in_freespace=false;
        for(size_t i=0, j=freespace_polygon.size()-1; i<freespace_polygon.size(); j=i++)
            if(rayCast(freespace_polygon[i],freespace_polygon[j],point))
                point_in_freespace=!point_in_freespace;
        return point_in_freespace;
    }

    /**
     * @brief pointInFreespace @see pointInFreespace()
     * @param x
     * @param y
     * @return
     */
    virtual bool pointInFreespace(const double x, const double y) const {
        std::array<double,2> point;
        point[0]=x;
        point[1]=y;
        return pointInFreespace(point);
    }

    /**
     * @brief pointInDispenser Checks whether a point is inside the dispenser area. @see pointInFreespace().
     * @param point
     * @return
     */
    virtual bool pointInDispenser(const std::array<double,2> &point) const {
        bool point_in_dispenser_area=false;
        for(size_t i=0, j=freespace_dispenser_area.size()-1; i<freespace_dispenser_area.size(); j=i++)
            if(rayCast(freespace_dispenser_area[i],freespace_dispenser_area[j],point))
                point_in_dispenser_area=!point_in_dispenser_area;
        return point_in_dispenser_area;
    }

    /**
     * @brief pointInDispenser @see pointInDispenser().
     * @param x in mm
     * @param y
     * @return
     */
    virtual bool pointInDispenser(const double x, const double y) const {
        std::array<double,2> point;
        point[0]=x;
        point[1]=y;
        return pointInDispenser(point);
    }
};
