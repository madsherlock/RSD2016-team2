#include "controller.hpp"
#include <sstream>
#include <fstream>
#include "settings.h"
#include <iostream>

/**
 * @brief is_legal - checks if a string contains chars that is not legal in a filename
 * @param str - filename
 * @return true if it is legal.
 */
bool is_legal(std::string str){
    std::string illegal_chars = "#%&{}\\<>*?/ $!\'\":@+´`|=";
    for(size_t i = 0; i < str.length(); ++i){
        for(size_t j = 0; j < illegal_chars.length(); ++j){
            if(str[i] == illegal_chars[j]){
                return false;
            }
        }
    }
    return true;
}

controller::controller():
    protocols({"TakeOrder","MoveRobot","EndMoveRobot","AutoRobot", "ControlARobotCell","ControlMRobotCell","oeeControl"}),
    settings(QSettings::NativeFormat, QSettings::UserScope, ORGANIZATION , APPLICATION),
    current_state(TakeOrder),
    save_dir(),
    filename(),
    confirmation(0),
    substate(-1),
    old_message(),
    incomming_message(),
    mobile_robot_busy(false),
    mobile_robot_connected(true),
    robot_cell_busy(false),
    robot_cell_connected(true),
    mr_status(),
    rc_status(),
    new_oee_status()
{
    mr_status.busy = false;
    rc_status.conveyor_busy = false;
    std::cout << "starting controller" << std::endl;
    save_dir = settings.value("orders/save_dir","/home/user/").toString().toStdString();
}

void controller::user_connected(void){
    std::cout << "new user connected" << std::endl;
}

void controller::msg_received(std::string input){
    ++substate;
    old_message = incomming_message;
    incomming_message = input;
    std::cout << "msg received: " << input << std::endl;
    std::cout << "Current-state : " << substate << " " << protocols.at(static_cast<int>(current_state) ) << std::endl;
process_msg();
}

void controller::protocol_rec(int protocol){
    states input = static_cast<states>(protocol);
    if(current_state == AutoRobot && input == MoveRobot)
    {
        emit control_mobile_robot(0);
        substate = -1;
    }
    if(substate == -1){
        current_state = input;
        substate = 0;
        confirmation = 1;
    } else if(protocol == static_cast<int>(EndMoveRobot) ){
        substate = 65536;
    }
    std::cout << "Protocol rec er blevet kaldt!"<< std::endl;
    std::cout << "received protocol: " << protocols.at(protocol) << std::endl;
    process_msg();
}

void controller::process_msg(void){
    switch(current_state){
    case TakeOrder:
        switch(substate){
        case 0:
            emit new_msg("Ack TakeOrder");
            break;
        case 1:
            if(is_legal(incomming_message) ){
                filename = save_dir;
                filename += incomming_message;
                std::ifstream order_file(filename);
                if(order_file.good()){
                    if(confirmation --> 0){ //if the file exist (1st attempt)
                        --substate;
                        emit new_msg("File exists");
                    } else if(incomming_message == old_message) { //if filename is confirmed
                        confirmation = 1;
//                        order_handler.load(filename);
                        /// @todo load existing orders?.
                        emit new_msg("It resolves");
                    } else { //if the new name also hits an existing file
                        ++confirmation;
                        emit new_msg("File exists");
                    }
                } else {
                    emit new_msg("It resolves");
                }
                order_file.close();
            } else {
                --substate;
                emit new_msg("Illegal chars");
            }
            break;
        case 2:
            std::ofstream order_file(filename);
            order_file << incomming_message;
            order_file.close();
            substate = -1;
            emit new_msg("Successfully received");
            std::cout << "wrote to file: " << filename << std::endl;
            break;
        }
        break;
    case MoveRobot:
        switch(substate){
        case 0:
            //handshake to agree on state
            if(mr_status.busy){
                emit new_msg("Robot Busy");
                substate = -1;
            } else if( !mobile_robot_connected ){
                emit new_msg("Robot Disconnected");
                substate = -1;
            } else {
                emit new_msg("Ack MoveRobot");
                emit control_mobile_robot(1);
            }
            break;
        case 1:
            mr_status.busy = true;
            ++substate;
            emit move_mobile_robot(incomming_message);
            break;
        case 65536: //complete
            emit new_msg("Move Robot Complete");
            emit control_mobile_robot(0);
            substate = -1;
            break;
        default:
            //this way, the client can send multiple requests, but the robot will not respond unless it is ready
            if(!mr_status.busy){
                substate = 0;
                emit new_msg(mr_status.toString());
            }
            break;
        }
        break;
    case AutoRobot:
        switch (substate) {
        case 0:
            //handshake to agree on state
            if(mr_status.busy){
                emit new_msg("Robot Busy");
                substate = -1;
            } else if( !mobile_robot_connected ){
                emit new_msg("Robot Disconnected");
                substate = -1;
            } else {
                emit new_msg("Ack AutoRobot");
                emit control_mobile_robot(2);
                substate = 1;
            }
            break;
        case 1:
            if(incomming_message == "stop auto protocol"){
                emit control_mobile_robot(0);
                substate = -1;
                break;
            } else {
                ++substate;
                emit move_mobile_robot("Nowhere");
            }
            break;
        default:
            if(!mr_status.busy){
                emit new_msg(mr_status.toString());
            } else {
                std::cout << "got here: robot must be busy" << std::endl;
            }
            substate = 0; // wait for new request
            break;
        }
        break;
    case ControlARobotCell:
        std::cout << "Current substate: " << substate << " Incomming_message: " << incomming_message << std::endl;
        switch(substate) {
            case 0:
                // control robotcell Automode => publishs current state
                //if(incomming_message != "Requesting current status" && incomming_message != "Stop ControlARobotcell" )
                //{
                    emit new_msg("Ack ControlARobotCell");
                    rc_status.current_navigation_mode = "auto";
                    status_controller(rc_status);
                //}
                break;
            case 1:
                if(incomming_message == "Requesting current status")
                {
                    emit control_conveyorbelt(1);
                    std::cout << "Inside controller ControlARobotCell - current conveyor speed: " <<  rc_status.conveyor_speed << std::endl;
                    emit new_msg(rc_status.toString());
                    substate = 2;
                }
                else if(incomming_message == "Stop ControlARobotcell")
                {
                    std::cout << "Stopping ControlARobotcell" << std::endl;
                    emit new_msg("Ack Stopping ControlARobotCell");
                    substate = -1;
                }
                break;
            case 2:
                std::cout << "Not spamming rostopic - just the terminal window heheh!" << std::endl;
                if(incomming_message == "Stop ControlARobotcell")
                {
                    std::cout << "Using emit here" << std::endl;
                    emit control_conveyorbelt(0);
                    std::cout << "Stopping ControlARobotcell" << std::endl;
                    emit new_msg("Ack Stopping ControlARobotCell");
                    substate = -1;
                }
                break;
            case 3:
                if(incomming_message == "Received current status")
                {
                    emit control_conveyorbelt(0);
//                     std::cout << "Frontend received current status, shh a thank would be nice hm!" << std::endl;
                    substate = 0;
                }
                else if(incomming_message == "Stop ControlARobotcell")
                {
                    emit control_conveyorbelt(0);
                    std::cout << "Stopping ControlARobotcell" << std::endl;
                    emit new_msg("Ack Stopping ControlARobotCell");
                    substate = -1;
                } 
                else if(incomming_message == "Emergency Stop")
                {
                    emit emergency_stop(true);
                }
                break;
            }
            break;
    case ControlMRobotCell:
        switch(substate)
        {
            case 0:
                emit new_msg("Ack ControlMRobotcell");
                rc_status.current_navigation_mode = "manual";
                status_controller(rc_status);
                substate = 1;
                break;
            case 1:
                std::cout << "waiting for new message!" << std::endl;
                break;
            case 2:
                if(incomming_message != old_message)
                {
                    if(incomming_message == "Stop ControlMRobotcell")
                    {
                        std::cout << "Stopping m mode:" << std::endl;
                        emit control_conveyorbelt(0);
                        std::cout << "Stopping ControlRobotcell" << std::endl;
                        emit new_msg("Ack Stopping ControlMRobotCell");
                        substate = -1;

                    }
                    else
                    {
                        rc_status.adjust_parameter(incomming_message);
                        status_controller(rc_status);
                        emit control_conveyorbelt(1);
                        emit new_msg(rc_status.toString());
                        substate = 3;
                    }
                }
                break;
            case 3:
                std::cout << "I am working! - I will let my frustation out on the terminal!!" << std::endl;
//                if(incomming_message == "Stop ControlMRobotcell")
//                {
//                        std::cout << "emitting here: Manual!" << std::endl;
//                        //emit control_conveyorbelt(0);
//                        std::cout << "Stopping ControlRobotcell" << std::endl;
//                        emit new_msg("Ack Stopping ControlMRobotCell");
//                        substate = -1;
//                }

                break;
            case 4:
                if(incomming_message == "Received current status - In Manual-mode")
                {
                    //emit control_conveyorbelt(0);
                    substate = 1;
                    std::cout << "Frontend received current state"<< std::endl; // Changed order

                }
                else if(incomming_message == "Stop ControlMRobotcell")
                {
                    //emit control_conveyorbelt(0);
                    std::cout << "Stopping ControlMRobotcell" << std::endl;
                    emit new_msg("Ack Stopping ControlMRobotCell");
                    substate = -1;
                }                
//                else if(incomming_message != old_message)
//                {
//                    std::cout << "Don't spam - I am still processing " << std::endl;
//                    substate = 1;
//                }
//                else
//                {
//                    substate = 3;
//                }
                break;

        }
    case oeeControl:
        std::cout << "Current substate: " << substate << " Incomming_message: " << incomming_message << std::endl;
        switch(substate) {
            case 0:
                    emit new_msg("Ack oeeControl");
                    new_oee_status.current_navigation_mode = "oeeControl";
                    oee_status_control(new_oee_status);
                break;
            case 1:
                if(incomming_message == "Requesting current status")
                {
                    emit oee_control(1);
                    std::cout << "Inside controller oee " << std::endl;
                    emit new_msg(new_oee_status.toString());
                    substate = 2;
                }
                else if(incomming_message == "Stop oeeControl")
                {
                    std::cout << "Stopping oeeControl" << std::endl;
                    emit new_msg("Ack Stopping oeeControl");
                    substate = -1;
                }
                break;
            case 2:
                std::cout << "Not spamming rostopic - just the terminal window heheh!" << std::endl;
                if(incomming_message == "Stop oeeControl")
                {
                    std::cout << "Using emit here" << std::endl;
                    emit oee_control(0);
                    std::cout << "Stopping oeeControl" << std::endl;
                    emit new_msg("Ack Stopping oeeControl");
                    substate = -1;
                }
                break;
            case 3:
                if(incomming_message == "Received current status")
                {
                    emit oee_control(0);
//                     std::cout << "Frontend received current status, shh a thank would be nice hm!" << std::endl;
                    substate = 0;
                }
                else if(incomming_message == "Stop oeeControl")
                {
                    emit oee_control(0);
                    std::cout << "Stopping oeeControl" << std::endl;
                    emit new_msg("Ack Stopping oeeControl");
                    substate = -1;
                }
                else if(incomming_message.find("button_type:") != std::string::npos)
                {
                    new_oee_status.adjust_parameter(incomming_message);
                    oee_status_control(new_oee_status);
                    emit oee_control(1);
                    emit new_msg(new_oee_status.toString());
                    substate = 0;
                }
                break;
            }
            break;
        break;
    default:
        std::cout << "wrong state : " << current_state << " " << protocols.at(static_cast<int>(current_state) ) << std::endl;
    }
}
