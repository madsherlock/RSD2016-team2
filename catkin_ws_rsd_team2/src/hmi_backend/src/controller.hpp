#pragma once

#include <QThread>
#include <QSettings>
#include <iostream>
#include "mobile_robot_wrapper.h"
#include "mobile_robot_status.h"
#include "robot_cell_wrapper.h"
#include "robot_cell_status.h"
#include "oee_wrapper.h"
#include "oee_status.h"
/**
 * @brief The controller class is the implementation of the communication protocol.
 * Messages comes in, gets processed and that should result in a response.
 */
class controller : public QThread
{
    Q_OBJECT
private:
    enum states{TakeOrder=0,
                MoveRobot,
                EndMoveRobot,
                AutoRobot,
                ControlARobotCell,
                ControlMRobotCell,
                oeeControl
               };

public:
    controller();
    /**
     * @brief protocols - a vector containing the names of all protocols.
     * They are defined in the constructor
     */
    std::vector<std::string> protocols;
private:
    /**
     * @brief settings - loading of a config file
     */
    QSettings settings;
    /**
     * @brief current_state - currently active state
     */
    states current_state;
    /**
     * @brief save_dir - the directory where stuff should be stored
     */
    std::string save_dir;
    /**
     * @brief filename variable to store the filename. Used to write orders to file.
     */
    std::string filename;
    /**
     * @brief confirmation - variable used to confirm steps in protocols
     */
    int confirmation;
    /** substate:
     *  All protocols will have a substate to keep track of time.
     *  -1 means the protocol is over and a new protocol can be executed.
     *  All protocols must set the substate to -1 when the protocol is over.
     *  It is up to the protocol designer how many substates is needed.
     *
     *  Protocol messages can also be a "end protocol", in this situation,
     *  two protocol messages is reserved.
     */
    int substate;
    std::string old_message;
    std::string incomming_message;

    bool mobile_robot_busy;
    bool mobile_robot_connected;
    bool robot_cell_busy;
    bool robot_cell_connected;
    mobile_robot_status mr_status;
    robot_cell_status rc_status;
    oee_status new_oee_status;
    /**
     * @brief process_msg
     * This function is the direct implication of the protocol
     */
    void process_msg(void);

signals:
    /**
     * @brief new_msg
     * This emits outgoing messages.
     */
    void new_msg(std::string);

    void move_mobile_robot(std::string);

    /**
     * @brief control_mobile_robot
     * True, starts the mobile robot thread
     * False, stops the thread.
     */
    void control_mobile_robot(int);
    /**
     * @brief control_conveyorbelt
     * True, starts the mobile robot thread
     * False, stops the thread.
     */
    void control_conveyorbelt(bool);
    void status_controller(robot_cell_status);
    void emergency_stop(bool);
    void emergency_reset(bool);
    void oee_status_control(oee_status);
    void oee_control(bool);
public slots:
    /**
     * @brief protocol_rec - incomming message handling
     * @param protocol index equavilent to the states enum and the index in the protocols vector.
     */
    void protocol_rec(int protocol);
    /**
     * @brief user_connected
     * User handling, currently not used
     */
    void user_connected(void);
    /**
     * @brief msg_received - incomming message handling
     * @param input - a string containing the message.
     * This function is where the incomming messages are handled.
     * This calls the process_msg() function
     */
    void msg_received(std::string input);
    void update_mobile_robot_status(mobile_robot_status status) { mr_status = status; std::cout << "MR - got reply" << std::endl; process_msg();}
    void update_mobile_robot_connected(bool state) { mobile_robot_connected = state; }
    void update_robobt_cell_connected(bool state){robot_cell_connected = state;}
    void update_robot_cell_status_from_wrapper(robot_cell_status status){rc_status = status; std::cout << "RC - got reply from wrapper"<< std::endl;}
    void update_oee_status_from_wrapper(oee_status status){new_oee_status = status; std::cout << "OEE - reply " << std::endl;}
    void user_disconnected(void){ substate = -1; }
};
