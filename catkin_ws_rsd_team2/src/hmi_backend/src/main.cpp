#include "websocket_server.hpp"
#include "controller.hpp"
#include "mobile_robot_wrapper.h"
#include "robot_cell_wrapper.h"
#include "oee_wrapper.h"

#include "ros/ros.h"
#include "std_msgs/String.h"

#include "mobile_robot_status.h"

#include <QApplication>

Q_DECLARE_METATYPE( mobile_robot_status )
Q_DECLARE_METATYPE( std::string )

int main(int argc, char * argv[]){
    qRegisterMetaType<mobile_robot_status>("mobile_robot_status");
    qRegisterMetaType<robot_cell_status>("robot_cell_status");
    qRegisterMetaType<oee_status>("oee_status");
    qRegisterMetaType<std::string>("std::string");
    QApplication app(argc,argv);

    ros::init(argc, argv, "mobile_robot_hmi");
    controller ctrl;
    mobile_robot_wrapper mr;
    std::cout << "Hello" << std::endl;
    robot_cell_wrapper rc;
    oee_wrapper oee;
    wss server;
    server.incomming_match = ctrl.protocols;
    QObject::connect(&ctrl,SIGNAL(move_mobile_robot(std::string)),&mr,SLOT(move(std::string)));
    QObject::connect(&ctrl,SIGNAL(control_mobile_robot(int)),&mr, SLOT(ctrl_mr(int)));
    QObject::connect(&mr,SIGNAL(status(mobile_robot_status)),&ctrl, SLOT(update_mobile_robot_status(mobile_robot_status)));
    //QObject::connect(&rc,SIGNAL(status(robot_cell_status)),&ctrl, SLOT(update_robot_cell_status(robot_cell_status)));
    QObject::connect(&ctrl,SIGNAL(status_controller(robot_cell_status)),&rc, SLOT(update_robot_cell_status_from_controller(robot_cell_status)));
    QObject::connect(&rc,SIGNAL(status_wrapper(robot_cell_status)),&ctrl, SLOT(update_robot_cell_status_from_wrapper(robot_cell_status)));
    //    QObject::connect(&mr,SIGNAL(connected(bool)),&ctrl, SLOT(update_mobile_robot_connected(bool)));
    QObject::connect(&ctrl,SIGNAL(control_conveyorbelt(bool)),&rc, SLOT(ctrl_rc(bool)));

    QObject::connect(&ctrl,SIGNAL(oee_status_control(oee_status)),&oee, SLOT(update_oee_status_from_controller(oee_status)));
    QObject::connect(&oee,SIGNAL(oee_status_wrapper(oee_status)),&ctrl, SLOT(update_oee_status_from_wrapper(oee_status)));
    QObject::connect(&ctrl,SIGNAL(oee_control(bool)),&oee, SLOT(ctrl_oee(bool)));

    QObject::connect(&ctrl,SIGNAL(new_msg(std::string)), &server, SLOT(new_message(std::string)));
    QObject::connect(&server.wrapper,SIGNAL(protocol_message_received(int)), &ctrl, SLOT(protocol_rec(int)));
    QObject::connect(&server.wrapper,SIGNAL(message_received(std::string)), &ctrl, SLOT(msg_received(std::string)));
    QObject::connect(&server.wrapper,SIGNAL(user_connected(void)), &ctrl, SLOT(user_connected(void)));
    QObject::connect(&server.wrapper,SIGNAL(user_disconnected(void)), &ctrl, SLOT(user_disconnected(void)));
    QObject::connect(&server.wrapper,SIGNAL(close(void)),&mr,SLOT(about_to_quit()));
    QObject::connect(&server.wrapper,SIGNAL(close(void)),&app,SLOT(quit()));
//    server.run();
    server.start();
    app.exec();
    return 0;
}
