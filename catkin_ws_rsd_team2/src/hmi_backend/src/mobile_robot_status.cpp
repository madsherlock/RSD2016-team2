#include "mobile_robot_status.h"
#include <sstream>
mobile_robot_status::mobile_robot_status(double lin, double ang) :
    received_vel_linear(lin),
    received_vel_angular(ang),
    busy(true),
    io_status(false),
    battery_state_of_charge(90),
    pos_x(0),
    pos_y(0),
    tipper_pos(0),
    orientation(0),
    current_navigation_mode("manual")
{}

std::string mobile_robot_status::toString(){
    std::ostringstream msg;
    msg << "lin:"  << received_vel_linear                             << '\n'  // 0
        << "ang:"  << received_vel_angular                            << '\n'  // 1
        << "busy:" << (busy ? "taken" : "ready")                      << '\n'  // 2
        << "IO:"   << (io_status ? "True" : "False")                  << '\n'  // 3
        << "vol:"  << battery_state_of_charge                         << '\n'  // 4
        << "pos:"  << pos_x << " , " << pos_y << " , " << orientation << '\n'  // 5
        << "tipper:" << tipper_pos                                    << '\n'  // 6
        << "nav:"  << current_navigation_mode                         << '\n'; // 7
    return msg.str();
}
