#pragma once
#include <string>
/**
 * @brief The mobile_robot_status struct
 * Data structure for transmitting mobile robot status
 */
struct mobile_robot_status{
public:
    mobile_robot_status(double lin = 0, double ang = 0);

    double received_vel_linear;
    double received_vel_angular;
    bool busy;
    /**
     * @brief io_status
     * true = available
     * false = not available
     */
    bool io_status;
    double battery_state_of_charge;
    /**
     * @brief pos_{x,y}
     * Position in meters
     */
    double pos_x;
    double pos_y;
    double tipper_pos;
    /**
     * @brief orientation
     * Orientation (yaw) in degrees
     */
    double orientation;
    /**
     * @brief current_navigation_mode
     * String that will be printed on the HMI
     */
    std::string current_navigation_mode;
    std::string toString();
};
