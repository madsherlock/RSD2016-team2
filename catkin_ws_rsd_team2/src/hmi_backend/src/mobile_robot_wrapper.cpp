#include "mobile_robot_wrapper.h"
#include <unistd.h> //for usleep, remove when possible
#include <cmath>
#include "utils.h"

#include <std_srvs/Empty.h>

mobile_robot_wrapper::mobile_robot_wrapper() :
    nodehandle("~"),
    cmd_vel_pub(nodehandle.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel", 1,true)),
    dead_man_pub(nodehandle.advertise<msgs::BoolStamped>("/fmSafe/deadman", 1,true)),
    tipper_pub(nodehandle.advertise<std_msgs::Int16>("/tipper_control",1,true)),
    nav_mode_request(nodehandle.advertise<std_msgs::String>("/mr_nav_mode_request",1,true)),
    behaviour_mode_pub(nodehandle.advertise<msgs::IntStamped>("/fmPlan/automode",1, true)),
    wpt_nav_route_pub(nodehandle.advertise<waypoint_navigation::waypoint_control_msg>("/waypoint_control",1,false)),
    cmd_vel_sub(nodehandle.subscribe("/fmCommand/cmd_vel", 1, &mobile_robot_wrapper::vel_callback, this)),
    pos_sub(nodehandle.subscribe("/tf", 1, &mobile_robot_wrapper::pos_callback, this)),
    battery_sub(nodehandle.subscribe("/fmSignal/nmea_from_frobit",1,&mobile_robot_wrapper::battery_callback, this)),
    tipper_sub(nodehandle.subscribe("/tipper_status",1,&mobile_robot_wrapper::tipper_callback, this)),
    nav_mode_sub(nodehandle.subscribe("/mr_nav_mode",1,&mobile_robot_wrapper::nav_mode_callback, this)),
    wpt_nav_complete_sub(nodehandle.subscribe("/waypoint_complete",1, &mobile_robot_wrapper::wpt_complete_callback, this)),
    marker_pose_reset_client(nodehandle.serviceClient<std_srvs::Empty>("/markerpose_reset")),
    loop_rate(100),
    vel_linear(0),
    vel_angular(0),
    lin_inc(0.07),
    ang_inc(0.06),
    twist_msg(),
    dead_man(),
    desired_tipper_pos(),
    thread_running(false),
    supply_voltage_scale_factor(0.03747),
    busy(false),
    display_status()
{
    std::cout << "Waiting up to 3 seconds for marker pose reset service to init..." << std::endl;
    marker_pose_reset_client.waitForExistence(ros::Duration(3));
    std::cout << "robot constructor completed" << std::endl;
}

void mobile_robot_wrapper::nav_mode_callback(const std_msgs::StringPtr& type){
    std::cout << "---- GOT NAV MODE: " << type->data << " ----" << std::endl;
    display_status.current_navigation_mode = type->data;
}

void mobile_robot_wrapper::tipper_callback(const std_msgs::Int16Ptr& tipper){
    display_status.tipper_pos = tipper->data;
}

void mobile_robot_wrapper::battery_callback(const msgs::nmeaPtr& battery){
    if (battery->valid == true) {
        display_status.battery_state_of_charge = battery_percent(battery, supply_voltage_scale_factor);
    }
}

void mobile_robot_wrapper::vel_callback(const geometry_msgs::TwistStampedPtr& vel){
    display_status.received_vel_linear = vel->twist.linear.x;
    display_status.received_vel_angular = vel->twist.angular.z;
}

void mobile_robot_wrapper::pos_callback(const tf2_msgs::TFMessagePtr& pos){
    display_status.pos_x = pos->transforms.data()->transform.translation.x;
    display_status.pos_y = pos->transforms.data()->transform.translation.y;

    double x = pos->transforms.data()->transform.rotation.x;
    double y = pos->transforms.data()->transform.rotation.y;
    double z = pos->transforms.data()->transform.rotation.z;
    double w = pos->transforms.data()->transform.rotation.w;

    /* rpy convertion
    double roll = std::atan2(2.0 * (y * z + w * x), -2.0 * (x * x + y * y) + 1);
    double p = -2.0 * (x * z - w * y);
    if(p > 1.0) {
        p = 1.0;
    } else if(p < -1.0) {
        p = -1.0;
    }
    double pitch = std::asin(p);
    //*/

    //double yaw  = std::atan2(2.0 * (x * y + w * z), -2.0 * (y * y + z * z) + 1);
    double yaw = std::atan2(2.0*(x*y+w*z),w*w+x*x-y*y-z*z); //same as l.266 of waypoint navigation py
    display_status.orientation = yaw * 180.0/M_PI;
}

void mobile_robot_wrapper::wpt_complete_callback(const msgs::BoolStampedPtr &complete) {
    if(complete->data){
        busy = false; 
    }
}


void mobile_robot_wrapper::move(std::string direction){
    const double lin_threshold = 0.7;
    const double ang_threshold = 0.6;
    
    if(direction == "brake"){
        vel_linear = 0;
        vel_angular = 0;
    } else if(direction == "forward"){
        vel_linear  += lin_inc;
    } else if(direction == "reverse"){
        vel_linear  -= lin_inc;
    } else if(direction == "left"){
        vel_angular += ang_inc;
    } else if(direction == "right"){
        vel_angular -= ang_inc;
    } else if(direction.find("tipper: ") != std::string::npos) {
        desired_tipper_pos.data = std::stoi(direction.substr(8));
        tipper_pub.publish(desired_tipper_pos);
        ros::spinOnce();
    } else if(direction.find("wpt:") != std::string::npos) {
        double x, y, theta;
        bool success = true;
        try{
            std::stringstream ss(direction);
            std::string buf;
            std::getline(ss,buf,':');
            std::getline(ss,buf,',');
            x = std::stod(buf);
            std::getline(ss,buf,',');
            y = std::stod(buf);
            std::getline(ss,buf);
            theta = std::stod(buf);
            theta = theta *M_PI/180;
        } catch(std::runtime_error){
            success = false;
            std::cout << "bad wpt" << std::endl;
        }
//         if(success){
//             FreespaceAnalyzer fa;
//             success = fa.pointInFreespace(x*1000,y*1000);
//         }
//         std::cout << "got wpt: " << (success ? "inside" : "outside") << " free area" << std::endl;
        if(success){
            pose_t p(x,y,theta,1); //1 means reset before moving there.
            msgs::RoutePt waypoint;
            waypoint_navigation::waypoint_control_msg route;
            p.convert(waypoint);
            route.wpt_list.push_back(waypoint);
            route.nav_mode = "replace";
            wpt_nav_route_pub.publish(route);
            msgs::IntStamped behaviour;
            behaviour.data = 1;
            behaviour.header.stamp = ros::Time::now();
            behaviour_mode_pub.publish(behaviour);
            ros::spinOnce();
            busy = true;
        }
        
        tipper_pub.publish(desired_tipper_pos);
        ros::spinOnce();
    } else if(direction == "reset_marker_pose"){
        std::cout << "Calling marker pose reset service..." << std::endl;
        std_srvs::Empty dummy;
        marker_pose_reset_client.call(dummy);
        ros::spinOnce();
    }
    if(vel_linear >= lin_threshold){
        vel_linear = lin_threshold;
    } else if(vel_linear < -lin_threshold){
        vel_linear = -lin_threshold;
    }
    if(vel_angular >= ang_threshold){
        vel_angular = ang_threshold;
    } else if(vel_angular < -ang_threshold){
        vel_angular = -ang_threshold;
    }
//    loop_rate.sleep();
//    if(vel_linear == 0.00){ //if resulting velocity is 0, then wait more time.
//        for(int i = 0; i < 99; ++i){
//            loop_rate.sleep();
//        }
//    }
    display_status.busy = false;
    emit status(display_status);
}

void mobile_robot_wrapper::ctrl_mr(int enable){
    if(enable){
        thread_running = true;
    } else {
        thread_running = false;
    }
    if(enable == 1){
        std::cout << "requesting manual control of mobile robot" << std::endl;
        std_msgs::String request_msg;
        request_msg.data = "hmi_request_manual_control";
        nav_mode_request.publish(request_msg);
        ros::spinOnce();
    } else if(enable == 2){
        std::cout << "requesting auto mode of mobile robot" << std::endl;
        std_msgs::String request_msg;
        request_msg.data = "hmi_request_auto_control";
        nav_mode_request.publish(request_msg);
        ros::spinOnce();
    }
    if(thread_running){
        busy = false;
        this->start();
    } else {
        busy = true;
        vel_linear = 0;
        vel_angular = 0;
    }
}
void mobile_robot_wrapper::about_to_quit(){
    ctrl_mr(0);
}
void mobile_robot_wrapper::run(){
    std::cout << "[MR] started thread" << std::endl;
    while(thread_running) {
        loop_rate.sleep();
        if(!thread_running) break;
        if(busy){
            msgs::IntStamped behaviour;
            behaviour.data = 1;
            behaviour.header.stamp = ros::Time::now();
            behaviour_mode_pub.publish(behaviour);
            dead_man.header.stamp = ros::Time::now();
            dead_man.data = 1;
            dead_man_pub.publish(dead_man);
        } else if(display_status.current_navigation_mode == "manual") {
            dead_man.header.stamp = ros::Time::now();
            dead_man.data = 1;
            dead_man_pub.publish(dead_man);

            twist_msg.twist.linear.x  = vel_linear;
            twist_msg.twist.angular.z = vel_angular;
            twist_msg.header.stamp = ros::Time::now();
            cmd_vel_pub.publish(twist_msg);
        }
        ros::spinOnce();
    }
    vel_linear  = 0;
    vel_angular = 0;

    std_msgs::String request_msg;
    request_msg.data = "hmi_request_auto_control";
    nav_mode_request.publish(request_msg);
    ros::spinOnce();

    std::cout << "[MR] completed thread" << std::endl;
}
