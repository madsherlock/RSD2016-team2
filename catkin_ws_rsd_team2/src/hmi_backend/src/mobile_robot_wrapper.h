#pragma once

#include <QObject>
#include <iostream>
#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
#include <msgs/BoolStamped.h>
#include <tf2_msgs/TFMessage.h>
#include <msgs/nmea.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include <msgs/IntStamped.h>
#include <QThread>
#include "mobile_robot_status.h"
#include <waypoint_navigation/waypoint_control_msg.h>
/**
 * @brief The mobile_robot_wrapper class publishes the velocity over ROS
 * It gets a input as QSignal, which updates the velocity.
 * It contains a thread which publishes a deadman switch and the velocity
 */
class mobile_robot_wrapper : public QThread
{
    Q_OBJECT
private:

    ros::NodeHandle nodehandle;
    ros::Publisher cmd_vel_pub;
    ros::Publisher dead_man_pub;
    ros::Publisher tipper_pub;
    ros::Publisher nav_mode_request;
    ros::Publisher behaviour_mode_pub;
    ros::Publisher wpt_nav_route_pub;
    ros::Subscriber cmd_vel_sub;
    ros::Subscriber pos_sub;
    ros::Subscriber battery_sub;
    ros::Subscriber tipper_sub;
    ros::Subscriber nav_mode_sub;
    ros::Subscriber wpt_nav_complete_sub;
    ros::ServiceClient marker_pose_reset_client;
    ros::Rate loop_rate;
    double vel_linear;
    double vel_angular;
    const double lin_inc;
    const double ang_inc;
    geometry_msgs::TwistStamped twist_msg;
    msgs::BoolStamped dead_man;
    std_msgs::Int16 desired_tipper_pos;
    bool thread_running;
    double supply_voltage_scale_factor;
    bool busy;

    void vel_callback(const geometry_msgs::TwistStampedPtr &vel);
    void pos_callback(const tf2_msgs::TFMessagePtr &pos);
    void battery_callback(const msgs::nmeaPtr &battery);
    void tipper_callback(const std_msgs::Int16Ptr &tipper);
    void nav_mode_callback(const std_msgs::StringPtr &type);
    void wpt_complete_callback(const msgs::BoolStampedPtr &complete);
    mobile_robot_status display_status;

public:
    mobile_robot_wrapper();
signals:
    void status(std::string);
    /**
     * @brief busy
     * The respond used to limit how fast the interface can set the velocity.
     */
    void status(mobile_robot_status);
public slots:
    /**
     * @brief move - takes a direction as input and sets the velocity
     * @param direction - string containing left, right, forward, reverse or brake
     */
    void move(std::string direction);
    /**
     * @brief ctrl_mr - starts and stops the thread
     * @param enable - true starts the thread, false stops it
     */
    void ctrl_mr(int enable);
    void about_to_quit();
protected:
    /**
     * @brief run - the thread that sends the velocity
     */
    void run();
};
