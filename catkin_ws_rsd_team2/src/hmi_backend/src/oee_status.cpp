#include "oee_status.h"
#include <iostream>
#include <algorithm>
oee_status::oee_status() :
    nodehandle("~"),
    bad_robot_client(nodehandle.serviceClient<oee::bad_robot>("/oee/bad_robot_service")),
    bad_mobile_robot_client(nodehandle.serviceClient<oee::bad_mobile_robot>("/oee/bad_mobile_robot")),
    runtime_downtime_client(nodehandle.serviceClient<oee::runtime_downtime>("")),
    target_counter{0, 0},
    total_count{0, 0},
    good_count{0, 0},
    run_time{"", ""},
    setup_time{"", ""},
    down_time{"", ""},
    total_time{"", ""}
{}

std::string oee_status::toString()
{
    std::ostringstream msg;
    msg << "target_counter:"        << target_counter	<< '\n'  // 0
        << "total_count:"      		<< total_count      << '\n'  // 1
        << "good_count:"  		  	<< good_count   	<< '\n'  // 2
        << "run_time:"           	<< run_time         << '\n'  // 3
        << "setup_time:"          	<< setup_time       << '\n'  // 4
        << "down_time:"            	<< down_time        << '\n'  // 5
        << "total_time:"        	<< total_time       << '\n'; // 6
   return msg.str();
}

void oee_status::adjust_parameter(std::string changes)
{
//button_type:RM_Run_Time_button
//button_type:RC_Run_Time_button
//button_type:RM_Down_Time_button
//button_type:RC_Down_Time_button
//button_type:RM_BAD_1
//button_type:RC_BAD_1
    if(changes.find("Run_Time_button") != std::string::npos)
    {
        oee::runtime_downtime srv;

        if(changes.find("RM_Run_Time_button") != std::string::npos)
        {
            srv.request.requested_time = "run_mobile";
        }
        if(changes.find("RC_Run_Time_button") != std::string::npos)
        {
            srv.request.requested_time = "run_robot";
        }

        if(runtime_downtime_client.call(srv))
        {
            std::string response = static_cast<std::string>(srv.response.current_time);
        }

    }
    if(changes.find("Down_Time") != std::string::npos)
    {
        oee::runtime_downtime srv;
        if(changes.find("RM_Down_Time_button") != std::string::npos)
        {
            srv.request.requested_time = "down_mobile";
        }
        if(changes.find("RC_Down_Time_button") != std::string::npos)
        {
            srv.request.requested_time = "down_robot";
        }

        if(runtime_downtime_client.call(srv))
        {
            std::string response = static_cast<std::string>(srv.response.current_time);
        }
    }
    if(changes.find("RM_BAD_1") != std::string::npos)
    {
        oee::bad_mobile_robot srv;

        if(bad_mobile_robot_client.call(srv))
        {
            int response = srv.response.current_count;
        }
    }
    if(changes.find("RC_BAD_1") != std::string::npos)
    {
        oee::bad_robot srv;

        if(bad_robot_client.call(srv))
        {
            int response = srv.response.current_count;
        }
    }
}
