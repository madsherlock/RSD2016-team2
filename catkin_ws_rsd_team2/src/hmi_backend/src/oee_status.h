#pragma once
#include <string>
#include <sstream>
#include <ros/ros.h>
#include "oee/bad_robot.h"
#include "oee/bad_mobile_robot.h"
#include "oee/runtime_downtime.h"

/**
 * @brief The oee_status struct
 * Data structure for transmitting oee status
 */

struct oee_status{
private:

    ros::NodeHandle nodehandle;
    ros::ServiceClient bad_robot_client;
    ros::ServiceClient bad_mobile_robot_client;
    ros::ServiceClient runtime_downtime_client;

public:
    oee_status();
    /**
     * @brief 
     * 
     * 
     */
    int target_counter[2];
    /**
     * @brief 
     * 
     * 
     */
    int total_count[2];
    /**
     * @brief 
     * 
     * 
     */
    int good_count[2];
    /**
     * @brief 
     * 
     * 
     */  
    std::string run_time[2];
    /**
     * @brief 
     * 
     * 
     */
    std::string setup_time[2];
    /**
     * @brief 
     * 
     * 
     */
    std::string down_time[2];
    /**
     * @brief 
     * 
     * 
     */
    std::string total_time[2];
    std::string current_navigation_mode;
    /**
     * @brief std::string::toString
     */
    std::string toString();

    void adjust_parameter(std::string changes);
};

