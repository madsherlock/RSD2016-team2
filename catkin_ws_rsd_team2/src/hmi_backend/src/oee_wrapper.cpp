#include "oee_wrapper.h"
#include <unistd.h> //for usleep, remove when possible
#include <istream>
#include <cmath>
#include <bitset>
#include <math.h>

oee_wrapper::oee_wrapper() :
     nodehandle("~"),
     OEE_sub(nodehandle.subscribe("/oee/current_oee", 1, &oee_wrapper::oee_callback, this)),
     loop_rate(100),
//     checked(false),
     send(false),
     thread_running(false),
     display_status()
{
    std::cout << "oee_wrapper constructed" << std::endl;
}

 void oee_wrapper::ctrl_oee(bool enable){
     thread_running = enable;
     ros::spinOnce();
     if(thread_running){
         std::cout << "Thread_running" << std::endl;
         this->start();
     } else {
         std::cout << "Thread_notrunning" << std::endl;
         //vel_linear = 0;
         //vel_angular = 0;
     }
 }

 void oee_wrapper::run(){
     std::cout << "[OEE] started thread" << std::endl;
     while(thread_running)
     {
         loop_rate.sleep();
         if(!thread_running) break;
         //std::cout << "running" << std::endl;

         if(send == false)
         {
             oee_status_wrapper(display_status);
             thread_running=false;
         }
         ros::spinOnce();
     }
     std::cout << "[RC] completed thread" << std::endl;
 }

 void oee_wrapper::oee_callback(const std_msgs::String &mystring){

     std::vector<std::string> vect;
     std::stringstream ss(mystring.data);
     std::string i;

     while (ss >> i)
     {
         vect.push_back(i);
         if (ss.peek() == ',')
             ss.ignore();
     }
//robot 1
//mobile 0
//     total_time(0)
//     run_time_robot(1)
//     run_time_mobile(2)
//     down_time_robot(3)
//     down_time_mobile(4)
//     setup_time(5)
//     total_count_robot(6)
//     total_count_mobile(7)
//     target_count_robot(8)
//     target_count_mobile(9)
//     good_robot(10)
//     bad_robot(11)
//     good_mobile_robot(12)
//     bad_mobile_robot(13)
     display_status.total_time[0] = " "; //vect.at(0);
     display_status.total_time[1] = " "; //vect.at(0);

     display_status.run_time[1] = " "; //vect.at(1);
     display_status.run_time[0] = " "; //vect.at(2);

     display_status.down_time[1] = " "; //vect.at(3);
     display_status.down_time[0] = " "; //vect.at(4);

     display_status.setup_time[0] = " "; //vect.at(5);
     display_status.setup_time[1] = " "; //vect.at(5);

     display_status.total_count[1] = 0; //std::stoi(vect.at(6));
     display_status.total_count[0] = 0; //std::stoi(vect.at(7));

     display_status.target_counter[1] = 0; //std::stoi(vect.at(8));
     display_status.target_counter[0] = 0; //std::stoi(vect.at(9));

     display_status.good_count[1] = 0; //std::stoi(vect.at(10));
     display_status.good_count[0] = 0; //std::stoi(vect.at(12));

 }
