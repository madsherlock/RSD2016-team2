#pragma once

#include <QObject>
#include <iostream>
#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_srvs/Empty.h>
#include <std_srvs/SetBool.h>
#include <string>
#include "oee_status.h"
#include <QThread>

/**
 * @brief The robot_cell_wrapper class publishes the status of
 * robotcell over ROS.
 * It gets a input as QSignal, which updates the velocity.
 * It contains a thread which publishes a deadman switch and the velocity
 */

class oee_wrapper : public QThread
{
    Q_OBJECT

private:

    ros::NodeHandle nodehandle;
    ros::Subscriber OEE_sub;
    ros::Rate loop_rate;
    bool send;
    bool thread_running;
    oee_status display_status;
    void oee_callback(const std_msgs::String &mystring);

public:
    oee_wrapper();
    bool handelsrv();
signals:
    /**
     * @brief status
     * @param std::string
     * used by qt
     */
    void status(std::string);
    /**
     * @brief status
     * @param oee_status
     * used to extract the information.
     */
    void oee_status_wrapper(oee_status);
public slots:
    /**
     * @brief ctrl_rc - starts and stops the thread
     * @param enable - true starts the thread, false stops it
     */
    void ctrl_oee(bool enable);
    void update_oee_status_from_controller(oee_status status_oee){display_status = status_oee; std::cout << "oee - got reply - from controller"<< std::endl;}

protected:
/**
 * @brief run - the thread that sends the velocity
 */
    void run();

};

