#include "robot_cell_status.h"
#include <iostream>
#include <algorithm>
robot_cell_status::robot_cell_status() :
    conveyor_busy(false),
    conveyor_speed(NONE),
    conveyor_powered(OFF),
    conveyor_direction(BACKWARDS),
    robot_arm_q("0.0;0.0;0.0;0.0;0.0;0.0"),
    gripper_open("true"),
    robot_cell_active(true),
    cellstatus("Status: Automatic")
{}


std::string string_to_int(int state)
{
    if(state == FAST)
    {
        return "FAST";
    }
    if(state == MEDIUM)
    {
        return "MEDIUM";
    }
    if(state == SLOW)
    {
        return "SLOW";
    }
    //if(state == NONE)
    return "NONE";
}

std::string robot_cell_status::toString()
{
    std::ostringstream msg;
    msg << "conveyor_busy:"         << (conveyor_busy ? "manual" : "auto")              << '\n'  // 0
        << "conveyor_speed:"        << string_to_int(conveyor_speed)                    << '\n'  // 1
        << "conveyor_powered:"      << (conveyor_powered ? "ON" : "OFF")                << '\n'  // 2
        << "conveyor_direction:"    << (conveyor_direction ? "FORWARD" : "BACKWARDS")   << '\n'  // 3
        << "robot_arm_q:"           << robot_arm_q                                      << '\n'  // 4
        << "gripper_open:"          << gripper_open                                     << '\n'  // 5
        << "cellstatus:"            << cellstatus                                       << '\n'  // 6
        << "cell active:"        << (robot_cell_active ? "ON" : "OFF")               << '\n'; // 7
   return msg.str();
}

void robot_cell_status::adjust_parameter(std::string changes)
{
    if(changes == "speedF1")
    {
        conveyor_powered = ON;
        conveyor_direction = FORWARD;
        conveyor_speed = SLOW;
    }

    if(changes == "speedF2")
    {
        conveyor_powered = ON;
        conveyor_direction = FORWARD;
        conveyor_speed = MEDIUM;
    }

    if(changes == "speedF3")
    {
        conveyor_powered = ON;
        conveyor_direction = FORWARD;
        conveyor_speed = FAST;
    }

    if(changes == "speedB1")
    {
        conveyor_powered = ON;
        conveyor_direction = BACKWARDS;
        conveyor_speed = SLOW;
    }

    if(changes == "speedB2")
    {
        conveyor_powered = ON;
        conveyor_direction = BACKWARDS;
        conveyor_speed = MEDIUM;
    }

    if(changes == "speedB3")
    {
        conveyor_powered = ON;
        conveyor_direction = BACKWARDS;
        conveyor_speed = FAST;
    }

    if(changes == "speed0")
    {
        conveyor_powered = ON;
        conveyor_speed = NONE;
        conveyor_direction = BACKWARDS;
    }

    if(changes == "Deactive")
    {
        conveyor_powered = OFF;
        conveyor_speed = NONE;
        conveyor_direction = BACKWARDS;
    }

    if(changes.find("Set_Arm:") != std::string::npos)
    {
        std::string set_arm_changes = changes;
        std::string s = "Set_Arm:";

        std::string::size_type i = set_arm_changes.find(s);

        if(i != std::string::npos)
        {
            set_arm_changes.erase(i, s.length());
        }
        std::replace(set_arm_changes.begin(), set_arm_changes.end(), ',', ';');
        robot_arm_q = set_arm_changes;
        std::cout << "set_arm_changes: " << robot_arm_q << std::endl;
    }
    if(changes == "openGripper")
    {
        gripper_open = true;
        std::cout << "gripper_open true: " << gripper_open << std::endl;
    }
    if(changes == "closeGripper")
    {
        gripper_open = false;
        std::cout << "gripper_open false: " << gripper_open << std::endl;
    }

    if(changes == "emergencyStop"){
        robot_cell_active = false;
        std::cout << "Emergency stop pressed!" << std::endl;
    }

}
