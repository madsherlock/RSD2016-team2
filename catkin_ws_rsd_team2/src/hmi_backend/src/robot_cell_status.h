#pragma once
#include <string>
#include <sstream>
/**
 * @brief The robot_cell_status struct
 * Data structure for transmitting robot cell status
 */

#define FAST        300
#define MEDIUM      200
#define SLOW        100
#define NONE        000
#define ON          1
#define OFF         0
#define FORWARD     1
#define BACKWARDS   0

struct robot_cell_status{
public:
    robot_cell_status();
    /**
     * @brief conveyor_busy
     * true =  HMI is currently handling a message in manual mode
     * false = HMI is not currently handling a message in manual mode, could be in auto mode.
     */
    bool conveyor_busy;
    /**
     * @brief conveyor_speed
     * Variable containing the current speed of the conveyorbelt
     * Speed possibilities [FAST;MEDIUM;SLOW]
     */
    int conveyor_speed;
    /**
     * @brief conveyor_powered
     * A boolean determining whether the conveyor is powered or not.
     * true = ON
     * false = OFF
     */
    bool conveyor_powered;
    /**
     * @brief conveyor_direction
     * A boolean determing the direction the conveyor is moving
     * true = FORWARD
     * false = BACKWARDS
     */
    bool conveyor_direction;
    /**
     *  @brief std::string robot_arm_q
     *  
     */   
    std::string robot_arm_q;
    /**
     *  @brief bool gripper_oppen
     *  true = open
     *  false = closed
     */
    bool gripper_open;
    /**
     *  @brief bool robot_cell_active
     *  true = robot cell is running
     *  false = robot cell is emergency stopped
     */
    bool robot_cell_active;
    /**
     *  @brief std::string cellstatus
     *
     */
    std::string cellstatus;
    std::string current_navigation_mode;
    //bool emergency_stop_activated;
    /**
     * @brief std::string::toString
     */
    std::string toString();
    void adjust_parameter(std::string changes);

};
