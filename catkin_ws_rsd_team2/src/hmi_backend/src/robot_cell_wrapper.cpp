#include "robot_cell_wrapper.h"
#include <unistd.h> //for usleep, remove when possible
#include <istream>
#include <cmath>
#include <bitset>
#include <math.h>

robot_cell_wrapper::robot_cell_wrapper() :
    nodehandle("~"),
    getConveyor_configurations(nodehandle.serviceClient<conv_service::get>("/conveyor_belt_get_service")),
    setConveyor_configurations(nodehandle.serviceClient<conv_service::set>("/conveyor_belt_set_service")),
    loop_rate(100),
    robot_arm_client(nodehandle.serviceClient<kuka_ros::getConfiguration>("/KukaNode/GetConfiguration")),
    robot_arm_set_client(nodehandle.serviceClient<kuka_ros::setConfiguration>("/KukaNode/SetConfiguration")),
    gripper_set_client(nodehandle.serviceClient<kuka_ros::setGripper>("/KukaNode/SetGripper")),
    gripper_get_client(nodehandle.serviceClient<kuka_ros::getIsGripOpen>("/KukaNode/IsGripOpen")),
    stop_robot_arm_client(nodehandle.serviceClient<kuka_ros::stopRobot>("/KukaNode/StopRobot")),
    cellstatus_client(nodehandle.serviceClient<rc_state_listener::get_current_event>("/rc_state_listener/Get_Current_Event")),
    emergency_stop_sub(nodehandle.subscribe("/fmSafety/emergency_stop", 1, &robot_cell_wrapper::safety_system_callback, this)),
    checked(false),
    send(false),
    thread_running(false),
    display_status()
{
    std::cout << "Robot_cell_wrapper constructed" << std::endl;
}

int string_to_int(int a, int b)
{
    if(a == 1 && b == 1)
    {
        return FAST;
    }
    if(a == 1 && b == 0)
    {
        return MEDIUM;
    }
    if(a == 0 && b == 1)
    {
        return SLOW;
    }
    if(a == 0 && b == 0)
    {
        return NONE;
    }
    else
    {
        return NONE;
    }
}

bool robot_cell_wrapper::handelsrv()
{
    kuka_ros::getConfiguration srv_robot_arm;
    kuka_ros::getIsGripOpen srv_gripper_open;
    rc_state_listener::get_current_event srv_cellstatus;

    if (robot_arm_client.call(srv_robot_arm))
    {
        std::stringstream str;
        std::cout << "Hey you got a Q and it is: " << srv_robot_arm.response << std::endl;
        str << srv_robot_arm.response.q[0]*(M_PI/180.0)
            << ';' << srv_robot_arm.response.q[1]*(M_PI/180.0)
            << ';' << srv_robot_arm.response.q[2]*(M_PI/180.0)
            << ';' << srv_robot_arm.response.q[3]*(M_PI/180.0)
            << ';' << srv_robot_arm.response.q[4]*(M_PI/180.0)
            << ';' << srv_robot_arm.response.q[5]*(M_PI/180.0);

        std::cout <<"Output: "<< str.str() << std::endl;
        display_status.robot_arm_q = str.str();
    }
    if (gripper_get_client.call(srv_gripper_open))
    {
        //std::cout << "Is the gripper open?: " << srv_gripper_open.response << std::endl;
        display_status.gripper_open = srv_gripper_open.response.isGripOpen;
        std::cout << "Is the gripper open?: " << display_status.gripper_open << std::endl;
    }
    if (cellstatus_client.call(srv_cellstatus))
    {
        display_status.cellstatus = srv_cellstatus.response.current_event;
        std::cout << "Cell Status: " << display_status.cellstatus << std::endl;
	ROS_ERROR("call service get_current_event");
    }
    else
    {
        ROS_ERROR("Failed to call service");
        return false;
    }
    return true;
}

bool robot_cell_wrapper::set_robot_cell()
{
    kuka_ros::setConfiguration srv_robot_arm_set;
    kuka_ros::setGripper srv_gripper_set;

    std::stringstream s(display_status.robot_arm_q);
    double d = 0.0;
    int i = 0;
    while(s >> d)
    {
        std::cout << "Q to send is: " << d << std::endl;
        srv_robot_arm_set.request.speed[i] = 5;
        srv_robot_arm_set.request.q[i++] = d;
        if(s.peek() == ';' || s.peek() == ' ')
            s.ignore();

    }
    if(display_status.gripper_open)
    {
        srv_gripper_set.request.grip[0] = 0;
    }
    else
    {
        srv_gripper_set.request.grip[0] = 1;
    }

    std::cout << "Q to send is: " << srv_robot_arm_set.request << std::endl;
    stop_robot_arm();
    if (robot_arm_set_client.call(srv_robot_arm_set))
    {
        std::cout << "Hey you got a Q and it is: " << srv_robot_arm_set.response << std::endl;
    }
    else
    {
        ROS_ERROR("Failed to call service SetConfiguration");
        return false;
    }
    if (gripper_set_client.call(srv_gripper_set))
    {
        std::cout << "Gripper is now open: " << srv_robot_arm_set.response << std::endl;
    }
    else
    {
        ROS_ERROR("Failed to call service SetGripper");
        return false;
    }
    return true;
}

bool robot_cell_wrapper::stop_robot_arm()
{
    kuka_ros::stopRobot srv_stop_robot;

    if(stop_robot_arm_client.call(srv_stop_robot))
    {}
    else
    {
        ROS_ERROR("Failed to call service StopRobot");
        return false;
    }
    return true;
}

std::bitset<2>int_to_bit(int value)
{
    std::bitset<2> state;
    if(value == FAST)
    {
        state.set(1,1);
        state.set(0,1);
    }
    if(value == MEDIUM)
    {
        state.set(1,0);
        state.set(0,1);
    }
    if(value == SLOW)
    {
        state.set(1,1);
        state.set(0,0);
    }
    if(value == NONE)
    {
        state.set(1,0);
        state.set(0,0);
    }
    return state;
}

void robot_cell_wrapper::ctrl_rc(bool enable){
    thread_running = enable;
    ros::spinOnce();
    if(thread_running){
        std::cout << "Thread_running" << std::endl;
        this->start();
    } else {
        std::cout << "Thread_notrunning" << std::endl;
        //vel_linear = 0;
        //vel_angular = 0;
    }
}

void robot_cell_wrapper::run(){
    std::cout << "[RC] started thread" << std::endl;
    bool all_good;
    while(thread_running)
    {
        loop_rate.sleep();
        if(!thread_running) break;
        //std::cout << "running" << std::endl;
        //std::cout <<"current navi_mode: " << display_status.current_navigation_mode << std::endl;
        if(display_status.current_navigation_mode == "manual" && send == false)
        {   /*
            [0  0   0   0   Start/stop   Direction   Speed1  Speed2]
            */
            all_good = set_robot_cell();
            if(!all_good)
            {
                ROS_ERROR("ROBOT CELL NOT SET!!");
            }
            std::bitset<4> msg_bit;
            msg_bit.set(3,static_cast<int>(display_status.conveyor_powered));
            msg_bit.set(2,static_cast<int>(display_status.conveyor_direction));
            msg_bit.set(1,int_to_bit(display_status.conveyor_speed)[1]);
            msg_bit.set(0,int_to_bit(display_status.conveyor_speed)[0]);
            std_msgs::Int8 msg;
            msg.data = int(msg_bit.to_ulong());

            conv_service::set srv;
            srv.request.request = int(msg_bit.to_ulong());//msg.data;
            if(setConveyor_configurations.call(srv))
            {
                std::cout << "Got response" << std::endl;
                std::cout << "Response is: " << static_cast<std::string>(srv.response.response) << std::endl;
                std::cout << "Updating status: " << std::endl;
                std::string response = static_cast<std::string>(srv.response.response);
                display_status.conveyor_powered  = std::stoi(std::string(1,response[0]));
                display_status.conveyor_direction = std::stoi(std::string(1,response[1]));
                display_status.conveyor_speed = string_to_int(std::stoi(std::string(1,response[2])) , std::stoi(std::string(1,response[3])));

            }

            status_wrapper(display_status);
            thread_running=false;
        }
        if(display_status.current_navigation_mode == "auto" && send ==  false)
        {
            std::cout << "In automode" << std::endl;

            all_good = handelsrv();

            if(!all_good)
            {
                ROS_ERROR("ROBOT CELL NOT UP TO DATE!!");
            }

            conv_service::get srv;

            if(getConveyor_configurations.call(srv))
            {
                std::cout << "Response is: " << static_cast<std::string>(srv.response.response) << std::endl;
                std::string response = static_cast<std::string>(srv.response.response);
                display_status.conveyor_powered  = std::stoi(std::string(1,response[0]));
                display_status.conveyor_direction = std::stoi(std::string(1,response[1]));
                display_status.conveyor_speed = string_to_int(std::stoi(std::string(1,response[2])) , std::stoi(std::string(1,response[3])));

            }

            status_wrapper(display_status);
            thread_running=false;

        }
        ros::spinOnce();
    }
    std::cout << "[RC] completed thread" << std::endl;
}

//false means robot is off, stop is pressed, red light, etc.
void robot_cell_wrapper::safety_system_callback(const std_msgs::BoolPtr &stop_pressed){
    display_status.robot_cell_active = !stop_pressed->data;
}
