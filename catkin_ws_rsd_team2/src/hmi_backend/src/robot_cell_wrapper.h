#pragma once

#include <QObject>
#include <iostream>
#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_srvs/Empty.h>
#include <std_srvs/SetBool.h>
#include <string>
#include "robot_cell_status.h"
#include "kuka_ros/getConfiguration.h"
#include "kuka_ros/setConfiguration.h"
#include "kuka_ros/getIsGripOpen.h"
#include "kuka_ros/setGripper.h"
#include "kuka_ros/stopRobot.h"
#include "conv_service/get.h"
#include "conv_service/set.h"
#include "rc_state_listener/get_current_event.h"
#include <QThread>

/**
 * @brief The robot_cell_wrapper class publishes the status of
 * robotcell over ROS.
 * It gets a input as QSignal, which updates the velocity.
 * It contains a thread which publishes a deadman switch and the velocity
 */

class robot_cell_wrapper : public QThread
{
    Q_OBJECT

private:

    ros::NodeHandle nodehandle;
    ros::ServiceClient getConveyor_configurations;
    ros::ServiceClient setConveyor_configurations;
    ros::Rate loop_rate;
    ros::ServiceClient robot_arm_client;
    ros::ServiceClient robot_arm_set_client;
    ros::ServiceClient gripper_set_client;
    ros::ServiceClient gripper_get_client;
    ros::ServiceClient stop_robot_arm_client;
    ros::ServiceClient cellstatus_client;
    ros::Subscriber emergency_stop_sub;
    bool checked;
    bool send;
    bool thread_running;
    std_msgs::Empty conveyor_msg;
    robot_cell_status display_status;
    
    void safety_system_callback(const std_msgs::BoolPtr &stop_pressed);

public:
    robot_cell_wrapper();
    bool handelsrv();
    bool set_robot_cell();
    bool stop_robot_arm();
signals:
    /**
     * @brief status
     * @param std::string
     * used by qt
     */
    void status(std::string);
    /**
     * @brief status
     * @param robot_cell_status
     * used to extract the information.
     */
    void status_wrapper(robot_cell_status);
public slots:
    /**
     * @brief ctrl_rc - starts and stops the thread
     * @param enable - true starts the thread, false stops it
     */
    void ctrl_rc(bool enable);
    void update_robot_cell_status_from_controller(robot_cell_status status_rc){display_status = status_rc; std::cout << "RC - got reply - from controller"<< std::endl;}
	
protected:
/**
 * @brief run - the thread that sends the velocity
 */
    void run();

};
