 #include "socket_wrapper.hpp"

socket_wrapper::socket_wrapper(int argc, char *argv[]) :
    QCoreApplication(argc,argv)
{
}
void socket_wrapper::emit_close(void){
    emit close();
}
void socket_wrapper::emit_connected(void){
    emit user_connected();
}
void socket_wrapper::emit_protocol(int protocol){
    emit protocol_message_received(protocol);
}
void socket_wrapper::emit_message(std::string message){
    emit message_received(message);
}
void socket_wrapper::emit_disconnected(void){
    emit user_disconnected();
}

