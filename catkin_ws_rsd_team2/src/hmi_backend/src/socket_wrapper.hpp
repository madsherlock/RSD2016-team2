#pragma once
#include <iostream>
#include <QCoreApplication>
#include <string>

/**
 * @brief The socket_wrapper class
 * This class allows the websocket server to emit Qt signals even though they are static variables
 * Consider this a singleton which should not exist in a perfect world.
 */

class socket_wrapper : public QCoreApplication {
    Q_OBJECT
public:
    socket_wrapper(int argc, char *argv[]);
signals:
    void close(void);
    void protocol_message_received(int);
    void message_received(std::string);
    void user_connected(void);
    void user_disconnected(void);
public:
    //wrappers for signals
    void emit_close(void);
    void emit_connected(void);
    void emit_protocol(int protocol);
    void emit_message(std::string message);
    void emit_disconnected();
};
