#include "utils.h"

double battery_percent(const msgs::nmeaPtr& battery, double supply_voltage_scale_factor){
    double voltage, pct;
    voltage = std::stod(battery->data[3]) * supply_voltage_scale_factor;
    if( voltage > 12.8 ) {
        pct = 100;
    } else if(voltage > 12.6) {
        pct = (100-75)*((voltage-12.6)/(12.8-12.6))+75;
    } else if(voltage > 12.3) {
        pct = (75-50)*((voltage-12.3)/(12.6-12.3))+50;
    } else if(voltage > 12.0) {
        pct = (50-25)*((voltage-12.0)/(12.3-12.0))+25;
    } else if(voltage > 11.8) {
        pct = (25)*((voltage-11.8)/(12.0-11.8));
    } else {
        pct = 0;
    }
    return pct;
}
