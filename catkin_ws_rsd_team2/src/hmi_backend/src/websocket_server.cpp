#include "websocket_server.hpp"
#include "settings.h"

//default values for static strings
std::string wss::website_path = "/var/www/";
std::string wss::website_file = "index.html";
std::string wss::outgoing_message = "msg";
std::string wss::incomming_message = "msg";

std::vector<std::string> wss::incomming_match;
std::vector<wss::per_session_data_simple *> wss::all_users;
std::vector<lws *> wss::all_sockets;
socket_wrapper wss::wrapper(0,0);

volatile int force_exit = 0;

void sighandler(int sig){
    if(sig == 2){ //handle ^C
        force_exit = 1;
    }
}

/**
 * mount a filesystem directory into the URL space at /
 * point it to our /usr/share directory with our assets in
 * stuff from here is autoserved by the library
 */

static struct lws_http_mount mount = {
    NULL,		    /* linked-list pointer to next*/
    "/",		    /* mountpoint in URL namespace on this vhost */
    "/var/www/",    /* where to go on the filesystem for that */
    "index.html",	/* default filename if none given */
    NULL,NULL,NULL,NULL,0,
    0,0,0,0,0,
    LWSMPRO_FILE,	/* mount type is a directory in a filesystem */
    1,		/* strlen("/"), ie length of the mountpoint */
};

wss::wss() :
    settings(QSettings::NativeFormat, QSettings::UserScope, ORGANIZATION , APPLICATION),
    context()
{
    website_path = (settings.value("Website/path",website_path.c_str()).toString().toStdString());
    website_file = (settings.value("Website/file",website_file.c_str()).toString().toStdString());
    std::cout << "Reading config from: " << settings.fileName().toStdString() << std::endl;
    // server url will be http://localhost:7681 (9000 "default")
    struct lws_context_creation_info info;
    memset(&info, 0, sizeof info);
    info.port = settings.value("Network/port",9000).toInt();
    info.iface = NULL;
    info.protocols = protocols;
    info.ssl_cert_filepath = NULL;
    info.ssl_private_key_filepath = NULL;
    info.ssl_ca_filepath = NULL;
    info.gid = -1;
    info.uid = -1;
    info.max_http_header_pool = 16;
    info.extensions = NULL;
    info.timeout_secs = 5;
    info.ssl_cipher_list = "ECDHE-ECDSA-AES256-GCM-SHA384:"
            "ECDHE-RSA-AES256-GCM-SHA384:"
            "DHE-RSA-AES256-GCM-SHA384:"
            "ECDHE-RSA-AES256-SHA384:"
            "HIGH:!aNULL:!eNULL:!EXPORT:"
            "!DES:!MD5:!PSK:!RC4:!HMAC_SHA1:"
            "!SHA1:!DHE-RSA-AES128-GCM-SHA256:"
            "!DHE-RSA-AES128-SHA256:"
            "!AES128-GCM-SHA256:"
            "!AES128-SHA256:"
            "!DHE-RSA-AES256-SHA256:"
            "!AES256-GCM-SHA384:"
            "!AES256-SHA256";
    //when there is external javascript / other files, mount them
    mount.origin = settings.value("Website/path","/var/www/").toString().toStdString().c_str();
    mount.def = settings.value("Website/file","index.html").toString().toStdString().c_str();
    info.mounts = &mount;
    std::cout << "Mounting: " << info.mounts->origin << " + " << mount.def << std::endl;
    info.options = 0;
    /*
      info.options = 0 | LWS_SERVER_OPTION_EXPLICIT_VHOSTS;
      lws_create_vhost(context,&info);
    */
    context = lws_create_context(&info);
    // create libwebsocket context representing this server

    if (context == NULL) {
        lwsl_err("libwebsocket init failed\n");
        exit(-1);
    }
    lwsl_notice("Starting server...\n");
}

std::string get_mimetype(std::string file)
{
    if (file.find(".ico") != std::string::npos)
        return "image/x-icon";
    if (file.find(".png") != std::string::npos)
        return "image/png";
    if (file.find(".html") != std::string::npos)
        return "text/html";
    if (file.find(".js") != std::string::npos)
        return "application/javascript";
    return "";
}

int wss::callback_http(struct lws *wsi,
                       enum lws_callback_reasons reason, void *user,
                       void *in, size_t len)
{
    int n;
    std::string buffer;
    std::string mime_t;

    switch (reason) {
    case LWS_CALLBACK_HTTP:{
        char name[100], rip[50];
        lws_get_peer_addresses(wsi, lws_get_socket_fd(wsi), name, sizeof(name), rip, sizeof(rip));
        std::ostringstream ss;
        ss << name << '(' << rip << ')';

        // Serve file
        std::string input(static_cast<const char*>(in),strlen(static_cast<const char *>(in)));
        buffer = website_path;
        if(input == "/"){ //workaround because I couldn't get vhost to work
            lwsl_notice("HTTP connect from %s\n", ss.str().c_str());
            input = website_file;
        }
        buffer += input;
        mime_t = get_mimetype(buffer);

        n = lws_serve_http_file(wsi, buffer.c_str(), mime_t.c_str(), NULL, 0);

        if (n < 0 || ((n > 0) && lws_http_transaction_completed(wsi))){
            return -1; // error or can't reuse connection: close the socket
        }else{
            // Success (do nothing)
        }
        break;
    }
    case LWS_CALLBACK_HTTP_BODY:
        buffer = std::string(static_cast<const char*>(in),strlen(static_cast<const char *>(in)));
        lwsl_notice("LWS_CALLBACK_HTTP_BODY: %s... len %d\n",buffer.c_str(), buffer.length());
        if(buffer.length() != len){
            lwsl_err("HTTP: wrong string length %d", reinterpret_cast<size_t>(user));
        }
        break;
    case LWS_CALLBACK_HTTP_BODY_COMPLETION:
        lwsl_notice("LWS_CALLBACK_HTTP_BODY_COMPLETION\n");
        lws_return_http_status(wsi, HTTP_STATUS_OK, NULL);
        break;
    case LWS_CALLBACK_HTTP_FILE_COMPLETION:
        lwsl_info("LWS_CALLBACK_HTTP_FILE_COMPLETION\n");
        break;
    case LWS_CALLBACK_HTTP_WRITEABLE:
        lwsl_info("LWS_CALLBACK_HTTP_WRITEABLE\n");
        break;
    case LWS_CALLBACK_ESTABLISHED_CLIENT_HTTP:
        break;
    case LWS_CALLBACK_CLOSED_HTTP:
        break;
    case LWS_CALLBACK_RECEIVE_CLIENT_HTTP:
        break;
    case LWS_CALLBACK_COMPLETED_CLIENT_HTTP:
        break;
    default:
        break;
    }

    return 0;
}

void wss::new_message(std::string msg){
    for(auto &user : all_users){
        user->msg_queue->push(msg);
    }
}

int wss::callback_simple(struct lws *wsi,
                         enum lws_callback_reasons reason,
                         void *user, void *in, size_t len)
{
    struct per_session_data_simple *pss = static_cast<per_session_data_simple *>(user);
    int write_lenght;
    std::string buffer;
    unsigned char buf[LWS_PRE + 512];
    unsigned char *p = &buf[LWS_PRE];
    switch (reason) {
    case LWS_CALLBACK_ESTABLISHED:
        lwsl_notice(" Connection established\n");
        all_sockets.push_back(wsi);
        all_users.push_back(pss);
        pss->msg_queue = new std::queue<std::string>;
        wrapper.emit_connected();
        break;
    case LWS_CALLBACK_SERVER_WRITEABLE:
        if(!pss->msg_queue->empty()){
            buffer = pss->msg_queue->front();
            strcpy(reinterpret_cast<char*>(p),buffer.c_str());
            std::cout << "writing: ";
            if(buffer.size() < 30){
                std::cout << p << std::endl;
            } else {
                std::cout.write(&buffer[0],30);
                std::cout << std::endl;
            }
            pss->msg_queue->pop();
            write_lenght = lws_write(wsi,p,buffer.length(),LWS_WRITE_TEXT);
            if(write_lenght < static_cast<int>(buffer.length())){
                lwsl_err("Error %d writing to (DI) socket\n",buffer.length());
            }
        }
        break;
    case LWS_CALLBACK_RECEIVE:{
        incomming_message = std::string(static_cast<const char*>(in),strlen(static_cast<const char *>(in)));
        if(len != incomming_message.length()){
            lwsl_err("User gave wrong length %d vs %d\n", len, buffer.length());
        }
        bool protocol_message = false;
        //        check if the message is a protocol message
        for (size_t i = 0; i < incomming_match.size(); ++i) {
            if(incomming_message == incomming_match.at(i)){
                lwsl_notice("Incomming Protocol Message: %d\n",i);
                wrapper.emit_protocol(i);
                protocol_message = true;
                break;
            }
        }
        //if it is not, let the receiver handle it
        if(!protocol_message){
            lwsl_notice("Incomming Message: %s\n",incomming_message.c_str());
            wrapper.emit_message(incomming_message);
        }
        break;
    }
    case LWS_CALLBACK_CLOSED:{
        lwsl_notice("Connection closed\n");
        size_t placeholder = reinterpret_cast<size_t>(pss);
        delete pss->msg_queue;
        for (size_t i = 0; i < all_users.size(); ++i) {
            size_t comp = reinterpret_cast<size_t>(all_users.at(i));
            if(comp == placeholder){
                all_users.erase(all_users.begin()+i);
                all_sockets.erase(all_sockets.begin()+i);
                break;
            }
        }
        wrapper.emit_disconnected();
        break;
    }
    default:
        break;
    }
    return 0;
}

void wss::run(){
    // Server Thread: infinite loop, to end this server send SIGTERM. (CTRL+C)
    signal(SIGINT, sighandler);
    int n = 0;
    while (n >= 0 && !force_exit) {
        if(all_users.size()){
            for (size_t i = 0; i < all_sockets.size(); ++i) {
                if(all_users.at(i)->msg_queue->size()){
                    lws_callback_on_writable_all_protocol(context,&protocols[SIMPLE_PROTOCOL]);
                }
            }
        }
        n = lws_service(context,50);
        /*\ libwebsocket_service will process all waiting events with their    |*|
        |*| callback functions and then wait 50 ms.                            |*|
        |*| (this is a single threaded webserver and this will keep our server |*|
        |*| from generating load while there are not requests to process)      \*/
    }
    lws_cancel_service(context);
    lws_context_destroy(context);
    lwsl_notice("WebSocket Server exited cleanly\n");
    wrapper.emit_close();
}
