#ifndef _WEBSOCKETSERVER_H
#define _WEBSOCKETSERVER_H

#include <cstdio>
#include <cstdlib>
#include <csignal>
#include <libwebsockets.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <QSettings>
#include <QThread>
#include <queue>

#include "socket_wrapper.hpp"
/**
 * @brief sighandler - correctly shutdown if user press ctrl-c
 * @param sig signal event
 */
void sighandler(int sig);
/**
 * @brief The wss class is the websocket server.
 * It handles all websocket communication.
 * It will serve the user the human machine interface and deliver / receive the websocket messages
 * A simple protocol is used for websocket communication
 */
class wss : public QThread{
    Q_OBJECT
    private:
        /// Member functions
        static int callback_http(struct lws*, enum lws_callback_reasons, void*, void*, size_t);
        static int callback_simple(struct lws*, enum lws_callback_reasons, void*, void*, size_t);

        /// Config
        QSettings settings;
        static std::string website_path;
        static std::string website_file;
        static std::string outgoing_message;
        static std::string incomming_message;

        /// Data
        struct lws_context *context;

        struct per_session_data_simple{
            std::queue<std::string> *msg_queue;
        };
        static std::vector<per_session_data_simple *> all_users;
        static std::vector<lws *> all_sockets;
        struct per_session_data__http {
            lws_filefd_type fd;
            unsigned int client_finished:1; //@TODO
        };

        enum protocols_i {
            PROTOCOL_HTTP = 0,          // Allways first
            SIMPLE_PROTOCOL,
            PROTOCOL_COUNT              // Allways last
        };

        struct lws_protocols protocols[3] = {
            /* first protocol must always be HTTP handler */
            {
                "http-only",   // name
                callback_http, // callback
                sizeof(struct per_session_data__http),
                0              ,// per_session_data_size
                0              ,//id
                0               //user
            },
            {
                "simple-protocol", // protocol name - very important!
                callback_simple,   // callback
                sizeof(struct per_session_data_simple),
                40000          ,
                0              ,//id
                0               //user
            },
            { NULL, NULL, 0,0 ,0,0}   /* terminator */
        };
    public:
        /**
         * @brief wrapper - wrapper class so this class can emit variables even though they are static
         */
        static socket_wrapper wrapper;
        static std::vector<std::string> incomming_match;
        wss();
        void run();
public slots:
        /**
         * @brief new_message - sends a message to all users
         * @param msg - the message
         */
        void new_message(std::string msg);
};



#endif //_WEBSOCKETSERVER_H
