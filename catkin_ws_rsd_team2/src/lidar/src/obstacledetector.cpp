#include "obstacledetector.h"
#include "sensor_msgs/LaserScan.h"
#include "laser_geometry/laser_geometry.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

obstacleDetector::obstacleDetector():
     status(),
     obstacle(),
     degreeResolution(1),
     samples_to_center(160),
     angle_threshold(20),
     last_scan(0),
     distance_to_objects(0.2),
     working_range_start(100),
     working_range_end(-100),
     n("~"),
     subs(n.subscribe("/scan", 10, &obstacleDetector::scanCallback,this)),
     obstacle_pub(n.advertise<std_msgs::Bool>("/obstacle_status", 1000)),
     wall(n.advertise<std_msgs::Float64MultiArray>("/nearest_object",1000)), 
     clearance_service_cell(n.advertiseService("/obstacle/charger_clearance", &obstacleDetector::checkClearanceChargerCallback, this)),
     clearance_service_charger(n.advertiseService("/obstacle/robot_cell_clearance", &obstacleDetector::checkClearanceCellCallback, this)),
     loop_rate(10)
{
    status.data = true;
    obstacle.data = std::vector<double> (6,0);
    n.param("distance_to_objects", distance_to_objects,0.2f);
    n.param("lidar_samples_to_center", samples_to_center,160);
    n.param("lidar_degree_resolution", degreeResolution,1);
    n.param("angle_threshold", angle_threshold,20.0f);
    n.param("lidar_working_range_start", working_range_start,-100.0f);
    n.param("lidar_working_range_end", working_range_end,100.0f);
    
}

void obstacleDetector::run(){
    while(ros::ok()){
        obstacle_pub.publish(status);
        wall.publish(obstacle);
        double old_param = distance_to_objects;
        n.param("distance_to_objects",distance_to_objects,0.2f) ;
        if(old_param != distance_to_objects){
            ROS_INFO_STREAM(ros::this_node::getName() << ": Changed the bool distance to " << distance_to_objects);
        }
        ros::spinOnce();
        loop_rate.sleep();
    }
}

int obstacleDetector::number_of_obstacles(std::vector<angular_pos> &positions){
    int n_obstacles = 0;
    for(size_t i = 0; i < last_scan.size(); ++i){
        float dist = last_scan.at(i); //in meters right?
        float angle = static_cast<float>(i-samples_to_center)/static_cast<float>(degreeResolution);
        if( !(std::isnan(dist)) && dist > 0.01){
            for(auto p : positions){
                if(angle < p.ang){
                    if(dist < p.dist){
                        ++n_obstacles;
                    }
                    break;
                }
            }
        }
    }
    return n_obstacles;
}

bool obstacleDetector::checkClearanceCellCallback(lidar::checkClearance::Request  &req,
                                                  lidar::checkClearance::Response &res)
{
    std::vector<angular_pos> positions;
    //negative degrees are to the right
    positions.push_back(angular_pos(85,1.34));
    positions.push_back(angular_pos(-std::asin(0.57/1.14)*180/M_PI,1.43));
    positions.push_back(angular_pos(-std::asin(1.00/1.43)*180/M_PI,1.95));
    positions.push_back(angular_pos(-std::asin(1.69/1.95)*180/M_PI,2.50));
    positions.push_back(angular_pos(-std::asin(2.30/2.50)*180/M_PI,2.90));
    positions.push_back(angular_pos(-85,2.90));
    
    res.obstacles = number_of_obstacles(positions);
    return true;
}

bool obstacleDetector::checkClearanceChargerCallback(lidar::checkClearance::Request  &req,
                                                     lidar::checkClearance::Response &res)
{
    std::vector<angular_pos> positions;
    //negative degrees are to the right
    positions.push_back(angular_pos(85,0.60));
    positions.push_back(angular_pos(-40,0.60));
    
    res.obstacles = number_of_obstacles(positions);
    return true;
}

void obstacleDetector::scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan){
    last_scan = scan->ranges;
    double distance=10000;
    double angle;
    int reduce_noise = 3;
    std::vector<double> _obstacle;
    int counter     = 0;
    int ucounter    = 0;
    int rcounter    = 0;
    int lcounter    = 0;
    double averageD = 0;
    double averageA = 0;
    
    double totRange = 0;
    
    double upahead  = 0;
    double dueRight = 0;
    double dueLeft  = 0;
    
    for(int i=0;static_cast<size_t>(i)<scan->ranges.size();i++){
        float range = scan->ranges[i];
        float th = static_cast<float>(i-samples_to_center)/static_cast<float>(degreeResolution);
        
        //if range is a number, range is not detecting itself and the angle is within -100 and 100 degrees
        if( !(std::isnan(range)) && range > 0.01 && th > working_range_start &&  th < working_range_end){
            
            counter++;
            if( th < angle_threshold/2.0 && th > -angle_threshold/2.0 ){
                upahead+=range;
                ucounter++;
            }
            if( th < -90+angle_threshold/2.0 && th > -90-angle_threshold/2.0 ){
                dueRight+=range;
                rcounter++;
            }
            if( th < 90+angle_threshold/2.0 && th > 90-angle_threshold/2.0 ){
                dueLeft+=range;
                lcounter++;
            }
            
            if(counter%reduce_noise==0){
                averageD+=range;
                averageA+=th;
                totRange+=range;
                averageD=averageD/static_cast<double>(reduce_noise);
                averageA=averageA/static_cast<double>(reduce_noise);
                if (averageD<distance){
                    distance = averageD;
                    angle = th;
                }
                averageD=0;
                averageA=0;
            }else{
                totRange+=range;
                averageD+=range;
                averageA+=th;
            }
        }
    }
    upahead=upahead/static_cast<double>(ucounter);
    dueRight=dueRight/static_cast<double>(rcounter);
    dueLeft=dueLeft/static_cast<double>(lcounter);
    totRange=totRange/static_cast<double>(counter);     
    _obstacle.push_back(distance); //distance to closest object
    _obstacle.push_back(angle);    //angle to closest object
    _obstacle.push_back(totRange); //average distance from -100 to 100 degrees
    _obstacle.push_back(upahead);  //average distance around   0 degrees
    _obstacle.push_back(dueRight); //average distance around -90 degrees
    _obstacle.push_back(dueLeft);  //average distance around  90 degrees
    obstacle.data = _obstacle;
    
    if (distance<distance_to_objects){
        status.data = true;
    }else{
        status.data = false;
    }
}


int main(int argc,char **argv){
    
    ros::init(argc, argv, "lidar");
    obstacleDetector detector;
    detector.run();
    return 0;
}
