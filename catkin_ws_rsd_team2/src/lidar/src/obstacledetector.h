#ifndef OBSTACLEDETECTOR_H
#define OBSTACLEDETECTOR_H
#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float64MultiArray.h"
#include <lidar/checkClearance.h>
#include "sensor_msgs/LaserScan.h"

struct angular_pos{
    float ang;
    float dist;
    angular_pos(float angle=0, float distance=0):ang(angle),dist(distance){}
};

class obstacleDetector
{
public:
    obstacleDetector();
    void run();
    std_msgs::Bool status;
    std_msgs::Float64MultiArray obstacle;
private:
    /**
     * @brief degreeResolution
     * reduce the resolution because why not?
     */
    int degreeResolution;
    int samples_to_center;
    float angle_threshold;
    std::vector<float> last_scan;
    float distance_to_objects;
    float working_range_start;
    float working_range_end;
    ros::NodeHandle n;
    ros::Subscriber subs;
    /**
     * @brief obstacle_pub
     * true if there is any obstacle within 20cm.
     */
    ros::Publisher obstacle_pub;
    /**
     * @breif wall
     * [0] distance to nearest point
     * [1] angle to nearest point 
     * [2] avg distance in               (-100...100) degrees 
     * [3] average distance ahead        (  0) degrees (+- angle_threshold)
     * [4] average distance to the right (-90) degrees (+- angle_threshold)
     * [5] average distance to the left  ( 90) degrees (+- angle_threshold)
     */
    ros::Publisher wall;
    ros::ServiceServer clearance_service_cell;
    ros::ServiceServer clearance_service_charger;
    ros::Rate loop_rate;
    void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
    bool checkClearanceCellCallback(lidar::checkClearance::Request  &req,lidar::checkClearance::Response &res);
    bool checkClearanceChargerCallback(lidar::checkClearance::Request  &req,lidar::checkClearance::Response &res);
    int number_of_obstacles(std::vector<angular_pos> &positions);
};

#endif // OBSTACLEDETECTOR_H
