#include "wallfollower.h"
#include "ros/ros.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/TwistStamped.h"
#include "msgs/BoolStamped.h"
#include "msgs/nmea.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <cmath>

wallFollower::wallFollower() :
    param_speedMax(0.7),
    param_controller_P(1),
    param_controller_I(0),
    param_controller_D(10),
    _integral(0),
    _pre_error(0),
    _distance(0),
    keeping_left(false),
    turn_away(false),
    turn_toward(false),
    current_crowd(0),
    check_situ(false),
    follow_mode('w'),
    n("~"),
    subs(n.subscribe("/nearest_object", 10, &wallFollower::nearCallback,this)),
    movemode(n.subscribe("/fmSignal/lidar_mode",10,&wallFollower::moveCallback,this)),
    turning(),
    reverse(),
    stopping(),
    batt_state(),
    charge_time(),
    voltage(),
    pct(),
    mode("idle"),
    gotMessage(false),
    lidar_data(6,0),
    nearest(),
    pub_cmd_vel(n.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel",1,false)),
    pub_dead_man(n.advertise<msgs::BoolStamped>("/fmSafe/deadman", 1,false)),
    pub_lidar_charger_position(n.advertise<std_msgs::String>("/fmSignal/lidar_position",1000,true)),
    perfect_turn_left_linear_vel(0.2),
    perfect_turn_left_angular_vel(1.0),
    to_charger_linear_vel(0.1)
{
    n.param("perfect_turn_left_linear_vel", perfect_turn_left_linear_vel,0.2);
    n.param("perfect_turn_left_angular_vel", perfect_turn_left_angular_vel,1.0);
    n.param("to_charger_linear_vel", to_charger_linear_vel,0.1);
    
    ROS_INFO_STREAM(ros::this_node::getName() << ": created the wall follower");
}
void wallFollower::run(){
    while(ros::ok()){
        //*  Wall following behaviour
        
        //True right is -90, true centre is 0, true left is 90
        //Angular_movement: 1 is go left, 0 is go forwards, -1 is go right
        if (gotMessage){
            float error;
            float speed=0.2;
            float desired_distance_to_wall_out=0.37; //ideal distance
            float desired_distance_to_wall_in=0.32; //ideal distance
            double turn_intensity = 3;
            //             bool following_wall = false;
            
            if (mode=="toCharger"){
                //                 if(lidar_data[dist]<distance+0.3&&!following_wall){
                //                     following_wall = true;
                //                 }
                if(turn_toward) { //The wall turns away from you
                    ROS_INFO_STREAM(ros::this_node::getName() << ": mode = to charger turn toward l:" << lidar_data[leftDist] << " a: " << lidar_data[ang] << " fd: " << lidar_data[frontDist]);
                    wallFollower::fun_pub_cmd_vel(perfect_turn_left_linear_vel,perfect_turn_left_angular_vel);
                    if(lidar_data[leftDist]<0.4 || lidar_data[leftDist] < 0.6/*|| (lidar_data[ang] > 80 && lidar_data[ang] < 95)*/ ){
                        turn_toward = false;
                    }
                } else if(stopping){ //Second time wall moves away
                        wallFollower::fun_pub_lidar_position("atCharger");
                        wallFollower::fun_pub_cmd_vel(0,0);
                        mode = "idle";
                } else { //The wall continues
                    ROS_INFO_STREAM(ros::this_node::getName() << ": mode = to charger wall continues. ld:" << lidar_data[leftDist] << " d: " << lidar_data[dist] << ", a:" << lidar_data[ang] << " fd: " << lidar_data[frontDist]);
                    if(lidar_data[dist]-_distance>0.2){// && following_wall){
//                     if(lidar_data[dist]>_distance+0.2||lidar_data[ang]>90){// && following_wall){
                        turn_toward = true;
                    }else if(lidar_data[frontDist] < 0.165 && lidar_data[frontDist]!=0){ //because 0 means invalid?
                        stopping = true;
                    }
                    error = turn_intensity*(lidar_data[leftDist]-desired_distance_to_wall_in);
                    
                    // Proportional term
                    double pid_out = static_cast<double>(param_controller_P * error);
                    
                    // Integral term
                    _integral += error;
                    pid_out+= static_cast<double>(param_controller_I * _integral);
                    
                    // Derivative term
                    double derivative = error - _pre_error;
                    pid_out+= static_cast<double>(param_controller_D * derivative);
                    
                    if (pid_out>1){
                        pid_out=1;
                    }else if(pid_out<-1){
                        pid_out=-1;
                    }
                    if (lidar_data[dist]<0.2){
                        speed=0.10;
                    }else{
                        speed=to_charger_linear_vel;
                    }
                    
                    wallFollower::fun_pub_cmd_vel(speed,pid_out);
                    _pre_error = error;
                    _distance = lidar_data[leftDist];
                }
                gotMessage=false;
                
            }else if (mode=="toOutsideBox"){
                //distance = 0.3;                
                if (lidar_data[frontDist]<0.17&&reverse){
                    ROS_INFO_STREAM(ros::this_node::getName() << ": mode = to outside, reverse");
                    wallFollower::fun_pub_cmd_vel(-0.1,0);
                }else if(lidar_data[frontDist]>=0.17&&reverse){
                    reverse=false;
                    turning=true;
                    ROS_INFO_STREAM(ros::this_node::getName() << ": mode = to outside, resetting turning around");
                }else if(turning){
                    ROS_INFO_STREAM(ros::this_node::getName() << ": mode = to outside, turning. " << lidar_data[dist] << "/" << desired_distance_to_wall_out << ", " << lidar_data[ang] << "/" << -85 );
                    if(lidar_data[dist]<desired_distance_to_wall_out&&lidar_data[ang]<-85){
                        turning=false;
                        turn_toward = false; //make sure the pid is called
                    }else{
                        wallFollower::fun_pub_cmd_vel(0,-1);
                    }
                }else{
                    
                    //                     if(lidar_data[dist]<distance+0.3&&!following_wall){
                    //                         following_wall = true;
                    //                     }
                    
                    if(turn_toward&&!stopping){ //The wall turns away from you
                        ROS_INFO_STREAM(ros::this_node::getName() << ": mode = to outside, hugging, too close r:" << lidar_data[rightDist] << " l: " << lidar_data[leftDist]);
                        speed = 0.2;
                        wallFollower::fun_pub_cmd_vel(speed,-1);
                        if(lidar_data[rightDist]<0.4){
                            turn_toward = false;
                        } else if(lidar_data[leftDist]>2.0){
                            stopping = true;
                        }
                    }else if(stopping){ //Second time wall moves away
                        wallFollower::fun_pub_lidar_position("outsideBox");
                        wallFollower::fun_pub_cmd_vel(0,0);
                        mode = "idle";
                    }else{ //The wall continues
                        if(lidar_data[dist]>_distance+0.2||lidar_data[ang]<-90){// && following_wall){
                            turn_toward = true;
                            ROS_INFO_STREAM(ros::this_node::getName() << ": mode = not out of the woods: " << lidar_data[leftDist]);
                            if(lidar_data[leftDist]>2.0f||std::isnan(lidar_data[leftDist])){
                                stopping = true;
                            }
                        }
                        error = turn_intensity*(desired_distance_to_wall_out-lidar_data[dist]);
                        
                        // Proportional term
                        double pid_out = static_cast<double>(param_controller_P * error);
                        
                        // Integral term
                        _integral += error;
                        pid_out+= static_cast<double>(param_controller_I * _integral);
                        
                        // Derivative term
                        double derivative = error - _pre_error;
                        pid_out+= static_cast<double>(param_controller_D * derivative);
                        
                        if (pid_out>1){
                            pid_out=1;
                        } else if(pid_out<-1){
                            pid_out=-1;
                        }
                        if (lidar_data[dist]<0.2){
                            speed=0.5;
                        } else {
                            speed=0.5l;
                        }
                        wallFollower::fun_pub_cmd_vel(speed,pid_out);
                        ROS_INFO_STREAM(ros::this_node::getName() << ": hugging using PID: " << pid_out);
                        _pre_error = error;
                        _distance = lidar_data[dist];
                        
                    }
                }
                gotMessage=false;
            }else{
                //wallFollower::fun_pub_cmd_vel(0,0);
                ros::Duration(0.1).sleep();
            }
        }
        ros::spinOnce();
    }
    
    ros::spin();
}
void wallFollower::fun_pub_cmd_vel(float linear, float angular)
{
    msgs::BoolStamped dead_man;
    dead_man.header.stamp = ros::Time::now();
    dead_man.data = 1;
    pub_dead_man.publish(dead_man);
    
    geometry_msgs::TwistStamped msg;
    msg.header.stamp = ros::Time::now();
    msg.twist.linear.x = linear;
    msg.twist.angular.z = angular; //between -0.7 (go right) and 0.7 (go left)
    pub_cmd_vel.publish(msg);
}

void wallFollower::fun_pub_lidar_position(std::string position)
{
    std_msgs::String MRposition;
    MRposition.data = position;
    pub_lidar_charger_position.publish(MRposition);
}

void sigintHandler(int param)
{
    ros::shutdown();
    ROS_INFO_STREAM(ros::this_node::getName() << ": Stopped by Ctrl+C");
}

void wallFollower::nearCallback(const std_msgs::Float64MultiArray& nearest){
    gotMessage = true;
    if(nearest.data.size() >= 6){
        lidar_data.at(dist)=nearest.data[dist];
        lidar_data.at(ang)=nearest.data[ang];
        lidar_data.at(avgDist)=nearest.data[avgDist];
        lidar_data.at(frontDist)=nearest.data[frontDist];
        lidar_data.at(leftDist)=nearest.data[leftDist];
        lidar_data.at(rightDist)=nearest.data[rightDist];
    } else {
        throw std::runtime_error(std::string("Wall follower: Nearest_data callback is publishing unitilialized data"));
    }
}

void wallFollower::moveCallback(const std_msgs::String& MRmode){
    ROS_INFO_STREAM(ros::this_node::getName() << ": got move callback: go from '" << mode << "' to '" << MRmode.data << '\'');
    if(MRmode.data == "idle"){
        mode = "idle";
    } else if(mode == "idle"){
        mode = MRmode.data;
        stopping = false;
        turn_toward = false;
        if(mode=="toOutsideBox"){
            reverse=true;
            turning=false;
            _integral = 0;
            _distance = lidar_data[dist];
        } else if(mode == "toCharger"){
            _distance = lidar_data[leftDist];
            _integral = 0;
        }
        wallFollower::fun_pub_lidar_position("onWay");
    } 
}

int main(int argc,char **argv){
    std::cout<<"Node started"<<std::endl;
    ros::init(argc, argv, "wall_follow");
    wallFollower follower;
    signal(SIGINT, sigintHandler);
    follower.run();
    return 0;
}
