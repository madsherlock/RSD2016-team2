#ifndef WALLFOLLOWER_H
#define WALLFOLLOWER_H

#include "std_msgs/Float64MultiArray.h"
#include "msgs/nmea.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include <vector>
#include "std_msgs/String.h"
#include <string>


class wallFollower
{
public:
    wallFollower();
    void run();
    float param_speedMax, param_controller_P, param_controller_I, param_controller_D;
    double _integral,_pre_error,_distance;
    bool keeping_left;
    bool turn_away;
    bool turn_toward;
    double current_crowd;
    bool check_situ;
    char follow_mode; // 'c' for centre, 'w' for wall
private:
    ros::NodeHandle n;
    ros::Subscriber subs;
    ros::Subscriber movemode;
    
    bool turning;
    bool reverse;
    bool stopping;    
    int batt_state; // 0 is go to charger, 1 is behave normally
    int charge_time;
    double voltage;
    double pct;
    std::string mode;
    bool gotMessage;
    std::vector<double> lidar_data;
    std_msgs::Float64MultiArray nearest;
    ros::Publisher pub_cmd_vel;
    ros::Publisher pub_dead_man;
    ros::Publisher pub_lidar_charger_position;
    double perfect_turn_left_linear_vel;
    double perfect_turn_left_angular_vel;
    double to_charger_linear_vel;

    enum lidar_message_enum {dist=0, ang=1, avgDist=2, frontDist=3, rightDist=4, leftDist=5};
    enum charge_message_enum {empty=0, charging=1, charged=2};

    void nearCallback(const std_msgs::Float64MultiArray& nearest);
    void batteryCallback(const msgs::nmeaPtr& battery);
    void moveCallback(const std_msgs::String& MRmode);
    void fun_pub_cmd_vel(float linear, float angular);
    void fun_pub_lidar_position(std::string position);
};

#endif // WALLFOLLOWER_H
