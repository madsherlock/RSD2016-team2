#include "lineDetector.hpp"



lineDetector::lineDetector(int cam_res_width, int cam_rest_height, int areaThresh_max, int areaThresh_min, bool debug)
{
    _cam_res_width = cam_res_width;
    _cam_res_height = cam_rest_height;
    _areaThresh_max = areaThresh_max;
    _areaThresh_min = areaThresh_min;
    _debug = debug;
    _error = 0;
    _area = 0;
}

lineDetector::~lineDetector()
{

}

void lineDetector::setFrame(Mat frame)
{
    _error = 0;
    _area = 0;
    cv::Mat imgHSV ;
    cv::cvtColor(frame, imgHSV, CV_BGR2HSV);
    cv::Mat imgBinary;
    cv::inRange(imgHSV, cv::Scalar(15,60,70), cv::Scalar(80, 255, 255), imgBinary);

    vector<Moments> mu(10); // If getting more than one moment due to noise or so

    Rect rect_main(0,(_cam_res_height-50),_cam_res_width,40);

    Mat sq_main = Mat(imgBinary, rect_main);
    Mat sq_left;
    Mat sq_right;
    // Center of the line
    mu[0] = moments(sq_main,true); // Pending implementation of more moments
    int i = 0;
    _error = (_cam_res_width/2)-mu[i].m10/mu[i].m00;

    // Count threshored pixels
    //_area = countNonZero(sq_main);
    _area = mu[0].m00;
    if (_debug)
    {
        Scalar color = Scalar(0,0,0);
        vector<Point2f> mc( 2 );

        mc[i] = Point2f( mu[i].m10/mu[i].m00 , _cam_res_height/2 + mu[i].m01/mu[i].m00 );
        circle( imgBinary, mc[0], 4, color, 5, 8, 0 );

        cv::namedWindow("this",CV_WINDOW_NORMAL);
        cv::imshow("this",imgBinary);
    }
    cv::waitKey(1);
}

double lineDetector::getError()
{
    if (_area > _areaThresh_max) //If it is computing the error on a cross
        return 0;
    else
        return _error;
}
double lineDetector::getArea()
{
    return _area;
}
bool lineDetector::getCross()
{
    if (_area > _areaThresh_max)
        return true;
    else
        return false;
}
bool lineDetector::getNoLine()
{
    if (_area < _areaThresh_min)
        return true;
    else
        return false;
}
