#ifndef LINEDETECTOR_H
#define LINEDETECTOR_H

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace std;
using namespace cv;

class lineDetector
{
public:
    lineDetector(int cam_res_width, int cam_rest_height, int areaThresh_max, int areaThresh_min, bool debug);
    virtual ~lineDetector();

    void setFrame(Mat frame);
    double getError();
    double getArea();
    bool getCross();
    bool getNoLine();
private:
    bool _debug;
    int _cam_res_width;
    int _cam_res_height;
    int _areaThresh_max;
    int _areaThresh_min;
    double _error;
    double _area;
};

#endif // LINEDETECTOR_H
