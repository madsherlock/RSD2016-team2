#include <string.h>
#include <signal.h>
#include <cmath>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Odometry.h>
#include <msgs/BoolStamped.h>
#include <msgs/IntStamped.h>

#include <eigen3/Eigen/Geometry>

#include "lineDetector.cpp"
//#include "linefollower.cpp"

//#include <stdexcept>

using namespace cv;

#define CAM_FREQ 30 // Frequency in Hz to read from the camera
#define FM_COM_FREQ 30 // Frequency at which inputs msg are sent to frobomind. //Maybe not recheable after the cam and PID
#define ODOM_TURN_ERR 0.1 // Error in radians for the turnings with odometry

#define RAD2DEG 57.295779
#define RAD_90 M_PI_2*0.9
#define RAD_180 M_PI
// PATHS 0 Go straight 1 Go left 2 Go right 3 Turn 180 4 Approach coveyor
std::vector< std::vector<int> > PATHS;
std::vector< std::vector<int> > PATHS_STATUS;
std::vector<int> PATH_A_TO_CELL ={0,0};
std::vector<int> STATUS_A_TO_CELL ={0,1};
std::vector<int> PATH_A_OFFLOAD ={0,0,2,0,3,5};
std::vector<int> STATUS_A_OFFLOAD ={0,0,0,0,0,1};
std::vector<int> PATH_A_GET_BRICKS ={0,0,0,2,0,4,5};
std::vector<int> STATUS_A_GET_BRICKS ={0,0,0,0,0,0,1};
std::vector<int> PATH_A_OFFLOAD_HOME ={0,0,1,0,0};
std::vector<int> STATUS_A_OFFLOAD_HOME ={0,0,0,0,1};
std::vector<int> PATH_A_GET_BRICKS_HOME ={0,0,1,0,0,0};
std::vector<int> STATUS_A_GET_BRICKS_HOME ={0,0,0,0,0,1};

std::vector<int> PATH_B_TO_CELL ={0,0,0,0};
std::vector<int> STATUS_B_TO_CELL ={0,0,0,1};
std::vector<int> PATH_B_OFFLOAD ={0,0,0,0,0,2,0,4,6};
std::vector<int> STATUS_B_OFFLOAD ={0,0,0,0,0,0,0,0,1};
std::vector<int> PATH_B_GET_BRICKS ={0,0,0,0,2,0,3,6};
std::vector<int> STATUS_B_GET_BRICKS ={0,0,0,0,0,0,0,1};
std::vector<int> PATH_B_OFFLOAD_HOME ={0,0,1,0,0,0,0,0};
std::vector<int> STATUS_B_OFFLOAD_HOME ={0,0,0,0,0,0,0,1};
std::vector<int> PATH_B_GET_BRICKS_HOME ={0,0,1,0,0,0,0};
std::vector<int> STATUS_B_GET_BRICKS_HOME ={0,0,0,0,0,0,1};

std::vector<int> PATH_TO_DISPENSER ={0,3};
std::vector<int> STATUS_TO_DISPENSER ={0,1};
std::vector<int> PATH_EXIT_GATE ={0};
std::vector<int> STATUS_EXIT_GATE ={1};

std::vector<int> PATH_PARAM;
std::vector<int> STATUS_PARAM;

#define ERR_TURN 5 // TUrning error to accept. Pixels?

bool _run_lf = false;
bool _run_path = false;
bool _run_odom = false;
bool _obstacle = false;

vector<int> _path;

std::map<std::string,int> map_ros_inputs;
std::map<int,std::string> map_ros_outputs;

enum enum_inputs
{
    cell_a_to_cell,              //0
    cell_a_offload_bricks,       //1
    cell_a_get_sorted_bricks,    //2
    cell_a_cell_to_line,         //3
    cell_a_to_lab,               //4
    cell_b_to_cell,              //5
    cell_b_offload_bricks,       //6
    cell_b_get_sorted_bricks,    //7
    cell_b_cell_to_line,         //8
    cell_b_to_lab,              //9
    param,
    robot_stop,                       //10
    robot_pause                       //11
};

//enum enum_states {checkpoint_before_cell_a = 0,    //0
//    checkpoint_before_cell_b,    //1
//    at_the_cell,                 //2
//    at_the_drop_off,             //3
//    at_the_pickup,               //4
//    at_the_end_of_the_line       //5
//};

enum enum_states
{
    starts,     //0
    ends        //1
};

struct odom_state
{
    double pos_x;
    double pos_y;
    double yaw;
    double distance()
    {
        return sqrt(pos_x*pos_x+pos_y*pos_y);
    }
};

bool param_debug, param_wheels_blocked;
int param_camRes_width, param_camRes_height, param_areaThresh_max, param_areaThresh_min;
float param_controlerL_P, param_speedTurn_ang, param_speedTurn_lin, param_controler_P, param_controler_I, param_controler_D, param_odom_distance,param_speed_odom,param_odom_minCross,param_backwardDist_cellA,param_backwardDist_cellB,param_speedBackward_lin;

odom_state _odom, _odomOld;

size_t iter_path =0;
int _stepNext, inputLast;
float _errorOld = 10000;

ros::Publisher pub_cmd_vel;
ros::Publisher pub_dead_man;
ros::Publisher pub_pathFollow_state;

void sigintHandler(int /*param*/)
{
    _run_lf = false;
    _run_path = false;
    _run_odom = false;
    ros::shutdown();
    std::cout << "Stopped by Control+C" << endl;
}

static void toEulerianAngle(const Eigen::Quaterniond& q, double& pitch, double& roll, double& yaw)
{
    double ysqr = q.y() * q.y();
    double t0 = -2.0f * (ysqr + q.z() * q.z()) + 1.0f;
    double t1 = +2.0f * (q.x() * q.y() - q.w() * q.z());
    double t2 = -2.0f * (q.x() * q.z() + q.w() * q.y());
    double t3 = +2.0f * (q.y() * q.z() - q.w() * q.x());
    double t4 = -2.0f * (q.x() * q.x() + ysqr) + 1.0f;

    t2 = t2 > 1.0f ? 1.0f : t2;
    t2 = t2 < -1.0f ? -1.0f : t2;

    pitch = std::asin(t2);
    roll = std::atan2(t3, t4);
    yaw = std::atan2(t1, t0);
}

void getStepNext()
{
    if(iter_path+1 == _path.size()){
            _stepNext = 100;
    } else {
            _stepNext = _path[iter_path+1];
    }
}

void fun_pub_cmd_vel(float linear, float angular)
{

    msgs::BoolStamped dead_man;
    dead_man.header.stamp = ros::Time::now();
    dead_man.data = 1;
    pub_dead_man.publish(dead_man);

    geometry_msgs::TwistStamped msg;
    msg.header.stamp = ros::Time::now();
    msg.twist.linear.x = linear;
    msg.twist.angular.z = angular;
    pub_cmd_vel.publish(msg);
}


void fun_pub_pathFollower_state(int /*action*/, int step)
{

    std_msgs::String msg;
    msg.data = map_ros_outputs[step];

    pub_pathFollow_state.publish(msg);
}

void sub_obstacle(const std_msgs::Bool& msg)
{
   bool obstacle_present = msg.data;
   if(obstacle_present){
    std::cout<<"Obstacle detected!"<<std::endl;
    _obstacle = true;
   }else{
    //std::cout<<"No obstacles, continuing as normal"<<std::endl;
    _obstacle = false;
   }
}


void sub_odom(const nav_msgs::Odometry& msg)
{
    Eigen::Quaterniond q(msg.pose.pose.orientation.w,msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,msg.pose.pose.orientation.z);
//    cout << "qdata " << " " << q.w() << " " << q.z() << endl;
    double pitch, roll, yaw;
    toEulerianAngle(q,pitch, roll, yaw);
    _odom.yaw = yaw;
    _odom.pos_x = msg.pose.pose.position.x;
    _odom.pos_y = msg.pose.pose.position.y;
//    cout << "odometry reading " << _odom.yaw << " " <<_odom.yaw*RAD2DEG << "pos " << _odom.distance() << endl;
}

odom_state odom_dif(odom_state od_old, odom_state od_new, bool direction) // 0 right 1 left
{
    odom_state odomDif;

    odomDif.pos_x = od_new.pos_x - od_old.pos_x;
    odomDif.pos_y = od_new.pos_y - od_old.pos_y;

    if (direction)
        if (od_new.yaw <= od_old.yaw)
            odomDif.yaw = abs(od_new.yaw - od_old.yaw);
        else
            odomDif.yaw = 6.2831 + od_old.yaw - od_new.yaw;
    else
        if (od_new.yaw  >= od_old.yaw)
            odomDif.yaw = abs(od_new.yaw - od_old.yaw);
        else
            odomDif.yaw = 6.2831 - od_old.yaw + od_new.yaw;

    return odomDif;
}

///
/// \brief line_follower
/// Follows a line using a PID. It finishes when the line ends or when there is an intersection (line crosses)
/// Arguments: task: 0 follow line. 1 center the robot on the line (for finishing turnings)
/// \return  0 error opening camera, 1 finished task 0 succesfully, 2 finished task 1 sucessfully
///
int line_follower(int taskk, float odom_distance)
{
    int task = taskk;
    lineDetector linedet(param_camRes_width, param_camRes_height ,param_areaThresh_max, param_areaThresh_min, param_debug);
//    lineFollower linefol;

    // Get video from Cam
    cv::VideoCapture cap(0); // open the default camera // the camera will be deinitialized automatically in VideoCapture destructor
    cap.set(CV_CAP_PROP_FRAME_WIDTH, param_camRes_width);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, param_camRes_height);
    if(!cap.isOpened())  // check if we succeeded
        return 1;

    std::cout << "Line follower starts..." << std::endl;
    ros::Rate cam_loop_rate(CAM_FREQ);
    double pid_out;//, i_out;
    while(_run_lf)
    {
        cv::Mat frame;
        // get a new frame from camera
        cap >> frame;
        linedet.setFrame(frame);
        if(waitKey(30) >= 0) break; // If removed opencv wont show a thing...wtf

        // Get area (only for info)
        double area;
        area = linedet.getArea();

        // Detect error
        double error;
        error = linedet.getError();
        if (_errorOld == 10000)
            _errorOld = error;

        // PID controller
        pid_out = static_cast<double>(param_controler_P)*error;
        pid_out += static_cast<double>(param_controler_D)*(error-_errorOld);
//        i_out += (double)param_controler_I*error;
//        if (abs(error) < 20)
//            i_out = 0;
//        pidd_out += (double)i_out;

        std::cout << "Area " << area << " PI says " << pid_out << " I says " << "i_out" << " for error "<< error << std::endl;

        // Odom
        if (_run_odom)
        {
            std::cout << "Odometry on" << std::endl;
            odom_state odomDif = odom_dif(_odomOld, _odom, 0);
            std::cout << "Odom distance: " << odomDif.distance() << "min" << param_odom_minCross << std::endl;
            if(linedet.getCross() && _stepNext == 0 && odomDif.distance() > param_odom_minCross)
            {
                std::cout << "Cross detected while odom" << std::endl;
                ++iter_path;
                _odomOld = _odom;
            }
            if (odomDif.distance() > odom_distance)
            {
                std::cout << "Odometry end reached" << std::endl;
                _run_odom = false;
                _run_lf = false;
                continue;
            }
            if (odomDif.distance() > 0.1 && _stepNext == 0)
            {
                _run_odom = false;
                task = 0;
                ++iter_path;
                getStepNext();
            }
        }

        // Line? // Detect only when going forward
        if (linedet.getNoLine() && task == 0)
        {
            std::cout << "Line ends" << std::endl;
            _run_odom = true;
            task = 2;
            _odomOld = _odom;
        }
        // Cross? // Detect only when going forward
        if (linedet.getCross() && task == 0)
        {
            ROS_INFO_STREAM(ros::this_node::getName() << "Cross detected");
            _run_odom = true;
            task = 2;
            _odomOld = _odom;
        }

        // Actuation
        if (!param_wheels_blocked && !_obstacle)
            switch (task)
            {
            case 0: // FOllowing the line
                fun_pub_cmd_vel((param_camRes_width*0.5-abs(error))*param_controlerL_P,pid_out);
                break;
            case 1: // When aligning in turns
                if (abs(error) < ERR_TURN)
                    return 0;
                if (linedet.getNoLine())
                    return 1;
                fun_pub_cmd_vel(0,pid_out);
                break;
            case 2: // When using odometry
                fun_pub_cmd_vel(param_speed_odom,pid_out);
                break;
            case 3: // Backward
                fun_pub_cmd_vel(-param_speedBackward_lin,pid_out);
                break;
            }
        _errorOld = error;
        ros::spinOnce();

        cam_loop_rate.sleep();
    }
    return 0;
}

int line_turn(int turn)
{
    ros::Rate loop_rate(FM_COM_FREQ);
    odom_state odom_target;
    switch(turn)
    {
    case 0: // forward
        _run_lf = true;
        line_follower(0,param_odom_distance);
        break;
    case 1: // left
        ROS_INFO_STREAM(ros::this_node::getName() << "Turnning left...");
        if (_odom.yaw - RAD_90 < -M_PI)
            odom_target.yaw = 2*M_PI - (RAD_90 - _odom.yaw); // "new odom old" Target angle.
        else
            odom_target.yaw = _odom.yaw - RAD_90;

        std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,1).yaw << "current" << _odom.yaw << std::endl;
        _run_odom = true;
        while(odom_dif(_odom, odom_target,1).yaw > ODOM_TURN_ERR && _run_odom)
        {
            std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,1).yaw << " " << _odom.yaw << std::endl;
            fun_pub_cmd_vel(param_speedTurn_lin,param_speedTurn_ang);
            ros::spinOnce();
            loop_rate.sleep();
        }
        break;
    case 2: // right
        ROS_INFO_STREAM(ros::this_node::getName() << "Turnning right...");
        if (_odom.yaw + RAD_90 > M_PI)
            odom_target.yaw = - 2*M_PI + (RAD_90 + _odom.yaw); // "new odom old" Target angle.
        else
            odom_target.yaw = _odom.yaw + RAD_90;

        std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,0).yaw << "current" << _odom.yaw << std::endl;
        _run_odom = true;
        while(odom_dif(_odom, odom_target,0).yaw > ODOM_TURN_ERR && _run_odom)
        {
            std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,0).yaw << " " << _odom.yaw << std::endl;
            fun_pub_cmd_vel(param_speedTurn_lin,-param_speedTurn_ang);
            ros::spinOnce();
            loop_rate.sleep();
        }
        break;
    case 3: // 180 left
        ROS_INFO_STREAM(ros::this_node::getName() << "Turnning 180 to the left...");
        if (_odom.yaw - RAD_180 < -M_PI)
            odom_target.yaw = 2*M_PI - (RAD_180 - _odom.yaw); // "new odom old" Target angle.
        else
            odom_target.yaw = _odom.yaw - RAD_180;

        std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,1).yaw << "current" << _odom.yaw << std::endl;
        _run_odom = true;
        while(odom_dif(_odom, odom_target,1).yaw > ODOM_TURN_ERR && _run_odom)
        {
            std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,1).yaw << " " << _odom.yaw << std::endl;
            fun_pub_cmd_vel(param_speedTurn_lin,param_speedTurn_ang);
            ros::spinOnce();
            loop_rate.sleep();
        }
        break;
    case 4: // 180 right
        ROS_INFO_STREAM(ros::this_node::getName() << "Turnning 180 to the right...");
        if (_odom.yaw + RAD_180 > M_PI)
            odom_target.yaw = - 2*M_PI + (RAD_180 + _odom.yaw); // "new odom old" Target angle.
        else
            odom_target.yaw = _odom.yaw + RAD_180;

        std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,0).yaw << "current" << _odom.yaw << std::endl;
        _run_odom = true;
        while(odom_dif(_odom, odom_target,0).yaw > ODOM_TURN_ERR && _run_odom)
        {
            std::cout << "odom target: " << odom_target.yaw << "odom dif " << odom_dif(_odom, odom_target,0).yaw << " " << _odom.yaw << std::endl;
            fun_pub_cmd_vel(param_speedTurn_lin,-param_speedTurn_ang);
            ros::spinOnce();
            loop_rate.sleep();
        }
        break;
    case 5: // Backward
       ROS_INFO_STREAM(ros::this_node::getName() << "Going backward in cell A");
//        cout << "Odometry on" << endl;
        _odomOld = _odom;
        _run_odom = true;
        line_follower(3,param_backwardDist_cellA);
//        while(odom_dif(_odomOld,_odom,0).distance() < param_backwardDist_conv)
//        {
//            cout << "Odom distance: " << odom_dif(_odomOld,_odom,0).distance() << endl;
//            fun_pub_cmd_vel(-param_speedBackward_lin,0);
//            ros::spinOnce();
//            loop_rate.sleep();
//        }
        break;
    case 6: // Backward
        ROS_INFO_STREAM(ros::this_node::getName() << "Going backward in cell B");
//        cout << "Odometry on" << endl;
        _odomOld = _odom;
        _run_odom = true;
        line_follower(3,param_backwardDist_cellB);
//        while(odom_dif(_odomOld,_odom,0).distance() < param_backwardDist_box)
//        {
//            fun_pub_cmd_vel(-param_speedBackward_lin,0);
//            ros::spinOnce();
//            loop_rate.sleep();
//        }
        break;
    default:
        std::cout << "Turn value not valid!" << std::endl;
        break;
    }
    _run_odom = false;
    _run_lf = true;
    // Align to line with camera
//    if (_stepNext != 0)
    if (_stepNext == 0 || _stepNext == 1 || _stepNext == 2 || _stepNext == 3 || _stepNext == 4)
    {
        if (0 == line_follower(1,0))
            ROS_INFO_STREAM(ros::this_node::getName() << "Aligned");
        else
            ROS_INFO_STREAM(ros::this_node::getName() << "Not aligned");
    }
    return 0;
}


///
/// \brief path_follower
/// path: 0 go to workcells, 1 exit workcells, 2 go to workcell 1, 3 go to workcell 2
/// \return
///
int path_follower(int num)
{
    _path = PATHS.at(num);

    ROS_INFO_STREAM(ros::this_node::getName()<< "Path " << num);

    for(iter_path = 0; iter_path < _path.size(); ++iter_path) {
        fun_pub_pathFollower_state(num,0);
        
        getStepNext();

        line_turn(_path[iter_path]);

        if (_run_path == false){
            fun_pub_pathFollower_state(num,1);
            return 1;
        }
    }
    fun_pub_pathFollower_state(num,1);
    inputLast = num;
    return 0;
}

void sub_trigger(const std_msgs::String::ConstPtr& msg)
{
    string input = msg->data.c_str();
    try{
        switch(map_ros_inputs.at(input))
        {
        case 50:
            _run_lf = false;
            _run_path = false;
            ROS_INFO_STREAM(ros::this_node::getName() << "Line follower stops");
            break;
        case 51:
            _obstacle = true;
            ROS_INFO_STREAM(ros::this_node::getName() << "Line follower pauses");
            break;
        case 52:
            _obstacle = false;
            ROS_INFO_STREAM(ros::this_node::getName() << "Line follower continues");
            break;
        case 100:
            ROS_INFO_STREAM(ros::this_node::getName() << "Input message unknown");
            break;
        default:
            if (_run_path == true)
                ROS_INFO_STREAM(ros::this_node::getName() << "Path follower task already running!");
            else
            {
                _run_path = true;
                std::cout << "Path follower starts" << std::endl;
                int inputInt = map_ros_inputs.at(input);
                if (inputInt == 20)
                    switch(inputLast)
                    {
                    case 1:
                        inputInt = 3;
                        break;
                    case 2:
                        inputInt = 4;
                        break;
                    case 6:
                        inputInt = 8;
                        break;
                    case 7:
                        inputInt = 9;
                        break;
                    default:
                        std::cout << "Last input (path) unknown!" << std::endl;
                        break;
                    }

                int follower_out;
                follower_out = path_follower(inputInt);
                switch(follower_out)
                {
                case 0:
                    _run_path = false;
                    ROS_INFO_STREAM(ros::this_node::getName() << "Path follower ends");
                break;
                case 1:
                    ROS_INFO_STREAM(ros::this_node::getName() << "Path follower ends Unexpectedly");
                break;
                }
            }
            break;
        }
    }
    catch (const std::out_of_range& e) {
        std::cout << "Unknown Message!." << std::endl;
    }
//    ROS_INFO("I heard: [%s]", msg->data.c_str());
}


int main(int argc, char **argv)
{

    map_ros_inputs["cell a:to cell"] = 0;
    map_ros_inputs["cell a:offload bricks"] = 1;
    map_ros_inputs["cell a:get sorted bricks"] = 2;

    map_ros_inputs["cell b:to cell"] = 5;
    map_ros_inputs["cell b:offload bricks"] = 6;
    map_ros_inputs["cell b:get sorted bricks"] = 7;


    map_ros_inputs["to dispenser"] = 10;
    map_ros_inputs["exit gate"] = 11;
    map_ros_inputs["home"] = 20;

    map_ros_inputs["param"] = 12;
    map_ros_inputs["stop"] = 50;
    map_ros_inputs["pause"] = 51;
    map_ros_inputs["continue"] = 52;


//    map_ros_outputs[0] = "checkpoint before cell a";
//    map_ros_outputs[1] = "checkpoint before cell b";
//    map_ros_outputs[2] = "at the cell";
//    map_ros_outputs[3] = "at the drop off";
//    map_ros_outputs[4] = "at the pickup";
//    map_ros_outputs[5] = "at the end of the line";
//    map_ros_outputs[50] ="checkpoint";
    map_ros_outputs[0] = "busy";
    map_ros_outputs[1] = "free";

    PATHS.push_back(PATH_A_TO_CELL);
    PATHS.push_back(PATH_A_OFFLOAD);
    PATHS.push_back(PATH_A_GET_BRICKS);
    PATHS.push_back(PATH_A_OFFLOAD_HOME);
    PATHS.push_back(PATH_A_GET_BRICKS_HOME);

    PATHS.push_back(PATH_B_TO_CELL);
    PATHS.push_back(PATH_B_OFFLOAD);
    PATHS.push_back(PATH_B_GET_BRICKS);
    PATHS.push_back(PATH_B_OFFLOAD_HOME);
    PATHS.push_back(PATH_B_GET_BRICKS_HOME);

    PATHS.push_back(PATH_TO_DISPENSER);
    PATHS.push_back(PATH_EXIT_GATE);


//    PATHS.push_back(PATH_B_TO_LAB);

    PATHS_STATUS.push_back(STATUS_A_TO_CELL);
    PATHS_STATUS.push_back(STATUS_A_OFFLOAD);
    PATHS_STATUS.push_back(STATUS_A_GET_BRICKS);
    PATHS_STATUS.push_back(STATUS_A_OFFLOAD_HOME);
    PATHS_STATUS.push_back(STATUS_A_GET_BRICKS_HOME);

    PATHS_STATUS.push_back(STATUS_B_TO_CELL);
    PATHS_STATUS.push_back(STATUS_B_OFFLOAD);
    PATHS_STATUS.push_back(STATUS_B_GET_BRICKS);
    PATHS_STATUS.push_back(STATUS_B_OFFLOAD_HOME);
    PATHS_STATUS.push_back(STATUS_B_GET_BRICKS_HOME);

    PATHS_STATUS.push_back(STATUS_TO_DISPENSER);
    PATHS_STATUS.push_back(STATUS_EXIT_GATE);

    ros::init(argc, argv, "line_follower");
    ros::NodeHandle n("~");

    string param_path;

    n.param<bool>("param_debug", param_debug, false);
    n.param<bool>("param_wheels_blocked", param_wheels_blocked, false);
    n.param<int>("param_camRes_width", param_camRes_width, 320);
    n.param<int>("param_camRes_height", param_camRes_height, 240);
    n.param<int>("param_areaThresh_max", param_areaThresh_max, 10000);
    n.param<int>("param_areaThresh_min", param_areaThresh_min, 500);
    n.param<float>("param_controlerL_P", param_controlerL_P, 0.0026);
    n.param<float>("param_controler_P", param_controler_P, 0.0022);
    n.param<float>("param_controler_I", param_controler_I, 0.0);
    n.param<float>("param_controler_D", param_controler_D, 0.0);
    n.param<float>("param_speedTurn_ang", param_speedTurn_ang, 0.5);
    n.param<float>("param_speedTurn_lin", param_speedTurn_lin, 0);
//    n.param<int>("param_turnTime", param_turnTime, 90);
    n.param<float>("param_odom_distance", param_odom_distance, 1);
    n.param<float>("param_speed_odom", param_speed_odom, 0.2);
    n.param<float>("param_odom_minCross", param_odom_minCross, 0.2);
    n.param<float>("param_backwardDist_cellA", param_backwardDist_cellA, 0.3);
    n.param<float>("param_backwardDist_cellB", param_backwardDist_cellB, 0.2);
    n.param<float>("param_speedBackward_lin", param_speedBackward_lin, 0);
    n.param<string>("param_path",param_path,"0");

    stringstream param_path_ss(param_path);
    int i_param = 0;
    while(param_path_ss >> i_param)
    {
        PATH_PARAM.push_back(i_param);
        if (param_path_ss.peek() == ',')
                    param_path_ss.ignore();
    }
    PATHS.push_back(PATH_PARAM);
    STATUS_PARAM = PATH_PARAM;////
    PATHS_STATUS.push_back(STATUS_PARAM);

    ros::Subscriber sub0 = n.subscribe("/pathFollower/triggers", 1, sub_trigger);
//    ros::Subscriber sub1 = n.subscribe("/odom", 5, sub_odom); //for the simulation
    ros::Subscriber sub1 = n.subscribe("/fmKnowledge/pose", 1, sub_odom);
    ros::Subscriber sub2 = n.subscribe("/obstacle_status", 1, sub_obstacle);

    pub_cmd_vel = n.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel", 1,false);
    pub_pathFollow_state = n.advertise<std_msgs::String>("/pathFollower/state", 1, false);
    pub_dead_man = n.advertise<msgs::BoolStamped>("/fmSafe/deadman", 1,false);

    signal(SIGINT, sigintHandler);

    ros::spin();
    return 0;
}
