#!/usr/bin/env python
# Modified by Mikael Westermann
# Original code by Juan Lu and Mikael Westermann
import socket
import sys
import rospy
import time
from std_msgs.msg import String
from markerlocator.msg import markerpose

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print 'Socket is open.'
except socket.error, msg:
    print 'Failed to open socket. Error: ' + str(msg[0]) + ', Message: ' + msg[1]
    sys.exit()
    
#Set socket to non-blocking mode, allowing dropped packages, forcing old marker poses to be republished.
#s.setblocking(False)



s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)




 
#host = 'robolab01.projectnet.wlan.sdu.dk';
#port = 5005;

#Read ROS params
host = rospy.get_param("~udp_server_hostname",'robolab01.projectnet.wlan.sdu.dk')
port = rospy.get_param("~udp_server_port",5005)
udp_request_msg = rospy.get_param("~udp_request_msg",'markerpose_5')
topic = rospy.get_param("~markerpose_topic",'/markerlocator/markerpose_5')
frequency = rospy.get_param("~markerpose_frequency",15) #Should not be higher than server freq.

#Start node
rospy.init_node('markerpose_publisher', anonymous=True) 
pub = rospy.Publisher(topic, markerpose, queue_size=10)   #Queue size 1: ALWAYS latest pose.
rate = rospy.Rate(frequency)



s.settimeout(0.5/frequency)


while not rospy.is_shutdown():
    markerpose_msg = markerpose()
    try :
        #Send request message to UDP server in RoboLab
        #print 'sending request'
        s.sendto(udp_request_msg, (host, port))
         
        # receive data from server (data, addr)
        #print 'receiving data'
        d = s.recvfrom(1024)
        reply = d[0]
        addr = d[1]
        
        if not reply:
	  print 'NO REPLY!!!'
	  sys.exit()
        
        #Split data from server
        d_split = reply.split(',')
        
        #Generate markerpose msg from UDP packet
	markerpose_msg.header.stamp = rospy.get_rostime()
	#markerpose_msg.header.stamp = float(d_split[5]) #THATS NOT RIGHT
	markerpose_msg.order = int(d_split[0])
	markerpose_msg.x = float(d_split[1])
	markerpose_msg.y = float(d_split[2])
	markerpose_msg.theta = float(d_split[3])
	markerpose_msg.quality = float(d_split[4])
	#markerpose_msg.timestamp = float(d_split[5]) #THATS JUST WRONG
	markerpose_msg.timestamp = rospy.get_rostime()
        
        #Publish that pose
        rospy.loginfo(markerpose_msg)
        pub.publish(markerpose_msg)
        
        #rospy.spin()
        rate.sleep()
        
    except socket.error, msg: #Maybe msg is "timed out". In that case, try again!
	if str(msg[0]) == "timed out":
	  print "TIMED OUT!!!"
	  continue
        print 'Error Code : ' + str(msg[0]) + ' Message: ' + msg[1]
        #sys.exit()
        continue

s.close()