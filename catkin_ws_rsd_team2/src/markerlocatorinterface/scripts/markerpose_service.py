#!/usr/bin/env python
# Mikael Westermann
import socket
import sys
import rospy
import time
from std_msgs.msg import String
from markerlocator.msg import markerpose
#import markerlocator
from markerlocatorinterface.srv import markerpose_request
import select




try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print 'Socket is open.'
except socket.error, msg:
    print 'Failed to open socket. Error: ' + str(msg[0]) + ', Message: ' + msg[1]
    sys.exit()
    
#Set socket to non-blocking mode, allowing dropped packages, forcing old marker poses to be republished.
#s.setblocking(False)



s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)




 
#host = 'robolab01.projectnet.wlan.sdu.dk';
#port = 5005;

#Read ROS params
host = rospy.get_param("~udp_server_hostname",'robolab01.projectnet.wlan.sdu.dk')
port = rospy.get_param("~udp_server_port",5005)
#udp_request_msg = rospy.get_param("~udp_request_msg",'markerpose_5')
#topic = rospy.get_param("~markerpose_topic",'/markerlocator/markerpose_5')
#frequency = rospy.get_param("~markerpose_frequency",15) #Should not be higher than server freq.

udp_timeout = rospy.get_param("~udp_timeout",0.06)



def empty_socket(sock):
    """remove the data present on the socket"""
    input = [sock]
    while 1:
        inputready, o, e = select.select(input,[],[], 0.0)
        if len(inputready)==0: break
        for sd in inputready: sd.recv(1)

#Start node
rospy.init_node('markerpose_server', anonymous=True) 
#pub = rospy.Publisher(topic, markerpose, queue_size=10)   #Queue size 1: ALWAYS latest pose.
#rate = rospy.Rate(frequency)
#s.settimeout(0.5/frequency)

# Takes a markerpose_reques.srv (containing an int64 named order)
# Returns a markerpose obtained over UDP.
def handle_markerpose_request(req):
  #Make string from requested order
  print "Order requested: ", str(req.order)  
  udp_request_msg = "markerpose_"+str(req.order)
  response_ready = False
  markerpose_msg = markerpose()
  while not response_ready:
    try:
      empty_socket(s)
      s.sendto(udp_request_msg, (host, port))
      d = s.recvfrom(200)
      reply = d[0]
      addr = d[1]
      if not reply:
	print 'NO REPLY!!!'
	#sys.exit()
	#Try again
	pass
      #print 'REPLY: ', reply
      #Split data from server
      d_split = reply.split(',')
      #Generate markerpose msg from UDP packet
      markerpose_msg.header.stamp = rospy.get_rostime()
      #markerpose_msg.header.stamp = float(d_split[5]) #THATS NOT RIGHT
      markerpose_msg.order = int(d_split[0])
      markerpose_msg.x = float(d_split[1])
      markerpose_msg.y = float(d_split[2])
      markerpose_msg.theta = float(d_split[3])
      markerpose_msg.quality = float(d_split[4])
      #print 'markerpose requested. x: ', markerpose_msg.x, ', y: ', markerpose_msg.y, ', theta: ', markerpose_msg.theta
      #markerpose_msg.timestamp = float(d_split[5]) #THATS JUST WRONG
      # markerpose_msg.timestamp = rospy.get_rostime()  
      markerpose_msg.timestamp = markerpose_msg.header.stamp
      response_ready = True
      #return markerposeResponse(markerpose_msg)
    except socket.error, msg: #Maybe msg is "timed out". In that case, try again!
      if str(msg[0]) == "timed out":
	print "TIMED OUT!!!"
	continue
      print 'Error Code : ' + str(msg[0]) + ' Message: ' + msg[1]
      #sys.exit()
      continue
  return markerpose_msg # markerpose_requestResponse(markerpose_msg)



serv = rospy.Service('markerpose', markerpose_request, handle_markerpose_request)
print "markerpose_server started"
print "Doing a dummy request to get that connection going!"
dummy_request=markerpose_request()
dummy_request.order = 5
handle_markerpose_request(dummy_request)
print "OK All fine now"
rospy.spin()

s.close()







