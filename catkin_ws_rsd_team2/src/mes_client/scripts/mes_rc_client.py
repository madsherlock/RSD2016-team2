#!/usr/bin/env python

 # Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import rospy
from std_msgs.msg import String

import socket
import random
import time

IDLE = 0
REQUEST = 1
SUSPEND = 2

#2 amd 3 only
robotNumber = 2

sortingFinished = False
deliverFinished = False

port = 21212
hostname = socket.gethostbyname('robolab01.projectnet.wlan.sdu.dk')

#robot command to get from server
commandListServer_robot = ["No_task", "Wait_for_new_task", "Wait_for_task_accept", "Wait_for_bricks", "Sort_bricks", "Move_bricks_to_deliver", "Wait_for_mobile", "Deliver_to_mobile"]

#robot commands to send to server
commandListClient_robot = ["Idle", "Suspended", "Request_task", "Waiting_for_new_task", "Waiting_for_task_accept_mobile", "Waiting_for_task_accept_mobile_1", "Waiting_for_task_accept_mobile_2",  "Waiting_for_bricks", "Sorting_bricks", "Moving_to_delivering_position", "Waiting_for_mobile", "Moving_to_mobile", "Complete"]  


def processState(state):
    if(state.data == "FINISHED"):
        global sortingFinished
        sortingFinished = True
    if(state.data == "FINISHED_DELIVER"):
	global deliverFinished
	deliverFinished = True

#Log info
def command(command ,id, comment):  
    str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    str += "<log_entry>"
    str += "<event>%s</event>" % commandListClient_robot[command]
    str += "<time>%s</time>" % time.ctime() 
    str += "<cell_id>%s</cell_id>" % id
    str += "<comment>%s</comment>" % comment
    str += "</log_entry>"
    return str

#TCP/IP client
def sendStatus(string):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((hostname, port))

    sock.send(string)
    reply = sock.recv(16384)  # limit reply to 16K
    sock.close()
    return reply


def processReply(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    return string[start:end]

def findState(string):
    state = commandListServer_robot.index(string)
    return state
    
def main():
    global sortingFinished
    global deliverFinished
    rospy.init_node("mes_rc_client", anonymous=True)
    state_pub = rospy.Publisher(rospy.get_name() + "/MESState", String,queue_size=1, latch=True)
    process_node_subscriber = rospy.Subscriber("/rc/processNode/getCurrentState", String, processState)
    
    rate = rospy.Rate(1.0/3.0)
    
    init()
    
    nxt_state = REQUEST

    #Startup and go to idle
    str = command(commandListClient_robot.index("Idle"),robotNumber,"Idle")   
    reply = sendStatus(str)
    old_reply = "<event>d</event>"
    state = findState(processReply(reply))
    
    while not rospy.is_shutdown():
        if(state == commandListServer_robot.index("No_task")):
            #Idle, Request new task or suspend
            if(nxt_state == IDLE):
                str = command(commandListClient_robot.index("Idle"),robotNumber,"Idle")
            elif(nxt_state == REQUEST):
                str = command(commandListClient_robot.index("Request_task"),robotNumber,"Request new task")
            elif(nxt_state == SUSPEND):
                str = command(commandListClient_robot.index("Suspended"),robotNumber,"Suspend robot")
            reply = sendStatus(str)
            state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Wait_for_new_task")):
            #Waiting for mobile to take task
            str = command(commandListClient_robot.index("Waiting_for_new_task"),robotNumber,"Waiting for Task")
            reply = sendStatus(str)
            state = findState(processReply(reply))
            
        elif(state == commandListServer_robot.index("Wait_for_task_accept")):
            #Waiting for mobile to accept task
            str = command(commandListClient_robot.index("Waiting_for_task_accept_mobile"),robotNumber,"Waiting for mobile to accept task")
            reply = sendStatus(str)
            state = findState(processReply(reply))

            
        elif(state == commandListServer_robot.index("Wait_for_bricks")):
            #Wait for bricks to arrive
            str = command(commandListClient_robot.index("Waiting_for_bricks"),robotNumber,"Preparing for bricks")
            reply = sendStatus(str)
            state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Sort_bricks")):
            #Sort the bricks
            str = command(commandListClient_robot.index("Sorting_bricks"),robotNumber,"Sorting the bricks")
            reply = sendStatus(str)
            state = findState(processReply(reply))

            if( sortingFinished == True ):
                sortingFinished = False
                str = command(commandListClient_robot.index("Moving_to_delivering_position"),robotNumber,"Moving bricks to delivery position")
                reply = sendStatus(str)
                state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Move_bricks_to_deliver")):
            # Move bricks to delivery postion
            str = command(commandListClient_robot.index("Moving_to_delivering_position"),robotNumber,"Moving bricks to delivery position")
            reply = sendStatus(str)
            state = findState(processReply(reply))
	    
	    str = command(commandListClient_robot.index("Waiting_for_mobile"),robotNumber,"Wait until a mobile arrives")
	    reply = sendStatus(str)
	    state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Wait_for_mobile")):
            # Wait for mobile to arrive
            str = command(commandListClient_robot.index("Waiting_for_mobile"),robotNumber,"Wait until a mobile arrives")
            reply = sendStatus(str)
            state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Deliver_to_mobile")):
            # Move bricks to mobile
            str = command(commandListClient_robot.index("Moving_to_mobile"),robotNumber,"Moving bricks to mobile")
            reply = sendStatus(str)
            state = findState(processReply(reply))

            if ( deliverFinished == True ):
                deliverFinished = False
                str = command(commandListClient_robot.index("Complete"),robotNumber,"Task complete")
                reply = sendStatus(str)
                state = findState(processReply(reply))

        print "state %d " % state
        
        # ONLY PUBLISH WHEN REPLY FROM MES SERVER CHANGES!
        if(processReply(reply) != processReply(old_reply)):
	  state_pub.publish(reply)
        old_reply = reply;
        rate.sleep()

def init():
    if rospy.has_param("~port"):
        global port
        port = rospy.get_param("~port")
    if rospy.has_param("~hostname"):
        global hostname
        hostname = rospy.get_param("~hostname")

if __name__ == "__main__":
    main()
