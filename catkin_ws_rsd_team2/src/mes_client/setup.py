from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
  scripts=['scripts/mes_mr_client.py','scripts/mes_rc_client.py'],)
setup(**d)

