#!/usr/bin/env python
#Mobile MES Client

import socket
import random
import time
import rospy

#HOST = rospy.get_param("~mes_server_host",'mwlinux.projectnet.wlan.sdu.dk')
HOST = socket.gethostbyname('localhost')#('robolab01.projectnet.wlan.sdu.dk')


IDLE = 0
REQUEST = 1
SUSPEND = 2

#0 and 1 only
mobileNumber = 1


#mobile commands to get from server
commandListServer_mobile = ["No_task", "New_task_del", "New_task_recv", "Wait_for_free_feeder", "Get_bricks", "Wait_for_accept_drone", "Move_to_cell_1_del", "Move_to_cell_2_del", "Empty_bricks", "Move_home", "Move_to_cell_1_recv", "Move_to_cell_2_recv", "Wait_for_accept_robot"]

#mobile commands to send to server
commandListClient_mobile = ["Idle", "Suspended", "Request_task", "Accept_del", "Reject_del", "Accept_recv", "Reject_recv", "Waiting_for_free_feeder", "Moving_to_feeder", "Waiting_for_accept_drone", "Waiting_for_accept_drone_1", "Waiting_for_accept_drone_2", "Moving_to_cell_1_del", "Moving_to_cell_2_del", "Emptying_bricks", "Bricks_empty", "Moving_home", "Complete", "Moving_to_cell_1_recv", "Moving_to_cell_2_recv", "Waiting_for_accept_robot"]


#Log info
def command(command ,id, comment):  
    str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    str += "<log_entry>"
    str += "<event>%s</event>" % commandListClient_mobile[command]
    str += "<time>%s</time>" % time.ctime() 
    str += "<cell_id>%s</cell_id>" % id
    str += "<comment>%s</comment>" % comment
    str += "</log_entry>"
    return str

#TCP/IP client
def sendStatus(string):
    #HOST = socket.gethostbyname('mwlinux.projectnet.wlan.sdu.dk')
    #HOST ='localhost'
    PORT = 21212
    # SOCK_STREAM == a TCP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #sock.setblocking(0)  # optional non-blocking
    sock.connect((HOST, PORT))

    print "sending data => %s" % (string)
    sock.send(string)
    reply = sock.recv(16384)  # limit reply to 16K
    print "reply => \n [%s]" % (reply)
    sock.close()
    return reply


def processReply(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    return string[start:end]

def findState(string):
    state = commandListServer_mobile.index(string)
    return state
    
def main():
    # State is controlled by server
    testcounter = 0
    nxt_state = REQUEST
    accept_new_tasks_del = True
    accept_new_tasks_recv = True

    #Startup and go to idle
    str = command(commandListClient_mobile.index("Idle"),mobileNumber,"Idle")
    reply = sendStatus(str)
    print processReply(reply)
    state = findState(processReply(reply))
            
    while(1):
        if(state == commandListServer_mobile.index("No_task")):    
            #idle, Request new task or suspend
            if(nxt_state == IDLE):
                str = command(commandListClient_mobile.index("Idle"),mobileNumber,"Idle")
            elif(nxt_state == REQUEST):
                str = command(commandListClient_mobile.index("Request_task"),mobileNumber,"Request a new task")
            elif(nxt_state == SUSPEND):
                str = command(commandListClient_mobile.index("Suspended"),mobileNumber,"Suspend mobile")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("New_task_del")):
            #Accept or reject new tak
            if(accept_new_tasks_del == True):
                str = command(commandListClient_mobile.index("Accept_del"),mobileNumber,"Accept new task")
            elif(accept_new_tasks_del == False):
                str = command(commandListClient_mobile.index("Reject_del"),mobileNumber,"Reject new task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("New_task_recv")):
            #Accept or reject new tak
            if(accept_new_tasks_recv == True):
                str = command(commandListClient_mobile.index("Accept_recv"),mobileNumber,"Accept new task")
            elif(accept_new_tasks_recv == False):
                str = command(commandListClient_mobile.index("Reject_recv"),mobileNumber,"Reject new task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Wait_for_free_feeder")):
            #Wait for the feeder to become free
            str = command(commandListClient_mobile.index("Waiting_for_free_feeder"),mobileNumber,"Waiting for Task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))
            
        elif(state == commandListServer_mobile.index("Get_bricks")):
            #Move to feeder
            str = command(commandListClient_mobile.index("Moving_to_feeder"),mobileNumber,"Speeding toward feeder")
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( testcounter == 3):
                #complete and send status to server
                testcounter = 0
                str = command(commandListClient_mobile.index("Waiting_for_accept_drone"),mobileNumber,"Waiting to get bricks")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Wait_for_accept_drone")):
            #Wait for the feeder to be filled
            str = command(commandListClient_mobile.index("Waiting_for_accept_drone"),mobileNumber,"Waiting to get bricks")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))


        elif(state == commandListServer_mobile.index("Move_to_cell_1_del") or state == commandListServer_mobile.index("Move_to_cell_2_del") ):
            #Move to robot cell
            if(state == commandListServer_mobile.index("Move_to_cell_1_del")):
                str = command(commandListClient_mobile.index("Moving_to_cell_1_del"),mobileNumber,"Speeding toward cell 1")
            elif(state == commandListServer_mobile.index("Move_to_cell_2_del")):
                str = command(commandListClient_mobile.index("Moving_to_cell_2_del"),mobileNumber,"Speeding toward cell 2")
                
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( testcounter == 5):
                #complete and send status to server
                testcounter = 0
                str = command(commandListClient_mobile.index("Emptying_bricks"),mobileNumber,"Emptying bricks in cell")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Move_to_cell_1_recv") or state == commandListServer_mobile.index("Move_to_cell_2_recv") ):
            #Move to robot cell
            if(state == commandListServer_mobile.index("Move_to_cell_1_recv")):
                str = command(commandListClient_mobile.index("Moving_to_cell_1_recv"),mobileNumber,"Speeding toward cell 1")
            elif(state == commandListServer_mobile.index("Move_to_cell_2_del")):
                str = command(commandListClient_mobile.index("Moving_to_cell_2_recv"),mobileNumber,"Speeding toward cell 2")
                
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( testcounter == 5):
                #complete and send status to server
                testcounter = 0
                str = command(commandListClient_mobile.index("Waiting_for_accept_robot"),mobileNumber,"Waiting to be full")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Empty_bricks")):
            #emptying bricks in cell
            str = command(commandListClient_mobile.index("Emptying_bricks"),mobileNumber,"Emptying bricks in cell")     
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until emptying is complete
            if( testcounter == 3):
                #complete and send status to server
                testcounter = 0
                str = command(commandListClient_mobile.index("Bricks_empty"),mobileNumber,"Finished emptying bricks")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Move_home")):
            #Moving Home
            str = command(commandListClient_mobile.index("Moving_home"),mobileNumber,"Returning to box")     
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( testcounter == 5):
                #complete and send status to server
                testcounter = 0  
                str = command(commandListClient_mobile.index("Complete"),mobileNumber,"Order complete")
                #nxt_state = SUSPEND
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        
       
        elif(state == commandListServer_mobile.index("Wait_for_accept_robot")):
            #Wait for the feeder to be filled
            str = command(commandListClient_mobile.index("Waiting_for_accept_robot"),mobileNumber,"Waiting to get bricks")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))            

        print "state %d " % state
        time.sleep(3)

if __name__ == "__main__":
    main()
