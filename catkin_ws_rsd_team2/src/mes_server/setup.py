from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
  scripts=['scripts/Server.py','scripts/MobileClient_2.py','scripts/DroneClient_2.py','scripts/RobotClient_2.py'],)
setup(**d)

