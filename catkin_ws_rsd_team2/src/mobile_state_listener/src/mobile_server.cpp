#include "mobile_server.h"

mobile_server::mobile_server() :
	n_("~"),
    nav_mode_to_mobile_state_listener_sub_(n_.subscribe("/mobile_state_listener_subscriber/current_event_topic", 1, &mobile_server::callback_nav_mode_1, this)),
    getCurrent_Event_Service(n_.advertiseService("/mobile_state_listener/current_event_service", &mobile_server::get_current_event_callback, this))
{
	std::cout << "subscriber_and_server -  constructed" << std::endl;
    old_event = " ";
    current_event = "1";
}

void mobile_server::callback_nav_mode_1(const std_msgs::String& input)
{
    current_event = input.data;
    if(old_event != current_event)
    {
        std::cout << "New value called!" << current_event << std::endl;
        old_event = current_event;
    }

}

bool mobile_server::get_current_event_callback(mobile_state_listener::get_current_event_mobile::Request &req, mobile_state_listener::get_current_event_mobile::Response &res)
{
    std::cout << "response " << current_event << std::endl;
    res.current_event_mobile = current_event;
	return true;
}
