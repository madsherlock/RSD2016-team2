#include <ros/ros.h>
#include <iostream>
#include <std_msgs/String.h>
#include <string>
#include "mobile_state_listener/get_current_event_mobile.h"

class mobile_server
{
public:
	mobile_server();
	void callback_nav_mode_1(const std_msgs::String& input);
	bool get_current_event_callback(mobile_state_listener::get_current_event_mobile::Request &req, mobile_state_listener::get_current_event_mobile::Response &res);

private:
    std::string current_event;
	std::string old_event;
	ros::NodeHandle n_;
	ros::Subscriber nav_mode_to_mobile_state_listener_sub_;
	ros::ServiceServer getCurrent_Event_Service;
};
