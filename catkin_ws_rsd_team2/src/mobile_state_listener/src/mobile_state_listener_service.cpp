#include <ros/ros.h>
#include <iostream>
#include "mobile_server.h"

int main(int argc, char **argv)
{
	//Initiate ROS
    ros::init(argc, argv, "mobile_listener_server");

	//Create an object of class SubscriberAndServer that will take care of everything
    mobile_server mobile_middleman_server;

	ros::spin();

	return 0;
}
