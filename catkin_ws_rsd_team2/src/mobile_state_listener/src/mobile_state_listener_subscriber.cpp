#include <ros/ros.h>
#include <iostream>
#include "mobile_subscriber.h"

int main(int argc, char **argv)
{
	//Initiate ROS
    ros::init(argc, argv, "mobile_listener_subscriber");

    //Create an object of class SubscriberAndServer that will take care of everything
    mobile_subscriber mobile_middleman_subscriber;

	ros::spin();

	return 0;
}
