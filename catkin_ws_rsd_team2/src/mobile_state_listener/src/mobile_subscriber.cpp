#include "mobile_subscriber.h"

mobile_subscriber::mobile_subscriber() :
	n_("~"),
    nav_mode_to_mobile_state_listener_wrapper_sub_(n_.subscribe("/mr_nav_mode", 1, &mobile_subscriber::callback_nav_mode_1, this)),
    wrapper_to_mobile_state_listener_server_pub_(n_.advertise<std_msgs::String>("/mobile_state_listener_subscriber/current_event_topic",1))
{
	std::cout << "subscriber_and_server -  constructed" << std::endl;
    old_event = "";
    current_event = "1";
}

void mobile_subscriber::callback_nav_mode_1(const std_msgs::String& input)
{
    current_event = input.data;
    if(old_event != current_event)
    {
        std::cout << "input recived!: " << input.data << std::endl;
        old_event = current_event;
        wrapper_to_mobile_state_listener_server_pub_.publish(input);
    }

}
