#include <ros/ros.h>
#include <iostream>
#include <std_msgs/String.h>
#include <string>
#include "mobile_state_listener/get_current_event_mobile.h"

class mobile_subscriber
{
public:
	mobile_subscriber();
	void callback_nav_mode_1(const std_msgs::String& input);

private:
	std::string current_event;
	std::string old_event;
	ros::NodeHandle n_;
    ros::Subscriber nav_mode_to_mobile_state_listener_wrapper_sub_;
    ros::Publisher wrapper_to_mobile_state_listener_server_pub_;
};
