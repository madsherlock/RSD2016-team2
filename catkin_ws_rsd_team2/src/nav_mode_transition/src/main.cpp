#include <iostream>
#include <ros/ros.h>
#include "nav_mode_transition.h"
#include <QCoreApplication>

int main(int argc, char * argv[]){
    QCoreApplication app(argc, argv);

    ros::init(argc, argv, "navigation_mode_controller");
    nav_mode_transition mode_handler;

    QObject::connect(&mode_handler,SIGNAL(close()),&app,SLOT(quit()));

    mode_handler.start();
    app.exec();
    return 0;
}
