#include "nav_mode_ros_handler.h"
//#include "../utils/utils.h"
#include <hmi_backend/utils.h>

#include <ros/time.h>
//#include <ros/package.h>

#include <fstream>
#include <yaml-cpp/yaml.h>
void replace_with_file(std::vector<pose_t> &path_vec, std::string yaml_file, std::string path_name){
    YAML::Node file = YAML::LoadFile(yaml_file.c_str());
    if( file[path_name] ){
        const YAML::Node& path = file[path_name];
        path_vec.clear(); //replace with the file
        for (size_t i = 0; i < path.size(); ++i) {
            double x, y, theta = NAN;
            int task;
            x = path[i]["wpt"]["x"].as<double>();
            y = path[i]["wpt"]["y"].as<double>();
            if(path[i]["wpt"]["theta"]){ //theta is optional
                theta = path[i]["wpt"]["theta"].as<double>();
            }
            if(path[i]["wpt"]["task"]){ //theta is optional
                if(path[i]["wpt"]["task"].as<int>() == TASK_GPS_RESET){
                    task = TASK_GPS_RESET;
                } else {
                    task = TASK_NO_RESET;
                }
            }
            path_vec.push_back(pose_t(x,y,theta,task));
        }
    }
}

nav_mode_ros_handler::nav_mode_ros_handler() :
nodehandle("~"),
nav_mode_pub(nodehandle.advertise<std_msgs::String>("/mr_nav_mode", 1, true)),
mobile_robot_avail_pub(nodehandle.advertise<std_msgs::String>("/mr_avail", 1, true)),
behaviour_mode_pub(nodehandle.advertise<msgs::IntStamped>("/fmPlan/automode",1, true)),
line_follower_trigger_pub(nodehandle.advertise<std_msgs::String>("/pathFollower/triggers",1, false)),
wpt_nav_route_pub(nodehandle.advertise<waypoint_navigation::waypoint_control_msg>("/waypoint_control",1,false)),
lidar_pub(nodehandle.advertise<std_msgs::String>("/fmSignal/lidar_mode", 1, false)), 
mes_client_pub(nodehandle.advertise<std_msgs::String>("mr/processNode/getCurrentState",1,true)),
tipper_pub(nodehandle.advertise<std_msgs::Int16>("/tipper_control",1,true)),
hmi_request_mode(nodehandle.subscribe("/mr_nav_mode_request", 1, &nav_mode_ros_handler::hmi_callback, this)),
wpt_nav_complete_sub(nodehandle.subscribe("/waypoint_complete",1, &nav_mode_ros_handler::wpt_complete_callback, this)),
line_follower_sub(nodehandle.subscribe("/pathFollower/state", 1, &nav_mode_ros_handler::line_follower_callback, this)),
lidar_sub(nodehandle.subscribe("/fmSignal/lidar_position", 1, &nav_mode_ros_handler::wall_follower_callback, this)),
mes_client_sub(nodehandle.subscribe("/MESState",1,&nav_mode_ros_handler::mes_callback,this)),
tipper_pos_sub(nodehandle.subscribe("/tipper_status",1,&nav_mode_ros_handler::tipper_pos_callback,this)),
battery_status_sub(nodehandle.subscribe("/fmSignal/nmea_from_frobit", 1, &nav_mode_ros_handler::battery_callback, this)),
manual_reset_sub(nodehandle.subscribe("/oee/reset", 1, &nav_mode_ros_handler::manual_reset_callback, this)),
loop_rate(10),
thread_running(false),
current_nav_mode(),
behaviour(),
target_cell(2), //our cell
task_received(false),
battery_pct(0),
estimated_charge_time(30), //minutes
w_time(2),
charge_time(),
bricks_unloaded(false),
current_order_is_deliver(false),
active_test(),
run_all(false),
use_drone_delivery(false)
{
    std::cout << "THIS IS THE RIGHT FILE" << std::endl;
    std::string waypoint_path_file;
    nodehandle.getParam("current_active_test", active_test);
    if(!nodehandle.getParam("test_all_nav_modes",run_all)){
        std::cout << "Run all modes param not found" << std::endl;
        run_all = false;
    }
//    std::cout << (run_all ? : "Running all " : "Running single test") << std::endl;
    nodehandle.param<bool>("use_drone_delivery",use_drone_delivery, false);
    if(!run_all){
	std::cout << "loaded test: '" << active_test << "'" << std::endl;
    } else {
        std::cout << "running all modes" << std::endl;
    }
    if(!nodehandle.getParam("waypoint_path_file", waypoint_path_file)){
        throw std::runtime_error("No yaml file for the waypoints specified in launch file");
    }
    
    replace_with_file(to_drone_gate,   waypoint_path_file, "to_drone_gate");
    replace_with_file(gate_to_road,    waypoint_path_file, "gate_to_road");
    replace_with_file(to_line,         waypoint_path_file, "to_line");
    replace_with_file(line_to_charger, waypoint_path_file, "line_to_charger");
    
}

void nav_mode_ros_handler::hmi_callback(const std_msgs::StringPtr &msg){
    if(msg->data == "hmi_request_manual_control"){
        emit set_desired_nav_mode(std::string("manual"));
    } else if(msg->data == "hmi_request_auto_control" ){
        emit set_desired_nav_mode(std::string("resume_auto"));
    } else {
        std::cout << "Not a recognized message: '" << msg->data << '\'' << std::endl;
    }
}

void nav_mode_ros_handler::wpt_complete_callback(const msgs::BoolStampedPtr &complete){
    if(complete->data){
        if(task_received){
            if(current_nav_mode.data == "to_line") {
                emit set_mobile_robot_at(std::string("line"));
            } else if(current_nav_mode.data == "to_drone_gate") {
                emit set_mobile_robot_at(std::string("drone gate"));
            } else if(current_nav_mode.data == "gate_to_road") {
                emit set_mobile_robot_at(std::string("road"));
            } else if(current_nav_mode.data == "line_to_charger") {
                emit set_mobile_robot_at(std::string("charger wall"));
            } else {
//                 emit set_mobile_robot_at(std::string("unknown"));
            }
            task_received = false;
        }
    } else {
        task_received = true;
//         emit set_mobile_robot_at(std::string("unknown"));
    }
}

void nav_mode_ros_handler::wall_follower_callback(const std_msgs::StringPtr &complete){
    if(complete->data == "atCharger" || complete->data == "outsideBox"){
        if(task_received){
            if(current_nav_mode.data == "lidar_charger"){
                std_msgs::String msg;
                msg.data = "AT_HOME";
                mes_client_pub.publish(msg);
                emit set_mobile_robot_at(std::string("charger"));
            } else if(current_nav_mode.data == "lidar_out"){
                if( (!use_drone_delivery) && current_order_is_deliver){
                    std_msgs::String msg;
                    msg.data = "AT_FEEDER";
                    mes_client_pub.publish(msg);
                }
                emit set_mobile_robot_at(std::string("charger wall"));
            }
        }
    } else if(complete->data == "onWay"){
        task_received = true;
        emit set_mobile_robot_at(std::string("unknown"));
    }
}

void nav_mode_ros_handler::line_follower_callback(const std_msgs::StringPtr &complete){
    if(complete->data == "free"){
        std::cout << "job complete" << std::endl;
        if(task_received){
            if(current_nav_mode.data == "to_dispenser"){
                emit set_mobile_robot_at(std::string("dispenser"));
                std_msgs::String msg;
                msg.data = "AT_FEEDER";
                mes_client_pub.publish(msg);
            } else if(current_nav_mode.data == "exit_gate"){
                emit set_mobile_robot_at(std::string("gate exit"));
            } else if(current_nav_mode.data == "to_cell"){
                emit set_mobile_robot_at(std::string("cell"));
            } else if(current_nav_mode.data == "to_belt"){
                std_msgs::Int16 desired_tipper_pos;
                desired_tipper_pos.data = 100;
                tipper_pub.publish(desired_tipper_pos);
            } else if(current_nav_mode.data == "to_slide"){
                std_msgs::String msg;
                msg.data = "AT_ROBOT_CELL";
                mes_client_pub.publish(msg);
                emit set_mobile_robot_at(std::string("slide"));
            } else if(current_nav_mode.data == "line_following_away"){
                emit set_mobile_robot_at(std::string("end of line"));
            }
        }
        task_received = false;
    } else if(complete->data == "busy") {
        task_received = true;
    }    
}

void nav_mode_ros_handler::tipper_pos_callback(const std_msgs::Int16Ptr &pos){
    std::cout << "tipper is at" << pos->data << std::endl;
    if(pos->data > 98){
        bricks_unloaded = true;
    } else {
        bricks_unloaded = false;
    }
}

void nav_mode_ros_handler::mes_callback(const std_msgs::StringPtr &mes_state){
    std::cout << mes_state->data << std::endl;
    std_msgs::String response;
    response.data ="";
    if(false){
    } else if(mes_state->data == "New_task_del"){
        if(current_nav_mode.data != "idle"){
            response.data = "REJECT_TASKS";
        } else {
            response.data = "ACCEPT_TASKS";
        }
    } else if(mes_state->data == "Get_bricks"){
        current_order_is_deliver = true;
        emit set_new_order_given(current_order_is_deliver);
    } else if(mes_state->data == "Move_to_cell_1_del"){
        target_cell = 1;
        emit set_mobile_robot_at(std::string("bricks received"));
    } else if(mes_state->data == "Move_to_cell_2_del"){
        target_cell = 2;
        emit set_mobile_robot_at(std::string("bricks received"));
    } else if(mes_state->data == "New_task_recv"){
        if(current_nav_mode.data != "idle"){
            response.data = "REJECT_TASKS";
        } else {
            response.data = "ACCEPT_TASKS";
        }
    } else if(mes_state->data == "Move_to_cell_1_recv"){
        target_cell = 1;
        current_order_is_deliver = false;
        emit set_new_order_given(current_order_is_deliver);
    } else if(mes_state->data == "Move_to_cell_2_recv"){
        target_cell = 2;
        current_order_is_deliver = false;
        emit set_new_order_given(current_order_is_deliver);
    } else if(mes_state->data == "Move_home"){
        if(current_nav_mode.data == "receiving_sorted"){
            emit set_mobile_robot_at(std::string("box received"));
        }
    }
    if(response.data != ""){
        mes_client_pub.publish(response);
    }
}

void nav_mode_ros_handler::battery_callback(const msgs::nmeaPtr& battery){
    if (battery->valid == true) {
        battery_pct = battery_percent(battery);
    }
    if(battery_pct < 100){
        emit set_battery_charged(false);
    }
}
double nav_mode_ros_handler::get_current_battery_percent() {
    return battery_pct;
}
//TODO: remove test code (&& (in==active_test||run_all) )
void nav_mode_ros_handler::get_current_nav_mode(std::string in){
    if(false){
    } else if(in == "lidar_out" && (in==active_test||run_all)){
        std_msgs::String msg;
        msg.data = "toOutsideBox";
        lidar_pub.publish(msg);
    } else if(in == "lidar_charger" && (in==active_test||run_all) ){
        std_msgs::String msg;
        msg.data = "toCharger";
        lidar_pub.publish(msg);
        w_time = estimated_charge_time*60 * (1-get_current_battery_percent()); //seconds
    } else if(in == "to_dispenser" && (in==active_test||run_all)){
        std_msgs::String msg;
        msg.data = std::string("to dispenser");
        line_follower_trigger_pub.publish(msg);
    } else if(in == "exit gate" && (in==active_test||run_all)){
        std_msgs::String msg;
        msg.data = std::string("exit_gate");
        line_follower_trigger_pub.publish(msg);
    } else if(in == "to_belt" && (in==active_test||run_all)){
        std_msgs::String msg;
        msg.data = std::string((target_cell == 1 ? "cell a:" : "cell b:" )) + std::string("offload bricks");
        line_follower_trigger_pub.publish(msg) ;
    } else if(in == "to_slide" && (in==active_test||run_all)){
        std_msgs::String msg;
        msg.data = std::string((target_cell == 1 ? "cell a:" : "cell b:" )) + std::string("get sorted bricks");
        line_follower_trigger_pub.publish(msg);
    } else if(in == "line_following_away" && (in==active_test||run_all)){
        std_msgs::String msg;
        msg.data = std::string("home");
        line_follower_trigger_pub.publish(msg);
    } else if(in == "gate_to_road" && (in==active_test||run_all)){
        msgs::RoutePt waypoint;
        waypoint_navigation::waypoint_control_msg route;
        for(auto p : gate_to_road){
            p.convert(waypoint);
            route.wpt_list.push_back(waypoint);
        }
        route.nav_mode = "replace";
        wpt_nav_route_pub.publish(route);
        behaviour.data = BHV_WPTNAV;
    } else if(in == "to_drone_gate" && (in==active_test||run_all)){
        msgs::RoutePt waypoint;
        waypoint_navigation::waypoint_control_msg route;
        for(auto p : to_drone_gate){
            p.convert(waypoint);
            route.wpt_list.push_back(waypoint);
        }
        route.nav_mode = "replace";
        wpt_nav_route_pub.publish(route);
        behaviour.data = BHV_WPTNAV;
    } else if(in == "to_line" && (in==active_test||run_all)){
        msgs::RoutePt waypoint;
        waypoint_navigation::waypoint_control_msg route;
        for(auto p : to_line){
            p.convert(waypoint);
            route.wpt_list.push_back(waypoint);
        }
        route.nav_mode = "replace";
        wpt_nav_route_pub.publish(route);
        behaviour.data = BHV_WPTNAV;
    } else if(in == "charging"){
        charge_time = ros::Time::now() + ros::Duration(w_time);
    } else if(in == "line_to_charger" && (in==active_test||run_all)){
        msgs::RoutePt waypoint;
        waypoint_navigation::waypoint_control_msg route;
        for(auto p : line_to_charger){
            p.convert(waypoint);
            route.wpt_list.push_back(waypoint);
        }
        route.nav_mode = "replace";
        wpt_nav_route_pub.publish(route);
        behaviour.data = BHV_WPTNAV;
    } else if( in == "manual" ){
        behaviour.data = BHV_HMI;
    }
    
    current_nav_mode.data = in;
}

void nav_mode_ros_handler::control_handler(bool enable){
    thread_running = enable;
    bool emulate_deliver;
    nodehandle.getParam("emulate_deliver",emulate_deliver);
    if(thread_running){ 
            emit set_new_order_given(emulate_deliver); 
        for(int i = 0; i < 3; ++i){
//             std::cout << "adding orders" << std::endl;
//             emit set_new_order_given(true);
//             emit set_new_order_given(false);
        }
        this->start();
    } else {
        current_nav_mode.data = "";
    }
}

void nav_mode_ros_handler::manual_reset_callback(const std_msgs::StringPtr &redo_state){
    //TODO: implement a reset...
    if(redo_state->data == "manual"){
        
    }
}

void nav_mode_ros_handler::run(){
    while(thread_running) {
        if(current_nav_mode.data == "idle" || current_nav_mode.data == "manual"){
            mobile_robot_availability.data = "ready";
        } else {
            mobile_robot_availability.data = "taken";
        }
        if(current_nav_mode.data == "charging"){
            if(ros::Time::now().toSec() > charge_time.toSec()){
                emit set_battery_charged(true);
            }
        }
        if(current_nav_mode.data == "to_belt"){
            if(bricks_unloaded){
                std::cout << "starting tipper" << std::endl;
                emit set_mobile_robot_at(std::string("belt offloaded"));
                std_msgs::String msg;
                msg.data = "BRICKS_EMPTY";
                mes_client_pub.publish(msg);
                //move tipper back while moving away
                std_msgs::Int16 desired_tipper_pos;
                desired_tipper_pos.data = 0;
                tipper_pub.publish(desired_tipper_pos);
            }
        }
        mobile_robot_avail_pub.publish(mobile_robot_availability);
        behaviour.header.stamp = ros::Time::now();
        behaviour_mode_pub.publish(behaviour);
        std::cout << "Current nav mode is '" << current_nav_mode.data << '\'' << std::endl;
        nav_mode_pub.publish(current_nav_mode);
        ros::spinOnce();
        loop_rate.sleep();
    }
}
