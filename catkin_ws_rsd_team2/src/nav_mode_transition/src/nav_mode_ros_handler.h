#pragma once

#include <queue>
#include <QThread>
#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>
#include <msgs/nmea.h>
#include <msgs/RoutePt.h>
#include <msgs/IntStamped.h>
#include <msgs/BoolStamped.h>
#include <msgs/waypoint_navigation_status.h>
#include <markerlocator/markerpose.h>
#include <waypoint_navigation/waypoint_control_msg.h>


    enum wpt_tasks {
	TASK_GPS_RESET = 1,
        TASK_NO_RESET = -1000000
    };


/**
* @brief The nav_mode_ros_handler class
* Send and receive ROS packages.
* The information is enterpreted here, and the relevant
* meta information is sent over qt signals.
*
* The enterpretation happens in the seperate callbacks.
*/
class nav_mode_ros_handler : public QThread
{
    Q_OBJECT
private:
    ros::NodeHandle nodehandle;
    ros::Publisher nav_mode_pub;
    ros::Publisher mobile_robot_avail_pub;
    ros::Publisher behaviour_mode_pub;
    ros::Publisher line_follower_trigger_pub;
    ros::Publisher wpt_nav_route_pub;
    ros::Publisher lidar_pub;
    ros::Publisher mes_client_pub;
    ros::Publisher tipper_pub;
    ros::Subscriber hmi_request_mode;
    ros::Subscriber wpt_nav_complete_sub;
    ros::Subscriber line_follower_sub;
    ros::Subscriber lidar_sub;
    ros::Subscriber mes_client_sub;
    ros::Subscriber tipper_pos_sub;
    ros::Subscriber battery_status_sub;
    ros::Subscriber manual_reset_sub;
    ros::Rate loop_rate;
    bool thread_running;
    std_msgs::String current_nav_mode;
    std_msgs::String mobile_robot_availability;
    msgs::IntStamped behaviour;
    int target_cell;
    bool task_received;
    double battery_pct;
    const double estimated_charge_time;
    double w_time;
    ros::Time charge_time;
    bool bricks_unloaded;
    bool current_order_is_deliver;
    
    enum bhv_types {
	BHV_HMI = 0,
	BHV_WPTNAV = 1
    };
    enum wpt_status {
	WPT_ERR = -1,
	WPT_STOP = 0,
	WPT_DRIVE = 1,
	WPT_TURN = 2
    };
    
    double get_current_battery_percent();
    /**
    * @brief hmi_callback
    * The HMI can toggle between auto and manual control.
    */
    void hmi_callback(const std_msgs::StringPtr &msg);

    void wpt_complete_callback(const msgs::BoolStampedPtr &complete);

    /**
    * @brief line_pos_callback
    * The position on the yellow line is entrepreted.
    * If the position indicates the robot should be at the cell,
    * set_robot_at_cell will emit true
    */
    void line_follower_callback(const std_msgs::StringPtr &complete);
//     void line_follower_callback(const std_msgs::StringPtr &state);

    void wall_follower_callback(const std_msgs::StringPtr &complete);
    /**
    * @brief tipper_pos_callback
    * set_bricks_unloaded emits true if the tipper position indicates
    * the bricks has been pushed off the mobile robot
    */
    void tipper_pos_callback(const std_msgs::Int16Ptr &pos);

    void mes_callback(const std_msgs::StringPtr &mes_state);

    /**
    * @brief battery_callback
    * The battery callback reads the battery status
    * If the battery is charging, set_robot_at_charger will emit true.
    */
    void battery_callback(const msgs::nmeaPtr &battery);
    
    void manual_reset_callback(const std_msgs::StringPtr &redo_state);
public:
    //TODO: remove this once testing is over
    std::string active_test;
    bool run_all;
    bool use_drone_delivery;

    nav_mode_ros_handler();
public slots:
    void control_handler(bool enable);
    void get_current_nav_mode(std::string in);
signals:
    void set_desired_nav_mode(std::string);
    void set_mobile_robot_at(std::string);
    void set_battery_charged(bool);
    void set_new_order_given(bool);
    void set_open_area_clear(bool);
protected:
    void run();
};

Q_DECLARE_METATYPE(std::string)
