#include "nav_mode_transition.h"
#include <csignal>
#include <fstream>
//#include "../utils/utils.h"
#include <string>

bool nav_mode_transition::thread_running = false;

nav_mode_transition::nav_mode_transition() :
desired_nav_mode(),
current_nav_mode("idle"),
last_known_mode("idle"),
robot_pos("unknown"),
open_area_clear(false),
battery_charged(false),
order_deliver(true),
orders_given(),
ros_handler()
{
    qRegisterMetaType<std::string>("std::string");
    QObject::connect(&ros_handler,SIGNAL(set_mobile_robot_at(std::string)),  this,SLOT(get_robot_at(std::string))   );
    QObject::connect(&ros_handler,SIGNAL(set_new_order_given(bool)),         this,SLOT(get_new_order_given(bool))   );
    QObject::connect(&ros_handler,SIGNAL(set_battery_charged(bool)),         this,SLOT(get_battery_charged(bool))   );
    QObject::connect(&ros_handler,SIGNAL(set_open_area_clear(bool)),         this,SLOT(get_open_area_clear(bool))   );
    QObject::connect(&ros_handler,SIGNAL(set_desired_nav_mode(std::string)), this,SLOT(request_mode(std::string))   );
    
    QObject::connect(this,SIGNAL(enable_ros_thread(bool)),      &ros_handler,SLOT(control_handler(bool))            );
    QObject::connect(this,SIGNAL(set_current_mode(std::string)),&ros_handler,SLOT(get_current_nav_mode(std::string)));
    emit set_current_mode(current_nav_mode);
    thread_running = true;
    
    if(!ros_handler.run_all)
        std::cout << "... loaded test: '" << ros_handler.active_test << "'" << std::endl;

}

void nav_mode_transition::get_robot_at(std::string in) { robot_pos    = in; }
void nav_mode_transition::get_new_order_given(bool in) { orders_given.push(in); }
void nav_mode_transition::get_battery_charged(bool in) { battery_charged = in; }
void nav_mode_transition::get_open_area_clear(bool in) { open_area_clear = in; }
void nav_mode_transition::request_mode(std::string in) { desired_nav_mode.push(in); }

void nav_mode_transition::control_handler(bool enable){
    thread_running = enable;
    if(thread_running){
        this->start();
    } else {
        while(!desired_nav_mode.empty()){
            desired_nav_mode.pop();
        }
    }
}

void nav_mode_transition::sighandler(int sig){
    if(sig == 2){ //handle ^C
        thread_running = false;
        std::cout << "trying to exit" << std::endl;
    }
}

void nav_mode_transition::run(){
    signal(SIGINT, nav_mode_transition::sighandler);
    emit enable_ros_thread(true);
    while(thread_running) {
        if(!desired_nav_mode.empty()){
            std::string new_mode = desired_nav_mode.front();
            std::cout << "got a desired nav mode: " << new_mode << std::endl;
            desired_nav_mode.pop();
            if(new_mode == "manual"){
                last_known_mode = current_nav_mode;
                current_nav_mode = new_mode;
                emit set_current_mode(current_nav_mode);
            } else {
                std::cout << "Resuming automatic operation with " << last_known_mode << std::endl;
                current_nav_mode = last_known_mode;
                emit set_current_mode(current_nav_mode);
            }
        }
        if( current_nav_mode == "manual" ){
        } else if( current_nav_mode == "lidar_out" ){ 
            if(robot_pos == "charger wall"){
                if(open_area_clear){
                    if(order_deliver && ros_handler.use_drone_delivery){
                        current_nav_mode = "to_drone_gate";
                        if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                            robot_pos = "drone gate"; //TODO: remove once it is detected
                        }
                    } else {
                        current_nav_mode = "to_line";
                        if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                            robot_pos = "line"; //TODO: remove this once it is detected
                        }
                    }
                    emit set_current_mode(current_nav_mode);
                }
            }
        } else if( current_nav_mode == "to_drone_gate" ){
            if(robot_pos == "drone gate"){
                current_nav_mode = "to_dispenser";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "dispenser"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "to_dispenser" ){
            if(robot_pos == "dispenser"){
                current_nav_mode = "receiving_bricks";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "bricks received"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "receiving_bricks" ){
            if(robot_pos == "bricks received"){
                current_nav_mode = "exit_gate";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "gate exit"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "exit_gate" ){
            if(robot_pos == "gate exit"){
                current_nav_mode = "to_line";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "line"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "to_line" ){
            if(robot_pos == "line"){
                if(order_deliver){
                    current_nav_mode = "to_belt";
                    if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                        robot_pos = "belt offloaded"; //TODO: remove this once it is detected
                    } 
                } else {
                    current_nav_mode = "to_slide";
                    if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                        robot_pos = "slide"; //TODO: remove this once it is detected
                    }
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "to_belt" ){
            if(robot_pos == "belt offloaded"){
                current_nav_mode = "line_following_away";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "end of line"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "to_slide" ){
            if(robot_pos == "slide"){
                current_nav_mode = "receiving_sorted";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "box received"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "receiving_sorted" ){
            if(robot_pos == "box received"){
                current_nav_mode = "line_following_away";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "end of line"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "line_following_away" ){
            if(robot_pos == "end of line"){
                current_nav_mode = "line_to_charger";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "charger wall"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "line_to_charger" ){
            if(robot_pos == "charger wall"){
                current_nav_mode = "lidar_charger";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "charger"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "lidar_charger" ){
            if(robot_pos == "charger"){
                battery_charged = true;
                current_nav_mode = "charging";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "charger wall"; //TODO: remove this once it is detected
                }
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "charging" ){ 
            if(battery_charged){
                current_nav_mode = "idle";
                emit set_current_mode(current_nav_mode);
            }
        } else if( current_nav_mode == "idle" ){
            if(!orders_given.empty()){
                order_deliver = orders_given.front();
                orders_given.pop();
                current_nav_mode = "lidar_out";
                if(current_nav_mode != ros_handler.active_test && !ros_handler.run_all) {
                    robot_pos = "charger wall"; //TODO: remove this once it is detected
                }
                open_area_clear = true; //TODO: remove this once it is detected
                emit set_current_mode(current_nav_mode);
            } else {
                std::cout << "no orders" << std::endl;
                //                 thread_running = false; //TODO: remove this if you want to run forever.
            }
        } else {
            std::cerr << "ERROR: unknown nav mode: " << current_nav_mode << std::endl;
        }
        std::cout << "nav mode: " << current_nav_mode << " " << robot_pos << std::endl;
        if(!ros_handler.run_all){
        }
        QThread::msleep(1000); //TODO: remove once everything is running
    } //while thread_enable
    emit enable_ros_thread(false);
    ros_handler.wait();
    emit close();
    ros::shutdown();
}


