#ifndef NAV_MODE_TRANSITION_H
#define NAV_MODE_TRANSITION_H

#include <queue>
#include <QThread>
#include <iostream>
#include "nav_mode_ros_handler.h"

/**
 * @brief The nav_mode_transition class
 * This class controls the transition between navigation modes.
 */
class nav_mode_transition : public QThread
{
    Q_OBJECT
private:
    std::queue<std::string> desired_nav_mode;
    std::string current_nav_mode;
    std::string last_known_mode;
    static bool thread_running;
    std::string robot_pos;
    bool open_area_clear; //the area between the drone gate and the charger where collisons can happen
    bool battery_charged;
    bool order_deliver;
    std::queue<bool> orders_given; //true means deliver, false means recover
    /**
     * @brief ros_handler
     * The ros handler sends / receives packages over ros and
     * communicates with the decision thread via qt signals
     */
    nav_mode_ros_handler ros_handler;
    static void sighandler(int sig);
public:
    void control_handler(bool enable);
    nav_mode_transition();
    /**
     * @brief run
     * The transitions is updated here.
     * Currently, it is a thread which is updated at a specific interval,
     * but it could also be a function that is called every time a value is updated instead.
     * Right now, the other systems are not ready for that.
     */
    void run();
signals:
    void close(void);
    void enable_ros_thread(bool);
    void set_current_mode(std::string);
public slots:
    void get_robot_at(std::string in);
    void get_new_order_given(bool in);
    void get_battery_charged(bool in);
    void get_open_area_clear(bool in);
    void request_mode(std::string in);
};

#endif // NAV_MODE_TRANSITION_H
