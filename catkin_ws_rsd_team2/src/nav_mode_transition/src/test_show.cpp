#include <iostream>
//#include "../vision/imgshow.hpp"

#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <map>


int main(){
    //    cv::Mat img;
    //    img = cv::imread("../data/test.png", CV_LOAD_IMAGE_COLOR);
    //    imgshow(img);

    // ros: /pathFollower/triggers, msg: std_msgs/String
    std::vector<std::string>  inputs{"cell a:to cell",              //0
                                     "cell a:offload bricks",       //1
                                     "cell a:get sorted bricks",    //2
                                     "cell a:cell to line",         //3
                                     "cell a:to lab",               //4
                                     "cell b:to cell",              //5
                                     "cell b:offload bricks",       //6
                                     "cell b:get sorted bricks",    //7
                                     "cell b:to lab"};              //8
    // ros: /pathFollower/state, msg: std_msgs/String
    std::vector<std::string> outputs{"checkpoint before cell a",    //0
                                     "checkpoint before cell b",    //1
                                     "at the cell",                 //2
                                     "at the drop off",             //3
                                     "at the pickup",               //4
                                     "at the end of the line"};     //5

    enum my_enum {checkpoint_before_cell_a = 0,    //0
        checkpoint_before_cell_b,    //1
        at_the_cell,                 //2
        at_the_drop_off,             //3
        at_the_pickup,               //4
        at_the_end_of_the_line};     //5
    std::map<std::string,my_enum> table;

    for (size_t i = 0; i < outputs.size(); ++i) {
        table[outputs[i]] = static_cast<my_enum>(i);
    }
    std::string test("checkpoint before cell b");

    switch(table.at(test)){
    case checkpoint_before_cell_a:
        std::cout << "option 0" << std::endl;
        break;
    case checkpoint_before_cell_b:
        std::cout << "option 1" << std::endl;
        break;
    default:
        std::cout << "unknown option" << std::endl;
        break;
    }


    inputs.push_back(std::string("stop"));
    inputs.push_back(std::string("you need to stop"));
    inputs.push_back(std::string("it's time to stop"));
    inputs.push_back(std::string("stop!"));
    std::string robot_cell_id, task;
    for (size_t i = 0; i < inputs.size(); ++i) {
        std::string frobit_triggers = inputs.at(i);
        if(frobit_triggers.find("stop") != std::string::npos){
            std::cout << "I am told to stop" << std::endl;
        } else {
            std::istringstream split(frobit_triggers);
            std::string part;
            std::getline(split, part, ':');
            robot_cell_id = part;
            std::getline(split, part);
            task = part;

            std::cout << "The cell id is '" << robot_cell_id << "' and the task is '" << task << '\'' << std::endl;
        }
    }


    return 0;
}
