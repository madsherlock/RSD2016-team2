#!/bin/bash
rostopic echo /fmKnowledge/pose | tee pos.log
grep "x:" pos.log | cut -d : -f 2 > x.log
grep "y:" pos.log | cut -d : -f 2 > y.log
grep "z:" pos.log | cut -d : -f 2 > z.log
grep "w:" pos.log | cut -d : -f 2 > w.log
