#!/bin/bash
#Turn csv of x,y,theta into waypoint list
n=$(wc -l $@ | cut -d ' ' -f 1)
names=mktemp
printf 'x,y,theta\n%.0s' $(seq 1 $n) > $names

paste -d, <(paste -d: \ 
		<(cut $names -d, -f 1) \
		<(cut $@ -d, -f 1)\
	) \
	<(paste -d, \
		<(paste -d: \
			<(cut $names -d, -f 2) \
			<(cut $@ -d, -f 2) \
		) \
		<(paste -d: \
			<(cut $names -d, -f 3) \
			<(cut $@ -d, -f 3) \
		) \
	) | 
sed 's/^/  - wpt: {/g' | sed 's/$/}/g' #add yaml syntax

rm $names
