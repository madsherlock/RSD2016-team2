#include<ros/ros.h>
#include<ros/network.h>
#include<iostream>
#include"subscriber_and_server_oee.h"


int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "oee");

  //Create an object of class SubscribeAndPublish that will take care of everything
  subscriber_and_server_oee middleman;
  ros::spin();

  return 0;
}
