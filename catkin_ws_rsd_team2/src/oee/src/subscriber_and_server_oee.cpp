#include "subscriber_and_server_oee.h"


subscriber_and_server_oee::subscriber_and_server_oee() :
   state(Check_total_time),
   target_count_robot(0),
   target_count_mobile(0),
   dataout("data.csv"),
   read_robot(false),
   read_mobile(false),
   n_("~"),
   mes_client_current_state_robot(n_.serviceClient<rc_state_listener::get_current_event>("/rc_state_listener/Get_Current_Event")),
   mes_client_current_state_mobile(n_.serviceClient<mobile_state_listener::get_current_event_mobile>("/mobile_state_listener/current_event_service")),
   total_time(0),
   run_time_robot(0),
   run_time_mobile(0),
   down_time_robot(0),
   down_time_mobile(0),
   total_count_robot(0),
   total_count_mobile(0),
   setup_time(0),
   bad_mobile_robot(0),
   good_mobile_robot(0),
   bad_robot(0),
   good_robot(0),
   set_setup(0),
   running_robot(1),
   running_mobile(1),
   bad_robot_service(n_.advertiseService("/oee/bad_robot_service",&subscriber_and_server_oee::increment_robot_bad_count,this)),
   bad_mobile_service(n_.advertiseService("/oee/bad_mobile_service",&subscriber_and_server_oee::increment_mobile_bad_count,this)),
   maintenance(n_.advertiseService("/oee/maintenance",&subscriber_and_server_oee::switch_between_runtime_downtime,this)),
   current_oee(n_.advertise<std_msgs::String>("/oee/current_oee", 1,true))
{
    std::cout << "subscriber_and_server_oee -  constructed" << std::endl;
    dataout << "total_time,";
    dataout << "runtime_robot,";
    dataout << "runtime_mobile,";
    dataout << "down_time_robot,";
    dataout << "down_time_mobile,";
    dataout << "setup_time,";
    dataout << "total_count_robot,";
    dataout << "total_count_mobile,";
    dataout << "target_count_robot,";
    dataout << "target_count_mobile,";
    dataout << "good_robot,";
    dataout << "bad_robot,";
    dataout << "good_mobile_robot,";
    dataout << "bad_mobile_robot,";
    dataout << "Note_mobile,";
    dataout << "Note_robot "<< std::endl;
    time_start = ros::Time::now();
    csv_switch_case();
}

bool subscriber_and_server_oee::increment_robot_bad_count(oee::bad_robot::Request &/*req*/, oee::bad_robot::Response &res)
{
    std::cout << "bad_robot received" << std::endl;
    this->bad_robot++;
    res.current_count = this->bad_robot;
    return true;
}

bool subscriber_and_server_oee::increment_mobile_bad_count(oee::bad_mobile_robot::Request &/*req*/, oee::bad_mobile_robot::Response &res)
{
    std::cout << "bad_mobile received" << std::endl;
    this->bad_mobile_robot++;
    res.current_count = this->bad_mobile_robot;
    return true;
}

bool subscriber_and_server_oee::switch_between_runtime_downtime(oee::runtime_downtime::Request &req, oee::runtime_downtime::Response &res)
{

    if(req.requested_time == "run_robot")
    {
        running_robot = true;
        res.current_time = "running_robot";
    }
    else if(req.requested_time == "down_robot")
    {
        running_robot = false;
        res.current_time = "downtime_robot";
    }
    else if(req.requested_time == "run_mobile")
    {
        running_mobile = true;
        res.current_time = "running_mobile";
    }
    else if(req.requested_time == "down_mobile")
    {
        running_mobile = false;
        res.current_time = "down_mobile";
    }

}

void subscriber_and_server_oee::csv_handler()
{
    //OEE = Avalibiliy x Performance x Quality
        //Avalibility = run_time /total_time
            //run_time = time within certain cases.
            //total_time = alive time of mes_client
        //Performance = total_count/target_count
        //Quality = Good_count/total_count
}


subscriber_and_server_oee::robot subscriber_and_server_oee::string_to_enum_robot(std::string input_state)
{
    if(input_state == "Wait_for_bricks")
    {
        return Wait_for_bricks;
    }
    if(input_state == "Sort_bricks")
    {
        return Sort_bricks;
    }
    if(input_state == "Wait_for_mobile")
    {
        return Wait_for_mobile;
    }
    if(input_state == "Deliver_to_mobile")
    {
        return Deliver_to_mobile;
    }
}


subscriber_and_server_oee::mobile subscriber_and_server_oee::string_to_enum_mobile(std::string input_state)
{

    if(input_state == "charging")
    {
        return charging;
    }
    if(input_state == "idle")
    {
        return idle;
    }

    if(input_state == "lidar_out")
    {
        return lidar_out;
    }
    if(input_state == "to_drone_gate")
    {
        return to_drone_gate;
    }
    if(input_state == "to_dispenser")
    {
        return charging;
    }
    if(input_state == "receiving_bricks")
    {
        return receiving_bricks;
    }

    if(input_state == "exit_gate")
    {
        return exit_gate;
    }
    if(input_state == "gate_to_road")
    {
        return gate_to_road;
    }
    if(input_state == "to_line")
    {
        return to_line;
    }
    if(input_state == "to_cell")
    {
        return to_cell;
    }

    if(input_state == "to_belt")
    {
        return to_belt;
    }
    if(input_state == "line_following_away")
    {
        return line_following_away;
    }
    if(input_state == "line_to_charger")
    {
        return line_to_charger;
    }
    if(input_state == "lidar_charger")
    {
        return lidar_charger;
    }

    if(input_state == "to_slide")
    {
        return to_slide;
    }

    if(input_state == "receiving_sorted")
    {
        return receiving_sorted;
    }

}


void subscriber_and_server_oee::csv_switch_case()
{
    rc_state_listener::get_current_event srv_robot;
    mobile_state_listener::get_current_event_mobile srv_mobile;
    std_msgs::String dataout_publish;
    nodes.clear();
    ros::master::getNodes(this->nodes);
    switch(state)
    {
        case Check_total_time:
            //Check Aliveness of nodes.
            //0: Rosout
            //1: Control_node_r [subscriber_and_server :  command_center]
            //2: Rc_state_listener [subscriber_and_server]
            //3: Oee [oee]
            //4: Mes server [mes server]
            //5: MES robotclient [mes client]
            //6: mobile_state_listener_subcriber
            //7: mobile_state_listener_server
            //8: hmi_backend  -- Need number of handlers
            //9. nav_mode_transition_handler

            std::cout << "Check total time!: " << std::endl;
//          if(nodes.size() < 8)
//          {
//                ros::Time ros_setuptime = ros::Time::now();
//                setup_time = ros_setuptime.toSec() - time_start.toSec();
//                std::cout << "Nodes size: " << nodes.size() << std::endl;
//                std::cout << "All is nodes not on - waiting and incrementing setup time: " << setup_time << std::endl;
//                std::cout << "nodes being: " << std::endl;
//                for (auto i: nodes)
//                {
//                    std::cout << i <<", ";
//                }
//                std::cout << std::endl;
//                sleep(2);
//                state = Check_total_time;
//                csv_switch_case();
//          }
            if((total_count_robot == 0) && (set_setup == false))
            {
                std::cout << "set_setup is true!" << std::endl;
                set_setup = true;
            }
            std::cout << "All nodes on: " << nodes.size() << std::endl;
            if (std::find(nodes.begin(),nodes.end(), "/control_node_r") != nodes.end())
            {
                ros_totaltime = ros::Time::now();
                total_time = ros_totaltime.toSec() - time_start.toSec();
                std::cout <<"Total_time: "<< total_time << std::endl; //Total_time
            }
            else if(std::find(nodes.begin(),nodes.end(),"/nav_mode_transition") != nodes.end())
            {
                std::cout << "Do something " << std::endl;
            }
            sleep(2);
            ros::spinOnce();
            state = Check_run_time_and_total_count;
            //csv_switch_case();
        break;

        case Check_run_time_and_total_count:
            //Call some services
            std::cout << "Check runtime and total count" << std::endl;
            mes_client_current_state_robot.call(srv_robot);
            mes_client_current_state_mobile.call(srv_mobile);

            std::cout <<"Response received robot: "<< srv_robot.response.current_event << std::endl;
            robot_note  = srv_robot.response.current_event;
            std::cout <<"Response received mobile: "<< srv_mobile.response.current_event_mobile << std::endl;
            mobile_note = srv_mobile.response.current_event_mobile;


            //Robot switch
            switch(string_to_enum_robot(robot_note))
            {
                case Wait_for_bricks:
                    std::cout << "Bricks is incomming" << std::endl;
                    ros_runtime_robot = ros::Time::now();
                    read_robot = false;
                    break;
                case Sort_bricks:
                    std::cout << "Sort bricks" << std::endl;
                    if(running_robot)
                    {
                        run_time_robot  += ros::Time::now().toSec() - ros_runtime_robot.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_robot += ros::Time::now().toSec() - ros_runtime_robot.toSec();
                    }
                    ros_runtime_robot = ros::Time::now();
                    break;
                case Wait_for_mobile:
                    std::cout << "Wait for mobile" << std::endl;
                    if(running_robot)
                    {
                        run_time_robot  += ros::Time::now().toSec() - ros_runtime_robot.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_robot += ros::Time::now().toSec() - ros_runtime_robot.toSec();
                    }
                    ros_runtime_robot = ros::Time::now();
                    break;
                case Deliver_to_mobile:
                    std::cout << "Deliver_to_mobile" << std::endl;
                    if(running_robot)
                    {
                        run_time_robot  += ros::Time::now().toSec() - ros_runtime_robot.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_robot += ros::Time::now().toSec() - ros_runtime_robot.toSec();
                    }
                    if(!read_robot)
                    {
                        total_count_robot++;  // Total count_robot
                        good_robot = total_count_robot - bad_robot;
                        std::cout << "total_coun_robot: " << total_count_robot << std::endl;
                    }
                    read_robot = true;
                    break;
            }

            switch(string_to_enum_mobile(mobile_note))
            {
                case charging:
                    std::cout << "charging" << std::endl;
                    run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    if(!read_mobile)
                    {
                        total_count_mobile++;  // Total count_robot
                        good_mobile_robot = total_count_mobile - bad_mobile_robot;
                    }
                    read_mobile = true;
                    break;
                case idle:
                    break;
                case lidar_out:
                    std::cout << "Lidar_out" << std::endl;
                    ros_runtime_mobile = ros::Time::now();
                    read_mobile = false;
                   break;
                case to_drone_gate:
                    std::cout << "to_drone_gate" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case to_dispenser:
                    std::cout << "to_dispenser" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case receiving_bricks:
                    std::cout << "receiving_bricks" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case exit_gate:
                    std::cout << "exit_gate" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case gate_to_road:
                    std::cout << "gate_to_road" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case to_line:
                    std::cout << "to_line" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case to_cell:
                    std::cout << "to_cell" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case to_belt:
                    std::cout << "to_bell" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case line_following_away:
                    std::cout << "line_following_away" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case line_to_charger:
                    std::cout << "line_to_charger" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case lidar_charger:
                    std::cout << "lidar_charger" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case to_slide:
                    std::cout << "to_slide" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
                case receiving_sorted:
                    std::cout << "receiving_sorted" << std::endl;
                    if(running_mobile)
                    {
                        run_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    else
                    {
                        down_time_mobile  += ros::Time::now().toSec() - ros_runtime_mobile.toSec(); //Run_time
                    }
                    ros_runtime_mobile = ros::Time::now();
                    break;
            }

            if(running_robot == true)
            {
                std::cout << "runmode" << std::endl;
            }
            else
            {
                std::cout << "downmode" << std::endl;
            }
            /*  Something for incrementing for the mobile robot, event need to be detected.
             *  It is currently done  in the if statement above.
             *
             */
            sleep(2);
            ros::spinOnce();
            state = Check_target_count;
            //csv_switch_case();
        break;

        case Check_target_count:
            std::cout << "Check target count" << std::endl;
            std::cout << "total_count: " << total_count_robot << std::endl;
            if((total_time%360)==0)
            {
                target_count_robot++;
                target_count_mobile++;
            }
            if((set_setup == true) && (total_count_robot == 1))
            {
                set_setup = false;
                ros::Time ros_setuptime = ros::Time::now();
                setup_time = ros_setuptime.toSec() - time_start.toSec();
            }
            sleep(2);
            ros::spinOnce();
            state = CSV_handler_state;
            //csv_switch_case();
        break;

        case CSV_handler_state:
            //CSV style total_time, run_time, total_count, target_count, good_count, bad_count , note
                std::cout << "CSV_handler_state" << std::endl;
                dataout << std::to_string(total_time)           << ","
                        << std::to_string(run_time_robot)       << ","
                        << std::to_string(run_time_mobile)      << ","
                        << std::to_string(down_time_robot)      << ","
                        << std::to_string(down_time_mobile)     << ","
                        << std::to_string(setup_time)           << ","
                        << std::to_string(total_count_robot)    << ","
                        << std::to_string(total_count_mobile)   << ","
                        << std::to_string(target_count_robot)   << ","
                        << std::to_string(target_count_mobile)  << ","
                        << std::to_string(good_robot)           << ","
                        << std::to_string(bad_robot)            << ","
                        << std::to_string(good_mobile_robot)    << ","
                        << std::to_string(bad_mobile_robot)     << ","
                        << mobile_note << ","
                        << robot_note  << std::endl;
                dataout_publish.data = std::to_string(total_time)           + ","
                                    + std::to_string(run_time_robot)       + ","
                                    + std::to_string(run_time_mobile)      + ","
                                    + std::to_string(down_time_robot)      + ","
                                    + std::to_string(down_time_mobile)      + ","
                                    + std::to_string(setup_time)           + ","
                                    + std::to_string(total_count_robot)    + ","
                                    + std::to_string(total_count_mobile)   + ","
                                    + std::to_string(target_count_robot)   + ","
                                    + std::to_string(target_count_mobile)  + ","
                                    + std::to_string(good_robot)           + ","
                                    + std::to_string(bad_robot)            + ","
                                    + std::to_string(good_mobile_robot)    + ","
                                    + std::to_string(bad_mobile_robot)     + ","
                                    + mobile_note         + ","
                                    + robot_note          + '\n';
                current_oee.publish(dataout_publish);
                sleep(2);
                ros::spinOnce();
                state = Check_total_time;
                std::cout << "Read is: " << read << std::endl;
                //csv_switch_case();
             break;

        default:
            std::cout << "Something aint right!" << std::endl;
            std::cout << "State: " << state << std::endl;
        break;
    }

    csv_switch_case();
}

