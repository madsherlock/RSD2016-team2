#ifndef SUBSCRIBER_AND_SERVER_oee_H
#define SUBSCRIBER_AND_SERVER_oee_H

#include <ros/ros.h>
#include <iostream>
#include <std_msgs/String.h>
#include <string>
#include <string>
#include <vector>
#include <algorithm>
#include <rc_state_listener/get_current_event.h>
#include <mobile_state_listener/get_current_event_mobile.h>
#include <oee/bad_mobile_robot.h>
#include <oee/bad_robot.h>
#include <oee/runtime_downtime.h>
#include <fstream>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string

class subscriber_and_server_oee
{
    public:
    enum log{Check_total_time,
             Check_run_time_and_total_count,
             Check_target_count,
             CSV_handler_state

             };
   enum robot{
                Wait_for_bricks,
                Sort_bricks,
                Wait_for_mobile,
                Deliver_to_mobile
             };
   enum mobile{
                charging,
                idle,
                lidar_out,
                to_drone_gate,
                to_dispenser,
                receiving_bricks,
                exit_gate,
                gate_to_road,
                to_line,
                to_cell,
                to_belt,
                line_following_away,
                line_to_charger,
                lidar_charger,
                to_slide,
                receiving_sorted
             };
        robot string_to_enum_robot(std::string);
        mobile string_to_enum_mobile(std::string);
        log state;
        subscriber_and_server_oee();
        void csv_switch_case();
        long target_count_robot;
        long target_count_mobile;
        std::ofstream dataout;
        bool read_robot;
        bool read_mobile;
        bool increment_robot_bad_count(oee::bad_robot::Request &req ,oee::bad_robot::Response &res);
        bool increment_mobile_bad_count(oee::bad_mobile_robot::Request &req ,oee::bad_mobile_robot::Response &res);
        bool switch_between_runtime_downtime(oee::runtime_downtime::Request &req, oee::runtime_downtime::Response &res);
    private:
        ros::NodeHandle n_;
        ros::ServiceClient mes_client_current_state_robot;
        ros::ServiceClient mes_client_current_state_mobile;
        std::vector<std::string> nodes;
        std::string mobile_note;
        std::string robot_note;
        void csv_handler();
        long total_time;
        long run_time_robot;
        long run_time_mobile;
        long down_time_robot;
        long down_time_mobile;
        long total_count_robot;
        long total_count_mobile;
        long setup_time;
        long bad_mobile_robot;
        long good_mobile_robot;
        long bad_robot;
        long good_robot;
        bool set_setup;
        bool running_robot;
        bool running_mobile;
        ros::Time ros_runtime_robot;
        ros::Time ros_runtime_mobile;
        ros::Time ros_totaltime;
        ros::Time time_start;
        ros::ServiceServer good_robot_service;
        ros::ServiceServer bad_robot_service;
        ros::ServiceServer good_mobile_service;
        ros::ServiceServer bad_mobile_service;
        ros::ServiceServer maintenance;
        ros::Publisher current_oee;
};


#endif // SUBSCRIBER_AND_SERVER_oee_H

