#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <string>
#include <cstdint> // Standard types
#include <cstddef> // nullPtr
#include <iostream>
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#define SOCKET_BUFFER_SIZE 1024

namespace upr {

	class csocket {

		public:

			//--------------------------------------------------------------------------
			//
      // Custom Types
      //
      //--------------------------------------------------------------------------

      /*
      enum SOCKET_TYPE {

        UDP = 0,
        TCP = 1

      }
      */

			//
			enum class SocketType : uint8_t { UNDEFINED = 0, UDP = 1, TCP = 2 };

			//
			enum class SocketStatus : uint8_t { UNDEFINED = 0, ERROR = 1, SUCCESS = 2 };

      //--------------------------------------------------------------------------
      //
      // Constructors
      //
      //--------------------------------------------------------------------------

			/*
       * @description
       * @params
       *    hostAddress - IP of the the host/server to connect to
       *    hostPort    - Port number to use on the host/server
       *    type        - Socket type, TCP or UDP
       *    rxCallback  - Callback function to be called upon data reception
       */
      csocket(std::string hostAddress,
             std::string hostPort,
             upr::csocket::SocketType type);

      //--------------------------------------------------------------------------
      //
      // Deconstructors
      //
      //--------------------------------------------------------------------------

			/*
       * @descrpition
       */
			~csocket();

			//--------------------------------------------------------------------------
			//
			// Getters
			//
			//--------------------------------------------------------------------------

			/*
			 * @description
			 */
			std::string getHostAddress();

			/*
			 * @description
			 */
			std::string getHostPort();

			/*
			 * @description
			 */
			SocketStatus getStatus();

			//--------------------------------------------------------------------------
			//
			// Setters
			//
			//--------------------------------------------------------------------------

			/*
       * @description
       */

			//--------------------------------------------------------------------------
			//
			// Member Functions
			//
			//--------------------------------------------------------------------------

			/*
			 * @description
			 */
			int cbind();

			/*
       * @description Sends a given number of bytes through the socket
       * @params
       *    bytesOut  - pointer to buffer containing the bytes you want to sent
       *    nBytesOut -  Number of bytes to sent
       * @return Number of bytes sent, -1 means and error has occurred
       */
       int send(const char *bytesOut, int nBytes);

			/*
       * @description Sends a given number of bytes through the socket and
       *              listen on the socket for a response immediately after
       * @params
       *    bytesOut   - pointer to buffer containing the bytes you want to sent
       *    nBytesOut  - Number of bytes to sent
       *    bytesIn    - pointer to buffer where receied data can be stored
       * @return Number of bytes received
 			 */
			int sendAndReceive(char *bytesOut, int nBytesOut, char *bytesIn);

			/*
			 * @description
       * @params
       *   bytes - pointer to buffer where received data can be stored
       * @return The number of bytes received through the socket
			 */
			int receive(char *bytesIn );

     protected:

			//--------------------------------------------------------------------------
			//
			// Instance variables
			//
			//--------------------------------------------------------------------------

			// Handler id of socket created with c functoin socket()
			int _socket;

			//
			std::string _address;

			//
			std::string _port;

			// Socket address specifications
			struct sockaddr_in _serverAddress;

			//
			struct addrinfo *_addrinfo;

			// Integer value containing the byte size of the _serverAddress object
			socklen_t _serverAddressSize;

			//
			struct timeval _readTimeout;

			// Buffers to be used for transmission and reception
			char _bufferIn[ SOCKET_BUFFER_SIZE ];
			char _bufferOut[ SOCKET_BUFFER_SIZE ];

			//
			SocketType _type;

			//
			SocketStatus _status;

     };
}
