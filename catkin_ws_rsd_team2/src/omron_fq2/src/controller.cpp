#include "ros/ros.h"
#include "omron_fq2/Measure.h"
#include "omron_fq2/csocket.hpp"

#include <string>
#include <cstddef>

#include <new>

//#define CAMERA_IP "127.0.0.1"
//#define CAMERA_PORT "9600"

#define CAMERA_IP "192.168.100.70"
#define CAMERA_PORT "9600"

#define SERVER_IP "192.168.100.34"
#define SERVER_PORT "9600"

//------------------------------------------------------------------------------
//
// Global Variables
//
//------------------------------------------------------------------------------

//
upr::csocket *_pControlSocket = nullptr;

//
upr::csocket *_pOutputSocket = nullptr;

//------------------------------------------------------------------------------
//
// Service Handlers
//
//------------------------------------------------------------------------------

/*
 * @description
 */
bool measureServiceHandler(omron_fq2::Measure::Request &req,
													 omron_fq2::Measure::Response &res)
{

	//
	int i = 0;

	//ROS_INFO("Did receive service request with arguments: %s", req.param.c_str());

	//
	std::string command = "m";

	ROS_INFO("Command: %s", command.c_str());

	// Incoming buffer
	char bufferIn[1024];
	char bufferOut[1024];

	// Use to receive number of bytes when sending and receiving
	int nBytes = command.length();

	//
	for( i = 0; i < nBytes; i++ ) {

		//
		bufferOut[i] = command[i];

	}

	bufferOut[i] = '\0';

	//ROS_INFO("bufferOut: %s", bufferOut);

	//
	nBytes =_pControlSocket->send(bufferOut, nBytes);

	// If the number of bytes sent is lower than the
	// number of characters that command string consist,
	// something obvisouly went wrong
	if( nBytes < command.length() ) {

		// Handle the error

	}

	// Receive status response from camera
	nBytes = _pControlSocket->receive( bufferIn );

	//
	bufferIn[nBytes] = '\0'; 

	//
	//ROS_INFO("Camera response: %s", bufferIn);

	//
	std::string cameraResponse(bufferIn);

	//
	res.status = cameraResponse;

	//
	if( nBytes > 1 && bufferIn[0] == 'O' && bufferIn[1] == 'K' ) {

		//
		ROS_INFO("Trying to receive data from camera");

		//
		nBytes = _pOutputSocket->receive(bufferIn);

		// Was any data recieved?
		if( nBytes > 0 ) {

			//
			bufferIn[nBytes] = '\0'; 

			//
			ROS_INFO("%i bytes was received from the camera", nBytes);

			// Create c++ string of received camera output data
			std::string data(bufferIn);

			//
			res.data = data;

		}

	}
	else {
		res.data = "";
	}


	return true;
}

//------------------------------------------------------------------------------
//
// Utility Functions
//
//------------------------------------------------------------------------------

/*
 * @description
 */
void cleanup()
{

	delete _pControlSocket;

}

//------------------------------------------------------------------------------
//
// Node Start
//
//------------------------------------------------------------------------------

/*
 * @description
 */
int main(int argc, char **argv)
{

	// Configure socket to control the omron camera with
	_pControlSocket = new (std::nothrow) upr::csocket( CAMERA_IP, CAMERA_PORT, upr::csocket::SocketType::UDP );

	// Configure socket to control the omron camera with
	_pOutputSocket = new (std::nothrow) upr::csocket( SERVER_IP, SERVER_PORT, upr::csocket::SocketType::UDP );

	// mae _pOutputSocket a server
	_pOutputSocket->cbind();

	if( _pControlSocket->getStatus() == upr::csocket::SocketStatus::SUCCESS &&
 		_pOutputSocket->getStatus() == upr::csocket::SocketStatus::SUCCESS) {

		//
		ROS_INFO("Did create socket");

	}
	else {

		ROS_INFO("Failed to create socket");

	}

	// Initialise ros
	ros::init(argc, argv, "omron_fq2_controller");

	// Create node handle
	ros::NodeHandle nh;

	// Start advertisinf the measure service
	ros::ServiceServer service = nh.advertiseService("measure", measureServiceHandler);

	//
	ros::spin();

	// Test for node shutdown caused by sigint, ctrl + c etc.
	//while(ros::ok())
	//	;

	// Node has bee shutdown so do proper cleanup
	cleanup();

	return 0;

}
