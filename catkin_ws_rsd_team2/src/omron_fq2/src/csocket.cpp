#include "csocket.hpp"

//--------------------------------------------------------------------------
//
// Constructors
//
//--------------------------------------------------------------------------

/*
 * @descrpition
 */
upr::csocket::csocket(std::string hostAddress,
										std::string hostPort,
										upr::csocket::SocketType type)
{

	//
	_type = type;

	// Initialise socket status to disconnected
	_status = SocketStatus::UNDEFINED;

	// Use to configure address for socket
	struct addrinfo hints;

	switch ( _type ) {

		// User requested UDP socket
		case SocketType::UDP:

			//
			memset(&hints, 0, sizeof(hints));

			//
			hints.ai_flags 		= AI_NUMERICSERV; // Use numeric server address
			hints.ai_family 	= AF_UNSPEC; 			// Return socket address
			hints.ai_socktype = SOCK_DGRAM; 		// Socket Datagrams
			hints.ai_protocol = IPPROTO_UDP; 		// Protocol is UDP

			// Get the needed address info for used with socket
			// Error dectection mihgt be a good idea to use here
			getaddrinfo(	hostAddress.c_str(),
										hostPort.c_str(),
										&hints,
										&_addrinfo );

			// Create socket
			//_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
			_socket = socket(_addrinfo->ai_family, _addrinfo->ai_socktype, _addrinfo->ai_protocol);

			break;

		// User requested TCP socket
		case SocketType::TCP:

			//
			_socket = -1;

			break;

		//
		default:

			//
			_socket = -1;

			//
			_type = SocketType::UNDEFINED;

			break;
	}

	//
	if( _socket > 0 ) {


		// Configure Server Address
		//_serverAddress.sin_family = AF_INET;
		//_serverAddress.sin_family = AF_UNSPEC;
		//_serverAddress.ai_flags = AI_NUMERICSERV;
		//_serverAddress.sin_port = htons(hostPort);
		//_serverAddress.sin_addr.s_addr = inet_addr(hostAddress.c_str());
		//memset(_serverAddress.sin_zero, '\0', sizeof(_serverAddress.sin_zero));

		//
		//_serverAddressSize = sizeof(_serverAddress);

		// Configure socket read timeout
		_readTimeout.tv_sec = 0;
		_readTimeout.tv_usec = 200000;

		// Set socket read timeout
		setsockopt(	_socket,
							 	SOL_SOCKET,
								SO_RCVTIMEO,
								&_readTimeout,
								sizeof (_readTimeout) );

		//
		_status = SocketStatus::SUCCESS;

	}
	else {

		//
		_status = SocketStatus::ERROR;

	}

}

//--------------------------------------------------------------------------
//
// Deconstructors
//
//--------------------------------------------------------------------------

/*
 * @description
 */
upr::csocket::~csocket()
{
	freeaddrinfo(_addrinfo);
	close(_socket);
}

//--------------------------------------------------------------------------
//
// Getters
//
//--------------------------------------------------------------------------

/*
 * @description
 */
std::string upr::csocket::getHostAddress()
{
	return _address;
}

/*
 * @description
 */
std::string upr::csocket::getHostPort()
{
	return _port;
}

/*
 * @description
 */
upr::csocket::SocketStatus upr::csocket::getStatus()
{
	return _status;
}

//--------------------------------------------------------------------------
//
// Setters
//
//--------------------------------------------------------------------------

/*
 * @description
 */

//--------------------------------------------------------------------------
//
// Member Functions
//
//--------------------------------------------------------------------------

/*
 * @description
 */
int upr::csocket::cbind()
{
	return bind(_socket, _addrinfo->ai_addr, _addrinfo->ai_addrlen);
}

/*
 * @description Sends a given number of bytes through the socket
 * @params
 *    bytesOut  - pointer to buffer containing the bytes you want to sent
 *    nBytesOut - Number of bytes to sent
 * @return Number of bytes sent
 */
int upr::csocket::send(const char *bytesOut, int nBytes)
{

	// Iteration variable
	int i = 0;

	//
	int bytesSent = -1;

	// Copy bytes from bytesOut function argument
	// to the member variable _bufferOut
	for( i = 0; i < nBytes; i++ ) {

		// Copying byte by byte
		_bufferOut[i] = bytesOut[i];

	}

	//
	_bufferOut[i] = '\n';

	// Data is only sent through the socket if the socket type is defined
	if( _type != SocketType::UNDEFINED ) {

		// Send data inside the _bufferOut through the socket
		bytesSent = sendto( _socket,
												_bufferOut,
												nBytes,
												0,
												_addrinfo->ai_addr, //(struct sockaddr *)&_serverAddress,
												_addrinfo->ai_addrlen); //_serverAddressSize );


	}

	return bytesSent;
}

/*
 * @description Sends a given number of bytes through the socket and
 *              listen on the socket for a response immediately after
 * @params
 *    bytesOut   - pointer to buffer containing the bytes you want to sent
 *    nBytesOut  - Number of bytes to sent
 *    bytesIn    - pointer to buffer where receied data can be stored
 * @return Number of bytes received
 */
int upr::csocket::sendAndReceive(char *bytesOut, int nBytesOut, char *bytesIn)
{
	return 0;
}

/*
 * @description
 * @params
 *   bytes - pointer to buffer where received data can be stored
 * @return The number of bytes received through the socket
 */
int upr::csocket::receive(char *bytesIn )
{

	//
	int bytesReceived = -1;

	// Data can only be recived through the socket if the socket type is defined
	if( _type != SocketType::UNDEFINED ) {

		/*
		// Receive data through the socket
		bytesReceived = recvfrom( _socket,
															bytesIn ,
															1024,
															0,
															(struct sockaddr *)&_serverAddress,
															&_serverAddressSize);
		*/

		//
		bytesReceived = recv(_socket, bytesIn, 1024, 0);

	}

	return bytesReceived;
}
