#include "ros/ros.h"
#include "omron_fq2/Output.h"
#include "omron_fq2/csocket.hpp"

#include <string>
#include <cstddef>

#include <new>

//#define CAMERA_IP "127.0.0.1"
//#define CAMERA_PORT "8080"

#define SERVER_IP "192.168.100.50"
#define SERVER_PORT "9600"

//------------------------------------------------------------------------------
//
// Global Variables
//
//------------------------------------------------------------------------------

//
upr::csocket *_pOutputSocket = nullptr;

//------------------------------------------------------------------------------
//
// Utility Functions
//
//------------------------------------------------------------------------------

/*
 * @description
 */
void cleanup()
{

	delete _pOutputSocket;

}

//------------------------------------------------------------------------------
//
// Node Start
//
//------------------------------------------------------------------------------

/*
 * @description
 */
int main(int argc, char **argv)
{

	//
	int nBytes = 0;

	//
	char bufferIn[1024];
	bufferIn[0] = '\0';

	// Configure socket to control the omron camera with
	_pOutputSocket = new (std::nothrow) upr::csocket( SERVER_IP, SERVER_PORT, upr::csocket::SocketType::UDP );

	//
	_pOutputSocket->cbind();

	// Initialise ros
	ros::init(argc, argv, "omron_fq2_publisher");

	// Create node handle
	ros::NodeHandle nh;

	// Start advertisinf the measure service
	ros::Publisher publisher = nh.advertise<omron_fq2::Output>("output", 1000);

	//
	ros::Rate loop_rate(10);

	// Run loop until node terminates
	while(ros::ok()) {

		//
		nBytes = 0;

		//
		nBytes = _pOutputSocket->receive(bufferIn);

		// Was any data recieved?
		if( nBytes > 0 ) {

			// Create c++ string of received camera output data
			std::string data(bufferIn);

			// Ros message to publish
			omron_fq2::Output msg;

			// Setting
			msg.params = data;

		 	//
			publisher.publish(msg);

			//
			ros::spinOnce();

		}

	}

	// Node has bee shutdown so do proper cleanup
	cleanup();

	return 0;
}
