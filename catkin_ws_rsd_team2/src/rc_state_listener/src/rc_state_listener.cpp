/*      Personal note: Whenever you think, hey? why did I make this node....
 *      Then think back ... back to 6th of December where you deleted it and the HMI didn't not work.
 *      Since its status was provided by this one.
 *
*
 */
#include<ros/ros.h>
#include<iostream>
#include"subscriber_and_server.h"


int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "rc_state_listener");

  //Create an object of class SubscribeAndPublish that will take care of everything
  subscriber_and_server middleman;

  ros::spin();

  return 0;
}
