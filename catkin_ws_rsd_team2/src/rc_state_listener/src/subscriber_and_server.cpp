#include "subscriber_and_server.h"

subscriber_and_server::subscriber_and_server() :
   n_("~"),
   mes_server_to_rc_state_listener_sub_(n_.subscribe("/MESState", 1, &subscriber_and_server::callback_mes_1, this)),
   getCurrent_Event_Service(n_.advertiseService("/rc_state_listener/Get_Current_Event", &subscriber_and_server::get_current_event_callback,this))
{
    std::cout << "subscriber_and_server -  constructed" << std::endl;
}

void subscriber_and_server::callback_mes_1(const std_msgs::String& input)
{
    std::cout << "input received!" << std::endl;
    current_event = check_xml(input.data);
}

std::string subscriber_and_server::check_xml(std::string source)
{
// tag::code[]
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load(source.c_str());
    std::string output = doc.child("log_entry").child("event").child_value();
    return output;
// end::code[]
}


bool subscriber_and_server::get_current_event_callback(rc_state_listener::get_current_event::Request &/*req*/,
                                              rc_state_listener::get_current_event::Response &res)
{
    res.current_event = this->current_event;
    return true;
}
