#ifndef SUBSCRIBER_AND_SERVER_H
#define SUBSCRIBER_AND_SERVER_H

#include <ros/ros.h>
#include <iostream>
#include <std_msgs/String.h>
#include <string>
#include <pugixml.hpp>
#include <pugiconfig.hpp>
#include "rc_state_listener/get_current_event.h"
class subscriber_and_server
{
    public:
        subscriber_and_server();
        void callback_mes_1(const std_msgs::String& input);
        bool get_current_event_callback(rc_state_listener::get_current_event::Request &req,rc_state_listener::get_current_event::Response &res);
        std::string check_xml(std::string);
    private:
        std::string current_event;
        ros::NodeHandle n_;
        ros::Subscriber mes_server_to_rc_state_listener_sub_;
        ros::ServiceServer getCurrent_Event_Service;
};

#endif // SUBSCRIBER_AND_SERVER_H
