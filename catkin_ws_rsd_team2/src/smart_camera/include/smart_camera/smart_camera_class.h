#ifndef SMART_CAMERA_CLASS_H
#define SMART_CAMERA_CLASS_H

#include <ros/ros.h>
#include "std_msgs/Float64.h"
#include "std_msgs/String.h"
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include <smart_camera/brick_order.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>
#include <smart_camera/robot_coords_array.h>
#include <smart_camera/conveyor_stop.h>
#include <omron_fq2/Measure.h>


class smart_camera_class
{
public:
	std_msgs::String random_string();
	smart_camera::brick_info_array parse_string (std_msgs::String);
	smart_camera::robot_pick_coords_array transform (smart_camera::brick_info_array);
	std_msgs::String conveyor_start_stop (smart_camera::robot_pick_coords_array, smart_camera::brick_order);
};


class smart_camera_server_class
{
public: 
	smart_camera_server_class();
	smart_camera::brick_info_array parse_string (std_msgs::String);
	smart_camera::robot_pick_coords_array transform (smart_camera::brick_info_array);
	std_msgs::String conveyor_start_stop (smart_camera::robot_pick_coords_array, smart_camera::brick_order);
	bool return_robot_coords(smart_camera::robot_coords_array::Request  &req, smart_camera::robot_coords_array::Response &res);
private:
	ros::NodeHandle server_n;
	ros::Subscriber smart_camera_server_to_robot_sub;
	ros::ServiceClient client;
	ros::ServiceServer service;
};

class camera_stop_conveyor_server_class
{
public: 
	camera_stop_conveyor_server_class();
private: 
	ros::NodeHandle n2;
	ros::Subscriber order_sub;
	ros::Publisher camera_stop_pub;
	ros::ServiceClient client;
	ros::ServiceServer stop_service;
};

#endif // SMART_CAMERA_CLASS_H