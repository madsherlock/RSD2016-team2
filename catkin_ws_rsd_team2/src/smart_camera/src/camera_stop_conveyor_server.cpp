#include "ros/ros.h"
#include "smart_camera/smart_camera_class.h"
#include <omron_fq2/Measure.h>
#include <omron_fq2/Output.h>


camera_stop_conveyor_server_class::camera_stop_conveyor_server_class() :
	n2("another node handle"),
	client(server_n.serviceClient<omron_fq2::Measure>("/omron_fq2_controller/measure")),
  service(server_n.advertiseService("/Robot_coords_server/get_bricks_positions", &camera_stop_conveyor_server_class::return_robot_coords,this)),

{
    std::cout << "subscriber_and_server -  constructed" << std::endl;
}


bool camera_stop_conveyor_server_class::return_robot_coords(smart_camera::robot_coords_array::Request  &req,
                                                            smart_camera::robot_coords_array::Response &res)
{
  std::cout<< " here123"<<std::endl;
  smart_camera::conveyor_stop srv;

  srv.request.param = "";
  client.call(srv);

  std_msgs::String final_string;
  final_string.data = srv.response.string;

  smart_camera_class stuff;

  std::cout << "Here" << std::endl; 
  smart_camera::brick_info_array brick_array;
  brick_array = stuff.parse_string(final_string);

  smart_camera::robot_pick_coords_array coords_array;
  coords_array = stuff.transform (brick_array);

  res.array = coords_array;
  //res.array.push_back(coords_array.array);

  ROS_INFO("requested brick positions");
  ROS_INFO("sending back response: ", res.array);
  
  return true;
}