#include "ros/ros.h"
#include <sstream>
#include <iostream>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include <smart_camera/smart_camera_class.h>
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>
#include <smart_camera/brick_order.h>
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
smart_camera::robot_pick_coords_array coords_array;
smart_camera::brick_order order;

ros::Publisher conveyor_pub;

void callback1(smart_camera::robot_pick_coords_array coords_array_callback)
{
    coords_array = coords_array_callback;

    smart_camera_class conveyor_control;

    std_msgs::String run_command;

    run_command = conveyor_control.conveyor_start_stop (coords_array, order);

    conveyor_pub.publish(run_command);

}

void callback2(smart_camera::brick_order order_callback)
{
    order = order_callback;
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "Conveyor_start_stop_node");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ros::Subscriber sub1 = n.subscribe("Robot_pick_points_topic", 10, callback1);
  ros::Subscriber sub2 = n.subscribe("Order_topic", 10, callback2);


  conveyor_pub = n.advertise<std_msgs::String>("Conveyor_start_stop_topic", 10);

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

  return 0;
}


