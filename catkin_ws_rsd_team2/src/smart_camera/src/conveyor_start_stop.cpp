#include <sstream>
#include <iostream>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include <smart_camera/smart_camera_class.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>
#include <smart_camera/brick_order.h>

std_msgs::String smart_camera_class::conveyor_start_stop (smart_camera::robot_pick_coords_array coords_array,
									                      smart_camera::brick_order order)
{
	std_msgs::String run_command;
    std::stringstream ss;

    for (int i=0; i < coords_array.array.size(); i++)
    {
    	if (order.blue_order > 0 && coords_array.array.at(i).color == 1)
    	{
    		ss <<"STOP";
    		break;
    	}
    	if (order.red_order > 0 && coords_array.array.at(i).color == 2)
    	{
    		ss <<"STOP";
    		break;
    	}  
    	if (order.yellow_order > 0 && coords_array.array.at(i).color == 3)
    	{
    		ss <<"STOP";
    		break;
    	}  	
    	else 
    	{
    		ss <<"RUN";
    	}
    }

    run_command.data = ss.str();
	return run_command; 
}
