#include <iostream>
#include <sstream>
#include <smart_camera/smart_camera_class.h>
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include "std_msgs/String.h"

smart_camera::brick_info_array smart_camera_class::parse_string (std_msgs::String measurements_string)
{
	smart_camera::brick_info_array brick_array;
	smart_camera::brick_info brick;

    std::istringstream ss(measurements_string.data);
    std::string token;

    int counter = 0;

    while(std::getline(ss, token, ',')) 
    {
    	if(std::stod(token) == 0)
    	{
    		break;
    	}
    	switch (counter % 4)
    	{ 	
    		case 0:
    		brick.X_grav = std::stod(token); break;
    		case 1: 
    		brick.Y_grav = std::stod(token); break;
    		case 2: 
    		brick.Area = std::stod(token); break;
    		case 3: 
    		brick.Angle = std::stod (token);
    		brick_array.brick_info_array.push_back(brick); break;
    	}
    	counter ++;
    }
    return brick_array;
}
