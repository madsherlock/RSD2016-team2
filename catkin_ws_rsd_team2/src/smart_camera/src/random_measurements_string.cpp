#include <iostream>
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include <random>
#include <math.h>
#include "smart_camera/smart_camera_class.h"
#include "std_msgs/String.h"
#include <sstream>
//# define M_PI          3.141592653589793238462643383279502884L /* pi */
using namespace std;

std_msgs::String smart_camera_class::random_string()
{
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<std::mt19937::result_type> dist_X_grav(100,651); // distribution in range [1, 6]
    std::uniform_int_distribution<std::mt19937::result_type> dist_Y_grav(92,379); // distribution in range [1, 6]
    std::uniform_int_distribution<std::mt19937::result_type> dist_Area(4000, 20000); // distribution in range [1, 6]
    std::uniform_real_distribution<double> dist_Angle(0,2*M_PI); // distribution in range [1, 6]
    
    std_msgs::String measurements_string;
    std::stringstream ss;

    ss << " 189.6, 69.6, 6000, 3.4,";

    for (int i=0; i<2; i++)
    {
    	ss << dist_X_grav(rng)<<" ," ;
    	ss << dist_Y_grav(rng)<<" ," ;
    	ss << dist_Area(rng)<< " ,";
    	ss << dist_Angle(rng)<< " ,";
    }

    ss << " 0.00, 0, 00, 0,";

    measurements_string.data = ss.str();

    return measurements_string;
}
