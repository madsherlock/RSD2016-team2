#include "ros/ros.h"
#include "smart_camera/robot_coords_array.h"
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "get_brick_positions_client");
  /*if (argc != 1)
  {
    ROS_INFO("usage: getting bricks positions");
    return 1;
  }*/

  ros::NodeHandle n;
  ros::ServiceClient client2 = n.serviceClient<smart_camera::robot_coords_array>("get_bricks_positions"); 
  smart_camera::robot_coords_array srv;

  bool success = client2.call(srv);

  if (success)
  {
    ROS_INFO("Brick positions: ", srv.response.array);
  }
  else
  {
    ROS_ERROR("Failed to call service get_brick_positions");
    return 1;
  }

  return 0;
}