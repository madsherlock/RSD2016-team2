#include "ros/ros.h"
#include "smart_camera/robot_coords_array.h"
#include "std_msgs/String.h"
#include <smart_camera/smart_camera_class.h>
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>
#include <omron_fq2/Measure.h>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "Robot_coords_server");

  ROS_INFO("Ready to return robot coordinates of the bricks! ");

  smart_camera_server_class camera_man;
  
  ros::spin();

  return 0;
}