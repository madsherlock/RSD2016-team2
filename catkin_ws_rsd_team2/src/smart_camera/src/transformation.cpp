#include <smart_camera/smart_camera_class.h>
#include <smart_camera/brick_info.h>
#include <smart_camera/brick_info_array.h>
#include <smart_camera/robot_pick_coords.h>
#include <smart_camera/robot_pick_coords_array.h>

smart_camera::robot_pick_coords_array smart_camera_class::transform (smart_camera::brick_info_array brick_array)
{
	smart_camera::robot_pick_coords_array coords_array;

	double transformation[4][4] = {{0.018726,0.95337,0.30121,-26.592},
								   {0.99964,-0.012007,-0.024144,-582.86},
								   {0.019401,-0.30155,0.95325,86.549},
								   {0,0,0,1}};

	double scaling = 100.0/465.0;

	std::cout << scaling << std::endl;

	for (int i=0; i < brick_array.brick_info_array.size(); i++)
	{
		smart_camera::robot_pick_coords coords;


		coords.X = transformation[0][0] * brick_array.brick_info_array.at(i).X_grav * scaling + 
				   transformation[0][1] * brick_array.brick_info_array.at(i).Y_grav * scaling +
				   transformation[0][3];
		coords.Y = transformation[1][0] * brick_array.brick_info_array.at(i).X_grav * scaling + 
				   transformation[1][1] * brick_array.brick_info_array.at(i).Y_grav * scaling +
				   transformation[1][3];
		coords.Z = transformation[2][0] * brick_array.brick_info_array.at(i).X_grav * scaling + 
				   transformation[2][1] * brick_array.brick_info_array.at(i).Y_grav * scaling +
				   transformation[2][3];

		coords.angle = brick_array.brick_info_array.at(i).Angle;
	    
	    if (brick_array.brick_info_array.at(i).Area > 4000 &&
			brick_array.brick_info_array.at(i).Area < 6500)
		{
			coords.color = 1;
		}

		if (brick_array.brick_info_array.at(i).Area >= 6500 &&
			brick_array.brick_info_array.at(i).Area <= 12000)
		{
			coords.color = 2;
		}

		if (brick_array.brick_info_array.at(i).Area > 12000 &&
			brick_array.brick_info_array.at(i).Area < 20000)
		{
			coords.color = 3;
		}

		coords_array.array.push_back(coords);

	}


	return coords_array; 
}
