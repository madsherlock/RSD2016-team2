#include <iostream>
#include <opencv2/core/core.hpp>
#include "../utils/utils.h"

#include "../vision/imgshow.hpp"

using namespace std;

int main(){

    cout << "Testing freespace analyzer" << endl;

    cv::Mat1b freespace_img(1080,1920);
    cv::Mat1b dispenser_img(1080,1920);

    FreespaceAnalyzer fsa;

    for(int x=0; x<1920; ++x)
        for(int y=0; y<1080; ++y) {
            const bool in_freespace = fsa.pointInFreespace(x,y);
            const bool in_dispenser = fsa.pointInDispenser(x,y);
            freespace_img(y,x)=in_freespace?(126):(0);
            dispenser_img(y,x)=in_dispenser?(126):(0);
        }

    imgshow(freespace_img,"freespace");
    imgshow(dispenser_img,"dispenser");

    cv::Mat1b sum = freespace_img+dispenser_img;
    imgshow(sum,"sum");

    cout << "End of freespace analyzer test" << endl;

    return 0;
}
