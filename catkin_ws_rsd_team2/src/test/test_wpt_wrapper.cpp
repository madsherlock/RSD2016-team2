#include <iostream>
#include <fstream>
#include <ros/ros.h>
#include <std_msgs/String.h>

class wpt_wrapper {
private:
    ros::NodeHandle n;
    ros::Subscriber route_sub;
    void file_callback(const std_msgs::StringPtr &msg){
        std::ofstream wpts("~/.ros/waypoints.txt", std::ios_base::out);
        wpts << msg->data;
        wpts.close();
    }
public:
    wpt_wrapper() :
        n("~"),
        route_sub(n.subscribe("/wpt_file/route",1,&wpt_wrapper::file_callback,this))
    {
        while(ros::ok()){
            ros::spin();
        }
    }
};


int main(int argc, char* argv[]){
    ros::init(argc,argv,"wpt_wrapper");
    wpt_wrapper w;
    return 0;
}
