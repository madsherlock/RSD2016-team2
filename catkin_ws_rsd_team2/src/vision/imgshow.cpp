#include "../vision/imgshow.hpp"

void imgshow(cv::Mat &img, std::string window_name){
    cv::namedWindow(window_name, CV_WINDOW_NORMAL);
    cv::imshow(window_name, img);
    cv::waitKey(0);
}
