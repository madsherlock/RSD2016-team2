#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

/**
 * @brief imgshow
*  dummy function to show folder structure of project.
*  Shows an image and waits for a key.
 * @param img
 * input image to be shown.
 * @param window_name
 * Optional name of the window.
 * Defaults to "image".
 */
void imgshow(cv::Mat &img, std::string window_name="image");
