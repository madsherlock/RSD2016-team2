#include "ros/ros.h"
#include "vrep_common/simGetCameraViewField.h"
#include "vrep_common/simSetBrick.h"
#include "vrep_common/simGetLinearJointsPathToPoint.h"

#include <stdlib.h>
#include <time.h>

#include <string>
#include <iostream>
#include <sstream>
#include <cstddef>

#include <new>

#define NUMBER_OF_JOINTS 6
#define NUMBER_OF_TESTS 100

//------------------------------------------------------------------------------
//
// Global Variables
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
// Service Handlers
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//
// Utility Functions
//
//------------------------------------------------------------------------------

/*
 * @description
 */
void cleanup()
{


}

//------------------------------------------------------------------------------
//
// Node Start
//
//------------------------------------------------------------------------------

/*
 * @description
 */
int main(int argc, char **argv)
{

	//
	int i = 0;
	int j = 0;
	int h = 0;

	double cvfWidth 	= 0.0;
	double cvfHeight 	= 0.0;

	//
	std::stringstream ss;

	// Initialise ros
	ros::init(argc, argv, "vrep_graspTest");

	// Create node handle
	ros::NodeHandle nh;

	//
	ros::ServiceClient cvfClient 		= nh.serviceClient<vrep_common::simGetCameraViewField>("/vrep/SimGetCameraViewField");
	ros::ServiceClient brickClient 	= nh.serviceClient<vrep_common::simSetBrick>("/vrep/SimSetBrick");
	ros::ServiceClient graspClient 	= nh.serviceClient<vrep_common::simGetLinearJointsPathToPoint>("/vrep/SimGetLinearJointsPathToPoint");

	//
	vrep_common::simGetCameraViewField cvfSrv;
	vrep_common::simSetBrick brickSrv;
	vrep_common::simGetLinearJointsPathToPoint graspSrv;

	//
	graspSrv.request.steps 						= 100;

	// Always use the same intitial configuration for grasping
	graspSrv.request.startJoints[0] 	= 1.5411399602890015;
	graspSrv.request.startJoints[1] 	= 0.0;
	graspSrv.request.startJoints[2] 	= 0.0;
	graspSrv.request.startJoints[3] 	= 0.0;
	graspSrv.request.startJoints[4] 	= 0.0;
	graspSrv.request.startJoints[5] 	= 0.0;

	// Set random seed value
	srand (time(NULL));

	// Get Camera View Field Dimensions
	if( cvfClient.call(cvfSrv) ) {

		cvfWidth 	= cvfSrv.response.width;
		cvfHeight = cvfSrv.response.height;

	}

	// Main loop
	for( i = 0; i < NUMBER_OF_TESTS; i++ ) {

		brickSrv.request.x = (((double)(rand() % 100) / 100.0) * cvfWidth) - cvfWidth * 0.5;
		brickSrv.request.y = (((double)(rand() % 100) / 100.0) * cvfHeight) - cvfWidth * 0.5;

		//
		if( brickClient.call(brickSrv) ) {

				// Configure cartesian point and orientation
				graspSrv.request.goalPoint[0] 		= brickSrv.response.brickGeometry[0];
				graspSrv.request.goalPoint[1] 		= brickSrv.response.brickGeometry[1];
				graspSrv.request.goalPoint[2] 		= brickSrv.response.brickGeometry[2];
				graspSrv.request.goalPoint[3] 		= brickSrv.response.brickGeometry[3];
				graspSrv.request.goalPoint[4] 		= brickSrv.response.brickGeometry[4];
				graspSrv.request.goalPoint[5] 		= brickSrv.response.brickGeometry[5];

				// Call grasp planner service
				if(graspClient.call(graspSrv)) {

					//
					if(graspSrv.response.steps > 0) {

						ROS_INFO("%f, %f, 1", brickSrv.request.x, brickSrv.request.y);
						//printf("%f, %f, 1", brickSrv.request.x, brickSrv.request.y);

					}
					else {

						ROS_INFO("%f, %f, 0", brickSrv.request.x, brickSrv.request.y);
						//printf("%f, %f, 0", brickSrv.request.x, brickSrv.request.y);

					}

				}

		}

	}

	/*
	//
	if(client.call(getJointsPathSrv)) {

		ROS_INFO("GetJointsPath Service responded with success\n");
		ROS_INFO("Number of steps: %hi\n", getJointsPathSrv.response.steps);
		ROS_INFO("Path: \n");

		for( i = 0; i < getJointsPathSrv.response.steps*NUMBER_OF_JOINTS; i += 6) {
			for( j = 0; j < NUMBER_OF_JOINTS; j++ ) {

				//
				ss << getJointsPathSrv.response.path[i+j];

				//
				if( j < (NUMBER_OF_JOINTS-1) )
					ss << ", ";
				else
					ss << "\n";

			}

			ROS_INFO("%s", ss.str().c_str());

		}

	}
	else {

		ROS_INFO("GetJointsPath Service failed\n");

	}
	*/

	// Node has bee shutdown so do proper cleanup
	cleanup();

	return 0;

}
