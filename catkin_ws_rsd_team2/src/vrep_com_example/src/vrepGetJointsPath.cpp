#include "ros/ros.h"
#include "vrep_common/simGetJointsPath.h"

#include <string>
#include <iostream>
#include <sstream>
#include <cstddef>

#include <new>

#define NUMBER_OF_JOINTS 6

//------------------------------------------------------------------------------
//
// Global Variables
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
// Service Handlers
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//
// Utility Functions
//
//------------------------------------------------------------------------------

/*
 * @description
 */
void cleanup()
{


}

//------------------------------------------------------------------------------
//
// Node Start
//
//------------------------------------------------------------------------------

/*
 * @description
 */
int main(int argc, char **argv)
{

	//
	int i = 0;
	int j = 0;

	//
	std::stringstream ss;

	// Initialise ros
	ros::init(argc, argv, "vrep_getJointsPath");

	// Create node handle
	ros::NodeHandle nh;

	//
	ros::ServiceClient client = nh.serviceClient<vrep_common::simGetJointsPath>("/vrep/SimGetJointsPath");

	//
	vrep_common::simGetJointsPath getJointsPathSrv;

	//
	getJointsPathSrv.request.algorithm 				= 0;

	//
	getJointsPathSrv.request.steps 						= 100;

	//
	getJointsPathSrv.request.startJoints[0] 	= 0.0;
	getJointsPathSrv.request.startJoints[1] 	= 0.0;
	getJointsPathSrv.request.startJoints[2] 	= 0.0;
	getJointsPathSrv.request.startJoints[3] 	= 0.0;
	getJointsPathSrv.request.startJoints[4] 	= 0.0;
	getJointsPathSrv.request.startJoints[5] 	= 0.0;

	//
	getJointsPathSrv.request.goalJoints[0] 		= 0.0;
	getJointsPathSrv.request.goalJoints[1] 		= -1.57;
	getJointsPathSrv.request.goalJoints[2] 		= 1.57;
	getJointsPathSrv.request.goalJoints[3] 		= 0.0;
	getJointsPathSrv.request.goalJoints[4] 		= 1.57;
	getJointsPathSrv.request.goalJoints[5] 		= 0.0;

	//
	if(client.call(getJointsPathSrv)) {

		ROS_INFO("GetJointsPath Service responded with success\n");
		ROS_INFO("Number of steps: %hi\n", getJointsPathSrv.response.steps);
		ROS_INFO("Path: \n");

		for( i = 0; i < getJointsPathSrv.response.steps*NUMBER_OF_JOINTS; i += 6) {
			for( j = 0; j < NUMBER_OF_JOINTS; j++ ) {

				//
				ss << getJointsPathSrv.response.path[i+j];

				//
				if( j < (NUMBER_OF_JOINTS-1) )
					ss << ", ";
				else
					ss << "\n";

			}

			ROS_INFO("%s", ss.str().c_str());

		}

	}
	else {

		ROS_INFO("GetJointsPath Service failed\n");

	}

	// Node has bee shutdown so do proper cleanup
	cleanup();

	return 0;

}
