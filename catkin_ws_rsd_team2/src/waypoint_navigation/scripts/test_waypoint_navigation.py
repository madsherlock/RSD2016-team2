#!/usr/bin/env python

# imports
import rospy
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TwistStamped
from msgs.msg import BoolStamped, IntStamped, FloatStamped, FloatArrayStamped, waypoint_navigation_status, RoutePt
from math import pi, atan2
from waypoint_list import waypoint_list
from subclass_waypoint_navigation import class_waypoint_navigation
from markerlocatorinterface.srv import markerpose_request
from markerlocator.msg import markerpose
from waypoint_navigation.msg import waypoint_control_msg



topic = rospy.get_param("~waypoint_control_topic",'/waypoint_control')

automode_topic = rospy.get_param("~automode_pub",'/fmPlan/automode')




rospy.init_node('waypoint_test_publisher', anonymous=True) 
pub = rospy.Publisher(topic, waypoint_control_msg, queue_size=1)   #Queue size 1: ALWAYS latest pose.
automode_pub = rospy.Publisher(automode_topic, IntStamped, queue_size=1)


# cmd == 0:  Delete route plan
# cmd == 1:  Delete route plan then append route point
# cmd == 2:  Append route point
#
# mode == 0: Pure pursuit
# mode == 1: Minimize distance to AB line
#
# invalid heading, velocity, pause or task data: Use value -1000000

testpoint0 = RoutePt()
testpoint0.header.stamp = rospy.Time.now()
testpoint0.cmd = 1 #Ignored
testpoint0.easting = 0.0
testpoint0.northing = 0.0
testpoint0.heading = 0.0
testpoint0.id = "Mikaels test point 0"
testpoint0.nav_mode = 0 #1: Minimize cross-track error. 0: Pure pursuit.
testpoint0.linear_vel = 0.0
testpoint0.angular_vel = 0.0
testpoint0.pause = 5.0
testpoint0.task = 1 # 1: Wait for gps (markerlocator)


testpoint1 = RoutePt()
testpoint1.header.stamp = rospy.Time.now()
testpoint1.cmd = 2 #Ignored
testpoint1.easting = 0.0
testpoint1.northing = 0.5
testpoint1.heading = 0.0
testpoint1.id = "Mikaels test point 1"
testpoint1.nav_mode = 0 #1: Minimize cross-track error. 0: Pure pursuit.
testpoint1.linear_vel = 0.0
testpoint1.angular_vel = 0.0
testpoint1.pause = 5.0
testpoint1.task = 1 # 1: Wait for gps (markerlocator)

testpoint2 = RoutePt()
testpoint2.header.stamp = rospy.Time.now()
testpoint2.cmd = 2 #Ignored
testpoint2.easting = 0.0
testpoint2.northing = 0.0
testpoint2.heading = 0.0
testpoint2.id = "Mikaels test point 2"
testpoint2.nav_mode = 0 #1: Minimize cross-track error. 0: Pure pursuit.
testpoint2.linear_vel = 0.0
testpoint2.angular_vel = 0.0
testpoint2.pause = 5.0
testpoint2.task = 1 # 1: Wait for gps (markerlocator)



rate = rospy.Rate(0.5)

#fmPlan/automode

automode_test = IntStamped()
automode_test.header.stamp=rospy.Time.now()
automode_test.data = 1 #1: Waypoint navigation. 0: Manual

automode_pub.publish(automode_test)




#Frobit pose reset topic:
marker_pose_reset_topic_less = rospy.get_param("~odom_reset_topic","/fmInformation/odom_reset")
marker_pose_reset_less = rospy.Publisher(marker_pose_reset_topic_less, FloatArrayStamped, queue_size = 1)
#Service to get markerlocator poses from:
markerpose_service = rospy.get_param("~markerpose_service","/markerpose")
markerpose_order = rospy.get_param("~marker_pose_order",5)
rospy.wait_for_service(markerpose_service)
pose_srv = rospy.ServiceProxy(markerpose_service, markerpose_request, persistent=False)
pose = pose_srv(markerpose_order)
#self.marker_pose_reset_full.publish(pose)
reset_msg = FloatArrayStamped()
reset_msg.header.stamp = rospy.Time.now()
reset_msg.data = [pose.pose.x, pose.pose.y, pose.pose.theta]
rospy.loginfo(rospy.get_name() + ": Resetting frobit pose to markerlocator")
marker_pose_reset_less.publish(reset_msg)
rate.sleep()
rospy.loginfo(rospy.get_name() + ": Resetting frobit pose to markerlocator")
marker_pose_reset_less.publish(reset_msg)









#while not rospy.is_shutdown():
testmsg = waypoint_control_msg()
#      string nav_mode		#"clear","replace" or "append"
#      msgs/RoutePt[] wpt_list
testmsg.nav_mode = "replace"
testmsg.wpt_list = [testpoint0, testpoint1, testpoint2]

rospy.loginfo(rospy.get_name() + ": Length of waypoint list: %.2d" len(testmsg.wpt_list))

rospy.loginfo(rospy.get_name() + ": Names of waypoints:")
for i in xrange(len(testmsg.wpt_list)):
  rospy.loginfo(rospy.get_name() + ": ", testmsg.wpt_list[i].id)

rospy.loginfo(rospy.get_name() + ": publishing")

pub.publish(testmsg)
rospy.loginfo(rospy.get_name() + ": published")
rate.sleep()
rospy.loginfo(rospy.get_name() + ": done with this")
rospy.loginfo(rospy.get_name() + ": publishing")
automode_pub.publish(automode_test)

pub.publish(testmsg)
rospy.loginfo(rospy.get_name() + ": published")
rate.sleep()
rospy.loginfo(rospy.get_name() + ": done with this")

