#!/usr/bin/env python
#/****************************************************************************
# Waypoint Navigation: Waypoint list
# Copyright (c) 2013, Kjeld Jensen <kjeld@frobomind.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
"""
Either read a waypoint list in ROS_HOME, which by default is ~/.ros, or give it an array of RoutePt msgs.
The last allows for sending the list over ROS.

2013-06-07 KJ First version
2013-11-14 KJ Changed the waypoint list format to:
			  [easting, northing, yaw, wptid, modestr, tolerance, lin_spd, ang_spd, wait, implement]
              Added support for flexible waypoint csv length
2016-11-14 Niive12 Replaced support the "implement" mode with "wait_for_gps" 
                   mode, which makes the mobile robot reset the position in the Kalman filter
                   used for the odometry.
                   Added support for clearing the waypoint list and appending using an array of RoutePt msgs
"""
#import rospy
from msgs.msg import RoutePt

def isnum(num):
	return num == num
class waypoint_list():
	def __init__(self):
		self.INVALID = -1000000
		self.wpts = []
		self.next = 0
		self.wpt_num = 0
		
		#enum for task:
		self.WAIT_FOR_GPS = 1
		
	def clear_wpt_list(self):
		self.wpts = []
		self.next = 0
		self.wpt_num = 0
		
	def load_from_ros_msg(self, msg):
		self.wpts = []
		for i in xrange(len(msg)):
			data = msg[i]
			self.add_msg(data)
		self.next = 0
		
	def load_from_csv_ne_format(self, filename):
		self.wpts = []
		lines = [line.rstrip('\n') for line in open(filename)] # read the file and strip \n
		self.wpt_num = 0
		for i in xrange(len(lines)): # for all lines
			if len(lines[i]) > 0 and lines[i][0] != '#': # if not a comment or empty line
				data = lines[i].split (',') # split into comma separated list
				self.add(data)
		self.next = 0
	
	def add_msg(self, data):
		if isnum(data.easting) and isnum(data.northing):
			self.wpt_num += 1
			e = data.easting
			n = data.northing
			yaw = -1
			name = 'Wpt%d' % (self.wpt_num)
			mode = 1 # default is 'minimize cross track error'
			lin_spd = 0.0
			ang_spd = 0.0
			wait = -1
			task = data.task
			if isnum(data.heading):
				yaw = data.heading
			if data.id != '':
				name = data.id
			if data.nav_mode == 0: # pure pursiut
				mode = 0
			if isnum(data.linear_vel):
				lin_spd = data.linear_vel
			if isnum(data.angular_vel):
				ang_spd = data.angular_vel
			if isnum(data.pause):
				wait = data.pause
			tol = 0.0        #0  1  2    3     4     5    6        7        8     9
			self.wpts.append([e, n, yaw, name, mode, tol, lin_spd, ang_spd, wait, task])
		#else:
		#	rospy.loginfo(rospy.get_name() + ": Erroneous waypoint")

		
	def add(self, data): 
		if len(data) >= 2 and data[0] != '' and data[1] != '':
			self.wpt_num += 1
			e = float (data[0])
			n = float (data[1])
			if len(data) >= 3 and data[2] != '':
				yaw = float(data[2])
			else:
				yaw = -1
			mode = 1 # default is 'minimize cross track error'
			if len(data) >= 4 and data[3] != '':
				name = data[3]
			else:
				name = 'Wpt%d' % (self.wpt_num)
			if  len(data) >= 5 and data[4] == 'STWP': # 'straight to waypoint' 
				mode = 0 
			if len(data) >= 6 and data[5] != '':  # waypoint reached tolerance in meters
				tol = float(data[5])
			else:
				tol = 0.0 
			if  len(data) >= 7 and data[6] != '': # linear speed
				lin_spd = float(data[6])
			else:
				lin_spd = 0.0 
			if  len(data) >= 8 and data[7] != '': # angular speed
				ang_spd = float(data[7])
			else:
				ang_spd = 0.0
			if  len(data) >= 9 and data[8] != '': # wait X seconds after reaching waypoint
				wait = float(data[8])
			else:
				wait = -1.0
			if  len(data) >= 10 and data[9] != '': # 1 means wait for gps
				task = float(data[9])
			else:
				task = self.INVALID

			self.wpts.append([e, n, yaw, name, mode, tol, lin_spd, ang_spd, wait, task])
		#else:
		#	rospy.loginfo(rospy.get_name() + ": Erroneous waypoint")

	def get_next (self):	
		if self.next < len(self.wpts):
			wpt = self.wpts[self.next]
			self.next += 1
		else:
			wpt = False
		#rospy.loginfo(rospy.get_name() + ": Next wpt: %.2d / %.2d ", self.next, len(self.wpts))
		return wpt

	def get_previous (self):
		prev_wpt = False
		wpt = False
		if self.next > 1:
			self.next -= 1
			wpt = self.wpts[self.next-1]
			if self.next > 1:
				prev_wpt = self.wpts[self.next-2]
		return (wpt, prev_wpt)

	def status (self):		
		return (len(self.wpts), self.next)

