#!/usr/bin/env python
#/****************************************************************************
# Waypoint Navigation
# Copyright (c) 2013-2015, Kjeld Jensen <kjeld@frobomind.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************/
"""
2013-06-06 KJ First version
2013-10-01 KJ Fixed a bug causing the velocity to always be the maximum velocity.
              Added launch file parameters for the way-point navigation.
2013-11-13 KJ Added new launch file parameters for way-point defaults
              Added support for implement command and wait after arrival at way-point.
2013-12-03 KJ Added ramp up which works like the previous ramp down
2015-03-05 KJ Added queue_size to rospy. Publisher calls (Indigo compatibility)
2015-08-19 KJ Switched from Bool to BoolStamped
2015-03-13 KJ Changed automode message type from BoolStamped to IntStamped
              and default topic name to /fmPlan/automode
2015-10-03 KJ Separated the remote control output from the mission planner.
              this component now monitors /fmHMI/remote_control instead of
              /fmLib/joy
2016-11-14 Niive12 Removed remote control from waypoint navigation and changed so it 
                   subscribed to a topic for waypoints rather than reading from a file.
                   removed support the "implement" mode and added support for wait_for_gps 
                   mode, which waits a predefined amount of time and then calls the 
                   marker locater service to reset the position in the Kalman filter
                   used for the odometry.
"""

# imports
import rospy
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TwistStamped
from msgs.msg import BoolStamped, IntStamped, FloatStamped, FloatArrayStamped, waypoint_navigation_status#, RoutePt
from std_msgs.msg import Bool
from math import pi, atan2
from waypoint_list import waypoint_list
from subclass_waypoint_navigation import class_waypoint_navigation
from markerlocatorinterface.srv import markerpose_request
from markerlocator.msg import markerpose
from waypoint_navigation.msg import waypoint_control_msg

class WptNavNode():
	def __init__(self):
		self.wpt_msg = []
		self.wpt_ctrl_topic = rospy.get_param("~wpt_ctrl_sub",'/waypoint_control')
		rospy.Subscriber(self.wpt_ctrl_topic, waypoint_control_msg, self.on_wpt_message)
		self.gps_cam_timeout = rospy.get_param("~gps_cam_timeout", 1.0)
		self.gps_cam_timeout = 1.0
		self.gps_timeout = 0.0
		self.marker_pose_reset_topic = rospy.get_param("~odom_reset_topic","/fmInformation/odom_reset")
		self.marker_pose_reset = rospy.Publisher(self.marker_pose_reset_topic, FloatArrayStamped, queue_size = 1)
		self.markerpose_srv = rospy.get_param("~markerpose_service","/markerpose")
		self.wpt_complete_topic = rospy.get_param("~wpt_complete_topic","/waypoint_complete")
		self.wpt_complete_pub = rospy.Publisher(self.wpt_complete_topic, BoolStamped, queue_size = 1)
		self.markerpose_order = rospy.get_param("~marker_pose_order",5)
		self.markerpose_reset_enabled = True
		# defines
		self.update_rate = 20 # set update frequency [Hz]
		self.STATE_IDLE = 0
		self.STATE_NAVIGATE = 1
		self.STATE_WAIT = 2
		self.STATE_OBSTACLE = 3
		self.state = self.STATE_IDLE
		self.state_prev = self.state
		self.automode_warn = False
		self.wait_after_arrival = 0.0
		self.wait_timeout = 0.0
		self.status = 0
		self.wpt = False
		self.prev_wpt = False
		self.linear_vel = 0.0
		self.angular_vel = 0.0
		self.pos = False
		self.wpt_mode = ""
		self.bearing = False
		self.obstacle_delay_counter = 0
		self.obstacle_timeout = 1;
		rospy.Subscriber("/obstacle_status", Bool, self.on_obstacle_message)


		rospy.loginfo(rospy.get_name() + ": Start")
		self.quaternion = np.empty((4, ), dtype=np.float64)

		# get parameters
		self.debug = rospy.get_param("~print_debug_information", 'True') 
 		if self.debug:
			rospy.loginfo(rospy.get_name() + ": Debug enabled")
		self.status_publish_interval = rospy.get_param("~status_publish_interval", 0) 
		self.pid_publish_interval = rospy.get_param("~pid_publish_interval", 0) 

		# get topic names
		self.automode_topic = rospy.get_param("~automode_sub",'/fmPlan/automode')
		self.pose_topic = rospy.get_param("~pose_sub",'/fmKnowledge/pose')
		self.cmdvel_topic = rospy.get_param("~cmd_vel_pub",'/fmCommand/cmd_vel')
		self.wptnav_status_topic = rospy.get_param("~status_pub",'/fmInformation/wptnav_status')
		self.pid_topic = rospy.get_param("~pid_pub",'/fmInformation/wptnav_pid')
		self.deadman_topic = rospy.get_param("~deadman_topic","/fmSafe/deadman")
		# setup publish topics
		self.cmd_vel_pub = rospy.Publisher(self.cmdvel_topic, TwistStamped, queue_size=1)
		self.twist = TwistStamped()
		self.wptnav_status_pub = rospy.Publisher(self.wptnav_status_topic, waypoint_navigation_status, queue_size=5)
		self.wptnav_status = waypoint_navigation_status()
		self.status_publish_count = 0
		self.pid_pub = rospy.Publisher(self.pid_topic, FloatArrayStamped, queue_size=5)
		self.pid = FloatArrayStamped()
		self.pid_publish_count = 0
		self.dead_man = BoolStamped()
		self.dead_man_pub = rospy.Publisher(self.deadman_topic, BoolStamped, queue_size=1)

		# configure waypoint navigation
		self.w_dist = rospy.get_param("/diff_steer_wheel_distance", 0.2) # [m]
		drive_kp = rospy.get_param("~drive_kp", 1.0)
		drive_ki = rospy.get_param("~drive_ki", 0.0)
		drive_kd = rospy.get_param("~drive_kd", 0.0)
		drive_ff = rospy.get_param("~drive_feed_forward", 0.0)
		drive_max_output = rospy.get_param("~drive_max_output", 0.3)
		turn_kp = rospy.get_param("~turn_kp", 1.0)
		turn_ki = rospy.get_param("~turn_ki", 0.0)
		turn_kd = rospy.get_param("~turn_kd", 0.2)
		turn_ff = rospy.get_param("~turn_feed_forward", 0.0)
		turn_max_output = rospy.get_param("~turn_max_output", 0.5)

		max_linear_vel = rospy.get_param("~max_linear_velocity", 0.4)
		max_angular_vel = rospy.get_param("~max_angular_velocity", 0.4)

		self.wpt_def_tolerance = rospy.get_param("~wpt_default_tolerance", 0.5)
		self.wpt_def_drive_vel = rospy.get_param("~wpt_default_drive_velocity", 0.5)
		self.wpt_def_turn_vel = rospy.get_param("~wpt_default_turn_velocity", 0.3)
		self.wpt_def_wait_after_arrival = rospy.get_param("~wpt_default_wait_after_arrival", 0.0)

		target_ahead = rospy.get_param("~target_ahead", 1.0)
		turn_start_at_heading_err = rospy.get_param("~turn_start_at_heading_err", 20.0)
		turn_stop_at_heading_err = rospy.get_param("~turn_stop_at_heading_err", 2.0)
		ramp_drive_vel_at_dist = rospy.get_param("~ramp_drive_velocity_at_distance", 1.0)
		ramp_min_drive_vel = rospy.get_param("~ramp_min_drive_velocity", 0.1)
		ramp_turn_vel_at_angle = rospy.get_param("~ramp_turn_velocity_at_angle", 25.0)
		ramp_min_turn_vel = rospy.get_param("~ramp_min_turn_velocity", 0.05)
		stop_nav_at_dist = rospy.get_param("~stop_navigating_at_distance", 0.1)		

		rospy.loginfo(rospy.get_name() + ": -------------- DEFAULT SPEED ---------------")
		rospy.loginfo(rospy.get_name() + ": %.2f", self.wpt_def_drive_vel )
		self.wptnav = class_waypoint_navigation(self.update_rate, 
		                                  self.w_dist, 
		                                  drive_kp, 
		                                  drive_ki, 
		                                  drive_kd, 
		                                  drive_ff, 
		                                  drive_max_output, 
		                                  turn_kp, 
		                                  turn_ki, 
		                                  turn_kd, 
		                                  turn_ff, 
		                                  turn_max_output, 
		                                  max_linear_vel, 
		                                  max_angular_vel, 
		                                  self.wpt_def_tolerance, 
		                                  self.wpt_def_drive_vel, 
		                                  self.wpt_def_turn_vel, 
		                                  target_ahead, 
		                                  turn_start_at_heading_err, 
		                                  turn_stop_at_heading_err, 
		                                  ramp_drive_vel_at_dist, 
		                                  ramp_min_drive_vel, 
		                                  ramp_turn_vel_at_angle, 
		                                  ramp_min_turn_vel, 
		                                  stop_nav_at_dist, 
		                                  self.debug)

		self.wptlist = waypoint_list()
		self.wptlist_loaded = False

		# setup subscription topic callbacks
		rospy.Subscriber(self.automode_topic, IntStamped, self.on_automode_message)
		rospy.Subscriber(self.pose_topic, Odometry, self.on_pose_message)


		# call updater function
		self.r = rospy.Rate(self.update_rate)

	def load_wpt_list (self):
		rospy.loginfo(rospy.get_name() + ": Loading waypoint list of length: %.2d", len(self.wpt_msg))
		rospy.loginfo(rospy.get_name() + ": Names of waypoints:")
		for i in xrange(len(self.wpt_msg)):
			rospy.loginfo(rospy.get_name() + ": %s", self.wpt_msg[i].id )
		
		self.wptlist.load_from_ros_msg(self.wpt_msg)
		#self.wptlist.load_from_csv_ne_format ('waypoints.txt') //DEPRECATED
		(numwpt, nextwpt) = self.wptlist.status()
		self.prev_wpt = False 
		self.wpt = False 
		rospy.loginfo(rospy.get_name() + ": %d waypoints loaded", numwpt)

	def goto_next_wpt (self):
		self.prev_wpt = self.wpt
		self.wpt = self.wptlist.get_next()
		if self.wpt != False:
			if self.wpt[self.wptnav.W_TASK] == self.wptlist.WAIT_FOR_GPS: #reset before movement
				self.gps_timeout = rospy.get_time() + self.gps_cam_timeout
				complete_reset = False
				while not complete_reset:
					if rospy.get_time() > self.gps_timeout:
						self.reset_position()
						complete_reset = True
					else:
						self.linear_vel = 0.0
						self.angular_vel = 0.0
						self.publish_cmd_vel_message()
			self.wptnav.navigate(self.wpt, self.prev_wpt)
			rospy.loginfo(rospy.get_name() + 
			              ": Navigating to waypoint: %s (distance %.2fm, bearing %.0f)" , self.wpt[self.wptnav.W_ID], 
			              self.wptnav.dist, 
			              self.wptnav.bearing*180.0/pi)
			self.publis_complete_message(False)
		else:
			rospy.loginfo(rospy.get_name() + ": End of waypoint list reached")
			self.publis_complete_message(True)
			self.wptnav.stop()

	def goto_previous_wpt (self):
		(wpt, prev_wpt) = self.wptlist.get_previous()
		if wpt != False:
			self.wpt = wpt
			self.prev_wpt = prev_wpt
			self.wptnav.navigate(self.wpt, self.prev_wpt)
			rospy.loginfo(rospy.get_name() + 
			              ": Navigating to waypoint: %s (distance %.2fm, bearing %.0f)" % (self.wpt[self.wptnav.W_ID], 
			              self.wptnav.dist, 
			              self.wptnav.bearing*180.0/pi))
		else:
			rospy.loginfo(rospy.get_name() + ": This is the first waypoint")

	def on_automode_message(self, msg):
		if msg.data == 1: # if autonomous waypoint navigation mode is active
			if self.state == self.STATE_IDLE:
				if self.wptnav.pose != False: # if we have a valid pose				
					self.state = self.STATE_NAVIGATE
					rospy.loginfo(rospy.get_name() + ": Switching to waypoint navigation")
					if self.wptlist_loaded == False:
						rospy.loginfo(rospy.get_name() + ": Loading waypoint list")
						self.load_wpt_list()				
						self.goto_next_wpt()
						self.wptlist_loaded = True
					elif self.wptnav.state == self.wptnav.STATE_STANDBY:
						self.wptnav.resume()
						rospy.loginfo(rospy.get_name() + ": Resuming waypoint navigation")

				else: # no valid pose yet
					if self.automode_warn == False:
						self.automode_warn = True
						rospy.logwarn(rospy.get_name() + 
						              ": Absolute pose is required for autonomous navigation")
		else: # if manual mode requested
			if self.state != self.STATE_IDLE:
				self.state = self.STATE_IDLE
				self.wptnav.standby() 
				rospy.loginfo(rospy.get_name() + ": Switching to manual control")
			
	def on_pose_message(self, msg):
		qx = msg.pose.pose.orientation.x
		qy = msg.pose.pose.orientation.y
		qz = msg.pose.pose.orientation.z
		qw = msg.pose.pose.orientation.w
		yaw = atan2(2*(qx*qy + qw*qz), qw*qw + qx*qx - qy*qy - qz*qz)
		self.wptnav.state_update (msg.pose.pose.position.x, msg.pose.pose.position.y, yaw, msg.twist.twist.linear.x)
	
	def publish_cmd_vel_message(self):
		#print 'waypoint_navigation_node publishing cmd_vel'
		self.dead_man.header.stamp = rospy.Time.now()
	        self.dead_man.data = 1
        	self.dead_man_pub.publish(self.dead_man)

		self.twist.header.stamp = rospy.Time.now()
		self.twist.twist.linear.x = self.linear_vel
		self.twist.twist.angular.z = self.angular_vel		
		self.cmd_vel_pub.publish (self.twist)
	
	def publis_complete_message(self, complete):
		msg = BoolStamped()
		msg.header.stamp = rospy.Time.now()
		msg.data = complete
		self.wpt_complete_pub.publish(msg)
	
	def publish_status_message(self):
		self.wptnav_status.header.stamp = rospy.Time.now()
		self.wptnav_status.state = self.state
		if self.wptnav.pose != False:
			self.wptnav_status.easting = self.wptnav.pose[0]
			self.wptnav_status.northing = self.wptnav.pose[1]

		if self.state == self.STATE_NAVIGATE and self.wptnav.b != False:
			if  self.wptnav.state == self.wptnav.STATE_STOP or self.wptnav.state == self.wptnav.STATE_STANDBY:
				self.wptnav_status.mode = 0
			elif self.wptnav.state == self.wptnav.STATE_DRIVE:
				self.wptnav_status.mode = 1
			elif self.wptnav.state == self.wptnav.STATE_TURN:
				self.wptnav_status.mode = 2
			self.wptnav_status.b_easting = self.wptnav.b[self.wptnav.W_E]
			self.wptnav_status.b_northing = self.wptnav.b[self.wptnav.W_N]
			self.wptnav_status.a_easting = self.wptnav.a[self.wptnav.W_E]
			self.wptnav_status.a_northing = self.wptnav.a[self.wptnav.W_N]
			self.wptnav_status.distance_to_b = self.wptnav.dist
			self.wptnav_status.bearing_to_b = self.wptnav.bearing
			self.wptnav_status.heading_err = self.wptnav.heading_err
			self.wptnav_status.distance_to_ab_line = self.wptnav.ab_dist_to_pose
			if self.wptnav.target != False:
				self.wptnav_status.target_easting = self.wptnav.target[0]
				self.wptnav_status.target_northing = self.wptnav.target[1]
				self.wptnav_status.target_distance = self.wptnav.target_dist
				self.wptnav_status.target_bearing = self.wptnav.target_bearing
				self.wptnav_status.target_heading_err = self.wptnav.target_heading_err
			else:	
				self.wptnav_status.target_easting = 0.0
				self.wptnav_status.target_northing = 0.0
				self.wptnav_status.target_distance = 0.0
				self.wptnav_status.target_bearing = 0.0
				self.wptnav_status.target_heading_err = 0.0
			self.wptnav_status.linear_speed = self.wptnav.linear_vel
			self.wptnav_status.angular_speed = self.wptnav.angular_vel
		else:
			self.wptnav_status.mode = -1
		self.wptnav_status_pub.publish (self.wptnav_status)

	def publish_pid_message(self):
		if self.state == self.STATE_NAVIGATE:
			self.pid.header.stamp = rospy.Time.now()
			self.pid.data = self.wptnav.pid_status
			self.pid_pub.publish (self.pid)

	def updater(self):
		while not rospy.is_shutdown():
			if self.state == self.STATE_NAVIGATE:
				(self.status, self.linear_vel, self.angular_vel) = self.wptnav.update(rospy.get_time())
				if self.status == self.wptnav.UPDATE_ARRIVAL:
					rospy.loginfo(rospy.get_name() + 
					              ": Arrived at waypoint: %s (error %.2fm)" % (self.wpt[self.wptnav.W_ID], 
					              self.wptnav.dist))

					# activate wait mode
					if self.wpt[self.wptnav.W_WAIT] >= 0.0:
						self.wait_after_arrival = self.wpt[self.wptnav.W_WAIT]
					else:
						self.wait_after_arrival = self.wpt_def_wait_after_arrival

					#if self.wpt[self.wptnav.W_TASK] == self.wptlist.WAIT_FOR_GPS: #reset after movement
					if False: 
						self.linear_vel = 0.0
						self.angular_vel = 0.0
						self.publish_cmd_vel_message()
						rospy.loginfo(rospy.get_name() + ": Waiting for marker locater (%.1f seconds)" % (self.gps_cam_timeout))
						self.state = self.STATE_WAIT
						self.gps_timeout = rospy.get_time() + self.gps_cam_timeout
                                                self.wait_timeout = gps_timeout + 1;sub #this is to make sure it is larger than gps timeout
					elif self.wait_after_arrival > 0.01: # this way, it does not wait for two scenarios
						self.linear_vel = 0.0
						self.angular_vel = 0.0
						self.publish_cmd_vel_message()
						rospy.loginfo(rospy.get_name() + ": Waiting %.1f seconds" % (self.wait_after_arrival))
						self.state = self.STATE_WAIT
						self.wait_timeout = rospy.get_time() + self.wait_after_arrival
						self.gps_timeout = self.wait_timeout + 1; #this is to make sure it is larger than timeout
					else:
						self.state = self.STATE_NAVIGATE 		
						self.goto_next_wpt()	

				else:
					if not (self.linear_vel == 0.0 and self.angular_vel == 0.0):
						self.publish_cmd_vel_message()
						rospy.loginfo(rospy.get_name() +": Publishing cmd_vel. Not in update_arrival.")

			elif self.state == self.STATE_OBSTACLE:
				rospy.loginfo(rospy.get_name() +": Waiting because of obstacle.")
				if(rospy.get_time() > self.obstacle_timeout):
					self.state = self.STATE_NAVIGATE
					self.obstacle_delay_counter = 0
			elif self.state == self.STATE_WAIT:
				if rospy.get_time() > self.gps_timeout:
					self.reset_position()
					self.state = self.STATE_NAVIGATE 		
					self.goto_next_wpt()
				elif rospy.get_time() > self.wait_timeout:
					self.state = self.STATE_NAVIGATE 		
					self.goto_next_wpt()
				else:
					self.linear_vel = 0.0
					self.angular_vel = 0.0
					self.publish_cmd_vel_message()
					rospy.loginfo(rospy.get_name() +": Publishing ZERO cmd_vel!")

			elif self.state == self.STATE_OBSTACLE:
				if rospy.get_time() > self.wait_timeout:
					self.state = self.STATE_NAVIGATE
				else:
					self.linear_vel = 0.0
					self.angular_vel = 0.0
					self.publish_cmd_vel_message()
					rospy.loginfo(rospy.get_name() +": Publishing ZERO cmd_vel!")
					
			# publish status
			if self.status_publish_interval != 0:
				self.status_publish_count += 1
				if self.status_publish_count % self.status_publish_interval == 0:
					self.publish_status_message()

			# publish pid
			if self.pid_publish_interval != 0:
				self.pid_publish_count += 1
				if self.pid_publish_count % self.pid_publish_interval == 0:
					self.publish_pid_message()
			self.r.sleep()
			
	def reset_position(self):
		rospy.loginfo(rospy.get_name() + ": Resetting position")
		if(self.markerpose_reset_enabled):
			rospy.wait_for_service(self.markerpose_srv)
			try:
				pose_srv = rospy.ServiceProxy(self.markerpose_srv, markerpose_request, persistent=False)
				pose = pose_srv(self.markerpose_order)
				#self.marker_pose_reset_full.publish(pose)
				reset_msg = FloatArrayStamped()
				reset_msg.header.stamp = rospy.Time.now()
				reset_msg.data = [pose.pose.x, pose.pose.y, pose.pose.theta]
				self.marker_pose_reset.publish(reset_msg)
			except rospy.ServiceException, e:
				rospy.loginfo(rospy.get_name() + ": Marker pose service call failed")
		else:
			rospy.loginfo(rospy.get_name() + ": Marker pose service call not called here")
				
	def on_obstacle_message(self, msg):
		if( (self.state == self.STATE_NAVIGATE or self.state == self.STATE_OBSTACLE) and msg.data and self.obstacle_delay_counter < 100):
			self.obstacle_timeout = rospy.get_time() + 1
			self.state = self.STATE_OBSTACLE
			self.obstacle_delay_counter = self.obstacle_delay_counter + 1
			
	def on_wpt_message(self, msg):
		rospy.loginfo(rospy.get_name() + ": got wpt message")
		self.wpt_mode = msg.nav_mode
		self.wpt_msg = msg.wpt_list
		if self.wpt_mode == "clear":
			self.wptlist.clear_wpt_list();
		elif self.wpt_mode == "append":
			self.wpt_msg = msg.wpt_list
			self.load_wpt_list()
		elif self.wpt_mode == "replace":
			self.wptlist.clear_wpt_list();
			self.wpt_msg = msg.wpt_list
			self.load_wpt_list()
		else:
			rospy.loginfo(rospy.get_name() + ": Got '" + self.wpt_mode + "', expected 'clear', 'append' or 'replace'")
		self.goto_next_wpt()

# Main function.
if __name__ == '__main__':
	# Initialize the node and name it.
	rospy.init_node('wptnav_node')

	try:
		node_class = WptNavNode()
		node_class.updater() #loops until ^C
	except rospy.ROSInterruptException:
		pass

