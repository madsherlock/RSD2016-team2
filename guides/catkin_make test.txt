#Run each catkin_make line once,
#followed by rm -r build devel.
#This should test C++ dependencies thoroughly.
#Python dependencies, you have to run the scripts
#to see if messages and services are properly initialized.

catkin_make --only-pkg-with-deps conveyor_belt
catkin_make --only-pkg-with-deps frobit_control
catkin_make --only-pkg-with-deps frobit_description
catkin_make --only-pkg-with-deps frobit_gazebo
catkin_make --only-pkg-with-deps frobo_rsd2016_team2
catkin_make --only-pkg-with-deps hmi_backend
catkin_make --only-pkg-with-deps kuka_ros
catkin_make --only-pkg-with-deps lidar
catkin_make --only-pkg-with-deps line_follower
catkin_make --only-pkg-with-deps markerlocator
catkin_make --only-pkg-with-deps markerlocatorinterface
catkin_make --only-pkg-with-deps metapackage_controlpc
catkin_make --only-pkg-with-deps metapackage_frobitsim
catkin_make --only-pkg-with-deps metapackage_frobomind_rsd
catkin_make --only-pkg-with-deps nav_mode_transition
catkin_make --only-pkg-with-deps sick_tim

rm -r build devel
