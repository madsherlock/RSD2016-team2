#!/bin/bash
p () {
	echo " --- $@ --- ";
}

p "This is a guide to make config files"
org=$(grep "#define ORGANIZATION" ../code/src/hmi_backend/settings.h | grep -o '".*"' | tr -d '"')
app=$(grep "#define APPLICATION" ../code/src/hmi_backend/settings.h | grep -o '".*"' | tr -d '"')
filename=~/.config/${org}/${app}.conf
p "There is two variables, APP and ORG"
p "These are defined like in \"../code/src/hmi_backend/settings.h\""
p "This means the config file will be found (on a linux system) in
~/.config/${org}/${app}.conf"
echo -n " --- Would you like to create one now? y/N: " 
read -n 1 input
echo -ne "\n"
if [ $input == y ]; then
	current_dir=$(pwd)
	echo "this is running from $current_dir"
	mkdir -p ~/.config/$org
	cd ../code/src/hmi_frontend/
	frontend_dir=$(pwd)
	cd $current_dir
	mkdir -p ../code/data
	cd ../code/data
	save_dir=$(pwd)
	echo "[Website]
	path = ${frontend_dir}/ 
	file = website.html
	[Network]
	port = 9000
	[orders]
	save_dir = ${save_dir}/ " | tr -d '\t' > $filename
	p "Wrote : "
	cat $filename
	p "To $filename"
fi

