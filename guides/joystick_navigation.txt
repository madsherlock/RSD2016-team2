Test the MR joystick control
This guide is old, and assumes ethernet cable connection.
Also assumes frobit_demo installed on frobit.

To get it to work, you must:
 1. Make the config file for the HMI. See config_files.sh.
 2. Set the ROS_MASTER_URI to http://192.168.3.14:11311
 3. Turn on the pi. 
 4. Turn on the motor.
 5. Take the motor controller out and put it back in.
 6. Connect to the pi over network.
 7. Check IP of your local network. It should be 192.168.3.XXX where XXX > 14
 8. Set ROS_IP to whatever your IP is.
 9. ssh rsd1602@rsd1602.local
10. See if the controller is there (ls /dev/frobit)
11. If not, repeat step 5 until it is.
12. Roslaunch frobit_demo frobit_run_wpt_nav.launch
13. On your own computer, run the test_hmi_backend
14. On your own computer, go to localhost:9000 (or whatever you set in the config file) in your browser.
15. Press arrow keys
16. Try to break it.
17. Pressing the green thing is supposed to stop the robot.
