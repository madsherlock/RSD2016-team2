#!/bin/bash
echo "--- getting the code ---"
git clone https://github.com/warmcat/libwebsockets.git 
echo "--- compiling it ---"
cd libwebsockets
mkdir build
cd build
cmake ..
make -j4
echo "--- installing it ---"
sudo make install
#verify that the file exist
ls /usr/local/lib/libwebsocket*
cd ../..
rm -rf libwebsockets
echo "--- adding the path to the linker ---"
echo "include /usr/local/lib/" >> sudo /etc/ld.so.conf
sudo ldconfig
if [ -n "$(ldconfig -p | grep libwebsockets.so)" ]; then 
	echo "Succesfully installed"; 
fi
