\section{Human Machine Interface}

A human machine interface (HMI) monitors the states of the system and provides a way to send user input to the system.
Based on the clients requirements, the HMI should be capable of monitoring and manipulating the state of 
%requirements:
the robot arm,
the gripper,
the conveyor belt,
the camera system,
the planning state of the robot cell and the mobile robot,
the overall equipment effectiveness (OEE) of the system,
the position and velocity of the mobile robot,
the tipper position on the mobile robot
and the status of the components as given to the MES server.

The HMI was designed to be used on a touch screen.
A task like manually moving the mobile robot was found to be impractical this setup, so keyboard functionality was added so both options could be used.
The interface design is inspired by the fictional user interface, used on the operating system on the touch computers in the TV series; 
Star Trek, Library Computer Access and Retrieval System (LCARS)\citep{lcars}.

The interface has 3 panes.
The first pane lets the user select which tab should be active.
Buttons on the side was coloured in bright pastel colours and the active mode is highlighted by having it extend, with round edges on the side.
These buttons are always available to the user.

The second pane shows the navigation state of the mobile robot.
The active state is shown by extending the button.
Here the user can press the manual navigation mode to override the movement of the mobile robot. Pressing any other button here would result in resuming the navigation in automatic mode from it's previous state.
This information is also available for the user at all times.

The third pane shows content from the active mode.
This content of this pane is replaced when going to a new mode.

\subsection{Interfaces}

The structure of the HMI is split into two components, a front-end and a back-end.
The front-end is a website communicating with the backend through a websocket.
The backend is acting as a webserver, so a user can connect to the computer running the backend and have the front-end displayed on their system.
The backend connects to the robot system through ROS.
In each tab, a communication protocol was made.
On the backend, the incoming messages is checked against a list of protocol names, splitting communication into protocol messages and regular messages.

\begin{figure}[t]
\centering
\begin{tikzpicture}[node distance=1.0cm,scale=0.8, every node/.style={scale=0.8}]
    \input{graphics/hmi/mobile_robot_protocol}
\end{tikzpicture}
\caption{Mobile Robot HMI protocol.}
\label{fig:move_robot_protocol}
\end{figure}

The move robot protocols works as depicted in figure \ref{fig:move_robot_protocol}.
The backend initialises by receiving the protocol message.
This starts a thread which updates the values associated with the robot state.
The frontend will then send a request to move the mobile robot. These requests are high level operations.
%
%>>>>>>>>>>>>>>>>>>>>>>>> verbose way: (not a merge thing, but I was not sure how to deal with these...
% There are request which gives a direction such as 
% ``forward'', 
% ``reverse'', 
% ``left'', 
% ``right'', 
% ``forward right'', 
% ``forward left'', 
% ``reverse right'' and 
% ``reverse left''.
% These commands will make the backend increment or decrement the velocities of the robot. 
% The commands ``brake'' and ``reset\_marker\_pose'' will reset the velocities or odometry regardless of previous state.
% Other commands such as ``wpt: x,y,theta'' and ``tipper:pos'' is direct parameters which will make the robot move to a waypoint or move the tipper respectively.
%======================= OR:
There are directional requests which moves increment or decrement the velocity of the robot 
and special commands for resetting the marker locator position, moving to a waypoint and moving the tipper.
%<<<<<<<<<<<<<<<<<<<<<<<< short way
%
Any command not recognised by the backend will not affect the robot, but the robot state will still be updated. 
So to have a continuous stream of updated data, a special command called ``Nowhere'' is sent with a interval of $250$ ms controlled by the front end.

The Response is a string created from the mobile robot states. It is formatted so each line is a separate entry with an ID and any number of fields.
The ID and the values are seperated by a colon and the fields in the values are separated by commas. 
This keeps the reply readable while easy to convert and deal with on the client side.

The Robot cell uses a similar updating strategy, 
but the automatic and the manual movement is split into two protocols as seen in figure \ref{fig:hmi_control_robot_cell}.
In figure \ref{fig:automatic_robot_cell} the automatic protocol is shown.
In this protocol, the backend governs the communication. 
The robot cell status is updated with an interval of $250$ ms. % and the 

\begin{figure}[t]
\centering
    \begin{subfigure}[b]{\smallimgwidth}
        \centering
        \begin{tikzpicture}[node distance=1.0cm,scale=0.8, every node/.style={scale=0.8}]
            \input{graphics/hmi/automatic_robot_cell}
        \end{tikzpicture}
    \caption{Automatic movement.}
    \label{fig:automatic_robot_cell}
    \end{subfigure}
    \begin{subfigure}[b]{\smallimgwidth}
        \centering
        \begin{tikzpicture}[node distance=1.0cm,scale=0.8, every node/.style={scale=0.8}]
            \input{graphics/hmi/manual_robot_cell}
        \end{tikzpicture}
        \caption{Manual movement.}
        \label{fig:manual_robot_cell}
    \end{subfigure}
\caption{HMI protocol for automatic and manual modes of the robot cell.}
\label{fig:hmi_robot_cell}
\end{figure}

\subsubsection{Mobile Robot}
% Describe what's in the Mobile robot tab, why it is designed this way, and how it actually interfaces to the Mobile Robot.
The mobile robot tab displays the linear velocity, angular velocity, tipper position, battery percentage and position.
For safety reasons, the maximum linear and angular velocities are fixed.
This means the velocity displayed on the screen is a percentage rather than the actual speed.
This gives a more analogue reading which should simplify the information.
Positive velocities are represented with green and red is used for negative velocities.
The tipper position and the battery levels are displayed in the same way.
The tipper position is again measured in percentage, 0\% when it is all the way in and 100\% when it is all the way out.
The mobile robot can be controlled manually through directional buttons.
Here the controller can add or decrease the velocity of the robot.
The position of the robot is displayed on a map of the laboratory.
Here an image of the robot is rotated and moved according to the odometry information from the robot.
Pressing on the map or dragging of the robot will make the robot move to that location.

In figure \ref{fig:hmi_control_mr} the interface for the mobile robot is shown.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\imgwidth]{graphics/hmi/control_mr}
    \caption{Screenshot of the HMI for controlling the mobile robot.}
    \label{fig:hmi_control_mr}
\end{figure}

\subsubsection{Robot Cell}
% Describe what's in the RC tab, why it is designed this way, and how it actually interfaces to the RC.
The robot cell tab displays the current configuration of the robot arm, the gripper status and the state of the conveyor belt.
Each joint in the robot can be controlled via a slider next to a visual representation of the robot, showing what the joint numbers mean.
The robot representation consists of two images, one image is with the gripper opened and the other is the gripper closed.
A button to toggle the gripper status also displays the gripper state, both by changing the colour and text on the button.
A field shows the robot cell status.
In manual mode, the field is green, showing manual mode.
In automatic mode, the field is red, and the current state is shown.
In figure \ref{fig:hmi_control_robot_cell} the interface for the robot cell is shown.
In order to alert the user if the emergency stop is triggered, the frames will toggle between blue and red with a frequency of 1.4 Hz while displaying the text:
``emergency stop engaged''.
When the emergency stop is reset, the frames will return to normal.

\begin{figure}[t]
    \centering
    \includegraphics[width=\imgwidth]{graphics/hmi/robot_cell_emergency_stopped}
    \caption{Screenshot of the HMI for controlling the robot cell.}
    \label{fig:hmi_control_robot_cell}
\end{figure}

\subsubsection{OEE}
Overall Equipment Effectiveness (OEE) is a standard used to
 keep track of a facility's performance, it is meant to help
 optimise production by identifying what part of a production
 is not optimal.
Some of the most common things\citep{oeeguide} to find with OEE are:
% \begin{table}[h]
% 	\caption{common uses OEE }
% 	\centering
	\begin{itemize}
		\item System bottleneck 
		\item Key instructions for resetting parts of the system
		\item Ways to reduce system setup time
		\item Ways to improve system reliability
        \item Ways to reduce maintenance
		\item Capacity of the system
        \item Reasons for down time
		\item Ways to eliminate down time
	\end{itemize}
% \end{table}

% \begin{equation}
% OEE = \text{Availability} * \text{Performance} * \text{Quality}\label{eq:OEE}
% \end{equation}

\begin{table}[t]
\centering
\caption{OEE factors.}\label{tb:oee_factors}
 \begin{tabular}{ll}
    Availability: & $\dfrac{\text{Run Time}}{\text{Total Time}}$        \\[12pt]
    Performance:  & $\dfrac{\text{Total Count}}{\text{Target Counter}}$ \\[12pt]
    Quality:      & $\dfrac{\text{Good Count}}{\text{Total Count}}$     \\[12pt]
    OEE:          & $\text{Availability} * \text{Performance} * \text{Quality}$ 
 \end{tabular}
\end{table}

In the HMI is a page for the OEE made.
This displays the different
 OEE factors such as OEE, Availability, Performance, Quality,
 Target count a count that needs to be reached before a defined time, counts
 up when defined time is reached.
OEE is represented with a percentage which is found as the product of the OEE factors. 
The factors can be calculated using table \ref{tb:oee_factors}.
Total Count is number of production cycles. 
Good Count tracks every time the production cycle succeeds.
Run time tracks the time the system is actively solving a task.
Setup time tracks time between initiating the task and solving a task.
Down time tracks the amount of time the robot is stopped when the system is running.
Total time tracks the time the system is running.

In the HMI is the mobile robot and the robot cell being tracked separately.
The HMI uses an OEE node when extracting OEE information from the system.
This node tracks the states of key ROS topics, to check for availability, when a task is initiated and when it is complete.
This is done by using the Mes client to listen for the state of the system,
 track the time and successful runs.
 Estimating when the system fails is not a trivial task, so situations will occur which will not be counted as mistakes automatically by the system.
 If such situations occur, a button in the HMI will manually set the current task as failed 
 and the time from the button is pressed to the time the robot accepts a new task will be viewed as down time.
The node produces a log file with one entry per production cycle, with all the information from that cycle.

\subsection{Results}
To test the HMI, all buttons were pressed in a randomised order to see if the expected behaviour was executed and the communication protocol was kept.
The HMI can display and modify the parameters of
the robot arm,
the gripper,
the conveyor belt,
the planning state of the robot cell and the mobile robot,
the OEE of the system,
the position and velocity of the mobile robot,
the tipper position on the mobile robot
and the status of the components as given to the MES server.
Having a simple communication system to the backend proved to make the system stable, 
but took more time to maintain than if multiple communication channels were used.

The control of the camera system was not implemented.
ROS topics for the camera system was not complete, so they were not integrated into the system.
% ROS topics were made for the camera system late in the process, but it was not prioritised to integrate that into the system.

Conversion of OEE data from the OEE node was not implemented, and the data
 shown on the front end is therefore not readable.
The OEE node does not have functionality for counting bad counts, and a unfixed
 bug means that the run time counter is counting more then it should, besides
 this the OEE node works as intended.