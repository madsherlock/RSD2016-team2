\subsection{Wall Following with LIDAR}
The charger for the mobile robot is located inside a large rectangular area surrounded by walls (a box).
To navigate inside the box, a wall following navigation mode is used.
By using a LIDAR, the mobile robot will be able to keep a fixed distance to a wall or an object.
% Inside the main Robolab area, the mobile robot can use way-point navigation
% to manoeuvre from place to place.
% However, the charging station for the robot is in a more occluded area,
% which limits the efficacy of way-point navigation.
% The solution to this uses a laser scanning system, known as LIDAR,
% to position the robot with respect to its surroundings.

The LIDAR used for this project is the SICK TIM310 safety LIDAR.
The SICK TIM310 is a laser scanner with a 1\degree angular resolution, a 270\degree aperture and an operating range between 0.05-4m \cite{sicktim310}.

The LIDAR is mounted on the top of the mobile robot.
Due to the other hardware mounted on the robot, when mounted straight the LIDAR is able to detect other elements.
While these areas are unchanging and could be removed in the software,
a better solution is to take advantage of the 90\degree blind-spot of the LIDAR, due to the limited aperture range, and place the LIDAR at an angle on the mobile
robot, thus maintaining the largest possible range of detection for the LIDAR.

The LIDAR data is obtained using the \textit{sick\_tim} ROS driver available from the official ROS wiki page \citep{sicktimnode}.
This driver outputs scan data as a ROS topic which can then be subscribed to or visualised using the rviz visualisation tool\citep{rviz}.
This tool was used initially to verify that the data from the LIDAR behaved as expected and helped to determine the cause or location of outlying data points,
as well as determining where the LIDAR was detecting parts of the mobile robot as objects.

The output of the scan node topic is analysed using the obstacle detector node.
This node combs the output for erroneous values;
takes an average over three measurements to reduce noise;
converts angular data to a range where 0\degree is directly ahead, -90\degree is due right and 90\degree is due left from the mobile robot frame of reference;
and publishes to two ROS topics.

One topic contains a simple Boolean indicating 'True' if the closest object is considered too close and therefore an obstacle, and 'False' otherwise.

The other topic publishes a vector containing the distance to the closest object;
the angle of the nearest object;
the average distance across the whole LIDAR range (giving an indication of how crowded the area is);
the distance to the nearest object in front of the robot;
the distance to the nearest object to the right of the robot;
and the distance to the nearest object to the left of the robot.
The wall follower node subscribes to this topic.

The wall follower is initiated by receiving a message.
This message dictates whether the mobile robot should move to the charger or away from it.
In figure \ref{fig:state_diagram_lidar_nav} can the state diagram for the LIDAR be seen.
When the mobile robot is at the charger, the mobile robot must reverse in order to stand clear of the charging rods.
When it is clear, the mobile robot will turn right until the wall on the right hand side is found.
A PD controller is used to maintain the distance between the wall and the
robot by controlling the angular response of the robot with respect to the
difference between the desired wall distance and the current distance.
Then the mobile robot will move right until the wall is found again.
This on-off controller will continue until the LIDAR looses track of the left wall. By this point, the mobile robot will be out of the charging box and the waypoint navigation can take over.

\begin{figure}[t]
\centering
\begin{tikzpicture}[node distance=1.0cm,scale=0.7, every node/.style={scale=0.7}]
    \node[state,minimum width=1.2cm,name=idle,accepting]            {idle};
    \node[state,minimum width=1.5cm,below right=0.75cm and 1.3cm of idle, name=rev ] {reverse};
    \node[state,minimum width=2cm,right=3.2cm of idle,  name=Right] {go right};
    \node[state,minimum width=2cm,below=1.5cm of Right,       name=PD_A] {PD ctrl};
    \begin{scope}[->,shorten >=1pt,very thick]
    \draw (idle) -- ++(1.0,0) to[out=0,in=135] node[pos=0.5,above=0.25cm] {task exit/}  (rev);
    \draw (Right) to[out=135,in=45]                 node[midway,above]     {left dist $>$ threshold/}     (idle);
    \draw (rev)  to[out=45,in=180]   node[midway,above right=0.0cm and -0.9cm,sloped]     {clear of charger/}  (Right);
    \draw (Right) to[in=45 ,out=-45] node[midway,below,sloped]            {wall nearby/}     (PD_A);
    \draw (PD_A)  to[in=225,out=135] node[midway,above,sloped,rotate=180]            {wall too distant/}  (Right);
    \draw (PD_A) to[out=180,in=270] node[pos=0.35,below=0.3cm]           {left dist $>$ threshold/}  (idle.270|-Right.270) -- (idle);
    \end{scope}
    \node[state,minimum width=2cm,left=3.2cm of idle, name=Left] {go left};
    \node[state,minimum width=2cm,below=1.5cm of Left,       name=PD_B] {PD ctrl};
    \begin{scope}[->,shorten >=1pt,very thick]
    \draw[rounded corners] (idle) -- node[midway,above] {task enter/}   (Left);
    \draw (Left) to[out=45,in=135]  node[midway,above]    {forward dist $<$ threshold/}     (idle);
    \draw (Left) to[in=45 ,out=-45] node[midway,below,sloped]          {wall nearby/}     (PD_B);
    \draw (PD_B) to[in=225,out=135] node[midway,above,sloped]          {wall too distant/}  (Left);
    \draw (PD_B) to[out=0,in=270] node[pos=0.35,below=0.3cm]         {forward dist $<$ threshold/}  (idle.270|-Left.270) -- (idle);
    \end{scope}
\end{tikzpicture}
\caption{State diagram for the LIDAR based navigation.}
\label{fig:state_diagram_lidar_nav}
\end{figure}

When moving to the charger, the mobile robot will move the other direction and will thus have to keep the wall at a fixed distance to the left.
The desired distance to the wall is the same as the desired end goal.
When the mobile robot approaches the charger, the forward distance will decrease and the mobile robot will be able to stop before hitting the wall, 
but not stop before the charger rods make contact to the metal bumpers on the mobile robot.

\begin{eqnarray}
E &=& k \times (S_w - S_d) 
\label{eq:error} \\
P &=& P_p \times E \\
D &=& D_p \times (E - E_{prev}) \\
O &=& P + D 
\label{eq:output}
\end{eqnarray}

Equations~\eqref{eq:error}-\eqref{eq:output} describe the PD controller.
Here $E$ is the error(the difference between desired and actual distance to the wall),
multiplied by a scalar, $k$, which controls the magnitude of the response and is found to have an ideal value of 3.
$P$ and $D$ give the proportional and derivative terms respectively
and $O$ represents the angular velocity.
The parameters $P_p$ and $D_p$ determine the relative input of the proportional
and derivative terms and are found to be 1 and 10 respectively.

% Whilst the node is only used in the final implementation to follow the wall from just inside the charging box to the charger,
% the wall following node can also handle walls turning away or towards the robot and so can be used from outside-to-inside box navigation in the event
% of a problem with the way-point navigation system.
