\subsection{Smart camera}
The Smart Camera is a vision sensor manufactured by Omron, model FQ2-S15050F \citep{smart_camera_datasheet}.
The Smart Camera records a rectangular area of
conveyor belt of size $103.2\times161.3$ mm. This sensor
localises LEGO bricks of blue, red and yellow colours on the conveyor 
belt and extracts information for every brick regarding the
$x$ and $y$ pixel coordinates of the centre of mass of the brick, 
the $area$ in pixels and the $master$ $angle$. This data was
appropriately transformed and passed to the control node so it could be 
used to plan the path of the robot and find a proper grasping for any given brick.
  
\subsubsection{Vision Algorithm}
The Omron vision sensor comes with its own high level vision software
that allows the user combine vision algorithms to a program on the device.
The camera image size is $480\times750$ pixels.
Before any algorithm was implemented, the shutter speed was adjusted to 1/250, which is the minimum,
and the gain was increased to get clearer and more contrasted images.

Colour segmentation seemed to be the best way to discriminate the features of interest. 
The fact that the camera image stays within the edges of the conveyor belt eased this segmentation,
as the background of the image was evenly black along the whole image,
and made a high contrast between the shiny and saturated colours of the bricks an the background. 

The ``Labelling'' inspection algorithm provided the most suitable tool for colour segmentation.
This algorithm searches for colour blobs within defined ranges in the HSV colour space. 
Three sets of ranges were found experimentally, one for each one of the colours,
and set in three different layers. Each of these layers filters out everything but bricks of one of the three colours.
The combination of the three layers finds bricks of blue, red and yellow colour.

The conditions under which the colour segmenting ``labelling'' algorithm extracted features were constrained.
A colour blob had to be in the range of 4000-20000 pixels. 
This constraint comprises the areas of the three different colour bricks and filters out noise.
Furthermore, the x and y coordinates of a brick are required to fall within an inner rectangle in the image, shown in figure \ref{fig:smart_camera_diagram}. 
This condition prevents that bricks on the conveyor belt that fall only partially inside 
the field of view of the camera are labelled as valid elements since it would lead to an error
in the colour classification of elements, as this classification is performed based on the area 
of the blobs due to the camera not providing colour information.
It is shown in figure \ref{fig:smart_camera_diagram} that the algorithm extracts all the colour blobs
in its field of view. However, it filters out bricks 2, 5 and 6 and only outputs the data of 1, 3, 4 and 7. 
Brick number 3 shows what the variables $x_{\mathrm{gravity}}$, $y_{\mathrm{gravity}}$, and $master$ $angle$ represent. 
Notice this measure prevents that bricks 5 and 6 qualify as valid bricks; 
they do not fully appear in the image and based on their area they would have 
likely been classified as red and blue bricks, respectively.

\begin{figure}[t]
	\centering
	\includegraphics[width=\imgwidth]{graphics/Robotcell/smart_camera_diagram.jpg}
	\caption[Example of a simulated view from the Smart Camera of a layout of LEGO bricks]{
	Example of a simulated view from the Smart Camera of a layout of LEGO bricks.
% 	\small
% 	\raggedright
	The bigger green rectangle of size $480\times750$ pixels represents the full field of view of the camera, while the green
	 rectangle of size $550\times290$ represents the region in which a brick's c.o.m. needs to be in order to be recognised by
	  the algorithm. Crosses in the middle of the bricks represent their c.o.m. and those ones highlighted 
	  in green are the bricks that stay within the limits of the green area. 
	}
	\label{fig:smart_camera_diagram}
\end{figure}  


Finally, the algorithm labels all the valid features from 0 to $n$ and outputs a data string, 
with the measurements for $x$ and $y$ gravity coordinates, $master$ $angle$ and $area$ of every brick. 
This data string is the input to the Camera UDP interface.

It should be noted that the camera algorithm, while it does not mix up different colours, 
finds colour blobs, and for instance, two or three blue bricks laying right next to each 
other will form a bigger blob which will be labelled as a unique element, as their area together 
remains within the area upper and lower limits of the algorithm. This will entail a problem 
for the smart camera node that will be explained in the following section. 
%This is, however, not an issue for red and yellow bricks. %Yes it is. Red+blue brick = issue.

\subsubsection{Smart Camera ROS Node}
The output of the camera is a list of positions and orientations.
This information needs to be processed to find a suitable brick for the robot to pick.
The smart camera ROS node converts the data string from
the camera output into usable information for the robot control node.
This node requests the service of the UDP node, takes the output string and performs the following algorithms: 
\begin{description}
\item[Parser:] Converts the string of numbers into an array of ROS messages, 
each of them containing the measurements of $x$ and $y$ gravity, $area$ and $master$ $angle$ of one LEGO brick.
\item[Transformation:] using the transformation computed in the camera calibration, 
it transforms the $x$ and $y$ coordinates contained in the previously mentioned messages, 
into $x$, $y$ and $z$ coordinates of the robot. It also assigns a colour information to every 
LEGO brick based on its $area$ and copies the $master$ $angle$ without further changes. 
These calculations are registered in another array of ROS messages. 
\end{description}

The problem is that the Parser algorithm does not contain color information,
but is simply applied to a colour segmented image allowing red, blue and yellow colours.
Two blue LEGO bricks next to each other might be seen as a red LEGO brick.
The solution would be to apply three algorithms in succession, where the colour segmentation
step only accounts for one LEGO colour.
This, in turn, results in a much larger cycle time because the algorithms would be switched between
using the digital inputs on the camera.

This node is utilised as either a single time service that commands the camera to take a single picture,
send out the data and transform into the protocol of communication with control node, 
or as a node that runs in a loop the previous steps until any of the bricks 
in the order are seen in the image. 
The latter is used to stop the conveyor belt when the off loaded bricks arrive to the 
sight of the camera, while the former is called for a single accurate measurement 
of the bricks information once the conveyor belt has been stopped.
