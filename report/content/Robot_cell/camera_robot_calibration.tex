\subsection{Camera-robot calibration}
The camera-robot calibration establishes the relationship between the coordinate systems
of the camera and the robot. Typically calibration is performed in two differentiated steps:
intrinsic calibration of the camera and extrinsic calibration. 
These methods yield a transformation matrix that converts any given point in the robot 
coordinate system into a point in the image plane, this is, a specific pixel in the image, and vice versa.
However in the latter, the transformation yields a 3D homogeneous point, which corresponds
to a ray of points in the Cartesian 3D space, leaving a degree of uncertainty depending
on the camera field of view as well as viewing angle with respect to the conveyor belt.
 
The field of view of the camera was narrow and encompassed an area of $103.2\times161.3$ $mm$ 
on the conveyor belt. This area was flat and regardless not being strictly parallel to the image plane, 
the distance to the camera from any point on the conveyor belt was approximately equal. 
This allowed to create an auxiliary 3D coordinate system laying on the conveyor belt, 
with origin coincident with the origin of the pixel coordinates system and $x$ and $y$ 
axes along their counterparts in the image plane. The $z$ axis is perpendicular to the conveyor belt surface. 
Therefore, transforming from this 3D system to image coordinates was as simple as dividing by a scale factor. 
This factor was experimentally calculated by measuring in the image the distance in pixels of a 
known line segment drawn on a paper laying on the conveyor belt. Due to the above mentioned imperfect 
parallel alignment between camera image plane and the conveyor belt, the scaling factor varies along the $y$ axis.

Note this was a practical solution and many parameters of a thorough intrinsic calibration were left aside, such as
shear,radial and tangential distortion effects. However, these were not included in the calculations as
they were considered to be negligible and might be accounted for in later sprints.

In order to find a transformation between the coordinate system of the robot and the above mentioned auxiliary 
system (see figure \ref{fig:calibration}), the coordinates of 10 different points were collected in both coordinate
systems. These points were spread as evenly as possible on the image plane by placing a LEGO brick on the 
conveyor belt within the field of view of the camera. The $x$ and $y$ coordinates of the $c.o.m$ of the 
brick were acquired from the camera and as the coordinates $x$, $y$ and $z$ of the robot TCP after
being jogged to a grasping position. 
\begin{figure}[t]
	\centering
	\includegraphics[width=\imgwidth]{graphics/Robotcell/Calibration.PNG}
	\caption[Coordinate systems of Smart Camera and robot in the simulated robot cell]{Coordinate systems of Smart Camera and robot in the simulated robot cell.
	The extrinsic calibration aims to find a transformation matrix between both systems.}
	\label{fig:calibration}
\end{figure}
  
As the robot has 6 $d.o.f$ a combination of $x$, $y$ and $z$ still leaves 3 $d.o.f$ in which the
 configuration of the robot can vary. The robot was jogged to a configuration in which the $z$ 
 axis of the tool frame was approximately perpendicular to the conveyor belt. 
 The $roll$, $pitch$ and $yaw$ of the tool frame were maintained constant for 
 all grasping position and the robot was jogged only translating 
 on the $x$, $y$ and $z$ axis of the tool frame.

This method led to two different sets of 10 points in each coordinate system.
Let $P^{\textrm{cam}}_i$ be
the 3D point in the auxiliary coordinate system and $P^{\textrm{base}}_i$ 
the points in the robot base frame. Then:
\begin{equation}
\mu_{\textrm{base}}=\frac{1}{N} \sum_{i=1}^{N} P^{\textrm{base}}_i
\end{equation}
\begin{equation}
\mu_{\textrm{cam}}=\frac{1}{N} \sum_{i=1}^{N} P^{\textrm{cam}}_i
\end{equation}
\begin{equation}
H=\sum_{i=1}^{N}(P^{\textrm{base}}_i-\mu_{\textrm{base}})(P^{\textrm{cam}}_i-\mu_{\textrm{cam}})^T
\end{equation}
Then, the transformation matrix between those was found using a SVD algorithm in Matlab \citep{matlab_svd}:
\begin{equation}
[U,S,V]=SVD(H)
\end{equation}
Finally rotation $R$ and translation $P$ are computed:
\begin{equation}
R=VU^T
\end{equation}
\begin{equation}
P= -R\cdot\mu_{\textrm{base}}+\mu_{\textrm{cam}}
\end{equation}

The validity of the transformation is assessed by comparing transformed camera points with their actual
 corresponding points in the robot coordinate system. Experimentally, calibration fitness can
  be studied in relation to the success rate of the robot picking up bricks.