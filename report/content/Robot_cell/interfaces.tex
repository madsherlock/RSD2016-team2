\subsection{Interfaces}
A challenge when integrating different hardware is
interfacing everything for efficient communication.
For the PLC, the Modbus TCP protocol was used.
For the smart camera, a special UDP format was used.
These protocols and interfaces are described in general terms below,
to give an initial understanding of the choices made before applying the interfaces
to the project application.

\subsubsection{Modbus TCP}
\label{sec:modbustcp}
Modbus TCP is a communication protocol found in the industry.
The Modbus protocol is defined as a master/slave protocol, which means that one 
device will be operating as a master and be sending or extracting information to 
its slaves.  
The Modbus protocol message is defined as shown in table \ref{tab:modbus_protocol}. %below plz

\begin{table}[ht]
\centering
\caption{Modbus protocol message. Adapted from \citep{modbus}.}
\label{tab:modbus_protocol}
 \begin{tabular}{c | c | c}
  Name & Length(bytes) & Function \\
  \hline
  Transaction ID & 2 & Used to identify request/response pairs \\
  Protocol ID & 2 & Idenditify protocol - 0 for modbus TCP \\
  Length field & 2 & Number of bytes will follow after this \\
  Unit ID & 1 & Slave address \\
  Function code & 1 & Function code \\
  Data & n & Data being either the response of command \\
 \end{tabular}
\end{table}

Modbus has an internal data table, which is shared amongst Modbus devices and can 
be accessed by them. Some of the data tables can either read, write or both. The 
master will write to a slave data register, and read from a slave data 
register. These data registers are specified in table \ref{tab:modbus_data_tables}. %below plz

\begin{table}[ht]
\centering
\caption{Modbus data registers shared between slave and master. Adapted from \citep{modbus}.}
\label{tab:modbus_data_tables}
 \begin{tabular}{c | c | c | c}
  Data type & size &  address - space & access\\
  \hline
  Coils & 1-bit &0000 - FFFF & Read-write \\
  Discrete input & 1-bit &0000 - FFFF & Read \\
  Input register & 16-bit &0000 - FFFF & Read \\
  Holding register & 16-bit &0000 - FFFF & Read-write \\
 \end{tabular}
\end{table} 

Reading or writing in these data tables is done by specifying a function code 
in the protocol message. The different function codes specify which
data table to access, and the action to perform. The data address 
is specified in the data part of the protocol message.
The function codes are listed in table \ref{tab:modbus_tcp_function_code}.

\begin{table}[ht]
\centering
\caption{Modbus TCP Function codes. Adapted from \citep{modbus}.}
\label{tab:modbus_tcp_function_code}
 \begin{tabular}{c | c | c | c}
  Function type & Function code\\
\hline
  Read Coil	 		&		1		\\
  Read Discrete Input 	&  		2		\\
  Read Holding Registers 	& 		3		\\
  Read Input Registers 	&  		4		\\
  Write Single Coil 		&		5		\\
  Write Single Holding Register 	&  		6		\\
  Write Multiple Coils 	& 		15		\\
  Write Multiple Holding Registers 	&  		16		\\
 \end{tabular}
\end{table} 

\subsubsection{Camera UDP interface}
The Omron FQ2 Smart Camera supports three different Ethernet based
protocols for communicating with third party hardware:
\begin{enumerate}
\item TCP
\item UDP
\item Omrons own communication protocol called FINS
\end{enumerate}

In order to maintain scalability in the robot cell, UDP was chosen,
given its lightweight implementation and non existing handshake feature.
Chosing UDP enables high throughput from the smart camera to a third party 
control device.

The Omron FQ2 uses a two-point configuration approach
in order to enable camera control and receive
measurements. This is done by letting the smart camera
communicate through two separate sockets.
The first socket - the control socket - is hosted by the smart camera.
The control socket is used for sending control commands to the camera,
and receiving information about the validity of the command.
The smart camera uses the second socket - the output socket
for communicating measurements to the control device.
The output socket is hosted by the control device, and the smart camera requires
a fixed IP address pointing to the control device in order to communicate through the 
socket.
Formatting of the data to be sent through the first socket can be configured in 
the following two ways:
\begin{enumerate}
\item Binary Format
\item String Format
\end{enumerate}

The string format was chosen, presenting a very intuitive understanding
of the commands being sent. A vast amount of commands are supported
by the smart camera, but they all conform to the structure of figure \ref{fig:omron_fq2_ctrl_cmd}.
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{graphics/Robotcell/omron_fq2_ctrl_cmd}
    \caption{Omron FQ2 Control command format.}
    \label{fig:omron_fq2_ctrl_cmd}
\end{figure}

First, the operation the camera has to perform is
specified in the command section.
Next, a white space is added and if needed additional data
is given through parameters by the given command,
adding a white space between each given parameter.
At the end of a command a Carriage Return character has
to be inserted.

When the command is communicated through the control socket,
the camera returns a string response indicating if the command
was valid or not. The two responses are illustrated in figure \ref{fig:omron_fq2_ctrl_response}.
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{graphics/Robotcell/omron_fq2_ctrl_response}
    \caption{Omron FQ2 Control response format.}
    \label{fig:omron_fq2_ctrl_response}
\end{figure}

The smart camera outputs measurement data through the output socket using the
standard of figure \ref{fig:omron_fq2_output}.
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{graphics/Robotcell/omron_fq2_output}
    \caption{Omron FQ2 output format.}
    \label{fig:omron_fq2_output}
\end{figure}

As it can be seen on figure \ref{fig:omron_fq2_output}, each measurement is output
as a string, using a white space to separate each measurement,
and again the string is terminated by a Carriage Return character.

When choosing between required commands of the smart camera
in order to fulfil the project requirements it was found that
only one smart camera command was needed. Sending the command string
"measure" through the control socket, as seen in figure \ref{fig:omron_fq2_measure}.
\begin{figure}[!h]
    \centering
    \includegraphics[scale=1]{graphics/Robotcell/omron_fq2_ctrl_cmd_ex1}
    \caption{Omron FQ2 "measure" command.}
    \label{fig:omron_fq2_measure}
\end{figure}

The camera will perform a single measure on defined output measurement
parameters, in the case of tracking LEGO bricks on the conveyor,
position, area and orientation values will be sent.

If a control device (a laptop) is hosting the 
output
socket on the correct fixed IP address specified in the smart camera, it will output the result of the measurement. 
Figure \ref{fig:omron_fq2_output_example} shows an example of an output.
\begin{figure}[!h]
    \centering
    \includegraphics[scale=1]{graphics/Robotcell/omron_fq2_output_ex1}
    \caption{Omron FQ2 measurements output example.}
    \label{fig:omron_fq2_output_example}
\end{figure}

This setup gives the user very precise control on how many
measurements should be performed, depending on the number of "measure"
command strings that are sent to the smart camera and reliability
of the UDP connection.

To enable easy distribution of measurements from the smart camera, the presented
two-point configuration of the smart camera was implemented into a ROS node,
as shown in figure \ref{fig:omron_ros_interface}.
\begin{figure}[!h]
    \centering
    \includegraphics[scale=1]{graphics/Robotcell/omron_fq2_ros_interface}
    \caption{Camera Interface wrapped in ROS node.}
    \label{fig:omron_ros_interface}
\end{figure}

The ROS node utilises a single service call to request a camera measurement and
in the same service call the measurement data is returned through its response.
The ROS node simply functions as a wrapper of the underlying C sockets,
hereby revealing an simple ROS message structure that does not require
any request parameters and a single respond parameter: the returned
output string from the camera.

