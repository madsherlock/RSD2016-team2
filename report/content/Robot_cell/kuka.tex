\subsection{KUKA robot arm}
The KUKA robot has the job of grasping the LEGO pieces on the conveyor belt.
This section describes everything regarding
the KUKA robot, the controller, pneumatic system, Robot Sensor Interface (RSI) and
other software needed to complete the task given.
The robot arm used for this task, is a KUKA
KR6 R700 sixx \citep{kukakr6} with a KUKA KRC C4
controller. These where provided to the project fully
configured, and needed only minimal
changes to fit the solution for the task.

\subsubsection{Attachments}
For the robot arm to be able to pick up LEGO pieces,
a pneumatic gripper is used \citep{festogripper},
with custom printed fingers to grasp the LEGO pieces.

\paragraph{Pneumatics}
The gripper is pneumatic and therefore needed to be
connected to an air supply. The air goes from the
supply through the robot arm, and then to the gripper.
The air can go through three valves which are
inside the robot, see figure \ref{fig:Valve_diagram}.

The valve chosen is valve 1 with the outputs
1A and 1B. One of these is
open at any given time, forcing the gripper to either open or close.
Which output will be
opened is controlled internally and
will be explained in section \ref{subsub:Kuka_software}.
Tubes are connected to the outputs, see figure
\ref{fig:interface_A4},
and then connected to the gripper, see figure
\ref{fig:full_gripper}.
The grey object without tubes on figure
\ref{fig:interface_A4} is the silencer.
\begin{figure}[t]
	\centering
	\begin{subfigure}[b]{0.25\textwidth}
		\centering
		\includegraphics[scale=0.5]{graphics/Robotcell/gripper_front}
		\caption{Front view.}
		\label{fig:full_gripper_front}
	\end{subfigure}
	\begin{subfigure}[b]{0.25\textwidth}
		\centering
		\includegraphics[scale=0.5]{graphics/Robotcell/gripper_side}
		\caption{Side view.}
		\label{fig:full_gripper_side}
	\end{subfigure}
	\caption{Pictures of the entire gripper.}
	\label{fig:full_gripper}
\end{figure}

\begin{figure}[h]
	\centering
	\hspace*{\fill}
	\begin{minipage}[b]{0.4\textwidth}
		\centering
		\includegraphics[scale=0.5]{graphics/Robotcell/Valve_diagram}
		\caption[Valve diagram]{Valve diagram. Source: \citep{krAgilusSixx}.}
		\label{fig:Valve_diagram}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.4\textwidth}
		\centering
		\includegraphics[scale=0.5]{graphics/Robotcell/Interface_A4}
		\caption{Interface A4 with pneumatic connection.}
		\label{fig:interface_A4}
	\end{minipage}
	\hspace*{\fill}
\end{figure}

\paragraph{Gripper Mount and Fingers}

In order to mount the Festo gripper on the
robot arm an auxiliary link (see figure \ref{fig:gripper_adapter}) was necessary due to
the physical constraints of mounting one directly on the other. The auxiliary link
was manufactured using a 3D printer and ABS plastic. Similarly, the gripper fingers
were produced using the same material.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.7]{graphics/Robotcell/GripperAdapter}
	\caption{Auxiliary link. Units are millimetres.}
	\label{fig:gripper_adapter}
\end{figure}

As the gripper fingers were designed in thick plastic for structural stability, they did not fit
for performing accurate brick grasping as the gripper would require greater clearance between the bricks.
For this reason a metal plate was bolted in each of the fingers, as shown in figure \ref{fig:full_gripper},
allowing the gripper to
access grasping position with narrow clearances around the LEGO bricks.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.7]{graphics/Robotcell/GripperFingers}
	\caption{Gripper fingers. Units are millimetres.}
	\label{fig:gripper_fingers}
\end{figure}

\subsubsection{KUKA Software}
\label{subsub:Kuka_software}
In this section, the software used both in and together with the KUKA robot is
described.

\paragraph{KUKA RSI}

The KUKA KRC C4 controller uses RSI to communicate with the
outside world.
This is pre-installed but needs to be
modified to account for the gripper.
To make these modifications three files needs
to be modified: an RSI file,
a Config XML document, and an SRC file.

In order to modify the first file a program
called RSIVisualShell is used \citep{kuka_RSI}.
It uses RSI objects to define the signal
processing in the controller.
The complete diagram can be seen on figure
\ref{fig:RSI_Visual_diagram}.
The objects in the red box are the added
outputs, and are used for reading
the values of the gripper.
The blue box has
the inputs which are used
to control the gripper.
The index number in each object is used when
mapping the I/O to the A4 interface.
%When saving the RSI file two more files are
%generated that are needed.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{graphics/Robotcell/RSI_Visual_diagram.PNG}
	\caption[RSI Visual diagram]{RSI Visual diagram, screenshot from RSDIVisualShell \citep{kuka_RSI}.}
	\label{fig:RSI_Visual_diagram}
\end{figure}

The next file modified is the configuration file,
which defines the UDP
communication to the RSI middleware. The
configuration file defines the \textbf{IP} and
\textbf{PORT} of
the external socket, its \textbf{ONLYSEND}
parameter defined in the config file.
It is set to be false, such that it
\textit{receives} messages.
\textbf{DEF\_AIPos} is a timestamp used to
 ensure that a package arrives on time.
\textbf{DEF\_Delay} is a counter that counts
the number of sequentially delayed
 packages. If the counter reaches 100,
 the controller will stop the program.
\textbf{AKorr.A1} up to \textbf{A6}
represent the joint angles of the robot arm.

The four RSI objects marked in
\ref{fig:RSI_Visual_diagram} are added to the
\textbf{send} and \textbf{receive} messages,
which are defined in the config file by the
\textbf{send} and \textbf{receive} tags.
The \textbf{receive} message includes the RSI
objects stating the input, and the
\textbf{send} message includes the RSI
objects related to the outputs.

Send messages are defined as per figure \ref{fig:rsisend}.
\begin{figure}[ht]
\centering
\begin{verbatim}
   <SEND>
      <ELEMENTS>
         <ELEMENT TAG="DEF_AIPos" TYPE="DOUBLE" INDX="INTERNAL" />
         <ELEMENT TAG="DEF_Delay" TYPE="LONG" INDX="INTERNAL" />
         <ELEMENT TAG="Digout.o1" TYPE="BOOL" INDX="1" />
         <ELEMENT TAG="GRIP.o1"   TYPE="BOOL" INDX="2" />
         <ELEMENT TAG="GRIP.o2"   TYPE="BOOL" INDX="3" />
      </ELEMENTS>
   </SEND>
\end{verbatim}
\caption{RSI Send message format.}
\label{fig:rsisend}
\end{figure}
Receive messages are defined as per figure \ref{fig:rsireceive}.
\begin{figure}[ht]
\centering
\begin{verbatim}
   <RECEIVE>
      <ELEMENTS>
         <ELEMENT TAG="DEF_EStr" TYPE="STRING" INDX="INTERNAL" />
         <ELEMENT TAG="AKorr.A1" TYPE="DOUBLE" INDX="1" HOLDON="1" />
         <ELEMENT TAG="AKorr.A2" TYPE="DOUBLE" INDX="2" HOLDON="1" />
         <ELEMENT TAG="AKorr.A3" TYPE="DOUBLE" INDX="3" HOLDON="1" />
         <ELEMENT TAG="AKorr.A4" TYPE="DOUBLE" INDX="4" HOLDON="1" />
         <ELEMENT TAG="AKorr.A5" TYPE="DOUBLE" INDX="5" HOLDON="1" />
         <ELEMENT TAG="AKorr.A6" TYPE="DOUBLE" INDX="6" HOLDON="1" />
         <ELEMENT TAG="GRIP.i1" TYPE="BOOL" INDX="7" HOLDON="1" />
         <ELEMENT TAG="GRIP.i2" TYPE="BOOL" INDX="8" HOLDON="1" />
       </ELEMENTS>
   </RECEIVE>
\end{verbatim}
\caption{RSI Receive message format.}
\label{fig:rsireceive}
\end{figure}

In the receive message definition,
HOLDON is set to 1, ensuring
that the last written value is remembered.

INDX indicates the pins which the RSI object
in the send and receive message connects onto
RSI object Ethernet, as seen in figure
\ref{fig:RSI_Visual_diagram}.

%TAG is mentioned later in the middleware.
In order to map the RSI object to the A4
interface, the program WorkVisual was used.
%The installation file can be found in the controller.
%It downloads the cell currently on the
%controller.
I/O setup is done by
mapping the KR C (controller) I/O to the
EM8905-1001 (fieldbus) I/O-Module. The
mapping is done as followis:\\
IN1 \(\rightarrow\) DO7 \(\rightarrow\) OUT1 \\
IN4 \(\rightarrow\) DO10 \(\rightarrow\) OUT4 \\
DO7/DO10 are the outputs for valve 1 as it
can be seen in \citep{krAgilusSixx}. When IN1
is 1 and IN4 is 0 the gripper is open.
The last file to modify is the SRC, which is
the main program and needs to
run when one wants to communicate and send
joint configurations to the robot arm.
Only one thing needs to be edited in this file, 
which is removing \#IPO from
the RSI\_ON() so that it would run with
\#IPO\_FAST which is default,
meaning that it runs with a signal
processing at sensor cycle rate of 4 ms
instead of 12 ms.
The reason this needed to be
changed, was to ensure that too many lost packages would not trigger the
controller to stop the program.
%This was changed after the gripper functionality was added.
 
\paragraph{RSI Middleware}
As ROS had problems keeping the connection to
the controller alive at all times, an RSI
middleware program was provided, which
talks to the controller using a
UDP connection, keeping the connection alive. It starts two treads; one for
a server and one for a client.
Messages sent to and from the controller are
in XML format.
To communicate with ROS a TCP connection is
established to a ROS node,
this node acts as a gateway to the robot
controller and vice versa.
Only minor changes were made in the
middleware, such as including the feature of
opening and closing the gripper, and to
monitor the state of the gripper.
Due to
the change made in RSI\_ON(), the
RSI\_TIME was set to 4 ms to match speed of
the controller when sending and receiving.
The RSI\_TIME is also used in the ramp
interpolation to compute joint
configurations between start and end configuration,
if this is needed. This also makes sure that
the constraints are obeyed.

\paragraph{KUKA ROS}
The \textit{kuka\_ros} node is the last piece of
software that was provided for the
KUKA robot. This node enables the
functionality of using ROS to interact
with the controller.
As mentioned it uses a TCP connection to
communicate with the middleware,
and acts as a gateway for all other nodes to
communicate with the KUKA robot.
It makes use of services which makes it easy
for other nodes to interface. The gripper was also added
as in the middleware; The gripper
functionality is implemented as two
services being \textit{SetGipper} which takes a
boolean true for opening the gripper
and false for closing the gripper, and the
other one is \textit{getIsGripOpen} which
returns true if the gripper is open and
false otherwise.

\paragraph{Control Node}
The ROS node \textit{control\_node} coordinates the entire robot cell.
This node interacts with all the nodes described above to complete the given tasks.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{graphics/Robotcell/ControlNodeStateMachine.pdf}
	\caption{State Machine representing the final workings of the Control Node}
	\label{fig:ControlNodeStateMachine}
\end{figure}

Figure \ref{fig:ControlNodeStateMachine}
depicts the final design of the control node.
The node changes the state based on the input
from two subscribers. The two nodes which
affects the state, are MES robot client and the
\textit{smart\_camera} node. In order to make sure
that both subscribers would not interfere with decision making,
the MES
subscriber was turned off while the
\textit{control\_node} was listening to the camera.

The camera topic is not being closed, as the
messages from the camera are only being sent
to the topic when the control node requests
the service of the camera, in which case the
subscriber listening to the MES client would
be closed beforehand. The responses from the
camera that can trigger a state change are
"STOP and "CONTINUE".
In the \textit{Pick Brick} state, the node will ask for bricks
by calling a service, and the position and
orientation is returned by the service call.

When the event \textit{wait for bricks} occurs,
the state changes to \textit{start\_camera} and goes
into a loop. In this loop
the MES topic is turned off and is not turned
on again before the order has been fulfilled.
The advantage of having the states changed
by an outside source is that
if the \textit{control\_node} shuts down and needs a
reset, it
will be able to start from the
state it was last in. The only time where
resetting more than one node would be
needed is if the camera node shuts down during interaction.
If the camera node shuts down, and is
reset, it would be waiting for a service
call telling it to start, which the control
node doesn't check for in this design.
If
both the MES server and MES client need to be
reset, the \textit{control\_node} will just jump back
to its idle state, as it is only being affected
by a limited number of messages published by
the client.
