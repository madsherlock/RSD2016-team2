\section{Safety System}
In any implemented system,
whether in an academic or an industrial setting,
it is of key importance that a safety system be implemented to protect
users and prevent damage.
This system is no exception.
It is important that each component within the system be able to be
safely stopped using a robust safety protocol at any moment,
were a risk to personal or component safety to arise.

\subsection{International Standards}
Part of the purpose of international standards is to ensure that all industrial systems follow standardised safety guidelines and that therefore
systems from multiple contractors work together effectively with no 
safety hazards.
It should be noted that a standard is distinct from a directive \citep{directive}.
A European directive is a law applying to European member states outlining the minimum safety requirements for a product or system to be legally sold. 

The International Organization for Standardization have published two documents detailing safety standards for industrial robot systems:
ISO 10218-1 and ISO 10218-2. The former is for use by producers of industrial
robots, whereas the latter details standards for robot systems.

In this project, it is therefore the latter which is considered \citep{ISO10218}.

The final robot system presented in this report is in full compliance with the standard and uses
%, with the exception of the additional HMI stop, 
standardised safety equipment.
The Kuka Robot comes with a control box which has an emergency stop.
This emergency stop should be connected, so it turns off the entire cell.
The control box contains two signals for doing this, described in section 6.6 in the instruction manual \citep{kuka_control_box}.

The lighting of the robot cell is not linked to the operation of the robot, 
so the system could be argued to be in conflict with the requirements laid out in 
Section 5.3.13 of the standard, which requires the robot system to be supplied with built-in lighting.
However, during normal operation it is a matter of course that the lights
in the room are on, and these lights fulfil the requirements for lighting
of the robot cell.

\subsection{Risk Assessment}
In order to make the system safe to use, a risk assessment must be carried out.
Each dangerous situation must be classified by how badly the user can be injured and how likely that is to happen.
The risk matrix used in the scoring of the risk assessment is shown in Table \ref{table:riskmatrix}.
Severe consequences are consequences which result in life long injuries or death. 
Major severity includes injuries that will cause the worker to take time off work to recover.
Moderate severity includes injuries that causes harm to the worker.
Minor severity includes disruptions that directly gives discomfort to the worker.
Minimal severity includes situations which could evolve into a more dangerous situation.
Using a risk matrix helps prioritise which situations should be solved.
A low risk level means the problem either does not have to be solved or can be solved with proper training of the workers.
Higher scores means the problem must be solved.
\begin{table}[hbtp]
\centering
\caption{Risk matrix used for risk assessment.}
\label{table:riskmatrix}
\begin{tabular}{ll|c|c|c|c|c}
    &\multicolumn{6}{c}{Likelihood}\\
    \multirow{7}{*}{Severity}& & Rare & Unlikely & Possible & Likely & Almost certain\\
    \cline{2-7}
    &Severe & Medium & Medium & High & Extreme & Extreme\\
    \cline{2-2}
    &Major & Low & Medium & Medium & High & Extreme\\
    \cline{2-2}
    &Moderate & Low & Low & Medium & Medium & High\\
    \cline{2-2}
    &Minor & Low & Low & Low & Medium & Medium \\
    \cline{2-2}
    &Minimal & Low & Low & Low & Low & Low\\

\end{tabular}
\end{table}


\begin{tabular}{l|p{10cm}}
\hline
Description of risk & Kuka robot hits a person inside the robot cell\\
\hline
Possible consequences & Death or injury to the person\\ 
\hline
Likelihood & Possible\\
\hline
Severity & Severe\\
\hline
Overall Risk Level & High \\
\hline
Risk Mitigation & 1. The kuka robot was set to operate at no more
than 62.5\% of its maximum speed, thus reducing the potential
severity of any collisions.

2. A light curtain was set up at the entrance of the robot cell which, when triggered, 
automatically and immediately halts the robot until the safety system has been manually reset.

3. A stack light was set up outside the robot cell indicating the current status of the robot cell, allowing easy checking of whether the robot is active. \\
\hline
Adjusted Likelihood & Rare\\
\hline
Adjusted Severity & Minor\\
\hline
Adjusted Risk Level & Low \\
\hline
\end{tabular}

\vspace{10pt}

\begin{tabular}{l|p{10cm}}
\hline
Description of risk & Kuka robot hits the wall of the robot cell\\
\hline
Possible consequences & Injury to person leaning on the wall\\
\hline
Likelihood & Possible\\
\hline
Severity & Minor\\
\hline
Overall Risk Level & Low \\
\hline
Risk Mitigation & 1. An emergency stop button was mounted within easy reach on the outer wall

2. A detailed simulation of the robot cell was produced to allow for testing of robot paths and robot path planning to avoid programming the robot in a way that would allow it to hit the wall.\\
\hline
Adjusted Likelihood & Rare\\
\hline
Adjusted Severity & Minor\\
\hline
Adjusted Risk Level & Low \\
\hline
\end{tabular}

\vspace{10pt}

\begin{tabular}{l|p{10cm}}
\hline
Description of risk & Kuka gripper comes into contact with person on LEGO box delivery\\
\hline
Possible consequences & Injury to hands or arms\\
\hline
Likelihood & Unlikely\\
\hline
Severity & Major\\
\hline
Overall Risk Level & Medium \\
\hline
Risk Mitigation & 1. A slider was built which allows users to stock the robot with LEGO boxes from outside the cell, 
thus not coming within reach of the robot at any time during production.

2. A light curtain detects the person coming into the robot cell and triggers the emergency stop.\\
\hline
Adjusted Likelihood & Rare\\
\hline
Adjusted Severity & Minor\\
\hline
Adjusted Risk Level & Low \\
\hline
\end{tabular}

\vspace{10pt}

\begin{tabular}{l|p{10cm}}
\hline
Description of risk & Mobile robot hits a person\\
\hline
Possible consequences & Injury to person in the way of the robot\\
\hline
Likelihood & Almost certain\\
\hline
Severity & Moderate\\
\hline
Overall Risk Level & High \\
\hline
Risk Mitigation & 1. The LIDAR mounted on the mobile robot remains on at all times and performs as an obstacle detector. In wall following mode the robot will automatically move away from the obstacle, in line following or waypoint navigation mode the robot will stop when the LIDAR detects an object within a set distance of it and remain stationary until the obstacle is removed.

2. The speed of the mobile robot is limited so the velocity at impact will not cause harm to a person if collision should occur.\\
\hline
Adjusted Likelihood & Unlikely\\
\hline
Adjusted Severity & Minimal\\
\hline
Adjusted Risk Level & Low \\
\hline
\end{tabular}

\vspace{10pt}

\begin{tabular}{l|p{10cm}}
\hline
Description of risk & Fingers or clothing get trapped in conveyor belt mechanism\\
\hline
Possible consequences & Injury to person\\
\hline
Likelihood & Possible\\
\hline
Severity & Moderate\\
\hline
Overall Risk Level & Medium \\
\hline
Risk Mitigation & 1. The conveyor belt only moves when it is required to transport bricks, otherwise it remains off.

2. The conveyor belt is turned off automatically when an emergency stop is triggered.

3. The conveyor belt is placed behind a light curtain which triggers the emergency stop, should a person enter the robot cell.\\
\hline
Adjusted Likelihood & Rare\\
\hline
Adjusted Severity & Minor\\
\hline
Adjusted Risk Level & Low \\
\hline
\end{tabular}

\subsection{Physical safety measures in the robot cell}
The cell containing the KUKA robot is constructed with safety in mind.
The robot is at the centre of a rectangular area penned in on three sides,
with mesh/perspex walls on either side and a solid wall at the back.
The combination perspex/mesh walls allow the robot to be visible at all
times.
The mesh walls are robust and maintain visibility even when dirtied,
whereas the perspex areas provide slightly more shielding in the event that the robot hits the wall. 

Entry to the robot cell can be obtained only from one direction,
where the side of the cell is left entirely open.
This allows for easy, quick and unobstructed entry and exit to the robot
cell at all times.
During operation is a black and yellow chain held across the cell to provide a physical barrier to people entering the cell. 

\subsection{Electronic and software safety measures in the robot cell}
In addition to physical safety precautions,
an electronic safety system based on a SICK FX3-CPU1 safety PLC
module programmed using 
the SICK Flexi Soft Designer software.
The safety system for the robot cell uses three separate emergency stop
inputs to the Flexi Soft FX3-XTIO expansion module,
shown as part of the system in Figure \ref{fig:safetyplc}, as well as the 
emergency stop button built into the Kuka robot pendant.
If any one of these are triggered,
both the robot and the conveyor belt will come to an immediate halt, as 
mandated by section 5.3.8 of the ISO standard 10218-2 \citep{ISO10218}. In compliance with these standards, particularly section 5.3.8.2, all emergency stop signals are negative. This means that a lack of signal, or 'False', is interpreted as an emergency stop, thus ensuring that the emergency stop remains in place even in the event of a power failure.

\begin{figure}[ht]
\centering
\includegraphics[height=0.2\textheight]{graphics/safety/safety_box}
\caption[Online image of the safety PLC]{Online image of the safety PLC, taken from Flexi Soft Designer.
In this case,
the physical emergency stop button, % and the HMI stop button,
input I1, % and I4 respectively,
is not pressed, however the light curtain has been
triggered (inputs I5-I6),
resulting in the red stack light (output Q2) being lit and amber and
green stack lights (outputs Q3 and Q4) being off.}
\label{fig:safetyplc}
\end{figure}

The elements of the electronic safety system and their interconnections can be seen in Figure \ref{fig:safetydig}.

\begin{figure}[htp]
\centering
\includegraphics[width=\imgwidth]{graphics/safety/system_overview_no_hmi_stop}
\caption[Safety connection diagram]{Diagram illustrating how the safety system is connected and the direction of arrows. Red arrows indicate stopping signals and black arrows indicate information signals.}
\label{fig:safetydig}
\end{figure}
% \nikolaj{Pin 34/35 and 45/46 is a NC signal for the control pad emergency stop which is not connected 6.6.1}.

While it is clearly important that a system be safe,
it is equally important that safety features do not impede users use of the system.
If this begins to happen,
users will begin to circumvent or disable the safety system resulting in a yet more dangerous system. 
In light of this, there are two %three
different ways to trigger an emergency stop of the robot cell, each with a separate reset.

All safety device resets and robot controls are placed outside the robot cell protected area, as required in the ISO standard 10218-2 section 5.3.2\citep{ISO10218}.

\subsubsection{Single-channel physical emergency stop button}
This is a traditional, red emergency stop button.
It is activated by pressing, and reset by twisting.
It is mounted at eye level outside the robot cell, on one of the mesh walls, next to
where the operations team are positioned.
This positioning ensures that in the case of an emergency,
the button will be between the user and the robot.
To use the button, the user does not need to take their eyes of the robot
at any time and should always be obvious and in sight.
The emergency button is simple to use, does not require any training or
any thinking and can be used by anybody.
This reduces the reaction time between the robot beginning to go wrong
and when the system is shut down,
thus reducing the amount of damage that
can occur.

When unpressed, the emergency stop button forms part of a low impedance circuit,
supplied with at 24V input from the safety PLC power supply and
thus passing a 24V input back to the PLC.
When the button is pressed,
it breaks this circuit and triggers the emergency stop.

\subsubsection{M4000 Multibeam safety light barrier, type 4}
The light curtain consists of a sender and a receiver,
mounted either side of the robot cell entrance.
A beam of light is passed between them, with both returning a 24V input
to the safety PLC. If the beam is broken,
potentially due to a person entering the robot cell,
the input to the PLC drops and the emergency stop is triggered.

The light curtain is reset with a physical button,
coloured green in order to distinguish it from an emergency stop button,
placed just outside of the robot cell.
Its positioning means that if the light curtain is accidentally triggered
it is easy to reset, and so users will not be tempted to remove the safety
system. However, the button is also positioned as to be difficult to reset
from inside the robot cell.
This prevents the robot cell from being activated whilst somebody is within range of the robot.

The light curtain does not cover the entire vertical area of the robot cell entrance.
There is a space at ground level which is not covered,
which allows the mobile robot to enter and leave the cell without triggering the safety system.
This means that at no point does the safety system need to be disabled during operation.

% \subsubsection{HMI Stop}
% The third stop button is on the HMI.
% This button will be within easy reach of a user who is working 
% with the HMI,
% removing the necessity to move in order to trigger a robot cell stop.
% This button works,
% from the perspective of both the user and the safety PLC,
% in a manner very similar to the physical emergency stop button.
% When unpressed, a 24V signal is sent to the safety PLC and when pressed
% this signal is broken, triggering a stop.
% It is reset using another button on the HMI which restores the 24V signal.
\subsubsection{Test Stop}
As a way to test the emergency stop within the ROS system, a service was created.
As the test stop is software rather than hardware it cannot interface directly with the safety PLC.
The interface between the safety PLC and the ros service is managed using the ABB PLC.
The ABB PLC has both input and output pins which connect to bit values on coils.
A 'True' bit on an output coil corresponds to a DC output of 24V, whereas a 'False' value corresponds to a 0V output.
Similarly, an input of 24V on an input is read as 'True' and an input of 0V is read as 'False'.

The ROS service is contained within the python script handling Modbus communication.
When called, it sets the bit on the coil connected to the ABB-safety PLC output to 'False' and then reads the safety-ABB input and publishes the result.
This allows a verification that the safety PLC has received the emergency stop correctly.

The handling of the conveyor belt emergency stop is done in the PLC programming itself.
It is performed using a simple if statement which relies on the input from
the safety PLC.
If the input from the safety PLC indicates that the robot cell is secure,
the conveyor belt control is handed to the Modbus communication from the
python script and behaves as normal.
If the safety PLC indicates that an emergency stop has been triggered,
the conveyor belt is automatically turned off.

A topic was read by the HMI so the user could see if the emergency stop was engaged.
The interface flashes by switching between two colors to catch the attention of the user.
In section 4.2.3.2 in 60073:2002 \citep{60073:2002} is such signals described to toggle with a frequency between 1.4 and 2.6 Hz.
1.4 Hz was selected.

A connection between the output for the green stack light and the ABB PLC is made,
setting the value for a bit on a coil in the ABB PLC.
A python script, using ROS, then uses the Modbus communication protocol to read from the
coil once every second.
The measurement is performed once a second as the HMI display of the status of the robot cell is not considered safety-critical and this reduces the strain on the PLC.
Every time the coil is read, the python script publishes the status as a Boolean value to a ROS topic.
The HMI then subscribes to the ROS topic and displays the corresponding information on the screen.

\subsubsection{Logic diagram}

\begin{figure}[htp]
\centering
\includegraphics[width=\textwidth]{graphics/safety/logic.png}
\caption[Ladder diagram of the safety PLC whilst online]{Ladder diagram of the safety PLC whilst online. 
This image shows that there has been an emergency stop due to the light curtain and due to the AND gate only the red stack light is on, 
the robot cell having been turned off.}
\label{fig:logic}
\end{figure}

Figure \ref{fig:logic} shows a ladder diagram of the internal logic of the safety PLC.
The three emergency stop inputs enter an AND logic gate.
This returns a signal if, and only if, all three are positive (i.e. not triggered).
This then goes through a 1500ms on-delay timer which verifies the signal
and allows users to rectify any accidentally-reset emergency stops.

The signal then splits into three branches.
The first is a simple NOT gate,
which turns on the red stack light when one of the emergency stops has been triggered. 
This can be seen in the Figure \ref{fig:logic} ladder diagram where the red light is lit green due to the light curtain stop. 

The second is an AND gate, which turns on the orange stack light if there is no emergency stop, but the second timer has not yet completed and so the robot cell is not yet active.

The third uses another timer,
this time of 3000ms,
before turning the system on.

The safe signal is connected to a relay which makes or breaks a connection between two pins on the Kuka control box X11 port.
When connected, the robot
is active but when broken the robot experiences an emergency stop. 
The use of a relay therefore allows the output from the safety PLC to issue an emergency stop of the robot.

\subsection{Mobile robot}
The mobile robot has a basic form of obstacle detection using the LIDAR.
During wall-following, specific obstacle-avoidance does not need to be considered as the program itself is designed to maintain distance from a wall.
However, in other modes the LIDAR can be used to create an emergency stop
function.

Due to the possibility that the robot may have to move through narrow spaces,
the obstacle detection is limited to obstacles directly ahead of the robot.
The mobile robot moves only in one direction,
so as the aim is to avoid crashing into obstacles, this is considered sufficient.

During waypoint navigation and line following, a simple ROS topic is published by the LIDAR handling node which publishes a Boolean value.
If there is an object detected within 30cm of the front of the robot, 
the topic returns 'True', and if the way is clean then it returns 'False'.

The movement of the robot in line-following and waypoint-navigation mode is depended on this topic publishing 'False'.
If a 'True' message is received, a braking signal is sent to the Frobit and the mobile robot halts until the obstacle has moved.