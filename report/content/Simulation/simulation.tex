\section{Simulation}
A simulated environment of the robotics lab was modelled,
using CAD drawings delivered by Technical Services at
SDU were a 3D representation of the robotics lab (RoboLab)
modeled in AutoDesk Fusion 360. The 3D model was constrained to
the lower floor of the robotics lab, where the robot working area was placed.
\begin{figure}[!h]
    \centering
    \includegraphics[width=\smallimgwidth]{graphics/Simulation/robo_lab_cropped}
    \caption{3D Rendering of SDU RoboLab used in simulation}
    \label{fig:robo_lab_model}
\end{figure}

The idea behind modeling the entire robotics lab was to enable
a full-scale simulation of every entity of the real setup such as robotic arm,
conveyor belt, mobile robot, etc. This helps discover and debug flaws in
the system design and inter-behavioral mismatch.
In the end, a full-scale simulation was to much
an effort in the given time.
However, since a full-scale simulation was not strictly necessary, the simulation discussed below is only
focused on the Robot Cell and the robotic arm, in which focus was also kept during the sprints.

The actual simulation was done in the Virtual Robotic Experimentation Platform \citep{vrep} (V-REP),
The final simulation of the Robot Cell is shown if figure \ref{fig:robo_lab_model}.
V-REP offers a large collection of different functionality,
relevant topics listed below:
\begin{itemize}
		\item 3D environment.
		\item Physics engine.
		\item Sensor Simulation.
		\item Collision detection.
		\item Motion planning through OMPL \citep{ompl}.
		\item Forward and Inverse Kinematics.
		\item External API.
		\item ROS \citep{ros} Integration.
		\item A large database of robots and utilities.
\end{itemize}

\begin{figure}[!h]
    \centering
    \includegraphics[width=\imgwidth]{graphics/Simulation/rsd_vrep_enviroment}
    \caption{Final representation of Robot Cell in V-REP}
    \label{fig:rsd_vrep_environment}
\end{figure}

The model of the KUKA R700 SIXX shown in figure \ref{fig:rsd_vrep_environment} was downloaded from KUKA's website \citep{kuka3dmodel}.
One of the key features of V-REP in relation to the project
is the integration of ROS inside the simulation.
This feature gives a direct connection between simulation objects and code
connected through ROS. V-REP enables the development of custom ROS services or
messages through its ROS Interface static library, which can be recompiled
fairly easy, with additional services or message of own creation, using
\textit{catkin\_tools} \citep{catkintools}.

Using \textit{catkin\_tools}, three custom services were created to enable
path and grasp planning delivered by V-REP through ROS.
Figure \ref{fig:v_rep_overview} gives an overview on how ROS is
integrated and the naming of custom services used for path and grasp planning.
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{graphics/Simulation/v_rep_overview}
    \caption{Brief overview of the V-REP ROS implementation}
    \label{fig:v_rep_overview}
\end{figure}

In the following sections, a more detailed view is presented on how the path and grasp
planning are implemented in V-REP and how to interact with them
through the ROS interface.

\subsection{Path planning}
Path planning was done using The Open Motion Planning Library (OMPL) \citep{ompl}.
OMPL is integrated directly into V-REP and is accessible
from its API, which can be used through either C++ or LUA. For this project,
all simulation code that V-REP is required to run is implemented in LUA.

The process of configuring OMPL to perform a path planning task was as follows:
\begin{enumerate}
	\item Create a new OMPL path planning task.
	\item Set the chosen path planning algorithm to be used by the task.
	\item Create State spaces for each joint that needs to be considered in the planning,
defining also the constraints.
	\item Configure collision pairs, for example, the KUKA robot
versus the rest of the collidable objects in the simulation environment.
	\item Define a starting state (KUKA robot starting joint configuration).
	\item Define a goal state (target joint configuration).
	\item Compute the path (if possible) with a maximum limit for number of configurations.
\end{enumerate}

The state space for doing path planning on the KUKA robot was configured with the constraints shown
in figure \ref{fig:planning_constraints}. The defined intervals were given by the
default constraints of the real robot.

\begin{figure}[!h]
    \centering
    \includegraphics[scale=0.6]{graphics/Simulation/planning_constraints}
    \caption[Path planning constraints on KUKA joints]{Path planning constraints (in degrees) on KUKA joints.}
    \label{fig:planning_constraints}
\end{figure}

The ROS service: \textit{/vrep/SimGetJointsPath} works as the interface
for performing the path planning. The service is structured as shown in figure \ref{fig:get_path_joints}. This service enables
the user to specify one of OMPL's supported path planning algorithms. In this project, the Probabilistic Roadmap Algorithm is used for general
path planning in simulation.
The service also requires the user to specify how many configuration
steps the planned path should use, hereby lowering the step size when many configuration
steps are used, and increasing the step size on few configuration. The last two arguments, specify the start configuration and the goal configuration of the path.
\begin{figure}[!h]
    \centering
    \includegraphics[width=\smallimgwidth,trim={0 1cm 0 0},clip]{graphics/Simulation/get_path_joints}
    \caption{Structure of the \textit{/vrep/SimGetPathJoints} service.}
    \label{fig:get_path_joints}
\end{figure}

\subsection{Grasp planning}
When the Kuka robot moves into the initial brick pickup position, the smart camera
will deliver a grasping point and orientation of a requested brick color.
Using the \textit{/vrep/SimGetLinearPathJointsToPoint} service, V-REP will generate
a straight collision free path onto grasping the brick.
Figure \ref{fig:get_linear_path_joints_to_point} shows the structure of the service.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\smallimgwidth,trim={0 1cm 0 0},clip]{graphics/Simulation/get_linear_path_joints_to_point}
    \caption{Structure of the \textit{/vrep/SimGetLinearPathJointsToPoint} service.}
    \label{fig:get_linear_path_joints_to_point}
\end{figure}

The service largely resembles the formerly discussed service \textit{/vrep/SimGetJointsPath}
with the exception that the goal in this case is a Cartesian point and an orientation, and
the algorithm to use cannot be specified.

When the service receives a request, the following process is undertaken:
\begin{enumerate}
	\item The start configuration is extracted from the request object
and the robot joints is configured to it.
	\item A special V-REP object called a dummy \citep{vrepdummy}, which is
used for representing 3D point targets and pairs, is placed at the position and
orientation given in the goalPoint argument of the request object.
	\item The V-REP API function \textit{simGenerateIkPath }\citep{vrepapi} is called
and uses the inverse Jacobian and linear interpolation to create a list of collision
free configuration onto the position dummy.
	\item The service responds with an array of found configurations if successful or
	an empty array if a collision free path could not be found.
\end{enumerate}

An additional third service was added: \textit{/vrep/SimGetPathJointsToPoint}
that enables path planning, using one of OMPL's algorithms, from a joint configuration
to a cartesian point. The service structure is shown in figure \ref{fig:get_path_joints_to_point}.
\begin{figure}[!h]
    \centering
    \includegraphics[width=\smallimgwidth,trim={0 1cm 0 0},clip]{graphics/Simulation/get_path_joints_to_point}
    \caption{Struture of the \textit{/vrep/SimGetPathJointsToPoint} service}
    \label{fig:get_path_joints_to_point}
\end{figure}

This service offers a mixed functionality of the two other mentioned
services. This service is specially helpful in situations where a linear grasp
planning is not possible.

\subsection{Results}
As general path planning was not fully configured before the FAT, a test was conducted only on
the grasp planning service. The test was constructed using a simple C++ ROS node
that calls the grasp planning service 100 times with different configurations.
To simulate as close to the real setup, the Smart Camera's field of view was used
to create a bounding box, and inside this bounding box a simulated lego brick would
be placed at random positions. At every random position the grasp planning service
was called either resulting in success or failure. The result of the test can be seen
on figure \ref{fig:grasping_result}.
\begin{figure}[t]
    \centering
    \includegraphics[width=\imgwidth]{graphics/Simulation/grasping_result}
    \caption[Grasping result over 100 attempts]{Grasping result over 100 attempts. Blue is a succesful grasping. The plotted values are the positions in the image frame in metres.}
    \label{fig:grasping_result}
\end{figure}

The graph frame of figure \ref{fig:grasping_result} corresponds to the image plane
of the smart camera. Out of the 100 grasp planning attempts 68 of the configurations,
colored in blue, were a success. It's easy to see how the grasp failures are positioned
near the edges of the graph frame. Some example of the resulting grasping can be seen in
figure \ref{fig:grasp_examples}.
\begin{figure}[t]
    \centering
    \includegraphics[width=\imgwidth]{graphics/Simulation/grasp_examples}
    \caption{Examples of grasp positions found in the test.}
    \label{fig:grasp_examples}
\end{figure}
