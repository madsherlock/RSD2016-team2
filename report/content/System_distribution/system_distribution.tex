\section{System distribution}
The robot system was distributed in a way allowing as much parallel development as possible,
with the main coupling element being the MES server, which allocates and schedules system resources.
\subsection{MES Server}
A base template for the MES server was given by the instructors,
and it was chosen to follow this template as closely as possible.
The MES server was implemented as three server-side state machines,
and the template provided simulated clients as well.
The state machines described the MES server and client behaviour
of the drone, robot cell and mobile robot subsystems respectively,
allowing easy parallel development. For example, the robot cell subsystem
was initially developed with simulated drone and mobile robot clients,
and the LEGO pieces were simply fed to the conveyor belt manually,
while the flow between MES server states remained the same as if the drone
and mobile robot had been part of the process.

Figures \ref{fig:mesmr} and \ref{fig:mesrc} show the server-side state
machines for the mobile robot and robot cell subsystems respectively.

\begin{figure}[t]
\centering
\includegraphics[width=\linewidth]{graphics/mes/Server_side_Mobile_state_diagram}
\caption[MES server state diagram for the mobile robot subsystem]{MES server state diagram for the mobile robot subsystem. Provided by instructors.}
\label{fig:mesmr}
\end{figure}
\begin{figure}[t]
\centering
\includegraphics[width=\linewidth]{graphics/mes/Server_side_Robot_state_diagram}
\caption[MES server state diagram for the robot cell subsystem]{MES server state diagram for the robot cell subsystem. Provided by instructors. This client was modified for ROS by group 1.}
\label{fig:mesrc}
\end{figure}

Figures \ref{fig:mesmr} and \ref{fig:mesrc} are self-explanatory for the most part.
However, certain parts need an explanation: First, the mobile robot client has the option to reject an order.
The relevant scenario is if the mobile robot is charging, and thus is in an idle state,
or if problems arrive during production, that are identified by the mobile robot subsystem.
Second, the LEGO piece orders are not contained in these state machines,
and their entry into the system is undefined.
Third, there are no incident handlers built into the MES server -- if a subsystem fails,
and resolves a problem by resetting to an Idle state,
the server side state machine does not reflect this change.

In order to overcome the problem with orders not entering the system,
a random order generator was added to the MES server,
inserting an \verb+<order>+ tag to every MES message.
Only the robot cell receives an actual order within this tag,
and it does this upon entry into the Waiting\_for\_task\_accept\_mobile\_(1 or 2) state.
It is up to the client side implementation to store this order for the sorting part.

It was deemed by the two groups that there was not enough time to properly handle
the problem of no incident handlers built into the MES server.
As a result, it was chosen to develop the robot system separately in each group,
keeping the same MES server software to allow an eventual merge of the subsystems
once in a mostly incident-free state. Otherwise, every incident would result in
a production stop and a restart of the MES server and its clients.
The separate development was facilitated by the fact that the shared positioning
system for the mobile robot was developed to be fully independent of the remaining
system implementation and by the fact that each group had an assigned robot cell
and mobile robot. 

All MES server and client state diagrams are found in appendix \ref{sec:mes}.
\subsection{ROS}
As ROS is a framework that consists of a collection of software
packages and tools aimed at a wide area of topics related to distributed robot systems,
it was chosen to use it for this project.

ROS organises software into packages that can be compiled all together
or separately, making it a low coupled framework and allowing
hierarchically structured software tests.

Communication in ROS can be done by using topics.
A topic is assigned a name, eg. "/namespace1/topicname1", and can then be published
and subscribed to.
Topics are typically used for continuous (or discontinuous) streams of data,
such as sensor data from an IMU.

Another means of communication is services. These offer a request / reply service.
This is done by a node requesting a specific service.
The node providing the service will then reply to the request.

The ROS framework requires the definition of a ROS master (a computer in the system),
responsible for coordinating inter-node communication and hosting the ROS parameter server.
For example, when a service request is made, the ROS master points the requesting node
to the service provider.

The mobile robot provided for the project was also provided
with an extensive library \citep{frobomindonline} written as ROS packages,
giving strong reason to use ROS for the development of the mobile robot subsystem.