​source /usr/local/etc/bash_completion.d/catkin-tools.completion.bash
1. (Update pip: pip install --upgrade pip)
1. (maybe this is is needed inorder to update to the correct version of pip: sudo -H python -m pip install --upgrade pip
1. this might work: curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py")
and then: sudo python get-pip.py
2. We need "hg" inorder to get som depenencies cloned: sudo apt install mercurial
3. Now clone the source for sphinx-contrib: hg clone http://bitbucket.org/birkenfeld/sphinx-contrib
4. Move into the sphinx-contrib-ansi directory: cd sphinx-contrib/ansi
5. There is a small error in the setup.py file, so you need to chang the README.rst to README inordr
for setup.py to work: cp README.rst README
6. Run the python application setup: sudo python setup.py install
7. Now get the source for catkin_tools: git clone https://github.com/catkin/catkin_tools.git
8. Enter the catkin_tools folder: cd catkin_tools
9. Install dependencis using pip: pip install -r requirements.txt --upgrade
10. And finally install catkin_tools: python setup.py install --record install_manifest.txt
11. Open your .bashrc fil: nano ~/.bashrc
12. Add the following at the end of the file: source /etc/bash_completion.d/catkin_tools-completion.bash
13. And also this line (if you have not already done so with your current ROS installation): source /opt/ros/kinetic/setup.bash
14. Now create a new folder called "catkin_ws" that should be the workspace folder for using catkin_tools and a subfolder called src, it's very important
that you create a new folder that is non related to an existing workspace folder that catkin_make are using: mkdir -p ~/catkin_ws/src
15. move into the catkin_ws folder: cd catkin_ws
16. And initialise the folder as a catkin_tools workspace: catkin init
17. Now run your first build command to generate the workspace: catkin build

refs:
https://catkin-tools.readthedocs.io/en/latest/installing.html
https://pythonhosted.org/sphinxcontrib-ansi/
