-- DO NOT WRITE CODE OUTSIDE OF THE if-then-end SECTIONS BELOW!! (unless the code is a function definition)


--######################################################################################
--#
--# Queue Implementation
--#
--######################################################################################


--######################################################################################
--#
--# Kuka Kr6 Control Functions
--#
--######################################################################################

--
-- @description
--
function setKukaJointsConfiguration(q)

    for i=1, tableLength(joints), 1
    do

        -- Change to passive mode
        simSetJointMode(joints[i], sim_jointmode_passive, 0)

        -- Change Joint Value
        simSetJointPosition( joints[i], q[i] )

    end


end

--
-- @description
--
function setKukaJointsTargetConfiguration(q)

    for i=1, tableLength(joints), 1
    do

        -- Change to Force/Torque Mode
        simSetJointMode(joints[i], sim_jointmode_force, 0)

        -- Change Joint Value
        simSetJointTargetPosition( joints[i], q[i] )

    end


end

--
-- @description
--
function tableLength(T)

    --
    local count = 0

    --
    for _ in pairs(T) do
        count = (count + 1)
    end

    return count
end

--
-- @description
--
function getPath(startJoints, goalJoints, steps)

    -- Create path planning task
    local omplKukaTask = simExtOMPL_createTask("kuka_planning")

    -- Configuring probalistic roadmap algorithm
    simExtOMPL_setAlgorithm(omplKukaTask, sim_ompl_algorithm_RRT)

    -- name: a name for this state space
    -- type: type of this state space component (must be one of sim_ompl_statespacetype_position2d, sim_ompl_statespacetype_pose2d, sim_ompl_statespacetype_position3d, sim_ompl_statespacetype_pose3d, sim_ompl_statespacetype_joint_position)
    -- objectHandle: the object handle (a joint object if type is sim_ompl_statespacetype_joint_position, otherwise a shape)
    -- boundsLow: lower bounds (if type is pose, specify only the 3 position components)
    -- boundsHigh: upper bounds (if type is pose, specify only the 3 position components)
    -- useForProjection: if true, this object position or joint value will be used for computing a default projection
    -- weight: (optional) the weight of this state space component, used for computing distance between states. Default value is 1.0
    -- refObjectHandle: (optional) an object handle relative to which reference frame position/orientations will be evaluated. Default value is -1, for the absolute reference frame
    --const rw::math::Q QMAX(6, 170, 135, 60, 185, 25, 350);
    --const rw::math::Q QMIN(6, -170, -100, -200, -185, -210, -350);
    -- -100*pi/180 + offset
    local j1_space = simExtOMPL_createStateSpace(   "j1_space",
                                                    sim_ompl_statespacetype_joint_position,
                                                    joints[1],
                                                    {-170*math.pi/180 + jointsOffsetKuka[1]},
                                                    {170*math.pi/180 + jointsOffsetKuka[1] },
                                                    1,
                                                    robotRefHandle
                                                )


    local j2_space = simExtOMPL_createStateSpace(
                                                    "j2_space",
                                                    sim_ompl_statespacetype_joint_position,
                                                    joints[2],
                                                    {-100*math.pi/180 + jointsOffsetKuka[2]},
                                                    {135*math.pi/180 + jointsOffsetKuka[2]},
                                                    2,
                                                    robotRefHandle
                                                )

    local j3_space = simExtOMPL_createStateSpace(
                                                    "j3_space",
                                                    sim_ompl_statespacetype_joint_position,
                                                    joints[3],
                                                    {-200*math.pi/180 + jointsOffsetKuka[3]},
                                                    {60*math.pi/180 + jointsOffsetKuka[3]},
                                                    3,
                                                    robotRefHandle
                                                )

    local j4_space = simExtOMPL_createStateSpace(
                                                    "j4_space",
                                                    sim_ompl_statespacetype_joint_position,
                                                    joints[4],
                                                    {-185*math.pi/180 + jointsOffsetKuka[4]},
                                                    {185*math.pi/180 + jointsOffsetKuka[4]},
                                                    0,
                                                    robotRefHandle
                                                )

    local j5_space = simExtOMPL_createStateSpace(
                                                    "j5_space",
                                                    sim_ompl_statespacetype_joint_position,
                                                    joints[5],
                                                    {-210*math.pi/180 + jointsOffsetKuka[5]},
                                                    {25*math.pi/180 + jointsOffsetKuka[5]},
                                                    0,
                                                    robotRefHandle
                                                )

    local j6_space = simExtOMPL_createStateSpace(
                                                    "j6_space",
                                                    sim_ompl_statespacetype_joint_position,
                                                    joints[6],
                                                    {-350*math.pi/180 + jointsOffsetKuka[6]},
                                                    {350*math.pi/180 + jointsOffsetKuka[6]},
                                                    0,
                                                    robotRefHandle
                                                )

    -- Configure path planning constraints
    simExtOMPL_setStateSpace(
                                omplKukaTask,
                                {
                                    j1_space,
                                    j2_space,
                                    j3_space,
                                    j4_space,
                                    j5_space,
                                    j6_space
                                }
                            )

    --
    simExtOMPL_setCollisionPairs(omplKukaTask,collisionPairs)

    -- Transform real kuka joint offsets to simulation model
    --for i=1, 1, tableLength(joints)
    --do
        --
        --startJointsOffsetted[i] = request.startJoints[i] + jointsOffsetSim[i]
        --goalJointsOffsetted[i]  = request.goalJoints[i] + jointsOffsetSim[i]
    --end

    --
    simExtOMPL_setStartState(omplKukaTask, startJoints)
    simExtOMPL_setGoalState(omplKukaTask, goalJoints)

    --
    local omplResult, path = simExtOMPL_compute(omplKukaTask, 4, -1, steps)

    --
    --local steps = tableLength(path) / tableLength(joints);

    --
    --for i=1, tableLength(path), tableLength(joints)
    --do

        --
        --local conf = {}

        --
        --for j=1, tableLength(joints), 1
        --do

            --
            --table.insert(conf, path[i+j-1])

            -- Transform kuka sim joints to real kuka
            --path[i+j-1] = path[i+j-1] - jointsOffsetSim[j]

            --
            --table.insert(pathQueue, path[i+j])
        --end

        --table.insert(pathQueue, conf)

    --end

    -- destroy path planning task
    simExtOMPL_destroyTask(omplKukaTask);

    return path;
end

--
-- @description
--
function addOffset( data, rows, cols, offsets )

    for i=1, rows*cols, cols
    do
        for j=1, cols, 1
        do
            data[i+j-1] = data[i+j-1] + offsets[j]
        end
    end

end

--######################################################################################
--#
--# ROS Service Callbacks
--#
--######################################################################################

--
-- @description
--
function simSetConfigurationCallback(request)

    local startJointsOffsetted  = request.q

    -- Add offsets before path planning
    addOffset(startJointsOffsetted, 1, 6, jointsOffsetKuka)

    -- Change joints
    setKukaJointsConfiguration(startJointsOffsetted)

    return {status = 1}

end

--
-- @description
--
function simGetIsMovingCallback(request)

    --print(req)
    --print(toString(req.q))
    --print(req.speed)

    return {isMoving = true}

end

--
-- @description
--
function simGetJointsPathCallback(request)

    -- Offset values
    local startJointsOffsetted = request.startJoints
    local goalJointsOffsetted  = request.goalJoints

    -- Add offsets before path planning
    addOffset(startJointsOffsetted, 1, 6, jointsOffsetKuka)
    addOffset(goalJointsOffsetted, 1, 6, jointsOffsetKuka)

    -- Change kuka to passed start joint configuration
    setKukaJointsConfiguration(goalJointsOffsetted)

    print(
            "Offsetted startJoints: ",
            startJointsOffsetted[1],
            startJointsOffsetted[2],
            startJointsOffsetted[3],
            startJointsOffsetted[4],
            startJointsOffsetted[5],
            startJointsOffsetted[6]
         )

    print(
            "Offsetted goalJoints: ",
            goalJointsOffsetted[1],
            goalJointsOffsetted[2],
            goalJointsOffsetted[3],
            goalJointsOffsetted[4],
            goalJointsOffsetted[5],
            goalJointsOffsetted[6]
         )

    -- Calculate path
    local path = getPath(startJointsOffsetted, goalJointsOffsetted,  request.steps)

    -- Calucalte number of steps the path took
    local steps = tableLength(path) / tableLength(joints)

    -- Revert offset
    addOffset(path, steps, 6, jointsOffsetKukaInv)

    return {steps = steps, path = path}
end

--
-- @description
--
function simGetJointsPathToPointCallback(request)

    --
    local path = {}

    --
    local steps = 0

    --
    local length = 0

    -- Offset values
    local startJointsOffsetted = request.startJoints

    -- Add offsets before path planning
    addOffset(startJointsOffsetted, 1, 6, jointsOffsetKuka)

    -- Change kuka to passed start joint configuration
    setKukaJointsConfiguration(startJointsOffsetted)

    --
    --simWait()

    -- Move tip target to requested position
    simSetObjectPosition(tipTargetHandle, robotRefHandle, {
                                                request.goalPoint[1],
                                                request.goalPoint[2],
                                                request.goalPoint[3]
                                              }
    )

    -- Change orientation of tip target to requested
    simSetObjectOrientation(tipTargetHandle, -1, {
                                                    request.goalPoint[4],
                                                    request.goalPoint[5],
                                                    math.pi*0.5 + request.goalPoint[6]
                                                 }
    )

    -- Get joint configuration from the passed cartesian point
    jointsConf = simGetConfigForTipPose(ikJacHandle,joints,0.65,10,nil,collisionPairs)

    --print(tableLength(jointsConf))

    -- Only if valid joint configuration was found do we proceed
    -- with path planning
    if jointsConf then
        print(tableLength(jointsConf))

        -- Get path
        path = getPath(request.startJoints, jointsConf, request.steps)

        -- Get number of steps used by the path
        steps = tableLength(path) / tableLength(joints)

        length =  tableLength(path)

        --
        steps = length / 6

        --
        for i=1, length, tableLength(joints)
        do

            --
            local conf = {}

            --
            for j=1, tableLength(joints), 1
            do

                --
                table.insert(conf, path[i+j-1])

            end

            --
            table.insert(pathQueue, conf)

        end

        -- Revert offset
        addOffset(path, steps, 6, jointsOffsetKukaInv)

    end

    return {steps = steps, path = path}
end

--
-- @description
--
function simGetLinearJointsPathToPointCallback(request)

    --
    local path = {}

    --
    local steps = 0

    -- Offset values
    local startJointsOffsetted = request.startJoints

    --
    local length = 0

    -- Add offsets before path planning
    addOffset(startJointsOffsetted, 1, 6, jointsOffsetKuka)

    -- Change kuka to passed start joint configuration
    setKukaJointsConfiguration(startJointsOffsetted)

    --
    --simWait()

    -- Move tip target to requested position
    simSetObjectPosition(
                            tipTargetHandle,
                            robotRefHandle,
                            {
                                request.goalPoint[1],
                                request.goalPoint[2],
                                request.goalPoint[3]
                            }
    )

    -- Change orientation of tip target to requested
    simSetObjectOrientation(
                                tipTargetHandle,
                                -1,
                                {
                                    request.goalPoint[4],
                                    request.goalPoint[5],
                                    math.pi*0.5 + request.goalPoint[6]
                                }
    )

    -- Try to get linear joint path
    path = simGenerateIkPath(ikJacHandle, joints, request.steps, collisionPairs)

    --print(tableLength(jointsConf))

    -- If simGenerateIkPath return nil, then reset the path to empty table
    if (path == nil) then

        -- Get path
        path = {}

    else

        --
        length =  tableLength(path)

        --
        steps = length / 6

        --
        local conf = {
                        path[length - 5],
                        path[length - 4],
                        path[length - 3],
                        path[length - 2],
                        path[length - 1],
                        path[length - 0]
                     }

        --
        setKukaJointsConfiguration(conf)

        --
        --for i=1, tableLength(path), tableLength(joints)
        --do

            --
            --local conf = {}

            --
            --for j=1, tableLength(joints), 1
            --do

                --
                --table.insert(conf, path[i+j-1])

            --end

            --table.insert(pathQueue, conf)

        --end

        -- Revert offset
        addOffset(path, steps, 6, jointsOffsetKukaInv)

        --for i=1, steps*6, 6
        --do
            --print(
                    --path[i+0],
                    --path[i+1],
                    --path[i+2],
                    --path[i+3],
                    --path[i+4],
                    --path[i+5]
            --)
        --end
    end

    return {steps = steps, path = path}
end

--
-- @description
--
function simSetBrickCallback(request)

    --
    local geometry = {0, 0, 0, 0, 0, 0}

    print(cameraViewFieldInfo.xMin)
    print(cameraViewFieldInfo.xMax)
    print(cameraViewFieldInfo.yMin)
    print(cameraViewFieldInfo.yMax)

    -- Requested position must be inside the camera viewfield
    if request.x > cameraViewFieldInfo.xMin and
       request.x < cameraViewFieldInfo.xMax and
       request.y > cameraViewFieldInfo.yMin and
       request.y < cameraViewFieldInfo.yMax then

       --
       local currentPosition = simGetObjectPosition(brick, cameraViewField)

       print(request.x)
       print(request.y)

       -- Configure brick
       simSetObjectPosition(brick,
                            cameraViewField,
                            {
                                request.x,
                                request.y,
                                currentPosition[3]
                            }
       )

       --
       currentPosition   = simGetObjectPosition(brickGrasp, cameraViewField)

       -- Configure brick
       simSetObjectPosition(brickGrasp,
                            cameraViewField,
                            {
                                request.x,
                                request.y,
                                currentPosition[3]
                            }
       )

       --
       local position       = simGetObjectPosition(brickGrasp, robotRefHandle)
       local orientation    = simGetObjectOrientation(brickGrasp, -1)

        geometry[1] = position[1]
        geometry[2] = position[2]
        geometry[3] = position[3]
        geometry[4] = orientation[1]
        geometry[5] = orientation[2]
        geometry[6] = orientation[3]

    end

    return {brickGeometry = geometry}
end

--
-- @description
--
function simGetCameraViewFieldCallback(request)
    return {
                width   = cameraViewFieldInfo.width,
                height  = cameraViewFieldInfo.height
           }
end

--######################################################################################
--#
--# Code Entry
--#
--######################################################################################

if (sim_call_type==sim_childscriptcall_initialization) then

	-- Put some initialization code here
    objectHandle = simGetObjectAssociatedWithScript(sim_handle_self)
    objectName = simGetObjectName(objectHandle)

    --
    robotRefHandle = simGetObjectHandle('robot_ref')

    --
    baseHandle = simGetObjectHandle('base')

    -- Tip target handle
    tipTargetHandle = simGetObjectHandle('kuka_tip_target')
    tipHandle = simGetObjectHandle('kuka_tip')

    --
    tipM = simGetObjectMatrix(tipTargetHandle, -1);

    --
    cameraViewField = simGetObjectHandle('camera_viewfield')

    --
    local cvfMinXR, cvfMinXP = simGetObjectFloatParameter(cameraViewField, sim_objfloatparam_objbbox_min_x)
    local cvfMaxXR, cvfMaxXP = simGetObjectFloatParameter(cameraViewField, sim_objfloatparam_objbbox_max_x)
    local cvfMinYR, cvfMinYP = simGetObjectFloatParameter(cameraViewField, sim_objfloatparam_objbbox_min_y)
    local cvfMaxYR, cvfMaxYP = simGetObjectFloatParameter(cameraViewField, sim_objfloatparam_objbbox_max_y)
    --local cvfMinZR, cvfMinZP = simGetObjectFloatParameter(cameraViewField, sim_objfloatparam_objbbox_min_z)
    --local cvfMaxZR, cvfMaxZP = simGetObjectFloatParameter(cameraViewField, sim_objfloatparam_objbbox_max_z)

    --
    cameraViewFieldInfo =  {
                                width   = (cvfMaxXP - cvfMinXP),
                                height  = (cvfMaxYP - cvfMinYP),
                                xMin    = cvfMinXP,
                                xMax    = cvfMaxXP,
                                yMin    = cvfMinYP,
                                yMax    = cvfMaxYP,
                            }

    --
    brick = simGetObjectHandle('brick')
    brickGrasp = simGetObjectHandle('brick_grasp')

    --
    conv = simGetObjectHandle('conv')

    --
    convPos = simGetObjectPosition(conv, -1)
    convDir = simGetObjectOrientation(conv, -1)

    -- Inverse kinematics handles
    ikJacHandle = simGetIkGroupHandle('KUKA_KR6_700_SIXX_NOT_DAMPED_IK')
    ikDampedHandle = simGetIkGroupHandle('KUKA_KR6_700_SIXX_DAMPED_IK')

    -- Generated q's for simulating of path planing
    pathQueue = {};

    -- Kuka Joints Offset
    jointsOffsetKuka    = {0.0, -math.pi/2, math.pi/2, 0.0, math.pi/2, 0.0}
    jointsOffsetKukaInv = {0.0, math.pi/2, -math.pi/2, 0.0, -math.pi/2, 0.0}

    --
    --setKukaJointsTargetConfiguration(jointsOffsetSim)

    -- Kuka Robot joints
    joints = {
                simGetObjectHandle('joint1'),
                simGetObjectHandle('joint2'),
                simGetObjectHandle('joint3'),
                simGetObjectHandle('joint4'),
                simGetObjectHandle('joint5'),
                simGetObjectHandle('joint6')
             }

    -- Collision Pairs
    collisionPairs = {
                        simGetCollectionHandle('KUKA_KR6_700_SIXX'),
                        simGetCollectionHandle('environment')
                    }

    -- Check if the required RosInterface is there:

    --
    moduleName = 0

    --
    index = 0

    --
    rosInterfacePresent = false

    -- Iterate through modules
    while moduleName do

        -- Get the module name at current iterating index
        moduleName = simGetModuleName(index)

        -- Does the name indicate that the ROS Interface is loaded?
        if (moduleName == 'RosInterface') then

            -- Then mark the ROS interface as loaded
            rosInterfacePresent = true

        end

        --
        index = index + 1

    end

    -- Prepare the float32 publisher and subscriber (we subscribe to the topic we advertise):
    if rosInterfacePresent then

        --
        setConfService = simExtRosInterface_advertiseService(
            '/vrep/SimSetConfiguration',
            'vrep_common/simSetConfiguration',
            'simSetConfigurationCallback'
        )

        --
        isMovingService = simExtRosInterface_advertiseService(
            '/vrep/SimGetIsMoving',
            'vrep_common/simGetIsMoving',
            'simGetIsMovingCallback'
        )

        --
        getJointsPathService = simExtRosInterface_advertiseService(
           '/vrep/SimGetJointsPath',
           'vrep_common/simGetJointsPath',
           'simGetJointsPathCallback'
        )

        --
        getJointsPathToPointService = simExtRosInterface_advertiseService(
            '/vrep/SimGetJointsPathToPoint',
            'vrep_common/simGetJointsPathToPoint',
            'simGetJointsPathToPointCallback'
        )

        --
        getLinearJointsPathToPointService = simExtRosInterface_advertiseService(
            '/vrep/SimGetLinearJointsPathToPoint',
            'vrep_common/simGetLinearJointsPathToPoint',
            'simGetLinearJointsPathToPointCallback'
        )

        --
        setBrickService = simExtRosInterface_advertiseService(
            '/vrep/SimSetBrick',
            'vrep_common/simSetBrick',
            'simSetBrickCallback'
        )

        --
        simGetCameraViewFieldService = simExtRosInterface_advertiseService(
            '/vrep/SimGetCameraViewField',
            'vrep_common/simGetCameraViewField',
            'simGetCameraViewFieldCallback'
        )



        --publisher=simExtRosInterface_advertise('/simulationTime','std_msgs/Float32')
        --subscriber=simExtRosInterface_subscribe('/simulationTime','std_msgs/Float32','subscriber_callback')
    end

	-- Make sure you read the section on "Accessing general-type objects programmatically"
	-- For instance, if you wish to retrieve the handle of a scene object, use following instruction:
	--
	-- handle=simGetObjectHandle('sceneObjectName')
	--
	-- Above instruction retrieves the handle of 'sceneObjectName' if this script's name has no '#' in it
	--
	-- If this script's name contains a '#' (e.g. 'someName#4'), then above instruction retrieves the handle of object 'sceneObjectName#4'
	-- This mechanism of handle retrieval is very convenient, since you don't need to adjust any code when a model is duplicated!
	-- So if the script's name (or rather the name of the object associated with this script) is:
	--
	-- 'someName', then the handle of 'sceneObjectName' is retrieved
	-- 'someName#0', then the handle of 'sceneObjectName#0' is retrieved
	-- 'someName#1', then the handle of 'sceneObjectName#1' is retrieved
	-- ...
	--
	-- If you always want to retrieve the same object's handle, no matter what, specify its full name, including a '#':
	--
	-- handle=simGetObjectHandle('sceneObjectName#') always retrieves the handle of object 'sceneObjectName'
	-- handle=simGetObjectHandle('sceneObjectName#0') always retrieves the handle of object 'sceneObjectName#0'
	-- handle=simGetObjectHandle('sceneObjectName#1') always retrieves the handle of object 'sceneObjectName#1'
	-- ...
	--
	-- Refer also to simGetCollisionhandle, simGetDistanceHandle, simGetIkGroupHandle, etc.
	--
	-- Following 2 instructions might also be useful: simGetNameSuffix and simSetNameSuffix

end


if (sim_call_type==sim_childscriptcall_actuation) then

	-- Put your main ACTUATION code here

	-- For example:
	--
	-- local position=simGetObjectPosition(handle,-1)
	-- position[1]=position[1]+0.001
	-- simSetObjectPosition(handle,-1,position)
    --if(tableLength(pathQueue) > 0) then

        --
        --print("Path available")
        --setKukaJointsConfiguration(table.remove(pathQueue, 1))

    --else

       --print("Path not available")

    --end


end


if (sim_call_type==sim_childscriptcall_sensing) then

	-- Put your main SENSING code here

end


if (sim_call_type==sim_childscriptcall_cleanup) then

	-- Put some restoration code here
    simExtRosInterface_shutdownServiceServer(setConfService)
    simExtRosInterface_shutdownServiceServer(isMovingService)
    simExtRosInterface_shutdownServiceServer(getJointsPathService)
    simExtRosInterface_shutdownServiceServer(getJointsPathToPointService)
    simExtRosInterface_shutdownServiceServer(setBrickService)
    simExtRosInterface_shutdownServiceServer(simGetCameraViewFieldService)

end
